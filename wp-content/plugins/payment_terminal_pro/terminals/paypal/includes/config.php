<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);

require (CCPT_HOME_DIR."/wp-load.php"); 
require("functions.php"); 
#******************************************************************************
#                       Credit Card Payments Wordpress v3.0
#
#	Author: Convergine.com
#	http://www.convergine.com
#	Version: 3.0
#   (c) 2013 Convergine.com
#
#******************************************************************************
$configProcessor=is_string(get_option('ccwp_details_PAYPALL'))?unserialize(get_option('ccwp_details_PAYPALL')):get_option('ccwp_details_PAYPALL');

//THIS IS TITLE ON PAGES
$title = "PayPal PRO Payment Terminal v1.0"; //site title
$titleAboveForm = "PayPal PRO Payment Terminal";
//THIS IS ADMIN EMAIL FOR NEW PAYMENT NOTIFICATIONS.
$admin_email = get_option('ccpt_admin_email'); //this email is for notifications about new payments
//CHANGE "USD" TO REQUIRED CURRENCY, SUPPORTED BY PROVIDER.USD, CAD, EUR
define("PTP_CURRENCY_CODE",get_option('ccpt_paypal_currency')); 
//IF YOU NEED TO ADD MORE SERVICES JUST ADD THEM THE SAME WAY THEY APPEAR BELOW.
			
//NOW, IF YOU WANT TO ACTIVATE THE DROPDOWN WITH SERVICES ON THE TERMINAL
//ITSELF, CHANGE BELOW VARIABLE TO TRUE;			
$show_services = get_option('ccpt_show_dd_text'); 

//IF YOU'RE GOING LIVE FOLLOWING VARIABLE SHOULD BE SWITCH TO true
// IT WILL AUTOMATICALLY REDIRECT ALL NON-HTTTPS REQUESTS TO HTTPS.
// MAKE SURE SSL IS INSTALLED ALREADY.
$redirect_non_https =(get_option('ccpt_test')==2)? true : false;

/****************************************************
//TEST CREDIT CARD CREDENTIALS for SANDBOX TESTING
Card Type: Visa
Account Number: 4683075410516684
Expiration Date: Any in future
Security Code: 123
****************************************************/
if(get_option('ccpt_test')==2){
//PAYPAL PRO RELATED VARIABLES
	define('API_USERNAME', $configProcessor['live']['API_USERNAME']);
	define('API_PASSWORD', $configProcessor['live']['API_PASSWORD']);
	define('API_SIGNATURE', $configProcessor['live']['API_SIGNATURE']);
	//DONT EDIT BELOW 2 LINES IF UNSURE.
	define('API_ENDPOINT', 'https://api-3t.paypal.com/nvp');
	define('PAYPAL_URL', 'https://www.paypal.com/webscr&cmd=_express-checkout&token=');
}else{
//UNCOMMENT FOLLOWING 5 LINES FOR SANDBOX TESTING AND COMMENT THE PREVIOUS 5 ONES.
	define('API_ENDPOINT', 'https://api-3t.sandbox.paypal.com/nvp');
	define('PAYPAL_URL', 'https://www.sandbox.paypal.com/webscr&cmd=_express-checkout&token=');
	define('API_USERNAME', $configProcessor['test']['API_USERNAME']);
	define('API_PASSWORD', $configProcessor['test']['API_PASSWORD']);
	define('API_SIGNATURE', $configProcessor['test']['API_SIGNATURE']);
}
//DO NOT CHANGE ANYTHING BELOW THIS LINE, UNLESS SURE OF COURSE
define('USE_PROXY',FALSE);
define('PROXY_HOST', '127.0.0.1');
define('PROXY_PORT', '808');
define('VERSION', '2.3');
define('ACK_SUCCESS', 'SUCCESS');
define('ACK_SUCCESS_WITH_WARNING', 'SUCCESSWITHWARNING');

if($redirect_non_https){
	if ($_SERVER['SERVER_PORT']!=443) {
		$sslport=443; //whatever your ssl port is
		$url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		header("Location: $url");
		exit();
	}
}
?>