<?php
#******************************************************************************
#                       Credit Card Payments Wordpress v3.0
#
#	Author: Convergine.com
#	http://www.convergine.com
#	Version: 3.0
#   (c) 2013 Convergine.com
#
#******************************************************************************

/* Luhn algorithm number checker - (c) 2005-2008 shaman - www.planzero.org *
 * This code has been released into the public domain, however please      *
 * give credit to the original author where possible.                      */
function luhn_check($number) {

  // Strip any non-digits (useful for credit card numbers with spaces and hyphens)
  $number=preg_replace('/\D/', '', $number);

  // Set the string length and parity
  $number_length=strlen($number);
  $parity=$number_length % 2;

  // Loop through each digit and do the maths
  $total=0;
  for ($i=0; $i<$number_length; $i++) {
    $digit=$number[$i];
    // Multiply alternate digits by two
    if ($i % 2 == $parity) {
      $digit*=2;
      // If the sum is two digits, add them together (in effect)
      if ($digit > 9) {
        $digit-=9;
      }
    }
    // Total up the digits
    $total+=$digit;
  }

  // If the total mod 10 equals 0, the number is valid
  return ($total % 10 == 0) ? TRUE : FALSE;

}
function validateCC($cc_num, $type) {
$verified = false;
        if($type == "A") {
        $denum = "American Express";
        } elseif($type == "DI") {
        $denum = "Diner's Club";
        } elseif($type == "D") {
        $denum = "Discover";
        } elseif($type == "M") {
        $denum = "Master Card";
        } elseif($type == "V") {
        $denum = "Visa";
        }

        if($type == "A") {
        $pattern = "/^([34|37]{2})([0-9]{13})$/";//American Express
        if (preg_match($pattern,$cc_num)) {
        $verified = true;
        } else {
        $verified = false;
        }


        } elseif($type == "DI") {
        $pattern = "/^([30|36|38]{2})([0-9]{12})$/";//Diner's Club
        if (preg_match($pattern,$cc_num)) {
        $verified = true;
        } else {
        $verified = false;
        }


        } elseif($type == "D") {
        $pattern = "/^([6011]{4})([0-9]{12})$/";//Discover Card
        if (preg_match($pattern,$cc_num)) {
        $verified = true;
        } else {
        $verified = false;
        }


        } elseif($type == "M") {
        $pattern = "/^([51|52|53|54|55]{2})([0-9]{14})$/";//Mastercard
        if (preg_match($pattern,$cc_num)) {
        $verified = true;
        } else {
        $verified = false;
        }


        } elseif($type == "V") {
        $pattern = "/^([4]{1})([0-9]{12,15})$/";//Visa
        if (preg_match($pattern,$cc_num)) {
        $verified = true;
        } else {
        $verified = false;
        }

        }

        return $verified;
}
function getActualYears(){
    $html = "";
    for($i=date("Y");$i<date("Y", strtotime(date("Y")." +10 years"));$i++){
        $html .= '<option value="'.$i.'">'.$i.'</option>';
    }
    return $html;
}
?>