<?php
	$check_admin=get_userdata($current_user->ID);
	$id=$_GET['id'];
	
		$cases= $wpdb->get_results("SELECT * FROM 
											wp_user_help_section hs 
									left join 
											wp_user_help_section_subject  hss
									ON 
											hs.subject_help_id=hss.subject_id
									left join 
											wp_user_help_section_reply   hsr 
									ON
											hs.help_id=hsr.comment_id
									WHERE 
											comment_id = '$id'");
	
	?>
	<!-- Main Container Start -->
	<div class="container">
			<!-- .Page heading -->
	  		<h2>Cases</h2>
	  		<!-- Link for add New vehicles -->
		
<?php
			if($cases):
			
?>
				<div class="table-responsive">
				 <table class="table" >
				    <thead>
				      <tr>
				      	<th>Case</th>
				      	<th>Subject</th>
				      	<th>Message</th>
				        <th>Replied By</th>
				        <th>Date</th>
				        
				      </tr>
				    </thead>
				    <tbody>
				    	<?php foreach ($cases as $case) {
				    		$message_author=get_userdata($case->user_id);
				    			?>
				      <tr>
				      	<td><?=$case->name;?></td>
				      	<td><?=$case->subject;?></td>
				      	<td><?=$case->message;?></td>
				        <td><?=$message_author->user_login;?></td>
				        <td><?php echo date_i18n(get_option("date_format"),strtotime($case->timestamp));?></td>
						
				      </tr>
				     <? }?>
				    </tbody>
				  </table>
				</div>
<?php 		else:
				echo "<h4>No cases Found</h4>";
			endif;
?>
		<div class="row">
     
    <div class="col-md-6">
       <form class="form-horizontal" id="payment-form" action="" method='post'>
         <div class="payment-errors alert alert-default text-danger"></div>
          
          
         
          <div class="form-group">
            <label class="control-label col-sm-4" for="message">Message:</label>
            <div class="col-sm-8"> 
              <textarea  class="form-control" id="message" required name="message" placeholder="Type Your Message"></textarea>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-4" for="message">Is problem solved:</label>
            <div class="col-sm-8"> 
              <input  class="form-control" type="checkbox" id="closecase" name="closecase" value="1">
            </div>
          </div>
          
          <div class="form-group"> 
            <div class="col-sm-offset-4 col-sm-8">
              <button type="submit" name="add_help_reply"  class="btn btn-danger">Submit</button>
            </div>
          </div>
      </form>


     </div>
		</div>