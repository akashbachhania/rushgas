��    �     �  w  l      H'     I'     \'     d'     i'     �'     �'     �'     �'     �'     �'     �'     
(  
   &(  /   1(  .   a(     �(     �(     �(  
   �(  |  �(  @   e*  ^   �*  1   +  Q   7+  Y   �+  ]   �+  V   A,  R   �,  b   �,  V   N-     �-     �-     �-     �-     �-  d   �-     S.     p.  
   �.     �.  	   �.     �.     �.  $   �.     �.     �.     /     /     %/  	   2/  *   </     g/     n/  
   r/     }/     �/     �/     �/     �/     �/     �/  �   �/     O0     \0     q0  C   �0     �0  @  �0     2     2     *2     D2     K2     g2     p2     �2     �2     �2     �2     �2  %   �2  	   �2     �2     3     3     3  	   3     3     &3     33  �   @3     �3  	   �3     �3     �3     �3     4     4  +   #4  ;   O4     �4    �4     �5     �5     �5     �5     �5     �5     �5  	   6     6     6     6  �   6    7    8  �   9    �9  �   ;  	   <  &   <     4<  !   I<  !   k<      �<     �<     �<     �<     �<     �<  	    =  	   
=     =     =     %=  %   2=     X=  2   `=     �=  	   �=     �=     �=     �=     �=     �=     �=     >     
>     >     ">  (   =>     f>     r>     �>  t   �>     ?     ?     &?  :   9?     t?     �?     �?     �?     �?  S   �?  M   7@     �@     �@     �@     �@     �@     �@     �@  	   �@  I   �@     0A     @A     OA  	   TA     ^A  i   fA     �A     �A  �   �A  �   �B  �   dC  9   .D  �   hD  �   �D  �   �E  7   �F     �F     �F     �F     �F     G     (G     CG  �   ]G  >   H  �   QH  �   4I  �   �I  �   �J  J   "K  {   mK  t   �K     ^L  ,   eL  �   �L  6   6M     mM     �M     �M     �M     �M  X   �M     7N     DN     YN     hN     wN     �N  
   �N     �N     �N  
   �N     �N     �N     �N     �N  m   �N     _O  =   gO  )   �O     �O     �O     �O     �O     �O     P     'P     4P     AP  
   FP  
   QP     \P     lP     �P     �P  -   �P  	   �P  '   �P  +    Q     ,Q      IQ     jQ  �   oQ     :R     @R  0   QR  2   �R  A   �R  ;   �R  6   3S     jS  $   xS     �S     �S     �S     �S     �S     �S     �S     �S     T     T     T     3T     ;T     DT     KT     `T     fT     }T  #   �T  �   �T  e   AU    �U     �V     �V     �V  "   �V  #   W     ?W  *   WW     �W     �W  
   �W     �W     �W     �W  	   �W     �W     �W     �W     �W     X     X     ,X     5X     BX     UX     eX     rX     ~X     �X     �X     �X     �X     �X     �X     �X  E  �X     6Z     VZ     cZ     hZ     uZ  
   �Z     �Z     �Z  
   �Z  &   �Z     �Z     �Z     �Z     [     [  P   $[  �   u[     \     +\     9\     U\     n\     |\     �\     �\     �\     �\     �\     �\     �\  �   �\  �   Z]     �]     �]     ^     ^     ,^     H^  	   c^  h   m^     �^     �^     �^  
   �^     _     _     &_  G   ._     v_     �_     �_  	   �_  
   �_  `   �_  �   `  2   �`  6   �`  X   *a  0   �a  /   �a  '   �a  J   b  6   Wb  ,   �b  +   �b  
   �b     �b     �b     c     c     c  ;   c  Z   Tc     �c     �c     �c     �c  7   �c     d     d     'd     0d     7d  a  Nd     �e     �e     �e  �   �e     �f     �f     �f  "  �f     �g     �g  	   �g  �   �g  �   �h  j   i  �   �i  �   j  !   �j  $   k     8k     Uk  �  bk  .   �l  "   )m     Lm  	   km     um     �m     �m     �m     �m  $   �m  =   �m     .n     Dn     Zn     nn  
   n     �n     �n     �n     �n     �n     �n     �n     �n      o     o     'o     7o     Fo     Uo     fo     so     �o     �o     �o     �o  J   �o     p     %p  l  8p     �s     �s     �s  !   �s     �s     �s     �s     t  "   7t  -   Zt  "   �t  0   �t     �t  :   �t  8   #u  1   \u     �u     �u     �u  �  �u  L   {w  b   �w  1   +x  V   ]x  _   �x  h   y  \   }y  W   �y  k   2z  d   �z     {     {     {     +{     ?{  g   L{  )   �{     �{     �{     	|     |  	   -|     7|  3   R|     �|     �|     �|     �|     �|     �|  4   �|     '}  	   /}     9}     @}     H}     U}     g}     t}     �}     �}  �   �}     9~     M~     h~  8   ~~     �~  e  �~  
   .�     9�  "   G�     j�  8   s�  	   ��     ��     ʀ  	   �     �     �     �  .   <�  	   k�     u�     ��     ��     ��     ��     ��     ��     ��  �   ��     `�     i�     r�     x�     ~�     ��     ��  '   ��  I   ʂ     �    $�     5�     =�     [�     b�     w�     ��     ��     ��     ��     ��  
   ��  �   Ą    ��    ��  �   ԇ    ��    Ή  
   �  -   �  "   �  *   @�  *   k�  (   ��     ��     ȋ     ֋     �  !   ��     �  
   %�     0�     9�  
   F�  /   Q�     ��  2   ��     ��          ӌ     ڌ  &   ��     �     '�     8�  	   K�     U�  	   e�  #   o�  9   ��     ͍     ލ     ��  �   �     ��     ��     ��  A   َ     �     9�  %   @�  (   f�  ,   ��  V   ��  k   �     �     ��     ��     ��     ��     Ȑ     ϐ  	   �  U   �     D�     Y�     k�     n�     |�  i   ��     �     ��  �   �  �   ��  �   ��  8   ��  �   ϔ  �   o�  �   e�  8   I�     ��     ��     ��     ��  (   ˗     ��     �  �   *�  X   �    A�  �   \�  �   �  �   ڛ  Q   e�  ~   ��  �   6�     ɝ  3   ѝ  �   �  =   Þ     �      �  "   1�  	   T�     ^�  a   }�  
   ߟ     �     �     �     (�     9�     I�     U�  $   i�     ��     ��     ��     ��     ��  h   ��  	   *�  8   4�  (   m�     ��     ��     ��     ��     ҡ     �     �     �     "�     )�     8�     F�     Z�     v�     ��  D   ��     �  9   �  4   +�     `�  !   |�     ��  �   ��     ��     ��  @   ��  1   �  8   %�  ;   ^�  1   ��  	   ̥  0   ֥     �     �     /�     3�     D�     L�     S�     e�     ��     ��  !   ��  	   ��  	   æ     ͦ     ۦ     �     ��     �  0   �  z   N�  u   ɧ  <  ?�     |�     ��      ��  &   ٩  (    �  !   )�  &   K�     r�     ��     ��     ��  	   ��     ��     ��     Ī     ̪     �     ��     �     0�  
   =�     H�     V�     l�     |�     ��     ��     ��     ʫ  	   ݫ     �     ��     ��     
�  j  �  #   ��     ��     ��     ��     ϭ     �     �  	   �  
   �  *   �     E�     ]�     o�     ��     ��  I   ��  �   ��     ��     ʯ  #   ޯ     �     �     0�     C�     U�     [�     c�     l�     ��     ��  �   ��  �   K�     �     ��     �     ,�  !   A�  %   c�     ��  h   ��     ��      �     �     &�  %   6�     \�     b�  F   j�     ��     г     �     �     ��  r   �  �   {�  .   /�  2   ^�  Y   ��  5   �  4   !�  )   V�  R   ��  ;   Ӷ  8   �  3   H�     |�     ��     ��     ��     ��     ��  H   ��  X   �     Z�     _�     s�     {�  C   ��     ĸ     Ը     �     �  $   ��  �  �     ��     ʺ     Ѻ  �   �     ˻     �  	   ��  B  ��     A�     M�     P�  �   U�  �   �  l   u�  �   �    m�  #   y�  +   ��  $   ��     ��  �  ��  3   ��  !   �  %   <�     b�  	   o�     y�  #   ��     ��     ��  0   ��  J   ��     A�     \�     w�     ��     ��     ��     ��     ��     ��     ��     ��     �     �     $�     ;�     M�     b�     k�     p�     ��  "   ��     ��     ��     ��     ��  N   ��     B�     Q�     �   j          �   �   O  i   �   v       r  �   �       [   y     d          7  �   �   �   �      S   �  D  _   �  R    �          �      �   �   �  �      �   G   �   �       &          f  �     �      /        M  �   �                  �   Q  �   �   �   |   �   �   ;  �      �   �  �      �      n   �          �   �     �   "       w      �   +  �     �  �                   }  �       '   �   �       `   �   �   �       8  �  ,   �   �   _  �   6              �   B   �           ~      �  7   �      �  |  �   {          �       a  �  N   @       X      P  U         �  ^   �      �      .  �  Y         q     �  �       �   �  H  :       j       z   �   �       �       �     �         �   �       �  V  �      �  ]  �          !  8       �   H   �           �   �  �   t   }       @  "  6   T          �  �   *      �  f   4  1      �   �   (       u           w   �       P       �  �   +           )  �   L  O   4           �  E    :    $      l   s   �   �               �             E   #                 �      �    �  >     Y         �      �  �   L       �   �  �   �       s  3   G  �  B                 �   �   2           x            Z  n  �      �   g   �   �              �   p  �   �  y  K  �  2    a   �      {  �   �   [      �       V       -   �      �  �  %      ?   �       �           h      �   �   �          �   �   �  o   �   �   #               Q       J  K   %       S      �   �   �   C   �  h   �   �  9        U              r      g  .   �          1          �  A  m   9   <   �  D             ]     '  �   J      �                         �   �  &   �  3      k  �     x   C  $   	          �       5          	   ?              W  �  0   �           d   m  �   �  /   z  �  ~   �   e  �  �   Z   �   b  �           �   I  e   ^  )   �    t  �  =  �      �   �   W   (  �  k         �  >  �   �  c   v  �  �           ,  �  �  �           �       F      �   �       �       0      5    =   p   !   �       i      �       �      -      �   �   �           �  \  �  R   �       N    A       �  �  I   �   q   �      �  �    F       �  b   o  �  �  �   ;   �         �   l  `  u  <  �           \   X  �              �      �       �   c      T  �   *       �   �     �      
      M      
   �      �     No staff selected  Reset   to  "I'm available on …" block %d h %d min * Invalid email * Invalid phone number * Please select a service * Please tell us your email * Please tell us your name * Please tell us your phone * Required * The start time must be less than the end time * This coupon code is invalid or has been used * This email is already in use -- Search customers -- -- Select a service -- 2 way sync <b>Important!</b> Please be aware that if your copy of Bookly was not downloaded from Codecanyon (the only channel of Bookly distribution), you may put your website under significant risk - it is very likely that it contains a malicious code, a trojan or a backdoor. Please consider buying a licensed copy of Bookly <a href="http://booking-wp-plugin.com" target="_blank">here</a>. <b>Leave this field empty</b> to work with the default calendar. <b>[[CATEGORY_NAME]]</b> - name of category, <b>[[NUMBER_OF_PERSONS]]</b> - number of persons. <b>[[NUMBER_OF_PERSONS]]</b> - number of persons. <b>[[SERVICE_NAME]]</b> - name of service, <b>[[STAFF_NAME]]</b> - name of staff, <b>[[SERVICE_PRICE]]</b> - price of service, <b>[[CATEGORY_NAME]]</b> - name of category, <b>[[SERVICE_PRICE]]</b> - price of service, <b>[[TOTAL_PRICE]]</b> - total price of booking, <b>[[SERVICE_TIME]]</b> - time of service,  <b>[[SERVICE_DATE]]</b> - date of service, <b>[[STAFF_NAME]]</b> - name of staff,  <b>[[SERVICE_NAME]]</b> - name of service, <b>[[TOTAL_PRICE]]</b> - total price of booking, <b>[[NUMBER_OF_PERSONS]]</b> - number of persons. <h3>The selected time is not available anymore. Please, choose another time slot.</h3> API Login ID API Password API Signature API Transaction Key API Username Accept <a href="javascript:void(0)" data-toggle="modal" data-target="#ab-tos">Terms & Conditions</a> Add Bookly appointments list Add Bookly booking form Add Coupon Add Service Add money Address Administrator phone Administrator phone number is empty. All Services All customers All payment types All providers All services All staff Allow staff members to edit their profiles Amount Any Appearance Apply Appointment Appointment Date Appointments Are you sure? Avatar Back Below you can find a list of available time slots for [[SERVICE_NAME]] by [[STAFF_NAME]].
Click on a time slot to proceed with booking. Booking Time Booking cancellation Booking product Bookly: You do not have sufficient permissions to access this page. Business hours By default Bookly pushes new appointments and any further changes to Google Calendar. If you enable this option then Bookly will fetch events from Google Calendar and remove corresponding time slots before displaying the second step of the booking form (this may lead to a delay when users click Next at the first step). Calendar Calendar ID Calendar ID is not valid. Cancel Cancel appointment page URL Capacity Card Security Code Cart item data Category Change password Checkbox Checkbox Group Click on the underlined text to edit. Client ID Client secret Code Codes Columns Comma (,) Company Company logo Company name Configure what information should be places in the title of Google Calendar event. Available codes are [[SERVICE_NAME]], [[STAFF_NAME]] and [[CLIENT_NAMES]]. Connect Connected Cost Country Country out of service Coupon Coupons Create WordPress user account for customers Create a product in WooCommerce that can be placed in cart. Create customer Create your project's OAuth 2.0 credentials by clicking <b>Create new Client ID</b>, selecting <b>Web application</b>, and providing the information needed to create the credentials. For <b>AUTHORIZED REDIRECT URIS</b> enter the <b>Redirect URI</b> found below on this page. Credit Credit Card Number Currency Custom Fields Custom Range Customer Customer Name Customers Date Day Days off Dear [[CLIENT_NAME]].

Thank you for choosing [[COMPANY_NAME]]. We hope you were satisfied with your [[SERVICE_NAME]].

Thank you and we look forward to seeing you again soon.

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Dear [[CLIENT_NAME]].

This is confirmation that you have booked [[SERVICE_NAME]].

We are waiting you at [[COMPANY_ADDRESS]] on [[APPOINTMENT_DATE]] at [[APPOINTMENT_TIME]].

Thank you for choosing our company.

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Dear [[CLIENT_NAME]].

We would like to remind you that you have booked [[SERVICE_NAME]] tomorrow on [[APPOINTMENT_TIME]]. We are waiting you at [[COMPANY_ADDRESS]].

Thank you for choosing our company.

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Dear [[CLIENT_NAME]].
Thank you for choosing [[COMPANY_NAME]]. We hope you were satisfied with your [[SERVICE_NAME]].
Thank you and we look forward to seeing you again soon.
[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Dear [[CLIENT_NAME]].
This is confirmation that you have booked [[SERVICE_NAME]].
We are waiting you at [[COMPANY_ADDRESS]] on [[APPOINTMENT_DATE]] at [[APPOINTMENT_TIME]].
Thank you for choosing our company.
[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Dear [[CLIENT_NAME]].
We would like to remind you that you have booked [[SERVICE_NAME]] tomorrow on [[APPOINTMENT_TIME]]. We are waiting you at [[COMPANY_ADDRESS]].
Thank you for choosing our company.
[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Deduction Deduction should be a positive number. Default country code Default value for category select Default value for employee select Default value for service select Delete Delete break Delete current photo Delete customer Delete this staff member Delimiter Delivered Details Disabled Discount (%) Discount should be between 0 and 100. Dismiss Display available time slots in client's time zone Done Drop Down Duration Edit appointment Edit booking details Email Email Notifications Email already in use. Employee Empty password. Enabled End time must not be empty End time must not be equal to start time Enter a URL Enter a label Enter a name Enter a phone number in international format. E.g. for the United States a valid phone number would be +17327572923. Enter a value Enter code from email Enter phone number Enter this URL as a redirect URI in the Developers Console Enter your country code Error Error adding the break interval Error connecting to server. Error sending email. Evening notification with the next day agenda to staff member (requires cron setup) Evening reminder to customer about next day appointment (requires cron setup) Expiration Date Expired Export to CSV Failed Failed to send SMS. Filter Final step URL Finish by Follow-up message in the same day after appointment (requires cron setup) Forgot password Form ID error. From Full name General Go to the <a href="https://console.developers.google.com/" target="_blank">Google Developers Console</a>. Google Calendar Google Calendar integration Hello.

An account was created for you at [[SITE_ADDRESS]]

Your user details:
user: [[NEW_USERNAME]]
password: [[NEW_PASSWORD]]

Thanks. Hello.

The following booking has been cancelled.

Service: [[SERVICE_NAME]]
Date: [[APPOINTMENT_DATE]]
Time: [[APPOINTMENT_TIME]]
Client name: [[CLIENT_NAME]]
Client phone: [[CLIENT_PHONE]]
Client email: [[CLIENT_EMAIL]] Hello.

You have new booking.

Service: [[SERVICE_NAME]]
Date: [[APPOINTMENT_DATE]]
Time: [[APPOINTMENT_TIME]]
Client name: [[CLIENT_NAME]]
Client phone: [[CLIENT_PHONE]]
Client email: [[CLIENT_EMAIL]] Hello.

Your agenda for tomorrow is:

[[NEXT_DAY_AGENDA]] Hello.
An account was created for you at [[SITE_ADDRESS]]
Your user details:
user: [[NEW_USERNAME]]
password: [[NEW_PASSWORD]]

Thanks. Hello.
The following booking has been cancelled.
Service: [[SERVICE_NAME]]
Date: [[APPOINTMENT_DATE]]
Time: [[APPOINTMENT_TIME]]
Client name: [[CLIENT_NAME]]
Client phone: [[CLIENT_PHONE]]
Client email: [[CLIENT_EMAIL]] Hello.
You have new booking.
Service: [[SERVICE_NAME]]
Date: [[APPOINTMENT_DATE]]
Time: [[APPOINTMENT_TIME]]
Client name: [[CLIENT_NAME]]
Client phone: [[CLIENT_PHONE]]
Client email: [[CLIENT_EMAIL]] Hello.
Your agenda for tomorrow is:
[[NEXT_DAY_AGENDA]] Hide this block Hide this field Holidays I will pay locally I will pay now with Credit Card I will pay now with PayPal I'm available on or after If email or SMS notifications are enabled and you want the customer or the staff member to be notified about this appointment after saving, tick this checkbox before clicking Save. If needed, edit item data which will be displayed in the cart. If there is a lot of events in Google Calendar sometimes this leads to a lack of memory in PHP when Bookly tries to fetch all events. You can limit the number of fetched events here. This only works when 2 way sync is enabled. If this option is enabled then all staff members who are associated with WordPress users will be able to edit their own profiles, services, schedule and days off. If this setting is enabled then Bookly will be creating WordPress user accounts for all new customers. If the user is logged in then the new customer will be associated with the existing user account. If this staff member requires separate login to access personal calendar, a regular WP user needs to be created for this purpose. If you are not redirected automatically, follow the <a href="%s">link</a>. If you do not see your country in the list please contact us at <a href="mailto:support@ladela.com">support@ladela.com</a>. If you will leave this field blank, this staff member will not be able to access personal calendar using WP backend. Import In the form below enable WooCommerce option. In the sidebar on the left, expand <b>APIs & auth</b>. Next, click <b>APIs</b>. In the list of APIs, make sure the status is <b>ON</b> for the Google Calendar API. In the sidebar on the left, select <b>Credentials</b>. Incorrect email or password. Incorrect password. Incorrect recovery code. Insert Insert Appointment Booking Form Insert a URL of a page that is shown to clients after they have cancelled their booking. Instructions Invalid date or time Invalid email. Invalid number Last 30 Days Last 7 Days Last Month Last appointment Limit number of fetched events Loading... Local Log in Log out Login Look for the <b>Client ID</b> and <b>Client secret</b> in the table associated with each of your credentials. Message Method to include Bookly JavaScript and CSS files on the page Minimum time requirement prior to booking Name New Category New Customer New Staff Member New appointment New booking information New customer New password Next Next Month Next month No appointments No appointments found No coupons found No customers No payments for selected period and criteria. No result No services found. Please add services. No time is available for selected criteria. No, delete just the customer No, update just here in services Note Note that once you have enabled WooCommerce option in Bookly the built-in payment methods will no longer work. All your customers will be redirected to WooCommerce cart instead of standard payment step. Notes Notes (optional) Notification settings were updated successfully. Notification to customer about appointment details Notification to customer about their WordPress user login details Notification to staff member about appointment cancellation Notification to staff member about appointment details Notifications Number of days available for booking Number of persons Number of times used OFF Old password Option Order Out of credit Page Redirection Participants Password Passwords must be the same. Payment Payments Period Personal Information Phone Phone number is empty. Photo Please accept terms and conditions. Please be aware that a value in this field is required in the frontend. If you choose to hide this field, please be sure to select a default value for it Please configure Google Calendar <a href="?page=ab-settings&type=_google_calendar">settings</a> first Please do not forget to specify your purchase code in Bookly <a href="admin.php?page=ab-settings">settings</a>. Upon providing the code you will have access to free updates of Bookly. Updates may contain functionality improvements and important security fixes. Please enter old password. Please select a customer Please select a service Please select at least one coupon. Please select at least one service. Please select service:  Please tell us how you would like to pay:  Previous month Price Price list Profile Provider Purchase Code Purchases Queued Quick search customer Radio Button Radio Button Group Recovery code expired. Redirect URI Register Registration Remember my choice Remove customer Remove field Remove item Repeat every year Repeat new password Repeat password Required Required field Reset SMS Details SMS Notifications SMS Notifications (or "Bookly SMS") is a service for notifying your customers via text messages which are sent to mobile phones.<br/>It is necessary to register in order to start using this service.<br/>After registration you will need to configure notification messages and top up your balance in order to start sending SMS. SMS has been sent successfully. Sandbox Mode Save Save Changes Save Member Save break Save category Schedule Secret Key Select a project, or create a new one. Select category Select file Select from WP users Select product Select service Select the product that you created at step 1 in the drop down list of products. Select the time interval that will be used in frontend and backend, e.g. in calendar, second step of the booking process, while indicating the working hours, etc. Selected / maximum Semicolon (;) Send copy to administrators Send email notifications Send test SMS Sender email Sender name Sending Sent Service Service paid locally Services Session error. Set a URL of a page that the user will be forwarded to after successful booking. If disabled then the default step 5 is displayed. Set a minimum amount of time before the chosen appointment (for example, require the customer to book at least 1 hour before the appointment time). Settings Settings saved. Show blocked timeslots Show calendar Show each day in one column Show form progress tracker Signed up Specify the number of days that should be available for booking at step 2 starting from the current day. Staff Staff Member Staff Members Start from Start time must not be empty Status Subject Synchronize the data of the staff member bookings with Google Calendar. Template for event title Terms & Conditions Text Text Area Text Field Thank you! Your booking is complete. An email with details of your booking has been sent to you. The Calendar ID can be found by clicking on "Calendar settings" next to the calendar you wish to display. The Calendar ID is then shown beside "Calendar Address". The client ID obtained from the Developers Console The client secret obtained from the Developers Console The maximum number of customers allowed to book the service for the certain time period. The number of customers should be not more than  The price for the service is [[SERVICE_PRICE]]. The requested interval is not available The selected period does't match default duration for the selected service The selected period is occupied by another appointment The start time must be less than the end one The value is taken from client’s browser. This Month Time Time slot length Title Titles To To find your client ID and client secret, do the following: To send scheduled notifications please execute the following script hourly with your cron: Today Total appointments Total:  Type URL for cancel appointment link (to use inside <a> tag) Uncategorized Undelivered Untitled Update Update service setting Upon providing the purchase code you will have access to free updates of Bookly. Updates may contain functionality improvements and important security fixes. For more information on where to find your purchase code see this <a href="https://help.market.envato.com/hc/en-us/articles/202822600-Where-can-I-find-my-Purchase-Code-" target="_blank">page</a>. Usage limit User User not found. User with "Administrator" role will have access to calendars and settings of all staff members, user with some other role will have access only to personal calendar and settings. We are not working on this day Website Week With "Enqueue" method the JavaScript and CSS files of Bookly will be included on all pages of your website. This method should work with all themes. With "Print" method the files will be included only on the pages which contain Bookly booking form. This method may not work with all themes. WooCommerce Yes Yesterday You are about to change a service setting which is also configured separately for each staff member. Do you want to update it in staff settings too? You are about to delete a customer which may have a WordPress account associated to them. Do you want to delete that account too (if there is one)? You may import list of clients in CSV format. The file needs to have three columns: Name, Phone and Email. You need to install and activate WooCommerce plugin before using the options below.<br/><br/>Once the plugin is activated do the following steps: You selected a booking for [[SERVICE_NAME]] by [[STAFF_NAME]] at [[SERVICE_TIME]] on [[SERVICE_DATE]]. The price for the service is [[SERVICE_PRICE]].
Please provide your details in the form below to proceed with booking. Your agenda for [[TOMORROW_DATE]] Your appointment at [[COMPANY_NAME]] Your appointment information Your balance Your clients must have their phone numbers in international format in order to receive text messages. However you can specify a default country code that will be used as a prefix for all phone numbers that do not start with "+" or "00". E.g. if you enter "1" as the default country code and a client enters their phone as "(600) 555-2222" the resulting phone number to send the SMS to will be "+1600555222". Your payment has been accepted for processing. Your payment has been interrupted. Your visit to [[COMPANY_NAME]] add break address of your company breaks: cancel appointment link characters max characters min combined values of all custom fields combined values of all custom fields (formatted in 2 columns) customer new password customer new username date of appointment date of next day disconnect email of client email of staff is too long is too short name of category name of client name of service name of staff name of your company number of persons phone of client phone of staff photo of staff price of service site address staff agenda for next day staff members this web-site address time of appointment to total price of booking (service price multiplied by the number of persons) your company logo your company phone Project-Id-Version: Bookly v7.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-06-04 14:49+0200
PO-Revision-Date: 2015-06-04 14:49+0200
Last-Translator: Alessandro Rivolta <alessandro@rivalex.com>
Language-Team: 
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-SourceCharset: UTF-8
X-Generator: Poedit 1.6.7
X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2
X-Poedit-Basepath: .
X-Textdomain-Support: yes
X-Loco-Target-Locale: it_IT
X-Poedit-SearchPath-0: ..
 Nessuno staff selezionato Reset  alle  Blocco "Sarà disponibile il ..." %d h %d min * Indirizzo eMail non valido * Numero di telefono non valido * Per favore seleziona un servizio * Per favore inserisci il tuo indirizzo eMail * Per favore inserisci il tuo nome * Per favore inserisci il tuo numero di telefono * Richiesto * L'orario iniziale deve essere precedente l'orario finale * Questo coupon non è valido o è già stato utilizzato * Questo indirizzo eMail è già stato utilizzato -- Cerca i clienti -- -- Seleziona un servizio -- Doppia sincronizzazione <b>Importante!<b> Se la tua copia di Bookly non è stata scaricata da CodeCanyon (unico canale di distribuzione ufficiale di Bookly), potresti mettere il tuo sito a rischio di attacchi - è molto probabile che il codice contenga codice dannoso, un trojan on una backdoor. Per favore prendi in considerazione di acquistare una licenza originale di Bookly 
<a href="http://booking-wp-plugin.com" target="_blank">qui</a>. <b>Lascia questo campo vuoto</b> per lavorare con il calendario predefinito. <b>[[CATEGORY_NAME]]</b> - nome della categoria, <b>[[NUMBER_OF_PERSONS]]</b> - numero di persone. <b>[[NUMBER_OF_PERSONS]]</b> - numero di persone. <b>[[SERVICE_NAME]]</b> - nome del servizio, <b>[[STAFF_NAME]]</b> - nome dello staff, <b>[[SERVICE_PRICE]]</b> - costo del servizio, <b>[[CATEGORY_NAME]]</b> - nome della categoria, <b>[[SERVICE_PRICE]]</b> - costo del servizio, <b>[[TOTAL_PRICE]]</b> - costo totale della prenotazione, <b>[[SERVICE_TIME]]</b> - orario del servizio,  <b>[[SERVICE_DATE]]</b> - data del servizio, <b>[[STAFF_NAME]]</b> - nome dello staff,  <b>[[SERVICE_NAME]]</b> - nome del servizio, <b>[[TOTAL_PRICE]]</b> - costo totale della prenotazione, <b>[[NUMBER_OF_PERSONS]]</b> - numero di persone. <h3>La fascia oraria selezionata non è più disponibile. Per favore seleziona un altro orario.</h3> API Login ID API Password API Signature API Transaction Key API Username Accetta <a href="javascript:void(0)" data-toggle="modal" data-target="#ab-tos">Termini e Condizioni</a> Aggiungi una lista appuntamenti di Bookly Aggiungi un form Bookly Aggiungi un coupon Aggiungi un Servizio Aggiungi soldi Indirizzo Telefono di amministratore Il numero di telefono dell'amministratore è vuota. Tutti i servizi Tutti i clienti Tutti i tipi di pagamento Tutti le categorie Tutti i servizi Tutto lo staff Permetti allo staff di modificare il proprio profilo Importo Qualsiasi Layout Applica Appuntamento Data Appuntamento Appuntamenti Sei sicuro? Avatar Indietro Di seguito puoi trovare una lista dell fasce orarie disponibili per 
[[SERVICE_NAME]] con [[STAFF_NAME]].
Clicca su una fascia oraria per procedere con la prenotazione. Orario prenotazione Cancellazione prenotazione Prodotto prenotazione Bookly: Non hai i permessi per accedere a questa pagina. Orari lavorativi Di default Bookly invia a Google Calendar ogni nuovo appuntamento e ogni altro cambiamento. Se abiliti questa opzione, Bookly recupererà gli eventi da Google Calendar e rimuoverà i corrispondenti gli intervalli di tempo prima di mostrare il secondo step del processo di prenotazione (penso può causare un ritardo nel passaggio dal primo al secondo step). Calendario ID Calendario L'ID del Calendario non è valido. Cancella URL della pagina per la cancellazione di un appuntamento Capacità Codice di sicurezza Dati del prodotto nel carrello Categoria Cambiare la password Casella di controllo Gruppo di caselle si controllo Clicca sui testi sottolineati per modificarli. Client ID Client secret Codice Codici Colonne Virgola (,) Azienda Logo Nome Configurare quali informazioni devono essere posti nel titolo di evento di Google Calendar. Codici disponibili sono [[SERVICE_NAME]], [[STAFF_NAME]] e [[CLIENT_NAMES]]. Connetti Connesso Costo Paese Paese fuori servizio Coupon Coupons Crea un account WordPress per i clienti Crea un prodotto in WooCommerce che possa essere inserito in un carrello. Crea un cliente Crea per il progetto le credenziali OAuth 2.0 cliccando su 
<b>Create new Client ID</b>, seleziona 
<b>Web application</b>, e inserisci le informazioni richieste. Per 
<b>AUTHORIZED REDIRECT URIS</b> inserisci la 
<b>Redirect URI</b> che trovi più sotto in questa pagina. Credito Numero della Carta di Credito Valuta Campi personalizzati Range personalizzato Cliente Nome del cliente Clienti Data Giorno Giorni off Gentile [[CLIENT_NAME]].

Grazie per aver scelto [[COMPANY_NAME]]. Speriamo tu sia rimasto soddisfatto per [[SERVICE_NAME]].

Grazie e speriamo di rivederti presto.

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Gentile [[CLIENT_NAME]].

Questa è la conferma per la tua prenotazione per [[SERVICE_NAME]].

Ti spettiamo presso [[COMPANY_ADDRESS]] il [[APPOINTMENT_DATE]] alle [[APPOINTMENT_TIME]].

Grazie per aver scelto la nostra azienda.

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Gentile [[CLIENT_NAME]].

Vorremmo ricordarti il tuo appuntamento per [[SERVICE_NAME]] fissato per domani alle ore [[APPOINTMENT_TIME]]. Ti aspettiamo presso [[COMPANY_ADDRESS]].

Grazie per aver scelto la nostra azienda.

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Gentile [[CLIENT_NAME]].

Grazie per aver scelto [[COMPANY_NAME]]. Speriamo tu sia rimasto soddisfatto per [[SERVICE_NAME]].

Grazie e speriamo di rivederti presto.

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Gentile [[CLIENT_NAME]].

Questa è la conferma per la tua prenotazione per [[SERVICE_NAME]].

Ti spettiamo presso [[COMPANY_ADDRESS]] il [[APPOINTMENT_DATE]] alle [[APPOINTMENT_TIME]].

Grazie per aver scelto la nostra azienda.

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Gentile [[CLIENT_NAME]].

Vorremmo ricordarti il tuo appuntamento per [[SERVICE_NAME]] fissato per domani alle ore [[APPOINTMENT_TIME]]. Ti aspettiamo presso [[COMPANY_ADDRESS]].

Grazie per aver scelto la nostra azienda.

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Detrazione Deduzione dovrebbe essere un numero positivo. Prefisso internazionale di default Valore predefinito per selezione categorie Valore predefinito per selezione impiegati Valore predefinito per selezione servizi Cancella Elimina pausa Rimuovi la foto Rimuovi cliente Rimuovi questo membro dello staff Delimitatore Consegnato Dettagli Disabilitato Sconto (%) Lo sconto dovrebbe essere compreso tra 0 e 100. Rifiuta Mostra le fasce orarie nella time zone del cliente Fatto Menu a scomparsa Durata Modifica appuntamento Modifica i dettagli della prenotazione Indirizzo eMail Notifiche e-mail Email già in uso. Impiegato Password vuota. Abilitato L'ora di fine non può essere vuota L'ora finale dovrebbe essere successiva all'ora di inizio Inserisci un URL Inserisci un'etichetta Inserisci un nome Digitare un numero di telefono in formato internazionale. Ad es per l'Italia un numero di telefono valido sarebbe +393555333222. Inserisci un valore Inserisci il codice da e-mail Inserire il numero di telefono Inserisci questa URL come "
redirect URI" nella Developer Console Inserisci il tuo codice Paese Errore Errore nell'aggiunta di un intervallo Errore durante la connessione al server. Errore durante l'invio di posta elettronica. Notifica serale allo staff con l'agenda del giorno dopo (richiede configurazione cron) Notifica serale al cliente con i dettagli della prenotazione del giorno dopo (richiede configurazione cron) Data di scadenza Scaduto Esporta in CSV Mancato Impossibile inviare SMS. Filtro URL dell'ultimo step Fino alle Messaggio di ringraziamento subito dopo l'appuntamento (richiede configurazione cron) Password dimenticata Errore ID modulo. Da Nome completo Generale Vai alla 
<a href="https://console.developers.google.com/" target="_blank">Google Developers Console</a>. Google Calendar Integrazione Google Calendar Ciao.

Abbiamo creato per te un account su [[SITE_ADDRESS]]

Ecco i dettagli del tuo account:
Nome utente: [[NEW_USERNAME]]
Password: [[NEW_PASSWORD]]

Grazie. Ciao.

La prenotazione seguente è stata cancellata.

Servizio: [[SERVICE_NAME]]
Data: [[APPOINTMENT_DATE]]
Orario: [[APPOINTMENT_TIME]]
Nome del cliente: [[CLIENT_NAME]]
Telefono del cliente: [[CLIENT_PHONE]]
eMail del cliente: [[CLIENT_EMAIL]] Ciao.

Hai una nuova prenotazione.

Servizio: [[SERVICE_NAME]]
Data: [[APPOINTMENT_DATE]]
Orario: [[APPOINTMENT_TIME]]
Nome del cliente: [[CLIENT_NAME]]
Telefono del cliente: [[CLIENT_PHONE]]
eMail del cliente: [[CLIENT_EMAIL]] Ciao.

La tua agenda per domani è:

[[NEXT_DAY_AGENDA]] Ciao.

Abbiamo creato per te un account su [[SITE_ADDRESS]]

Ecco i dettagli del tuo account:
Nome utente: [[NEW_USERNAME]]
Password: [[NEW_PASSWORD]]

Grazie. Ciao.

La prenotazione seguente è stata cancellata.

Servizio: [[SERVICE_NAME]]
Data: [[APPOINTMENT_DATE]]
Orario: [[APPOINTMENT_TIME]]
Nome del cliente: [[CLIENT_NAME]]
Telefono del cliente: [[CLIENT_PHONE]]
eMail del cliente: [[CLIENT_EMAIL]] Ciao.

Hai una nuova prenotazione.

Servizio: [[SERVICE_NAME]]
Data: [[APPOINTMENT_DATE]]
Orario: [[APPOINTMENT_TIME]]
Nome del cliente: [[CLIENT_NAME]]
Telefono del cliente: [[CLIENT_PHONE]]
eMail del cliente: [[CLIENT_EMAIL]] Ciao.

La tua agenda per domani è:

[[NEXT_DAY_AGENDA]] Nascondi questo blocco Nascondi questo campo Vacanze Pagherò localmente PagheròPago subito con Carta di Credito Pago subito con PayPal Sarò disponibile il o dopo il Se le eMail o SMS di notifica sono abilitate e desideri che i clienti e lo staff siano notificati con i dettagli di questo appuntamento, spunta questa casella di controllo prima di salvare. Se necessario, modifica le informazioni del prodotto che verranno mostrate nel carrello. Se ci sono molti eventi su Google Calendari è possibile che Bookly richieda al PHP una notevole quantità di memoria per catturare tutti gli eventi. Puoi limitare il numero di eventi da catturare qui. Questa funzionalità è attiva solo se la doppia sincronizzazione è abilitata.  Se quest'opzione è abilitata tutti i membri dello staff associati con un account WordPress, potranno modificare li proprie impostazioni, i servizi, gli orari e i giorni di chiusura.  Se questa impostazione è abilitata, Bookly creerà un account WordPress per tutti i nuovi clienti. Se il cliente è già loggato con un account, il nuovo cliente verrà associato a questo account. Se questo membro dello staff richiede un accesso separato per accedere al calendario personale, devi creare un account WordPress dedicato. Se non vieni automaticamente re-indirizzato, segui questo 
<a href="%s">link</a>. Se non vedete il vostro paese nella lista contattaci all'indirizzo <a href="mailto:support@ladela.com">support@ladela.com</a>. Se lasci questo campo vuoto, questo membro dello staff non sarà in grado di accedere al calendario personale utilizzando il backend di WordPress. Importa Nel form sottostante abilita l'opzione WooCommerce. Nel menu laterale di sinistra, espandi la sezione 
<b>APIs & auth</b>, quindi clicca su 
<b>APIs</b>. Nell'elenco delle API, assicurati che la 
Google Calendar API sia <b>ON</b> (abilitata) Nel menu laterale di sinistra, seleziona 
<b>Credentials</b>. Email o password non corretta. Password errata. Codice di ripristino non corretto. Inserisci Inserisci un form Prenotazioni Inserisci l'URL della pagina che verrà mostrata ai clienti dopo la rimozione di un appuntamento. Istruzioni Data o orario non validi E-mail non valido. Numero non valido Ultimi 30 giorni Ultimi 7 giorni Ultimo Mese Ultimo appuntamento Limita il numero di eventi catturati Caricamento in corso ... Locale Login Uscire Login Cerca il 
<b>Client ID</b> e la 
<b>Client secret</b> nella tabella associata con le credenziali create. Messaggio Metodo di includere Bookly JavaScript e CSS della pagina Tempo richiesto prima della prenotazione Nome Nuova Categoria Nuovo cliente Nuovo membro dello staff Nuovo appuntamento Informazioni nuova prenotazione Nuovo cliente Nuova password Avanti Prossimo mense Prossimo mese Nessun appuntamento Nessun appuntamento trovato Nessun coupon trovato Nessun cliente Nessun pagamento trovato per la fascia oraria e i criteri impostati. Nessun risultato Nessun servizio trovato. Per favore aggiungi un servizio. Nessun orario disponibile per i criteri selezionati. No, rimuovi solo il cliente No, aggiorna il servizio solo qui Nota Nota che una volta abilitato WooCommerce in Bookly, le modalità di pagamento predefinite non saranno più funzionanti. Tutti i tuoi clienti verranno re-indirizzati al carrello WooCommerce invece di procedere allo step predefinito per il pagamento. Note Note (opzionale) Le impostazioni di notifica sono state aggiornate correttamente. Notifica al cliente i dettagli della prenotazione Notifica al cliente i dettagli del suo account WordPress Notifica allo staff per la cancellazione di un appuntamento Notifica allo staff i dettagli della prenotazione Notifiche Numero di giorni disponibili per la prenotazione Numero di persone Numero di volte usato OFF Vecchia password Opzione Ordine Fuori del credito Pagina di re-indirizzamento Partecipanti Password Le password devono essere uguali. Pagamento Pagamenti Fascia oraria Informazioni personali Telefono Numero di telefono è vuoto. Foto Si prega di accettare i termini e le condizioni. Attenzione, nel frontend questo campo è richiesto. Se decidi di nasconderlo, assicurati di indicare il valore predefinito Per favore configura prima le 
<a href="?page=ab-settings&type=_google_calendar">impostazioni</a>
 di Google Calendar Per favore non dimenticare di specificare il codice d'acquisto nelle <a href="admin.php?page=ab-settings">impostazioni</a> di Bookly. Una volta inserito il codice avrai accesso agli aggiornamenti gratuiti di Bookly. Gli aggiornamenti possono contenere nuove funzionalità e importanti aggiornamenti per la sicurezza. Inserisci vecchia password. Per favore seleziona un cliente Per favore seleziona un servizio Per favore seleziona almeno un coupon. Per favore seleziona almeno un servizio. Per favore seleziona un servizio: Per favore indica come intendi pagare: Mese precedente Costo Listino prezzi Profilo Categoria Codice d'acquisto Acquisti In coda Ricerca veloce clienti Casella di scelta Gruppo di caselle di scelta Codice di recupero è scaduto. Redirect URI Registrare Registrazione Ricorda la mia scelta Rimuovi cliente Rimuovi il campo Rimuovi elemento Ripeti ogni anno Ripetere la nuova password Ripeti la password Richiesto Campo richiesto Reset SMS Dettagli Avvisi via SMS Notifiche SMS (o "Bookly SMS") è un servizio per la notifica ai clienti tramite messaggi di testo che vengono inviati ai telefoni cellulari.<br/>E 'necessario registrarsi per iniziare a utilizzare questo servizio.<br/>Dopo la registrazione sarà necessario configurare i messaggi di notifica e ricaricare il vostro equilibrio, al fine di iniziare a inviare SMS. SMS è stato inviato correttamente. Sandbox Mode Salva Salva i cambiamenti Salva nello staff Salva intervallo Salva la categoria Pianifica Secret Key Seleziona un progetto, o creane uno nuovo. Seleziona una categoria Seleziona un file Seleziona da utenti WP Seleziona un prodotto Seleziona un servizio Seleziona il prodotto che hai creato al passo 1 dalla lista dei prodotti. Seleziona l'intervallo di tempo da utilizzare nel frontend e nel backend, ad es. nel calendario, secondo step del processo di prenotazione o nelle impostazioni degli orari lavorativi. Selezionato / massimo Punto e virgola (;) Manda una copia agli amministratori Invia le eMail di notifica Invia SMS di prova eMail del mittente Nome del mittente Invio Inviati Servizio Servizio pagato il loco Servizi Errore di sessione. Inserisci l'URL della pagina a cui verranno reindirizzati i clienti alla fine del processo di prenotazione. Se disabilitato, i clienti visualizzeranno lo step finale (5). Imposta un minimo intervallo di tempo prima della scelta dell'appuntamento (da esempio, si richiede che il cliente prenoti almeno con un giorno di anticipo). Impostazioni Impostazioni salvate. Mostra le fasce orarie occupate Mostra il calendario Mostra ogni giorno in una colonna Mostra la barra di progresso nel form Loggato Specifica il numero di giorni disponibili a partire dalla data odierna entro cui fissare l'appuntamento. Staff Membro dello Staff Membri dello Staff A partire dalle L'ora di inizio non può essere vuota Stato Soggeto Sincronizza i dati delle prenotazioni dello staff con Google Calendar. Modello per titolo dell'evento Termini e Condizioni Testo Area di testo Campo di testo Grazie! La tua prenotazione è stata completata. Ti abbiamo inviato una eMail con i dettagli del tuo appuntamento. L'ID del Calendario può essere visualizzato cliccando su 
"Calendar settings" sul calendario che vuoi visualizzare. L'ID del Calendario è visibile alla voce
 "Calendar Address". Il 
Client ID ottenuto dalla Developer Console Il 
Client secret ottenuto dalla Developer Console Il numero massimo di clienti che possono prenotare il servizio in una data fascia oraria. Il numero dei clienti dovrebbe essere non maggiore di Il costo di questo servizio è di [[SERVICE_PRICE]]. L'intervallo richiesto non è disponibile Il periodo selezionato non corrisponde alla durata predefinita del servizio scelto Il periodo selezionato è occupato da un altro appuntamento L'orario iniziale deve essere precedente l'orario finale Il valore viene recuperato dal browser del cliente. Questo mese Orario dimensione fascia oraria Titolo Titoli A Per trovare il tuo Client ID e il tuo Client Secret, procedi come segue: Per inviare i messaggi di notifica esegui il seguente script con in cron del tuo server: Oggi Totale appuntamenti Totale: Tipo URL per rimozione appuntamenti (da usare all'interno di un tag <a>) Senza categoria Non consegnato Senza titolo Aggiorna Aggiorna le impostazioni dei servizi Una volta inserito il codice d'acquisto avrai accesso agli aggiornamenti gratuiti di Bookly. Gli aggiornamenti possono contenere nuove funzionalità e importanti aggiornamenti per la sicurezza. Per maggiori informazioni su dove trovare il tuo codice d'acquisto, visita questa
 <a href="https://help.market.envato.com/hc/en-us/articles/202822600-Where-can-I-find-my-Purchase-Code-" target="_blank">pagina</a>. Limite di utilizzo Utente Utente non trovato. L'utente con i permessi di "Amministratore" ha accesso a tutti i calendari e alle impostazioni di tutti i membri dello staff, gli utenti con altri permessi avranno accesso solo ai calendari personali e alle proprie impostazioni.  Questo giorno siamo chiusi Sito internet Settimana Con method "Enqueue" i file JavaScript e CSS di Bookly saranno inclusi in tutte le pagine del tuo sito web. Questo metodo dovrebbe funzionare con tutti i temi. Con "Print" metodo i file verranno inclusi solo sulle pagine che contengono modulo di prenotazione Bookly. Questo metodo potrebbe non funzionare con tutti i temi. WooCommerce Si Ieri Stai per modificare un servizio che è configurabile separatamente per ogni membro dello staff. Vuoi aggiornarlo anche nelle impostazioni dello staff? Sta per cancellare un cliente che potrebbe avere associato un account WordPress. Vuoi rimuovere anche questo account (se ne esiste uno)? Puoi importare una lista clienti in formato CSV. Il file deve contenere tre colonne: Nome, Telefono e eMail. Devi installare ed attivare WooCommerce prima di utilizzare le opzioni seguenti.<br/><br/>Una volta attivato il plugin segui questi passi: Hai selezionato un appuntamento per  [[SERVICE_NAME]] con [[STAFF_NAME]] presso [[SERVICE_TIME]] il giorno [[SERVICE_DATE]]. Il costo per il servizio è di [[SERVICE_PRICE]].
Per favore inserisci i tuoi dettagli nel form sottostante per procedere con la prenotazione. La tua agenda per [[TOMORROW_DATE]] Il tuo appuntamento presso [[COMPANY_NAME]] Le informazioni del tuo appuntamento Il saldo I vostri clienti devono avere i loro numeri di telefono in formato internazionale, al fine di ricevere messaggi di testo. Tuttavia è possibile specificare un codice di paese di default che verrà utilizzato come prefisso per tutti i numeri di telefono che non cominciano con "+" o "00". Ad es se si inserisce "1", come il codice del paese di default e un cliente entra nel loro telefono cellulare come "(600) 555-2222" il numero di telefono risultante per inviare l'SMS al sarà "+1600555222". Il pagamento è stato accettato per l'elaborazione. Il pagamento è stato interrotto. La tua visita presso [[COMPANY_NAME]] + intervallo indirizzo intervalli: link per cancellazione appuntamento caratteri max caratteri min valori combinati di tutti i campi personalizzati valori combinati di tutti i campi personalizzati (formattati in 2 colonne) nuova password del cliente nuova username del cliente data dell'appuntamento data del giorno dopo disconnetti eMail del cliente eMail è troppo lungo è troppo corto nome della categoria nome del cliente nome del servizio nome nome della tua azienda numero di persone telefono del cliente telefono foto costo del servizio sito internet agenda dello staff del giorno dopo membri dello staff sito internet orario dell'appuntamento alle prezzo totale della prenotazione (costo del servizio per il numero di persone) logo aziendale telefono 