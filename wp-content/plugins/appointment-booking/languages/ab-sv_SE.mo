��    x     �  �  �      x     y     �     �     �     �     �     �     �     �                 :   
   V   /   a   .   �      �      �      �   
   !  @   !  ^   Y!  1   �!  Q   �!  Y   <"  ]   �"  V   �"  R   K#  b   �#  V   $     X$     e$     r$     �$     �$     �$     �$  
   �$     �$     �$     �$     %     %     "%     0%  	   =%     G%     N%  
   R%     ]%     c%     o%     �%     �%     �%     �%  �   �%     /&     <&     Q&  C   a&     �&  @  �&     �'     �'     
(     $(     +(     G(     P(     c(     l(     u(  %   �(  	   �(     �(     �(     �(     �(  	   �(     �(     �(     �(     )  	   	)     )     )  +   ")     N)    ^)     q*     �*     �*     �*     �*     �*  	   �*     �*     �*     �*  �   �*    �+    �,  �   �-    �.  �   �/  !   �0  !   �0      1     $1     +1     @1     P1  	   i1     s1     {1     �1     �1  2   �1     �1  	   �1     �1     �1     �1     
2     2     2     !2  (   <2     e2     q2  :   2     �2  S   �2  M   .3     |3     �3     �3     �3     �3  	   �3  I   �3     4  	   4     4  i   #4     �4     �4  �   �4  �   C5  �   !6  9   �6  �   %7  �   �7  �   �8  7   Q9     �9     �9     �9     �9     �9     �9      :  �   :  �   �:  �   �;  {   4<  t   �<     %=  �   ,=  6   �=     >     >  X   .>     �>     �>     �>     �>     �>  
   �>     �>     �>     ?  m   ?     �?  )   �?     �?     �?     �?     �?     �?     �?     
@     @  
   @  
   '@     2@     B@     X@     i@  -   v@  	   �@  '   �@  +   �@     A      A     @A     EA     KA  0   \A  2   �A  A   �A  ;   B  6   >B     uB  $   �B     �B     �B     �B     �B     �B     �B     �B     �B     �B     C  #   C  �   /C  e   �C    /D     4E     ME  "   eE  #   �E     �E  *   �E     �E     �E     F     F     F     1F     >F     QF     ^F     qF     �F     �F     �F     �F     �F     �F     �F     �F     �F     �F  
   �F      G     G  
   G  &   "G     IG     YG     eG     zG     �G  �   �G     ;H     NH     \H     xH     �H     �H     �H     �H     �H  �   �H  �   SI     �I     �I      J     J     %J     AJ  	   \J  h   fJ     �J     �J     �J  
   �J     �J     K  G    K  	   hK  
   rK  `   }K  �   �K  2   �L  6   �L  X   �L  0   DM  /   uM  '   �M  J   �M  6   N  ,   ON  +   |N  
   �N     �N     �N     �N     �N     �N  ;   �N  Z   O     pO     vO     �O     �O  7   �O     �O     �O     �O     �O  a  P     eQ  �   jQ     R     <R     DR     IR  	   MR  �   WR  �   �R  j   �S  �   �S  !   �T  $   �T     U     -U  	   LU     VU     nU     vU     �U     �U  $   �U  =   �U     V     %V     ;V     OV  
   `V     kV     {V     �V     �V     �V     �V     �V     �V     �V     �V     W     W     'W     6W     GW     TW     nW     |W     �W     �W  J   �W     �W     X  M  X     gZ     |Z     �Z  $   �Z     �Z     �Z     �Z     �Z     �Z     �Z     
[     [     6[  -   E[  0   s[     �[     �[     �[     �[  J   �[  ]   J\  /   �\  X   �\  [   1]  e   �]  Y   �]  Y   M^  S   �^  \   �^     X_     e_     s_     �_     �_  "   �_     �_     �_     �_     `     `     %`     1`     @`     S`     b`     s`     y`     �`     �`     �`     �`     �`     �`  
   �`     �`  �   �`     qa     ~a     �a  ?   �a     �a  y  �a     ec     nc     zc     �c  :   �c  	   �c     �c     �c  	   �c     	d  5   d     Qd     Yd     gd     kd     qd  
   zd     �d     �d     �d     �d     �d     �d     �d  *   �d     �d    e     f     0f     7f     Gf     Xf     ]f     jf     qf     wf  	   {f  �   �f  �   Tg  �   Lh  �   Ai  �   j  �   k  $   �k  '   l  !   @l     bl     jl     �l     �l     �l     �l     �l     �l     �l  7   �l  	   m     m     *m     6m     Em     `m     gm     pm     ym  +   �m     �m     �m  B   �m     n  _   <n  -   �n     �n  
   �n     �n     �n     �n     o  X   
o     co     io     |o  c   �o     �o     �o  �   p  �   �p  �   eq  4   :r  �   or  �   �r  �   �s  2   �t     �t     �t     �t     �t     u     u  $   .u  �   Su  �   v  �   w  u   �w  n   $x  	   �x  �   �x  5   Ay     wy     ~y  V   �y     �y     �y     z     z     .z     >z     Nz  %   `z     �z  p   �z  
   �z     {     '{     ,{     8{     @{  
   O{     Z{     p{     x{     {     �{     �{     �{     �{     �{  0   �{     |  3   |  2   Q|     �|  $   �|  	   �|     �|     �|     �|     }  7   }     P}  '   p}     �}     �}     �}     �}  
   �}  	   �}  	   �}     �}  
   �}     �}     ~     ~     #~  �   B~  g   �~    L     U�     c�     t�     ��     ��  $   ��     �     ��  	   ��  
   �     �  
    �     +�  
   <�     G�     W�     d�     r�     z�     ��     ��     ��     ��     Ɂ     ρ     ��  
   �     ��     �     �  %   �     B�     Q�     ^�     x�     ��  �   ��  
   6�     A�  %   O�     u�     ��     ��     ��     ��  	   ǃ  �   у  �   ]�     ބ     �     	�     '�     5�     P�     k�  W   w�     υ     ۅ     �     �     ��     �  B   �     `�  	   i�  Z   s�  �   Ά  ,   }�  6   ��  E   �  '   '�  ,   O�  .   |�  P   ��  3   ��  ;   0�  (   l�     ��     ��  
   ��     ��     ��     ��  A   É  U   �     [�     `�     q�     y�  F   }�     Ċ  	   ӊ  	   ݊     �  y  �  
   ��  �   ��     s�  	   ��     ��     ��     ��  �   ��  �   J�  f   ю  �   8�  !   #�  %   E�     k�  !   ��  
   ��     ��  
   ɐ     Ԑ  
   �  
   ��  (   �  B   *�     m�     ��     ��     ��     ��     ϑ     ۑ     �     ��     �     �     #�     4�     F�     ]�     l�     x�     ��     ��     ��  %   ��     �     �     �     "�  P   '�     x�     ��     T   "  1  �   �   �   )   �   N   6   D  �   
    �   ;  
       (  +  �      �       �   |       E  >  �           b  Q   �                 �       {   7  Z  ^     ^   Y       �         �   �   a       �   �           k  G   g  \    �   �   f   �       �   5         �   �       L   �   P   z              �   w       8   t               ,   �   �     �      !   �   �   y   �   I   P         =  �   R  ~   O      �   �        ;   �               �       j  I  t      [  w   �       	  �   �   �          %   i  F  �       o           v   �   �   �       x   �   1   �   f  �   Y  �       �           �   /                   [   A       �   �   u   7   #         e  �   v      ?      <   k   �   �   T          �            �       p   �       �   �          �          �   3  �   "   %  0  Z   g       x       )      �           �         �   �   G  M   �   �                �   (   �   >       -            4  <    W   p  �   �   �   W          r  .      �   H   \   q       L  d         �       �   �   }   e           &       �                 B   2              m     O   -   �      8  �           X   _  �   S  J  �   �   S   �   �   H              �   �   �   c  �       �   `       �   3      r   =   ]   l   o  h   K  K   /     @  A  ,  	   �   n       ?   i   :  l  *       �                   �           &    U   �   �   �       !  �             �   �                   u  �   �           5  �      ]  D   �   9       �           .   9      �       '   a               '      :   0   �           m       C           +   $  V    X  `  #  N  2      U  4   M  �       C   �   V   �   �       �   *      �       6  E      $       �   �   �       s      �          d       _   J               c           B  h                  �   @   F   R   j   �   �   �   s   Q  q  �           �   n   �           �   b   �       �     No staff selected  Reset   to  "I'm available on …" block %d h %d min * Invalid email * Invalid phone number * Please select a service * Please tell us your email * Please tell us your name * Please tell us your phone * Required * The start time must be less than the end time * This coupon code is invalid or has been used * This email is already in use -- Search customers -- -- Select a service -- 2 way sync <b>Leave this field empty</b> to work with the default calendar. <b>[[CATEGORY_NAME]]</b> - name of category, <b>[[NUMBER_OF_PERSONS]]</b> - number of persons. <b>[[NUMBER_OF_PERSONS]]</b> - number of persons. <b>[[SERVICE_NAME]]</b> - name of service, <b>[[STAFF_NAME]]</b> - name of staff, <b>[[SERVICE_PRICE]]</b> - price of service, <b>[[CATEGORY_NAME]]</b> - name of category, <b>[[SERVICE_PRICE]]</b> - price of service, <b>[[TOTAL_PRICE]]</b> - total price of booking, <b>[[SERVICE_TIME]]</b> - time of service,  <b>[[SERVICE_DATE]]</b> - date of service, <b>[[STAFF_NAME]]</b> - name of staff,  <b>[[SERVICE_NAME]]</b> - name of service, <b>[[TOTAL_PRICE]]</b> - total price of booking, <b>[[NUMBER_OF_PERSONS]]</b> - number of persons. <h3>The selected time is not available anymore. Please, choose another time slot.</h3> API Login ID API Password API Signature API Transaction Key API Username Add Bookly appointments list Add Bookly booking form Add Coupon Add Service Address All Services All customers All payment types All providers All services All staff Amount Any Appearance Apply Appointment Appointment Date Appointments Are you sure? Avatar Back Below you can find a list of available time slots for [[SERVICE_NAME]] by [[STAFF_NAME]].
Click on a time slot to proceed with booking. Booking Time Booking cancellation Booking product Bookly: You do not have sufficient permissions to access this page. Business hours By default Bookly pushes new appointments and any further changes to Google Calendar. If you enable this option then Bookly will fetch events from Google Calendar and remove corresponding time slots before displaying the second step of the booking form (this may lead to a delay when users click Next at the first step). Calendar Calendar ID Calendar ID is not valid. Cancel Cancel appointment page URL Capacity Card Security Code Category Checkbox Checkbox Group Click on the underlined text to edit. Client ID Client secret Code Codes Columns Comma (,) Company Company logo Company name Connect Connected Coupon Coupons Create WordPress user account for customers Create customer Create your project's OAuth 2.0 credentials by clicking <b>Create new Client ID</b>, selecting <b>Web application</b>, and providing the information needed to create the credentials. For <b>AUTHORIZED REDIRECT URIS</b> enter the <b>Redirect URI</b> found below on this page. Credit Card Number Currency Custom Fields Custom Range Customer Customer Name Customers Date Day Days off Dear [[CLIENT_NAME]].

Thank you for choosing [[COMPANY_NAME]]. We hope you were satisfied with your [[SERVICE_NAME]].

Thank you and we look forward to seeing you again soon.

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Dear [[CLIENT_NAME]].

This is confirmation that you have booked [[SERVICE_NAME]].

We are waiting you at [[COMPANY_ADDRESS]] on [[APPOINTMENT_DATE]] at [[APPOINTMENT_TIME]].

Thank you for choosing our company.

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Dear [[CLIENT_NAME]].

We would like to remind you that you have booked [[SERVICE_NAME]] tomorrow on [[APPOINTMENT_TIME]]. We are waiting you at [[COMPANY_ADDRESS]].

Thank you for choosing our company.

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Dear [[CLIENT_NAME]].
Thank you for choosing [[COMPANY_NAME]]. We hope you were satisfied with your [[SERVICE_NAME]].
Thank you and we look forward to seeing you again soon.
[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Dear [[CLIENT_NAME]].
This is confirmation that you have booked [[SERVICE_NAME]].
We are waiting you at [[COMPANY_ADDRESS]] on [[APPOINTMENT_DATE]] at [[APPOINTMENT_TIME]].
Thank you for choosing our company.
[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Dear [[CLIENT_NAME]].
We would like to remind you that you have booked [[SERVICE_NAME]] tomorrow on [[APPOINTMENT_TIME]]. We are waiting you at [[COMPANY_ADDRESS]].
Thank you for choosing our company.
[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Default value for category select Default value for employee select Default value for service select Delete Delete current photo Delete customer Delete this staff member Delimiter Details Disabled Discount (%) Dismiss Display available time slots in client's time zone Done Drop Down Duration Edit appointment Edit booking details Email Employee Enabled End time must not be empty End time must not be equal to start time Enter a URL Enter a label Enter this URL as a redirect URI in the Developers Console Error adding the break interval Evening notification with the next day agenda to staff member (requires cron setup) Evening reminder to customer about next day appointment (requires cron setup) Expiration Date Expired Export to CSV Filter Final step URL Finish by Follow-up message in the same day after appointment (requires cron setup) From Full name General Go to the <a href="https://console.developers.google.com/" target="_blank">Google Developers Console</a>. Google Calendar Google Calendar integration Hello.

An account was created for you at [[SITE_ADDRESS]]

Your user details:
user: [[NEW_USERNAME]]
password: [[NEW_PASSWORD]]

Thanks. Hello.

The following booking has been cancelled.

Service: [[SERVICE_NAME]]
Date: [[APPOINTMENT_DATE]]
Time: [[APPOINTMENT_TIME]]
Client name: [[CLIENT_NAME]]
Client phone: [[CLIENT_PHONE]]
Client email: [[CLIENT_EMAIL]] Hello.

You have new booking.

Service: [[SERVICE_NAME]]
Date: [[APPOINTMENT_DATE]]
Time: [[APPOINTMENT_TIME]]
Client name: [[CLIENT_NAME]]
Client phone: [[CLIENT_PHONE]]
Client email: [[CLIENT_EMAIL]] Hello.

Your agenda for tomorrow is:

[[NEXT_DAY_AGENDA]] Hello.
An account was created for you at [[SITE_ADDRESS]]
Your user details:
user: [[NEW_USERNAME]]
password: [[NEW_PASSWORD]]

Thanks. Hello.
The following booking has been cancelled.
Service: [[SERVICE_NAME]]
Date: [[APPOINTMENT_DATE]]
Time: [[APPOINTMENT_TIME]]
Client name: [[CLIENT_NAME]]
Client phone: [[CLIENT_PHONE]]
Client email: [[CLIENT_EMAIL]] Hello.
You have new booking.
Service: [[SERVICE_NAME]]
Date: [[APPOINTMENT_DATE]]
Time: [[APPOINTMENT_TIME]]
Client name: [[CLIENT_NAME]]
Client phone: [[CLIENT_PHONE]]
Client email: [[CLIENT_EMAIL]] Hello.
Your agenda for tomorrow is:
[[NEXT_DAY_AGENDA]] Hide this block Hide this field Holidays I will pay locally I will pay now with Credit Card I will pay now with PayPal I'm available on or after If email or SMS notifications are enabled and you want the customer or the staff member to be notified about this appointment after saving, tick this checkbox before clicking Save. If there is a lot of events in Google Calendar sometimes this leads to a lack of memory in PHP when Bookly tries to fetch all events. You can limit the number of fetched events here. This only works when 2 way sync is enabled. If this staff member requires separate login to access personal calendar, a regular WP user needs to be created for this purpose. If you do not see your country in the list please contact us at <a href="mailto:support@ladela.com">support@ladela.com</a>. If you will leave this field blank, this staff member will not be able to access personal calendar using WP backend. Import In the sidebar on the left, expand <b>APIs & auth</b>. Next, click <b>APIs</b>. In the list of APIs, make sure the status is <b>ON</b> for the Google Calendar API. In the sidebar on the left, select <b>Credentials</b>. Insert Insert Appointment Booking Form Insert a URL of a page that is shown to clients after they have cancelled their booking. Instructions Invalid date or time Invalid number Last 30 Days Last 7 Days Last Month Last appointment Limit number of fetched events Local Look for the <b>Client ID</b> and <b>Client secret</b> in the table associated with each of your credentials. Message Minimum time requirement prior to booking Name New Category New Customer New Staff Member New appointment New booking information New customer Next Next Month Next month No appointments No appointments found No coupons found No customers No payments for selected period and criteria. No result No services found. Please add services. No time is available for selected criteria. No, delete just the customer No, update just here in services Note Notes Notes (optional) Notification settings were updated successfully. Notification to customer about appointment details Notification to customer about their WordPress user login details Notification to staff member about appointment cancellation Notification to staff member about appointment details Notifications Number of days available for booking Number of persons OFF Option Participants Payment Payments Period Personal Information Phone Photo Please accept terms and conditions. Please be aware that a value in this field is required in the frontend. If you choose to hide this field, please be sure to select a default value for it Please configure Google Calendar <a href="?page=ab-settings&type=_google_calendar">settings</a> first Please do not forget to specify your purchase code in Bookly <a href="admin.php?page=ab-settings">settings</a>. Upon providing the code you will have access to free updates of Bookly. Updates may contain functionality improvements and important security fixes. Please select a customer Please select a service Please select at least one coupon. Please select at least one service. Please select service:  Please tell us how you would like to pay:  Previous month Price Provider Purchase Code Quick search customer Radio Button Radio Button Group Redirect URI Remember my choice Remove customer Remove field Remove item Repeat every year Required Required field Reset Sandbox Mode Save Save Changes Save Member Save break Save category Schedule Secret Key Select a project, or create a new one. Select category Select file Select from WP users Select product Select service Select the time interval that will be used in frontend and backend, e.g. in calendar, second step of the booking process, while indicating the working hours, etc. Selected / maximum Semicolon (;) Send copy to administrators Send email notifications Sender email Sender name Service Service paid locally Services Set a URL of a page that the user will be forwarded to after successful booking. If disabled then the default step 5 is displayed. Set a minimum amount of time before the chosen appointment (for example, require the customer to book at least 1 hour before the appointment time). Settings Settings saved. Show blocked timeslots Show calendar Show each day in one column Show form progress tracker Signed up Specify the number of days that should be available for booking at step 2 starting from the current day. Staff Staff Member Staff Members Start from Start time must not be empty Subject Synchronize the data of the staff member bookings with Google Calendar. Text Area Text Field Thank you! Your booking is complete. An email with details of your booking has been sent to you. The Calendar ID can be found by clicking on "Calendar settings" next to the calendar you wish to display. The Calendar ID is then shown beside "Calendar Address". The client ID obtained from the Developers Console The client secret obtained from the Developers Console The maximum number of customers allowed to book the service for the certain time period. The number of customers should be not more than  The price for the service is [[SERVICE_PRICE]]. The requested interval is not available The selected period does't match default duration for the selected service The selected period is occupied by another appointment The start time must be less than the end one The value is taken from client’s browser. This Month Time Time slot length Title Titles To To find your client ID and client secret, do the following: To send scheduled notifications please execute the following script hourly with your cron: Today Total appointments Total:  Type URL for cancel appointment link (to use inside <a> tag) Uncategorized Untitled Update Update service setting Upon providing the purchase code you will have access to free updates of Bookly. Updates may contain functionality improvements and important security fixes. For more information on where to find your purchase code see this <a href="https://help.market.envato.com/hc/en-us/articles/202822600-Where-can-I-find-my-Purchase-Code-" target="_blank">page</a>. User User with "Administrator" role will have access to calendars and settings of all staff members, user with some other role will have access only to personal calendar and settings. We are not working on this day Website Week Yes Yesterday You are about to change a service setting which is also configured separately for each staff member. Do you want to update it in staff settings too? You are about to delete a customer which may have a WordPress account associated to them. Do you want to delete that account too (if there is one)? You may import list of clients in CSV format. The file needs to have three columns: Name, Phone and Email. You selected a booking for [[SERVICE_NAME]] by [[STAFF_NAME]] at [[SERVICE_TIME]] on [[SERVICE_DATE]]. The price for the service is [[SERVICE_PRICE]].
Please provide your details in the form below to proceed with booking. Your agenda for [[TOMORROW_DATE]] Your appointment at [[COMPANY_NAME]] Your appointment information Your visit to [[COMPANY_NAME]] add break address of your company breaks: cancel appointment link characters max characters min combined values of all custom fields combined values of all custom fields (formatted in 2 columns) customer new password customer new username date of appointment date of next day disconnect email of client email of staff is too long is too short name of category name of client name of service name of staff name of your company number of persons phone of client phone of staff photo of staff price of service site address staff agenda for next day staff members this web-site address time of appointment to total price of booking (service price multiplied by the number of persons) your company logo your company phone Project-Id-Version: Bookly v7.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-06-04 14:52+0200
PO-Revision-Date: 2015-06-04 14:52+0200
Last-Translator: 
Language-Team: 
Language: sv_SE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.6.7
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: .
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: ..
 Ingen personal valts Återställ till Jag är tillgänglig på eller efter %d h %d min Ogiltig e-post * Ogiltigt telefonnummer * Välj en tjänst * Berätta din e * Berätta ditt namn * Berätta din telefon *Obligatoriskt * Starttiden måste vara mindre än sluttiden * Denna kupongkod är ogiltig eller har använts Denna e-post är redan i bruk. - Sök kunder - - Välj en tjänst - 2 vägs synkronisering <b>Lämna det här fältet tomt</b> för att arbeta med standardkalendern. <b>[[CATEGORY_NAME]]</b> - namn på kategori, <b>[[NUMBER_OF_PERSONS]]</b> - antal besökare. <b>[[NUMBER_OF_PERSONS]]</b> - antal besökare. <b>[[SERVICE_NAME]]</b> - namn på tjänst, <b>[[STAFF_NAME]]</b> - namn på anställda, <b>[[SERVICE_PRICE]]</b> - pris för tjänst, <b>[[CATEGORY_NAME]]</b> - namn på kategori, <b>[[SERVICE_PRICE]]</b> - total summa på tjänst, <b>[[TOTAL_PRICE]]</b> - total summa på bokning, <b>[[SERVICE_TIME]]</b> - tid på tjänst,  <b>[[SERVICE_DATE]]</b> - datum för tjänst, <b>[[STAFF_NAME]]</b> - namn på anställda,  <b>[[SERVICE_NAME]]</b> - namn på tjänst, <b>[[TOTAL_PRICE]]</b> - total summa, <b>[[NUMBER_OF_PERSONS]]</b> - antal gäster. <h3> Den valda tiden är inte tillgänglig längre. Snälla, välj en annan tidslucka. </h3> API Login ID API-lösenord API-signatur API Transaktion Key API-användarnamn Lägg Bookly tillsättningar lista Lägg Bookly bokningsformulär Lägg till Kupong - Välj en tjänst - Adress Alla tjänster Alla kunder Betaltjänster Alla leverantörer Alla tjänster Alla Medarbetare Summa Valfri text Utseende Utför Möte Utnämning Datum Möte Är du säker? Profilbild Tillbaka Nedan hittar du en lista över tillgängliga tidsluckor för [[SERVICE_NAME]] av [[STAFF_NAME]].
Klicka på en tidslucka för att fortsätta med bokningen. Boknings Tid Bokning annullering Boknings Tid Du har inte rättigheter för att få åtkomst till denna sida. Öppettider Som standard Bookly skjuter nya möten och eventuella ytterligare ändringar av Google Kalender. Om du aktiverar det här alternativet så Bookly hämtar händelser från Google Kalender och ta bort motsvarande tidsluckor innan du visar det andra steget av bokningsformuläret (detta kan leda till en fördröjning när användare klickar du på Nästa vid det första steget). Kalender Kalender-ID Kalender-ID är inte giltigt. Avbryt Är du säker på att du vill ställa in markerade möten? Kapacitet CSV säkerhetskod Kategori Kryssruta Kryssruta gruppen Klicka på den understrukna texten för att redigera. Kund-id Kundhemlighet Kod Koder Kolumner +Komma(,). Företag Logga Företagens namn. Anslut Ansluten Rabattkupong Rabattkuponger Skapa Wordpress användarkonto för kunder Skapa ny kund Skapa din projektets OAuth 2.0 referenser genom att klicka på <b>Skapa ny klient-ID,</b> välja <b>webbprogram,</b> och ge den information som behövs för att skapa referenser. <b>För</b> behöriga <b>REDIRECT Uris</b> ange <b>Redirect URI</b> finns nedan på denna sida. Kreditkortsnummer Valuta Anpassade fält Anpassat område Kund Kundens namn Kunder Datum Dag ledig dag Bäste [[CLIENT_NAME]].

Tack för att ni har valt [[COMPANY_NAME]]. Vi hoppas ni är nöjda med er [[SERVICE_NAME]].

Tack, Hoppas vi ses snart igen.

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Kära [[CLIENT_NAME]].

Den är en bekräftelse på att du har bokat [[SERVICE_NAME]].

Bokad tid:  [[APPOINTMENT_DATE]] på [[APPOINTMENT_TIME]].

Tack för att du väljer vårt företag.

 [[COMPANY_NAME]]
 [[COMPANY_PHONE]]
 [[COMPANY_WEBSITE]] Bäste [[CLIENT_NAME]].

Vi vill påminna dig om att du har bokat [[SERVICE_NAME]] imorgon [[APPOINTMENT_TIME]]. Vi välkomnar dig till [[COMPANY_ADDRESS]].

Tack för att du har valt oss.

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Bäste [[CLIENT_NAME]].
Tack för att ni har valt [[COMPANY_NAME]]. Vi hoppas ni är nöjda med er [[SERVICE_NAME]].
Tack, Hoppas vi ses snart igen.
[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Kära [[CLIENT_NAME]].
Den är en bekräftelse på att du har bokat [[SERVICE_NAME]].
Bokad tid:  [[APPOINTMENT_DATE]] på [[APPOINTMENT_TIME]].
Tack för att du väljer vårt företag.
 [[COMPANY_NAME]]
 [[COMPANY_PHONE]]
 [[COMPANY_WEBSITE]] Bäste [[CLIENT_NAME]].
Vi vill påminna dig om att du har bokat [[SERVICE_NAME]] imorgon [[APPOINTMENT_TIME]]. Vi välkomnar dig till [[COMPANY_ADDRESS]].
Tack för att du har valt oss.
[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Standardvärde för kategori väljer Standardvärde för medarbetarnas välj Standardvärde för service välj Ta bort Radera nuvarande bild Radera kund Ta bort den här anställd Avgränsade Detaljer Inaktiverad Rabatt Avfärda Återstående tillgängliga ankomst- och avgångstider. Färdigt! Rullgardinsmeny Varaktighet Ändra bokning Redigera boknings detaljer E-post Personal Aktivera Sluttiden får inte vara tom Sluttiden får inte vara samma som starttid Ange URL Ange en etikett Ange denna webbadress som en omdirigering URI i Developers Console Fel lägga pausen intervallet Kvälls anmälan med nästa dag dagordningen till personal medlem (kräver cron inställningar) Event påminnelse för kunden om event kväll Utgångsdatum Förfallen Exportera till CSV Filter URL Slutför Uppföljning meddelande i samma dag efter överenskommelse (kräver cron inställningar) Från Fullständigt namn Allmän Gå till <a href="https://console.developers.google.com/" target="_blank">Google code Console</a> . Google Kalender Google Kalender integration Hello.

An account was created for you at [[SITE_ADDRESS]]

Your user details:
user: [[NEW_USERNAME]]
password: [[NEW_PASSWORD]]

Thanks. Hej

Du har ny bokning

Service:. [[SERVICE_NAME]]
Date: [[APPOINTMENT_DATE]]
 Tid: [[APPOINTMENT_TIME]]
Client namn: [[CLIENT_NAME]]
Client telefon: [ [CLIENT_PHONE]]
Client email: [[client_email]] Hej

Du har ny bokning

Tjänst:. [[SERVICE_NAME]] 
Datum: [[APPOINTMENT_DATE]]
 Tid: [[APPOINTMENT_TIME]]
Gästens namn: [[CLIENT_NAME]]
Gästens telefonnummer: [[CLIENT_PHONE]]
Gästens e-mail: [[CLIENT_EMAIL]] Hej.

Agendan för imorgon är:

[[NEXT_DAY_AGENDA]] Hello.
An account was created for you at [[SITE_ADDRESS]]
Your user details:
user: [[NEW_USERNAME]]
password: [[NEW_PASSWORD]]

Thanks. Hej
Du har ny bokning
Service:. [[SERVICE_NAME]]
Date: [[APPOINTMENT_DATE]]
 Tid: [[APPOINTMENT_TIME]]
Client namn: [[CLIENT_NAME]]
Client telefon: [ [CLIENT_PHONE]]
Client email: [[client_email]] Hej
Du har ny bokning
Tjänst:. [[SERVICE_NAME]] 
Datum: [[APPOINTMENT_DATE]]
Tid: [[APPOINTMENT_TIME]]
Gästens namn: [[CLIENT_NAME]]
Gästens telefonnummer: [[CLIENT_PHONE]]
Gästens e-mail: [[CLIENT_EMAIL]] Hej.
Agendan för imorgon är:
[[NEXT_DAY_AGENDA]] Göm detta block Göm detta fält Helgdag Jag kommer att betala på plats Betala med kort  Betala med PayPal Jag är tillgänglig på eller efter Om e-post eller SMS-meddelanden är aktiverade och du vill att kunden eller den anställde ska anmälas om denna utnämning efter att ha sparat, kryssa i kryssrutan innan du klickar på Spara. Om det finns en hel del händelser i Google Kalender ibland leder till bristande minne i PHP när Bookly försöker hämta alla händelser. Du kan begränsa antalet hämtade händelser här. Detta fungerar bara när 2 vägs synkronisering är aktiverad. Om detta anställd kräver separat inloggning för att komma åt personliga kalender, behöver en vanlig WP användare som ska skapas för detta ändamål. Om du inte ser ditt land i listan kan du kontakta oss på <a href="mailto:support@ladela.com">support@ladela.com</a>. Om du lämnar fältet tomt, kommer detta anställd inte att kunna komma åt personlig kalender med WP backend. Importera I sidofältet till vänster, expandera <b>API & auth.</b> Klicka sedan <b>API.</b> I listan över API, se till att statusen är <b>ON</b> för Google Kalender API. I sidofältet till vänster, välj <b>Referenser.</b> Infoga Infoga bokningsform Infoga en URL på en sida som visas för kunder efter att de har avbrutit sin bokning. Anvisningar Ogiltig datum eller tid Ogiltigt nummer Senaste 30 Dagar Senaste 7 Dagar Förra månaden Senast utnämning Begränsa antalet hämtade händelser Lokal Leta efter <b>klient-ID</b> och <b>klient hemlighet</b> i tabellen associerad med var och en av dina referenser. Meddelande Minsta tidskrav inför bokning Namn Ny kategori Ny kund Ny Medarbetare Ny bokning Ny boknings uppgifter Ny kund Nästa Nästa månad Nästa månad Inga bokningar Inga bokningar Inga kuponger hittades Inga kunder Inga betalningar för vald period och kriterier. Inget resultat. Inga tjänster hittades. Vänligen lägg tjänster. Ingen tid är tillgänglig för utvalda kriterier. Nej, ta bort bara kunden Nej, uppdatera bara här i tjänster Observera Anteckningar Anteckningar (valfritt) Notifiering uppdaterades! Kunddetaljer Meddela kunden om deras Wordpress  inloggningsuppgifter Meddela personalen om avbokning Meddela personalen om mötesinformation Notiser Antal tillgängliga Antal personer AV Alternativ Deltagare Betalning Betalningar Tidsperiod Personliga Uppgifter Mobiltelefon Foto Vänligen acceptera villkoren. Tänk på att det krävs ett värde i det här fältet i frontens. Om du väljer att dölja detta fält, vänligen se till att välja ett standardvärde för den Konfigurera Google Kalender <a href="?page=ab-settings&type=_google_calendar">inställningar</a> först Glöm inte att ange ditt köp kod i Bookly <a href="admin.php?page=ab-settings">inställningar</a> . Vid ge koden du kommer att ha tillgång till fria uppdateringar av Bookly. Uppdateringar kan innehålla funktionalitet förbättringar och viktiga säkerhetsfixar. Välj en kund Välj en tjänst Vänligen välj minst en fråga Välj minst en tjänst. Välj en tjänst Tala om för oss hur du vill betala: Föregående månad Pris Speditör Inköpskod - Sök kunder - Radioknapp Radioknapp Group Omdirigera Kom ihåg valet Ta bort kund Ta bort fält Ta bort Upprepa varje år Obligatoriskt Obligatoriska fält Återställ Sandbox-läget Spara Spara ändringar Spara medlem Spara paus Spara kategori Schema privat nyckel Välj ett projekt, eller skapa en ny. Välj kategori Välj en fil Välj från WP-användare Välj produkt Välj tjänst: Välja tidsintervallet som kommer att användas i frontens och backend, t.ex. i kalendern, andra steget i bokningsprocessen, samtidigt som anger arbetstid, etc. Vald / max Semikolon (;) Skicka en kopia till administratörer Skicka e-postmeddelanden E-post Avsändarnamn Tjänst Tjänsten betalas på plats Tjänster Ställ en URL på en sida som användaren kommer att vidarebefordras till efter lyckad bokning. Om inaktiverad sedan standard steg 5 visas. Ställ en minimal tid innan den valda utnämningen (t.ex. kräva att kunden för att boka minst 1 timme före utnämningen tid). Inställningar Inställningar har sparats. Visa otillgängliga tidluckor Visa kalender Visa varje dag i en kolumn Visa formulärets framsteg Blev medlem Du kan ange hur många steg som ska kunna ångras genom att välja ett värde i listan. Medarbetare Medarbetare Medarbetare Från Starttiden får inte vara tom Ämne Synkronisera data för de anställd bokningar med Google Kalender. Textarea Textfält Tack! Din bokning är klar. Ett mail med information om din bokning har skickats till dig. Kalendern ID kan hittas genom att klicka på &quot;Kalenderinställningar&quot; bredvid kalendern som du vill visa. Kalendern ID visas då bredvid &quot;kalenderadress&quot;. Klient ID erhålles från Developers Console Klient Hemligheten erhållits från Developers Console Det maximala antalet kunder får boka tjänsten för viss tidsperiod. Antalet besökare bör inte överskrida Priset för tjänsten är [[SERVICE_PRICE]]. Önskat urklippsformat är inte tillgängligt. Den valda perioden matchar inte den valda standardtiden för den valda tjänsten Den valda perioden är upptagen av en annan bokning Dragtiden måste vara mindre än eller lika med hålltiden. Värdet tas från klientens webbläsare. Denna månad Tid Tidslägnd Titel Rubrik Till För att hitta din klient-ID och klienthemlighet, gör följande: Om du vill skicka schemalagda meddelanden Utför följande skript timme med din cron: Idag Totala bokningar Totalt: Typ URL för avbryta utnämning länk (för att använda inne <a>tagg)</a> Okategoriserad Namnlöst Uppdatera Uppdater service Inställningar Vid tillhandahålla inköpskoden kommer du att ha tillgång till fria uppdateringar av Bookly. Uppdateringar kan innehålla funktionalitet förbättringar och viktiga säkerhetsfixar. För mer information om var du hittar ditt köp koden se denna <a href="https://help.market.envato.com/hc/en-us/articles/202822600-Where-can-I-find-my-Purchase-Code-" target="_blank">sida</a> . Användare Användare med &quot;Administratör&quot; roll kommer att ha tillgång till kalendrar och inställningar för alla anställda, kommer användaren med någon annan roll bara har tillgång till personlig kalender och inställningar. Vi arbetar inte på denna dag Webbplats Vecka Ja Igår Du är på väg att ändra en serviceinställning som också konfigureras separat för varje anställd. Vill du uppdatera den i personal inställningar också? Du är på väg att ta bort en kund som kan ha en Wordpress-konto kopplat till dem. Vill du ta bort kontot för (om det finns någon)? Du kan importera lista av kunder i CSV-format. Filen måste ha tre kolumner: Namn, telefon och e-post. Du valde en bokning för [[SERVICE_NAME]] av [[STAFF_NAME]] på [[SERVICE_TIME]] på [[SERVICE_DATE]]. Priset för tjänsten är [[SERVICE_PRICE]].
Vänligen lämna dina uppgifter i formuläret nedan för att gå vidare med bokningen. Din agenda för [[TOMORROW_DATE]] Din anställning hos [[COMPANY_NAME]] Din bokningsinformation Ditt besök till [[COMPANY_NAME]] lägg paus adressen till företaget Brytningar avbryta boknings länk tecken max tecken min kombinerade värden för alla egna fält kombinerade värden för alla egna fält (formaterad i 2 kolumner) kundens nya lösenord kund nya användarnamn utnämningsdagen datum för nästa dag Koppla ifrån kund e-mail personal e-mail är för lång är för kort namn på kategori kundnamn namn på tjänst namn på personal namn på ditt företag antal personer kund nummer telefon till personal foto av personal priset för tjänst site adress personalen dagordning för nästa dag Medarbetare denna webbplatsens adress utnämningstillfället till totala kostnaden för bokningen (servicepris multiplicerat med antalet personer) företagets logotyp företagets telefon 