<?php
#******************************************************************************
#                       Credit Card Payments Wordpress v3.0
#
#	Author: Convergine.com
#	http://www.convergine.com
#	Version: 3.0
#   (c) 2013 Convergine.com
#
#******************************************************************************

class elavon {

    var $field_string;
    var $fields = array();
    var $errorList = array();
    var $error = false;
    var $success = false;
    var $test_transaction = false;
    var $products_list_string;
    var $gateway_url = GATEWAY_URL;
    var $response_string;
    var $response = array();
    var $user_id = USER_ID;
    var $merchantID = MERCHANT_ID;
    var $pin = PIN;
    var $data = array();

    function __construct() {

        global $testAccount;


        //... test transactions?
        if ($testAccount) {
            $this->test_transaction = true;
        }
    }

    function add_field($field, $value) {

        // adds a field/value pair to the list of fields which is going to be 
        // passed to authorize.net.  For example: "x_version=3.1" would be one
        // field/value pair.  A list of the required and optional fields to pass
        // to the authorize.net payment gateway are listed in the AIM document
        // available in PDF form from www.authorize.net

        $this->fields["$field"] = $value;
    }

    function process() {



        $this->add_field('ssl_merchant_id', $this->merchantID);
        $this->add_field('ssl_user_id', $this->user_id);
        $this->add_field('ssl_pin', $this->pin);

        $this->add_field('ssl_show_form', 'False');
        $this->add_field('ssl_test_mode', $this->test_transaction);



        foreach ($this->fields as $key => $value) {
            $payment_request .= $key . '=' . urlencode(trim($value)) . "&";
        }
        $payment_request = rtrim($payment_request, "&");
        
        $request = $this->send_request($payment_request);

        return true;
    }

    function send_request($post_data) {

        

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->gateway_url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);

        $response_data = curl_exec($ch);

        curl_close($ch);

        
        //print "<pre>".htmlspecialchars($response_data)."</pre>";
        if ($response_data) {
            $this->process_response($response_data);
        } else {
            //
            $this->addError("Error");
        }
    }

    function process_response($response_data) {
        //print $response_data;
        //Explode the individual lines
        $tmp = explode("\n", $response_data);
        $response = array();
        foreach ($tmp as $value) {
            $value = trim($value);
            $itmp = explode("=", $value);
            $this->data[$itmp[0]] = $itmp[1];
        }
        //print "<pre>".print_r($this->data,true)."</pre>";;
        $this->data['errorCode']*=1;

		// Approved - Success!
		if ($this->data['ssl_result'] == '0') {
			$this->success=true;

			return True;
		}
		// Transaction Error
		elseif ($this->data['errorCode'] > '0') {
			
                        $this->addError($this->data['errorCode']." - ".$this->data['errorMessage']);
			return False;

		} 
		// Payment Declined
		else {
			$this->addError("Transaction declined from Elavon");
                        $this->addError($this->data['ssl_result_message']);
			return False;
		}
    }

    function addError($error) {

        $this->error = true;
        $this->errorList[] = $error;
    }

    function getError() {

        $errorStr = '';

        foreach ($this->errorList as $error) {

            $errorStr .= $error . "<br>";
        }
        return '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">' . $errorStr . '</div></div>';
    }

    function dump_response() {

        $str = '';
        foreach ($this->data as $name => $value) {
            $str .= $name . " : " . $value . "<br>";
        }
        return '<div class="ui-widget"><div class="ui-state-highlight ui-corner-all" style="padding: 0 .7em;">' . $str . '</div></div>';
    }

}

?>