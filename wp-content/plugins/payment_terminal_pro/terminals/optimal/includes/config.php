<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);

require (CCPT_HOME_DIR."/wp-load.php"); 
require("functions.php"); 
#******************************************************************************
#                       Credit Card Payments Wordpress v3.0
#
#	Author: Convergine.com
#	http://www.convergine.com
#	Version: 3.0
#   (c) 2013 Convergine.com
#
#******************************************************************************
$configProcessor=is_string(get_option('ccwp_details_OPTIMAL'))?unserialize(get_option('ccwp_details_OPTIMAL')):get_option('ccwp_details_OPTIMAL');

//THIS IS TITLE ON PAGES
$title = "OptimalPayments.com Payment Terminal v1.0"; //site title
$titleAboveForm = "OptimalPayments.com  Payment Terminal";
//THIS IS ADMIN EMAIL FOR NEW PAYMENT NOTIFICATIONS.
$admin_email = get_option('ccpt_admin_email'); //this email is for notifications about new payments
//Currency code which is set for your account on optimalpayments.com
define("PTP_CURRENCY_CODE",get_option('ccpt_paypal_currency')); 
//IF YOU NEED TO ADD MORE SERVICES JUST ADD THEM THE SAME WAY THEY APPEAR BELOW.
$show_services = get_option('ccpt_show_dd_text'); 
//NOW, IF YOU WANT TO ACTIVATE THE DROPDOWN WITH SERVICES ON THE TERMINAL
//ITSELF, CHANGE BELOW VARIABLE TO TRUE;			


//IF YOU'RE GOING LIVE FOLLOWING VARIABLE SHOULD BE SWITCH TO true
// IT WILL AUTOMATICALLY REDIRECT ALL NON-HTTTPS REQUESTS TO HTTPS.
// MAKE SURE SSL IS INSTALLED ALREADY.
$redirect_non_https =(get_option('ccpt_test')==2)? true : false;
if(get_option('ccpt_test')==2){
//OPTIMALPAYMENTS RELATED VARIABLES
$account=$configProcessor['live']['ACCOUNT'];//ACCOUNT NUMBER 
$merchantId=$configProcessor['live']['MERCHANT_ID']; //MERCHANT ID
$merchantPwd=$configProcessor['live']['MERCHANT_PASSWORD']; //MERCHANT PASSWORD
}else{
//OPTIMALPAYMENTS RELATED VARIABLES
$account=$configProcessor['test']['ACCOUNT'];//ACCOUNT NUMBER 
$merchantId=$configProcessor['test']['MERCHANT_ID']; //MERCHANT ID
$merchantPwd=$configProcessor['test']['MERCHANT_PASSWORD']; //MERCHANT PASSWORD
}
//DO NOT CHANGE ANYTHING BELOW THIS LINE, UNLESS SURE OF COURSE
$merchantRefNum=rand(10000, 99999); //DON'T EDIT.
if($redirect_non_https){
	if ($_SERVER['SERVER_PORT']!=443) {
		$sslport=443; //whatever your ssl port is
		$url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		header("Location: $url");
		exit();
	}
}
?>