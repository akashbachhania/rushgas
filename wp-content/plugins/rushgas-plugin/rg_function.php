<?php
// get include
 static $event_increase_id;
 $result = add_role(
    'staff',
    __( 'Staff Member' ),
    array(
        'read'         => true,  // true allows this capability
        'edit_posts'   => false,
        'delete_posts' => false, // Use false to explicitly deny
    )
);
 $event_increase_id =	1;
 $rg_user = wp_get_current_user();
  $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
  $c_admin_url= admin_url();
 //die;
 if(is_user_logged_in() && $c_admin_url==$actual_link && in_array('basic', $rg_user->roles) || is_user_logged_in() && $c_admin_url.'index.php'==$actual_link && in_array('basic', $rg_user->roles)){
 	wp_redirect(admin_url('admin.php?page=rg_dashboard_setup'));

 }
function rg_get_include($template, $data=false){	
	if(is_array($data)) extract($data);	
	ob_start();		
	@include($template);
	return ob_get_clean();
}
add_action('after_setup_theme','remove_core_updates');
function remove_core_updates()
{
//if(! current_user_can('update_core')){return;}
add_action('init', create_function('$a',"remove_action( 'init', 'wp_version_check' );"),2);
add_filter('pre_option_update_core','__return_null');
add_filter('pre_site_transient_update_core','__return_null');
}
add_filter( 'wp_nav_menu_items', 'maple_custom_menu_filter', 10, 2 );
function maple_custom_menu_filter( $items, $args ) {
    /**
     * If menu primary menu is set.
     */
    if ( $args->theme_location == 'toolbar-menu' && !is_user_logged_in() ) {        

        $home = '<li class="menu-item"><a href="' . esc_url( admin_url( '/' ) ) . '" title="'.esc_attr( get_bloginfo( 'name', 'display' ) ).'">Login</a></li>';
        $items = $home . $items;
    }

    return $items;
}
function rg_admin_enqueue() {
	   	wp_enqueue_script( 'jquery-core', RG_PLUGIN_ASSETS_URL.'js/jquery.min.js', false );
	    wp_enqueue_script( 'jquery-migrate.min.js', RG_PLUGIN_ASSETS_URL.'js/jquery-migrate.min.js', false );
	    wp_enqueue_script( 'bootstrap.min.js', RG_PLUGIN_ASSETS_URL.'js/bootstrap.min.js', false );
	    wp_enqueue_style( 'bootstrap.min.css', RG_PLUGIN_ASSETS_URL.'css/bootstrap.min.css', false );
	}
if('rg_history_setup' == $_GET['page'])	 {
	add_action( 'admin_enqueue_scripts', 'rg_admin_enqueue' );
}

function rg_remove_css() {
	?>
	<style>
	#menu-dashboard, #menu-users , #toplevel_page_vc-welcome {
	display: none;
	}

	</style>
	<?php

}
function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo site_url(); ?>/wp-content/uploads/2016/05/logo.png);
                background-size: 300px 83px;
    			padding-bottom: 30px;
    			width: 300px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

if( ! in_array('administrator', $rg_user->roles)) {
	//add_action('admin_head','rg_remove_css');
	function remove_menus(){
  
  remove_menu_page( 'index.php' );                  //Dashboard
  remove_menu_page( 'jetpack' );                    //Jetpack* 
  remove_menu_page( 'edit.php' );                   //Posts
  remove_menu_page( 'upload.php' );                 //Media
  remove_menu_page( 'edit.php?post_type=page' );    //Pages
  remove_menu_page( 'edit-comments.php' );          //Comments
  remove_menu_page( 'themes.php' );                 //Appearance
  remove_menu_page( 'plugins.php' );
  remove_menu_page( 'profile.php' );                //Plugins
  remove_menu_page( 'users.php' );                  //Users
  remove_menu_page( 'tools.php' );                  //Tools
  remove_menu_page( 'options-general.php' );  
  remove_menu_page( 'vc-welcome' );        //Settings
  
}
add_action( 'admin_menu', 'remove_menus' ,999 );
add_action('admin_menu', 'my_plugin_menu');

function my_plugin_menu() {
	add_dashboard_page('My Plugin Dashboard', 'My Plugin', 'read', 'my-unique-identifier', 'rg_management_setup');
}
add_action( 'wp_before_admin_bar_render', 'my_tweaked_admin_bar' ); 
function remove_dashboard_meta() {
	if ( ! current_user_can( 'manage_options' ) ) {
		remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
		remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
		remove_meta_box( 'dashboard_primary', 'dashboard', 'normal' );
		remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
		remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
		remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
		remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
		remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
		remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');
		remove_meta_box('pmpro_db_widget','dashboard','normal');
		remove_meta_box('mk_posts_like_stats','dashboard','normal');
	}
	function wpexplorer_add_dashboard_widgets() {
	wp_add_dashboard_widget(
		'wpexplorer_dashboard_widget', // Widget slug.
		'My Custom Dashboard Widget', // Title.
		'wpexplorer_dashboard_widget_function' // Display function.
	);
	wp_add_dashboard_widget(
		'wpexplorer_dashboard_widget2', // Widget slug.
		'My Custom Dashboard Widget2', // Title.
		'wpexplorer_dashboard_widget_function2' // Display function.
	);
}
add_action( 'wp_dashboard_setup', 'wpexplorer_add_dashboard_widgets' );

/**
 * Create the function to output the contents of your Dashboard Widget.
 */
function wpexplorer_dashboard_widget_function() {
	echo "Hello there, I'm a great Dashboard Widget. Edit me!";
}
function wpexplorer_dashboard_widget_function2() {
	echo "Hello there, I'm a great Dashboard Widget. Edit me!";
}
}
add_action( 'admin_init', 'remove_dashboard_meta' );
function my_tweaked_admin_bar() {
    global $wp_admin_bar;

    $wp_admin_bar->remove_menu('edit-profile');

    $wp_admin_bar->add_menu( array(
            'id'    => 'edit-profile',
            'title' => 'Edit My Profile',
            'href'  => admin_url('admin.php?page=rg_profile_setup'),
            'parent'=>'user-actions'
        ));
}
add_action( 'admin_bar_menu', 'remove_wp_logo', 999 );

function remove_wp_logo( $wp_admin_bar ) {
	$wp_admin_bar->remove_node( 'wp-logo' );
}
add_action( 'admin_bar_menu', 'remove_wp_new_content', 999 );

function remove_wp_new_content( $wp_admin_bar ) {
	$wp_admin_bar->remove_node( 'new-content' );
}
add_action( 'admin_bar_menu', 'wp_admin_bar_my_custom_account_menu', 11 );

function wp_admin_bar_my_custom_account_menu( $wp_admin_bar ) {
	$user_id = get_current_user_id();
	$current_user = wp_get_current_user();
	$profile_url = admin_url('admin.php?page=rg_profile_setup');

	if ( 0 != $user_id ) {
		/* Add the "My Account" menu */
		$id = get_user_meta($current_user->ID, 'avatar_manager_custom_avatar', true);
		if(empty($id)) {
				$avatar = get_avatar( $user_id, 28 );
		}
		else{
			$url =  wp_get_attachment_url( $id );
			$avatar= '<img class="avatar avatar-32 wp-user-avatar wp-user-avatar-32 photo avatar-default" width="200" height="200" alt="" src="'.$url.'">';
		}
$howdy = sprintf( __('Welcome, %1$s'), $current_user->display_name );
$class = empty( $avatar ) ? '' : 'with-avatar';

$wp_admin_bar->add_menu( array(
'id' => 'my-account',
'parent' => 'top-secondary',
'title' => $howdy . $avatar,
'href' => $profile_url,
'meta' => array(
'class' => $class,
),
) );

}
}
}
add_filter('get_user_option_admin_color', 'change_admin_color');
function change_admin_color($result) {
return 'midnight';
}
add_action('user_register', 'set_default_admin_color');
remove_action( 'admin_color_scheme_picker', 'admin_color_scheme_picker' );
function rg_menu_options(){	
	$rg_user_details = wp_get_current_user();
	//rg_pr($rg_user_details);
	if(in_array('subscriber', $rg_user_details->roles)) {
		$menu_access = 'subscriber';
	} 
	if(in_array('administrator', $rg_user_details->roles)) {
		$menu_access = 'administrator';
	}
	if(in_array('basic', $rg_user_details->roles)) {
		$menu_access = 'basic';
	}
	if(in_array('paid-member', $rg_user_details->roles)) {
		$menu_access = 'paid-member';
	}
	if(in_array('administrator', $rg_user_details->roles)) {
		add_menu_page( 
		    __( 'Management', 'rg' ),
		    __( 'Management', 'rg' ),
		     "$menu_access",
		    'rg_management_setup',
		    'rg_management_setup',
		    '',
		    6
		); 
	}
	if(in_array('basic', $rg_user_details->roles)) {
		add_menu_page( 
		    __( 'Home', 'rg' ),
		    __( 'Home', 'rg' ),
		     "$menu_access",
		    'rg_dashboard_setup',
		    'rg_dashboard_setup',
		    '',
		    6
		); 
	}
	add_menu_page( 
	    __( 'Profile', 'rg' ),
	    __( 'Profile', 'rg' ),
	     "$menu_access",
	    'rg_profile_setup',
	    'rg_profile_setup',
	    '',
	    7
	);

	add_menu_page( 
	    __( 'Vehicles', 'rg' ),
	    __( 'Vehicles', 'rg' ),
	    "$menu_access",
	    'rg_vehicles_setup',
	    'rg_vehicles_setup',
	    '',
	    8
	);
	add_menu_page( 
	    __( 'History', 'rg' ),
	    __( 'History', 'rg' ),
	    "$menu_access",
	    'rg_history_setup',
	    'rg_history_setup',
	    '',
	   11
	);
	// add_menu_page( 
	//     __( 'Get Free Gas', 'rg' ),
	//     __( 'Get Free Gas', 'rg' ),
	//     "$menu_access",
	//     'rg_get_free_gas_setup',
	//     'rg_get_free_gas_setup',
	//     '',
	//     6
	// );

	add_menu_page( 
	    __( 'Get Help', 'rg' ),
	    __( 'Get Help', 'rg' ),
	    "$menu_access",
	    'rg_get_help_setup',
	    'rg_get_help_setup',
	    '',
	    12
	);

	add_menu_page( 
	    __( 'Schedule Service', 'rg' ),
	    __( 'Schedule Service', 'rg' ),
	    "$menu_access",
	    'rg_schedule_service_setup',
	    'rg_schedule_service_setup',
	    '',
	    6
	);

	add_menu_page( 
	    __( 'Payments', 'rg' ),
	    __( 'Payments', 'rg' ),
	    "$menu_access",
	    'rg_payments_setup',
	    'rg_payments_setup',
	    '',
	    9
	);
	if(in_array('staff', $rg_user_details->roles)) {
		add_menu_page( 
		    __( 'Get Gas', 'rg' ),
		    __( 'Get Gas', 'rg' ),
		    "staff",
		    'rg_get_gas_setup',
		    'rg_get_gas_setup',
		    '',
		    9
		);
	}
}

add_action('init', 'rg_menu_options');


// function rg_management_setup() {	
// 	load_template( dirname( __FILE__ ) . '/templates/admin-ui/rg-setup.php' );
// }
function rg_dashboard_setup() {	

	wp_enqueue_script( 'bootstrap.min.js', RG_PLUGIN_ASSETS_URL.'js/bootstrap.min.js', false );
	wp_enqueue_style( 'bootstrap.min.css', RG_PLUGIN_ASSETS_URL.'css/bootstrap.min.css', false );
	load_template( dirname( __FILE__ ) . '/templates/admin-ui/dashboard/rg-dashboard-page-all-part.php' );
	
}
function rg_profile_setup() {	
	wp_enqueue_script( 'bootstrap.min.js', RG_PLUGIN_ASSETS_URL.'js/bootstrap.min.js', false );
	wp_enqueue_style( 'bootstrap.min.css', RG_PLUGIN_ASSETS_URL.'css/bootstrap.min.css', false );

	load_template( dirname( __FILE__ ) . '/templates/admin-ui/rg-profile-page-setup-new.php' );
}

function rg_history_setup() {	
	load_template( dirname( __FILE__ ) . '/templates/admin-ui/rg-history-page-details-new.php' );
}

function rg_vehicles_setup() {	
	wp_enqueue_script( 'bootstrap.min.js', RG_PLUGIN_ASSETS_URL.'js/bootstrap.min.js', false );
	wp_enqueue_style( 'bootstrap.min.css', RG_PLUGIN_ASSETS_URL.'css/bootstrap.min.css', false );
	load_template( dirname( __FILE__ ) . '/templates/admin-ui/rg-vehicles-page-details.php' );
	
}
function rg_schedule_service_setup() {	
	require_once(dirname( __FILE__ ) .'/Stripe/lib/Stripe.php');
	wp_enqueue_script( 'bootstrap.min.js', RG_PLUGIN_ASSETS_URL.'js/bootstrap.min.js', false );
	wp_enqueue_style( 'bootstrap.min.css', RG_PLUGIN_ASSETS_URL.'css/bootstrap.min.css', false );
	load_template( dirname( __FILE__ ) . '/templates/admin-ui/schedule/rg-schedule-page-details.php' );
	
}
function rg_management_setup() {	
	wp_enqueue_script( 'bootstrap.min.js', RG_PLUGIN_ASSETS_URL.'js/bootstrap.min.js', false );
	wp_enqueue_style( 'bootstrap.min.css', RG_PLUGIN_ASSETS_URL.'css/bootstrap.min.css', false );
	load_template( dirname( __FILE__ ) . '/templates/admin-ui/management/details.php' );
	
}

function rg_payments_setup() {	
	require_once(dirname( __FILE__ ) .'/Stripe/lib/Stripe.php');
	wp_enqueue_script( 'bootstrap.min.js', RG_PLUGIN_ASSETS_URL.'js/bootstrap.min.js', false );
	wp_enqueue_style( 'bootstrap.min.css', RG_PLUGIN_ASSETS_URL.'css/bootstrap.min.css', false );
	load_template( dirname( __FILE__ ) . '/templates/admin-ui/rg-payments-page-details.php' );
	
}

function rg_get_help_setup() {	
	wp_enqueue_script( 'bootstrap.min.js', RG_PLUGIN_ASSETS_URL.'js/bootstrap.min.js', false );
	wp_enqueue_style( 'bootstrap.min.css', RG_PLUGIN_ASSETS_URL.'css/bootstrap.min.css', false );
	load_template( dirname( __FILE__ ) . '/templates/admin-ui/rg-help-page-details.php' );
	
}
function rg_get_gas_setup() {	
		wp_enqueue_script( 'bootstrap.min.js', RG_PLUGIN_ASSETS_URL.'js/bootstrap.min.js', false );
		wp_enqueue_style( 'bootstrap.min.css', RG_PLUGIN_ASSETS_URL.'css/bootstrap.min.css', false );
		load_template( dirname( __FILE__ ) . '/templates/admin-ui/staff-gas/details.php' );
		
	}
/**
 * print
 */
function rg_pr($d, $return=false){
	// get
	$r = sprintf('<pre>%s</pre>', print_r($d, true) );	
	// return
	if( $return ) return $r;
	// echo
	echo $r;
}

/**
 * Logger
 */	
function rg_logger($data, $marker='debug'){
	if(!file_exists(RG_LOG_DIR)){
		@wp_mkdir_p(RG_LOG_DIR);
	}	
	if(is_array($data) || is_object($data)) $data = print_r($data, true);
	@file_put_contents(RG_LOG_DIR. $marker. '-' .time().'.txt', $data, FILE_APPEND);
}
//add_action('init',  'rg_logger');
add_action( 'wp_ajax_update_user_password', 'update_user_password_callback' );
add_action( 'wp_ajax_nopriv_update_user_password', 'update_user_password_callback' );
function update_user_password_callback(){
	global $current_user;
	// echo "<pre>";
	// print_r($current_user);
	// die();
	$pass=$_POST['old_password'];
	$password=$_POST['new_password'];
	$username=$current_user->user_login;
	$user = get_user_by( 'login', $username );
	if ( $user && wp_check_password( $pass, $user->data->user_pass, $user->ID) ){
		if($_POST['new_password']==$_POST['c_password']){
			wp_set_password( $password, $current_user->ID );
			wp_set_auth_cookie( $current_user->ID, true);
				$data['success']="Your password updated successfully";
			//}
		}
		else{
   			$data['error']="Your new password and confirm password does'nt match";

		}
	}
   		
	else
   		$data['error']='Your current password is incorrect.';
   	//$response=json_encode($data);
   	 wp_send_json( $data );
   	//wp_die();
}
add_filter( 'admin_footer_text', '__return_empty_string', 11 );
add_filter( 'update_footer',     '__return_empty_string', 11 );
add_action( 'wp_ajax_book_event_service', 'book_event_service_callback' );
add_action( 'wp_ajax_nopriv_book_event_service', 'book_event_service_callback' );
function book_event_service_callback(){
	global $current_user,$wpdb;
	

	$type 		= $_POST['type'];
	$event_id	= $_POST['event_id'];
	$order_id	= $_POST['order_id'];
	$start 		= $_POST['start'];
	//$is_edit 		= $_POST['start'];
	$service= $wpdb->get_row("SELECT * FROM 
												wp_user_event_service ws 
									where 
											event_id=2
								");
	if($type=='extra_service'){

		if($event_id==2){
		
			echo "<h4>Get Gas</h4>";
			echo "<label>Enter Gallons</label>";
			echo "<br>";
			
			echo "<input type='text' required class='gallons' name='event[".$order_id."][fee]' price_id='$service->price'>";
			echo "<p>Per Gallon Price:$service->price</p>";
			
		}
		elseif($event_id==1){
		
			$allservices= $wpdb->get_results("SELECT * FROM 
												wp_user_car_wash_service ws 
										left join 
												wp_user_event_service es
										on
												ws.service_id=es.event_id
										where 
												ws.service_id=1
									");
			
			echo "<h4>Car Wash</h4>";
			
			
			echo "<label>Select Extra Service</label>";
			echo "<br>";
			
			echo "<select  class='service' name='event[".$order_id."][fee][]' multiple required>";
			
			foreach ($allservices as $service) {
				
				echo "<option value='$service->wash_id' price_id='$service->sprice'>  $service->extra_service ";
				echo "<p>Price:$service->sprice</p>";
				echo "</option>";
				
			}
			echo '</select>';
			echo "<br>";
			echo "<br>";
			
		}
			echo "<input type='hidden' class='start' name='event[".$order_id."][start]' value='$start'>";
			echo "<input type='hidden' name='event[".$order_id."][id]' value='$event_id'>";
			
			echo "<label>Enter  Service Time</label>";
			echo "<br>";
			echo "<input type='text' class='datetimepicker' name='event[".$order_id."][time]' value='' required>";
			echo "<br>";
			echo "<br>";
			$vehicles= $wpdb->get_results("SELECT * FROM wp_vehicle_info  WHERE user_id = '$current_user->ID'");
			echo "<label>Select  Vechicle</label>";
			echo "<br>";
			echo "<select name='event[".$order_id."][vehicle]' required>";

			foreach ($vehicles as $vehicle) {
				echo "<option value='$vehicle->id'>$vehicle->lpm</option>";
			}
				
				
			echo '</select>';

	}
	else if($type=='edit_service'){
			$id=$_POST['id'];
			$books= $wpdb->get_row("SELECT * FROM wp_user_book_event_service  WHERE id='$id' and user_id = '$current_user->ID'");
			echo "<label>Enter  Service Time</label>";
			echo "<br>";
			echo "<input type='text' name='event[time]' value='$books->time' required>";
			echo "<br>";
			echo "<br>";
			$vehicles= $wpdb->get_results("SELECT * FROM wp_vehicle_info  WHERE user_id = '$current_user->ID'");
			echo "<label>Select  Vechicle</label>";
			echo "<br>";
			echo "<select name='event[vehicle]'  required>";

			foreach ($vehicles as $vehicle) {
				if($books->vehicle==$vehicle->id)
					echo "<option selected value='$vehicle->id'>$vehicle->lpm</option>";
				else
					echo "<option value='$vehicle->id'>$vehicle->lpm</option>";
			}
				
				
			echo '</select>';
	}
	else{
		$title		= $_POST['title'];
		$start 		= $_POST['start'];
		//$end 		= $_POST['end'];
		$user_id	= $current_user->ID;

		$add_book	=	array( 
								'title' 		=> $title, 
								'start'			=> $start , 
								'user_id' 		=> $user_id 
						);

		$book_id = $wpdb->insert( 
								'wp_user_book_event_service', 
								$add_book
								
								);
		 $book_id;
		if($book_id){
			$wpdb->insert( 'wp_user_history', array( 'notification' => 8, 'user_id' => $user_id ) );
			echo "success";
			exit();
		}
	}
	 die();
}

