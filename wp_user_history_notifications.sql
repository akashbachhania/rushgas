-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 17, 2016 at 12:55 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rushgas`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_user_history_notifications`
--

CREATE TABLE `wp_user_history_notifications` (
  `his_id` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  `type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wp_user_history_notifications`
--

INSERT INTO `wp_user_history_notifications` (`his_id`, `message`, `type`) VALUES
(1, 'User Signup', 1),
(2, 'User added New Vechicle', 2),
(3, 'User Update a vehicle', 2),
(4, 'User deleted a vehicle', 2),
(5, 'New credited Card added', 3),
(6, 'Credit card Deleted', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_user_history_notifications`
--
ALTER TABLE `wp_user_history_notifications`
  ADD PRIMARY KEY (`his_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_user_history_notifications`
--
ALTER TABLE `wp_user_history_notifications`
  MODIFY `his_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
