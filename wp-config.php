<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
ini_set("session.use_cookies", 0);
ini_set("session.use_trans_sid", 1);
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'rushgas');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ujIIC959n2uKPqFdMKJavaUb4wwJ45xHWSbvhUkpjyMTDesvxmcWN3IAMB0VdzHJ');
define('SECURE_AUTH_KEY',  '1cfwbVh5YHsyErNDqP0pMHp7zrDRDnHbxpnVswCMMcGK2FkmWMxrDqESiJpS9VSu');
define('LOGGED_IN_KEY',    '4U1S6fVRe7ZkLy1IXWx9mQ2cWV4iHg1xkyMXl4ciB6dKgkPjhc0rGjz76vHDECTl');
define('NONCE_KEY',        '0GAfWbcdOg0O2cRYAiys2lghNUxTtPWGaNaUcTDUGXFbp4l3r2XLV1gcZ2gKxVuG');
define('AUTH_SALT',        'h3qtH6HgRwjtNLfxhdDtLrHUPKciCvuOVBYTUIPAT0FOSWdmk6nUiCNGJlpe6ep6');
define('SECURE_AUTH_SALT', 'QAhP4OJIEZPv1b0UdtPEqUEfs18imbPvo2aoPJFruUq44goEwNWOQkBgjeptdGpt');
define('LOGGED_IN_SALT',   'IIkBPlX8gac5HSvLp8MEoighFNIdnz4UK1EXxP2HgoD22oa3lww0JLJdFAEEUuDB');
define('NONCE_SALT',       'MlIQonhgtJQvwdaN9ryCanidNifuB13TcxzWRW75lrFhoprZrR0tQL2zkVSU2Q0y');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');define('FS_CHMOD_DIR',0755);define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed upstream.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
