<?php
	/* define global varibles*/
	global $current_user,$wpdb;
	if(isset($_GET['payments']) && $_GET['payments']=='addnew'){
		if(isset($_POST['stripeToken'])){
			$card_number		= $_POST['card_number'];
			$expire_month		= $_POST['expire_month'];
			$expire_year		= $_POST['expire_year'];
			$cvv				= $_POST['cvv'];
			
			$user_id	= $current_user->ID;
			
				$api_key= pmpro_getOption("stripe_secretkey");
								Stripe::setApiKey("$api_key");
				$token= $charge = Stripe_Token::create(array(  "card" => array(
			    "number" => $card_number,
			    "exp_month" => $expire_month,
			    "exp_year" => $expire_year,
			    "cvc" => $cvv
			  )
			  ));
				
				
				 $customer=Stripe_Customer::create(array(
			  "description" => "Customer for $current_user->user_email",
			  "source" => $token->id // obtained with Stripe.js
			));
			
			$payment_id = $wpdb->insert( 
									'wp_add_credit_card_stripe', 
									array( 
										'account_number'	=> $card_number, 
										'expire_month' 		=> $expire_month , 
										'expire_year' 		=> $expire_year ,
										'customer_id'		=> $customer->id,
										'user_id' 			=> $user_id )
									
									);
		 	if($payment_id){

		 		$wpdb->insert( 'wp_user_history', array( 'notification' => 5, 'user_id' => $user_id ) );
          		wp_redirect(admin_url('admin.php?page=rg_payments_setup'));
      		}

		}
 		include_once('rg-payments-page-add-new-part.php');
	}
	
	else if(isset($_GET['payments']) && $_GET['payments']=='delete'){
		$id=$_GET['id'];
		$payment_id = $wpdb->delete( 
									'wp_add_credit_card_stripe',
									array('id' => $id ),
									array('%d') 
									);
		if($payment_id){
			    $user_id	= $current_user->ID;
			   // $wpdb->insert( 'wp_user_history', array( 'notification' => 6, 'user_id' => $user_id ) );
          		wp_redirect(admin_url('admin.php?page=rg_payments_setup'));
      	}
	}
	else{
		include_once 'rg-payments-page-all-part.php';
    }
	
