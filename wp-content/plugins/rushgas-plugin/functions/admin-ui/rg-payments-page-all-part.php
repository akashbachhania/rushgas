<?php
 	/* query for get all vehicles of current user*/
	$payments= $wpdb->get_results("SELECT * FROM wp_add_credit_card_stripe  WHERE user_id = '$current_user->ID'");
	?>
	<!-- Main Container Start -->
	<div class="container">
			<!-- .Page heading -->
	  		<h2 class="pull-left">Payments</h2>
	  		<!-- Link for add New vehicles -->
			<h2><a href="<?php echo admin_url('admin.php?page=rg_payments_setup&payments=addnew')?>" class="btn btn-danger pull-right"> 
				Add New Credit Card
			</a></h2>		

<?php
			if($payments):
			
?>
			<div class="col-md-12 col-sm-12 col-xs-12">
				 <div class="table-responsive" >
				 <table class="table" >
				    <thead>
				      <tr>
				        <th>Card </th>
				        <th> Month</th>
				        <th> Year</th>
				       
				        <th>Action</th>
				      </tr>
				    </thead>
				    <tbody>
				    	<?php foreach ($payments as $payment) {?>
				      <tr>
				      	
				      	<td><?=substr($payment->account_number,-4);?></td>
				        <td><?=$payment->expire_month;?></td>
				        <td><?=$payment->expire_year;?></td>
				      	
				        
				        <td>
				        	
				        	<a href="<?php echo admin_url('admin.php?page=rg_payments_setup&payments=delete&id='.$payment->ID);?>">
				        		Delete
				        	</a>
				        </td>
				      </tr>
				     <? }?>
				    </tbody>
				  </table>
				</div>
<?php 		endif;
?>
	</div>
		</div>