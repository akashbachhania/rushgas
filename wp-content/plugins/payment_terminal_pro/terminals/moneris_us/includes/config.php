<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);

require (CCPT_HOME_DIR."/wp-load.php");
require("functions.php"); 
#******************************************************************************
#                       Credit Card Payments Wordpress v3.0
#
#	Author: Convergine.com
#	http://www.convergine.com
#	Version: 3.0
#   (c) 2013 Convergine.com
#
#******************************************************************************
$configProcessor=is_string(get_option('ccwp_details_MONERIS_US'))?unserialize(get_option('ccwp_details_MONERIS_US')):get_option('ccwp_details_MONERIS_US');

//THIS IS TITLE ON PAGES
$title = "Moneris Payment Terminal v1.0"; //site title

$titleAboveForm = "Moneris US Payment Terminal";
//THIS IS ADMIN EMAIL FOR NEW PAYMENT NOTIFICATIONS.
$admin_email = get_option('ccpt_admin_email'); //this email is for notifications about new payments
//CHANGE "USD" TO REQUIRED CURRENCY, SUPPORTED BY PROVIDER.USD, CAD, EUR
define("PTP_CURRENCY_CODE",get_option('ccpt_paypal_currency')); 
//IF YOU NEED TO ADD MORE SERVICES JUST ADD THEM THE SAME WAY THEY APPEAR BELOW.
$show_services = get_option('ccpt_show_dd_text'); 
//NOW, IF YOU WANT TO ACTIVATE THE DROPDOWN WITH SERVICES ON THE TERMINAL
			

//IF YOU'RE GOING LIVE FOLLOWING VARIABLE SHOULD BE SWITCH TO true
// IT WILL AUTOMATICALLY REDIRECT ALL NON-HTTTPS REQUESTS TO HTTPS.
// MAKE SURE SSL IS INSTALLED ALREADY.
$redirect_non_https =(get_option('ccpt_test')==2)? true : false;

//MONERIS RELATED VARIABLES
define('COUNTRY', 'US'); //US or CA
define('MERCHANT_NAME', 'yourstorename');
define('MERCHANT_URL', 'http://yourpageurl.com');

if(get_option('ccpt_test')==2){

// FOR CANADIAN STORE USE THIS TO GO LIVE
define('MONERIS_STORE_ID', $configProcessor['live']['MONERIS_STORE_ID']);
define('MONERIS_API_TOKEN', $configProcessor['live']['MONERIS_API_TOKEN']);
define('MONERIS_API_URL', $configProcessor['live']['MONERIS_API_URL']);

}else{


define('MONERIS_STORE_ID', $configProcessor['test']['MONERIS_STORE_ID']);
define('MONERIS_API_TOKEN', $configProcessor['test']['MONERIS_API_TOKEN']);
define('MONERIS_API_URL', $configProcessor['test']['MONERIS_API_URL']); // THIS IS CANADIAN TEST API ENDPOINT


}
//DO NOT CHANGE ANYTHING BELOW THIS LINE, UNLESS SURE OF COURSE

if($redirect_non_https){
	if ($_SERVER['SERVER_PORT']!=443) {
		$sslport=443; //whatever your ssl port is
		$url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		header("Location: $url");
		exit();
	}
}
//$store_id='store1';
//$api_token='yesguy';
//'4242424242424242';
//'0812';
//$cust_id='CUST13343';//test
?>