<!-- <!DOCTYPE html>

<html <?php echo language_attributes();?> >

<head>

    <?php wp_head(); ?>

</head>

<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/bootstrap-cerulean.min.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/charisma-app.css" type="text/css" media="screen" />

<body <?php body_class(mk_get_body_class(global_get_post_id())); ?> <?php echo get_schema_markup('body'); ?> data-adminbar="<?php echo is_admin_bar_showing() ?>">

<div> <!-- main start -->
<?php $rg_user_id = get_current_user_id(); ?>
<div id="rg_menu_bar"  style="display:none"  
	<!-- left menu starts -->
        <div class="col-sm-2 col-lg-2">
            <div class="sidebar-nav">
                <div class="nav-canvas">
                    <div class="nav-sm nav nav-stacked">

                    </div>
                    <ul class="nav nav-pills nav-stacked main-menu">
                        <li class="nav-header">Main</li>
                        <li><a class="ajax-link" href="<?php echo home_url( '/management-dashboard/' ) ?>"><i class="glyphicon glyphicon-home"></i><span> Management</span></a>
                        </li>
                        <li><a class="ajax-link" href="<?php echo home_url( '/profile/' ) ?>"><i class="glyphicon glyphicon-eye-open"></i><span> Profile</span></a>
                        </li>
                        <li><a class="ajax-link" href="<?php echo home_url( '/vehicles/' ) ?>"><i
                                    class="glyphicon glyphicon-edit"></i><span> Vehicles</span></a></li>
                        <li><a class="ajax-link" href="<?php echo home_url( '/payments/' ) ?>"><i class="glyphicon glyphicon-list-alt"></i><span> History</span></a>
                        </li>
                        <li><a class="ajax-link" href="<?php echo home_url( '/get-free-gas/' ) ?>"><i class="glyphicon glyphicon-font"></i><span> Get Free Gas</span></a>
                        </li>
                        <li><a class="ajax-link" href="<?php echo home_url( '/get-help/' ) ?>"><i class="glyphicon glyphicon-picture"></i><span> Get Help</span></a>
                        </li>
                        <!-- <li class="nav-header hidden-md">Schedule Service</li> -->
                        <li><a class="ajax-link" href="<?php echo home_url( '/schedule-service/' ) ?>"><i class="glyphicon glyphicon-calendar"></i><span> Schedule Service</span></a>
                        </li>
                        <li><a class="ajax-link" href="<?php echo home_url( '/history/' ) ?>"><i
                                    class="glyphicon glyphicon-align-justify"></i><span> Payments</span></a></li>
                        <!-- <li class="accordion">
                            <a href="#"><i class="glyphicon glyphicon-plus"></i><span> Accordion Menu</span></a>
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="#">Child Menu 1</a></li>
                                <li><a href="#">Child Menu 2</a></li>
                            </ul>
                        </li>
                        <li><a class="ajax-link" href="calendar.html"><i class="glyphicon glyphicon-calendar"></i><span> Calendar</span></a>
                        </li>
                        <li><a class="ajax-link" href="grid.html"><i
                                    class="glyphicon glyphicon-th"></i><span> Grid</span></a></li>
                        <li><a href="tour.html"><i class="glyphicon glyphicon-globe"></i><span> Tour</span></a></li>
                        <li><a class="ajax-link" href="icon.html"><i
                                    class="glyphicon glyphicon-star"></i><span> Icons</span></a></li>
                        <li><a href="error.html"><i class="glyphicon glyphicon-ban-circle"></i><span> Error Page</span></a>
                        </li>
                        <li><a href="login.html"><i class="glyphicon glyphicon-lock"></i><span> Login Page</span></a>
                        </li> -->
                    </ul>
                    <!-- <label id="for-is-ajax" for="is-ajax"><input id="is-ajax" type="checkbox"> Ajax on menu</label> -->
                </div>
            </div>
        </div>
        <!--/span-->
        <!-- left menu ends -->
</div> <!-- chile 1 -->

<div> <!-- chile 2 -->

	<?php

		// Hook when you need to add content right after body opening tag. to be used in child themes or customisations.

		do_action('theme_after_body_tag_start');

	?>



	<!-- Target for scroll anchors to achieve native browser bahaviour + possible enhancements like smooth scrolling -->

	<div id="top-of-page"></div>



		<div id="mk-boxed-layout">



			<div id="mk-theme-container" <?php echo is_header_transparent('class="trans-header"'); ?>>



				<?php mk_get_header_view('styles', 'header-'.get_header_style()); -->