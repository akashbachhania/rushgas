
<?php 
if(isset($_POST['submit'])){
  unset($_POST['submit']);
    $save=$_POST;
    $save['user_id'] = $current_user->ID;
    $address_id = $wpdb->insert( 
                'wp_user_service_addresses', 
                $save
               
              );
    if($address_id){
        wp_redirect(admin_url('admin.php?page=rg_profile_setup&action=serviceall'));
    }
  }

  $sql="SELECT * from wp_user_service_address_type";
  $service_address_type =  $wpdb->get_results($sql);?>
  <div class="container">
    <div class="page-header">
      <h1>Add Service address</h1>
      </div>
    <div class="row">
      
    <div class="col-md-6 col-sm-6">
       <form class="form-horizontal" action="" method='post' enctype="multipart/form-data">
          <div class="form-group">
            <label class="control-label col-sm-4" >Address Title:</label>
            <div class="col-sm-8">
                <!-- <select class="form-control" name='service_type'>

                <?php 
                foreach ($service_address_type as $address_type) {
                  echo " <option value='$service_type_id->service_type_name'>$address_type->service_type_name</option>";
                }?>
                </select>
              -->
              <input type="text" class="form-control" id="title" required name="title" placeholder="Enter  Address Title">  
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-4">address:</label>
            <div class="col-sm-8"> 
              <input type="text" class="form-control" id="address" required name="address" placeholder="Enter address">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-4" for="address2">Apt,suite or unit #:</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="address2"  name="address2" placeholder="Enter address2">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-4" for="city">city:</label>
            <div class="col-sm-8"> 
              <input type="text" class="form-control" id="city" required name="city" placeholder="Enter city">
            </div>
          </div>
           <div class="form-group">
            <label class="control-label col-sm-4" for="state">state:</label>
            <div class="col-sm-8"> 
              <input type="text" class="form-control" id="state" required name="state" placeholder="Enter state">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-4" for="lpm">Postcode:</label>
            <div class="col-sm-8"> 
              <input type="text" class="form-control" id="postcode" required name="postcode" value="" placeholder="Enter postcode">
            </div>
          </div>
          <div class="form-group"> 
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" name="submit" class="btn btn-danger" required class="btn btn-default">Submit</button>
            </div>
          </div>
      </form>


     </div>
    </div>
  </div>