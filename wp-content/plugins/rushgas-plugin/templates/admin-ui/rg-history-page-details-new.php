<?php
global $wpdb, $pmpro_invoice, $pmpro_msg, $pmpro_msgt, $current_user;
//echo do_shortcode( '[pmpro_account]' );

?>

<div class="container">
	<h2>History</h2>
	<div class="row">
	<div class="col-md-12">  
<?php
// [pmpro_invoice]
//global $current_user, $pmpro_invoice;
//echo do_shortcode( '[pmpro_invoice]' );

		//Show all invoices for user if no invoice ID is passed	
       
		$histories = $wpdb->get_results("SELECT 
												h.*, UNIX_TIMESTAMP(h.timestamp) as timestamp,
												n.message as message, 
												nt.title  as title 
										FROM 
												wp_user_history h 
										LEFT JOIN 
												wp_user_history_notifications n 
										ON 
												h.notification = n.his_id 
										LEFT JOIN 
												wp_user_history_notification_type nt 
										ON 
												n.type = nt.noti_id 
										WHERE 
												h.user_id = '$current_user->ID' 
										and 
												h.notification in (1,7,8)
										ORDER BY 
												timestamp DESC");
		if($histories)
		{
			?>
			<table id="pmpro_invoices_table" class="table table-responsive" cellpadding="0" cellspacing="0" border="0">
			<thead>
				<tr>
					<th><?php _e('Date', 'pmpro'); ?></th>
					<th><?php _e('Message ', 'pmpro'); ?></th>
					<th><?php _e('Type ', 'pmpro'); ?></th>				
				</tr>
			</thead>
			<tbody>
			<?php
				foreach($histories as $history)
				{ 
					?>
					<tr>
						<td><?php echo date_i18n(get_option("date_format"), $history->timestamp)?></td>
						<td><?php echo $history->message; ?></td>
						<td><?php echo $history->title; ?></td>
																	
					</tr>
					<?php
				}
			?>
			</tbody>
			</table>
			<?php
		}
		else
		{
			?>
			<h4 ><?php _e('No History found.', 'pmpro');?></h4>
			<?php
		}
	 
?>


	</div>
</div>