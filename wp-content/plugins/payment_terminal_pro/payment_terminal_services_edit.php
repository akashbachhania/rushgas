<?php
#******************************************************************************
#                       Credit Card Payments Wordpress v3.0
#
#	Author: Convergine.com
#	http://www.convergine.com
#	Version: 3.0
#   (c) 2013 Convergine.com
#
#******************************************************************************
	global $wpdb;
	
	
	//$ccpt_serviceID = $_GET['ccpt_serviceID'];
	
	$ssdurl = $_SERVER['REQUEST_URI'];
	if(stristr($ssdurl, "&ccpt_serviceID=")){
		$myurltemp = explode("&ccpt_serviceID=",$ssdurl);
		$ccpt_serviceID = $myurltemp[1];
	}

	
	if(isset($_POST['ccpt_submit_service']) && $_POST['ccpt_submit_service'] == 'yes' && !empty($ccpt_serviceID) && is_numeric($ccpt_serviceID)) {
		//Form data sent
		$pt_title = $_POST['ccpt_services_title'];
		$pt_descr = $_POST['ccpt_services_descr'];
		$pt_price = $_POST['ccpt_services_price'];
		
		if(is_numeric($pt_price) && !empty($pt_title)){
			$query="UPDATE ".$wpdb->prefix."ccpt_services SET ccpt_services_title='".addslashes(strip_tags($pt_title))."', ccpt_services_descr='".addslashes(strip_tags($pt_descr))."', ccpt_services_price='".addslashes(strip_tags($pt_price))."' WHERE ccpt_services_id='".$ccpt_serviceID."'";
			mysql_query($query) or die(mysql_error());
			?><div class="updated"><p><strong><?php _e('Service updated! <a href="admin.php?page=ccpt_admin_services">Click here</a> to go back to all services' ); ?></strong></p></div><?php
		} else { 
		?><div class="updated"><p><strong><?php _e('Service not updated! Please check your input. Price must contain numbers only and name cannot be blank.' ); ?></strong></p></div><?php
		}

 	} 
	
	
	$query2="SELECT * FROM ".$wpdb->prefix."ccpt_services WHERE ccpt_services_id='".$ccpt_serviceID."'";
	$result2=mysql_query($query2) or die(mysql_error()); 	
	$row2=mysql_fetch_assoc($result2);
	$ccpt_services_title = $row2["ccpt_services_title"];
	$ccpt_services_price = $row2["ccpt_services_price"];
	$ccpt_services_descr = $row2["ccpt_services_descr"];
?>

	<?php //$wp_url = get_bloginfo('siteurl');
	$wp_url = get_site_url();  ?>
    <link rel="stylesheet" media="screen" href="<?php echo $wp_url?>/wp-content/plugins/payment_terminal_pro/resources/css/admin-style.css" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo $wp_url?>/wp-content/plugins/payment_terminal_pro/resources/js/functions.js"></script>
		<div class="wrap-ccwp-bw">
            <div class="wrap-ccwp-header" >
			<?php    echo "<h2>" . __('Credit Card Payments - Services','') . "</h2>"; ?>        
            </div><!-- end header -->
            <div class="wrap-ccwp-content" >
            	<div class="wrap-ccwp-breadcrumbs" >
                <?php    echo "<h4>" . __( 'Edit Service', '' ) . "</h4>"; ?>
            	</div><!-- end bc -->
                <form name="ccpt_form" method="post" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>">
                    <input type="hidden" name="ccpt_submit_service" value="yes">
                    <input type="hidden" name="ccpt_serviceID" value="<?php echo $ccpt_serviceID?>">
                    
                    <div class="ccpw-services-rowwsto143"><label for="ccpt_services_title" ><?php _e("Service Name: " ); ?></label><input class="ccpt_text" type="text" name="ccpt_services_title" id="ccpt_services_title" value="<?php echo $ccpt_services_title?>" size="40"><br /><em><?php _e("This is what customers will see in the services dropdown to select from when they decide to pay" ); ?></em></div>
                    <div class="ccpw-services-rowwsto143"><label for="ccpt_services_price" ><?php _e("Service Price&nbsp;&nbsp;: " ); ?></label><input class="ccpt_text" type="text" name="ccpt_services_price" id="ccpt_services_price" onkeyup="noAlpha(this)"  value="<?php echo $ccpt_services_price?>" size="40"><br /><em><?php _e("Numbers only. ex. 10.99" ); ?></em></div>
                    
                    <div id="poststuff">
						<div id="<?php echo user_can_richedit() ? 'postdivrich' : 'postdiv'; ?>" class="postarea postare-test-me2">
						<label for="ccpt_services_descr" style="font-weight: bold;color: #666;" ><?php _e("Service Description: " ); ?></label><?php the_editor($ccpt_services_descr, $id = 'ccpt_services_descr', $prev_id = 'ccpt_services_descr', $media_buttons = false, $tab_index = 2);?><?php _e(" (Optional small text describing the service, won't be displayed to customer, internal use only at the moment)" ); ?>
                     	</div>
					</div> 
    
                    <p class="submit">
                    <input style="margin: 0 10px;" class="button button-primary" type="submit" name="Submit" value="<?php _e('Update Service', '' ) ?>" />
                    </p>
                    
                </form>
			</div>  
                
		</div>
        <div class="pt_footer">
        	<a href="http://www.convergine.com" target="_blank"><img src="<?php echo $wp_url?>/wp-content/plugins/payment_terminal_pro/resources/images/convergine.png" /></a>
        </div>