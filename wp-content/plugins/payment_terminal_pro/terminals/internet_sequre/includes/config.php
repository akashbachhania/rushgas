<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);

require (CCPT_HOME_DIR."/wp-load.php"); 
require("functions.php"); 
#******************************************************************************
#                       Credit Card Payments Wordpress v3.0
#
#	Author: Convergine.com
#	http://www.convergine.com
#	Version: 3.0
#   (c) 2013 Convergine.com
#
#******************************************************************************
$configProcessor=is_string(get_option('ccwp_details_INTERNETSECURE'))?unserialize(get_option('ccwp_details_INTERNETSECURE')):get_option('ccwp_details_INTERNETSECURE');

//THIS IS TITLE ON PAGES
$title = "InternetSequre Payment Terminal v1.0"; //site title
$titleAboveForm = "InternetSequre Payment Terminal";
//THIS IS ADMIN EMAIL FOR NEW PAYMENT NOTIFICATIONS.
$admin_email = get_option('ccpt_admin_email'); //this email is for notifications about new payments

define("PTP_CURRENCY_CODE",get_option('ccpt_paypal_currency')); 
//IF YOU NEED TO ADD MORE SERVICES JUST ADD THEM THE SAME WAY THEY APPEAR BELOW.
$show_services = get_option('ccpt_show_dd_text'); 

//IF YOU'RE GOING LIVE FOLLOWING VARIABLE SHOULD BE SWITCH TO true
// IT WILL AUTOMATICALLY REDIRECT ALL NON-HTTTPS REQUESTS TO HTTPS.
// MAKE SURE SSL IS INSTALLED ALREADY.
$redirect_non_https = (get_option('ccpt_test')==2)? true : false;;

$testAccount=$redirect_non_https;

//Internetsequre RELATED VARIABLES
define('GATEWAY_URL','https://direct.internetsecure.com/process.cgi');
define("MERCHANT_ID", $configProcessor['live']['MERCHANT_ID']);
define("ENABLE_SSL",$redirect_non_https);

//DO NOT CHANGE ANYTHING BELOW THIS LINE, UNLESS SURE OF COURSE
if($redirect_non_https){
	if ($_SERVER['SERVER_PORT']!=443) {
		$sslport=443; //whatever your ssl port is
		
		$url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		print $url;
		//header("Location: $url");
		exit();
	}
}
?>