<?php
$wip_basic_settings = get_option('wip_basic_settings',true);
$basic_settings = maybe_unserialize( $wip_basic_settings );

$wip_advanced_settings = get_option('wip_advanced_settings',true);
$advanced_settings = maybe_unserialize( $wip_advanced_settings );

?>

<div class="container">
  <div class="panel-group" id="accordion">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
            <span class="glyphicon glyphicon-minus"></span>
            Basic Settings
          </a>
        </h4>
      </div>
      <div id="collapseOne" class="panel-collapse collapse in">
        <div class="panel-body">
            <form class="form-horizontal" name="basic_settings_form" id="basic_settings_form">
            <input type="hidden" name="wip_basic_settings" id="wip_basic_settings">
             
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">
                  Access Token:
                </label>
                <div class="col-xs-6 col-md-6">
                    <input type="text" class="form-control" name="access_token" id="access_token" placeholder="Enter Access Token" value="<?php echo $basic_settings['access_token'];?>">    
                </div>
                <div class="col-xs-6 col-md-4" style="margin-left:-20px;margin-top:9px;">
                  <a  href="#"  data-toggle="tooltip" data-placement="right" 
                    title="Enter your Instagram access token. This is used by the plugin to pull in your photos.">      
                      <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                  </a>
                </div>
              </div>

                              
              <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">
                  Instagram Mode:
                </label>
                <div class="col-xs-6 col-md-6">

                          <div class="radio">
                              <label>
                                <input type="radio" name="optionsMode" id="optionsUser" value="user" <?php if( empty ($basic_settings['options_mode'])) { ?> checked <?php } ?> <?php if( 'user' == $basic_settings['options_mode']) { ?> checked <?php } ?>>
                                User
                              </label>
                          </div>
                          <div class="radio">
                              <label>
                                <input type="radio" name="optionsMode" id="optionsTag" value="tag" <?php if( 'tag' == $basic_settings['options_mode']) { ?> checked <?php } ?>>                              
                                  Tag
                              </label>
                          </div>

                          <div class="radio">
                              <label>
                                <input type="radio" name="optionsMode" id="optionsMultiuser" value="multiusers" <?php if( 'multiusers' == $basic_settings['options_mode']) { ?> checked <?php } ?>>
                                Multiuser
                              </label>
                          </div>
                          <div class="radio">
                              <label>
                                <input type="radio" name="optionsMode" id="optionsLocation" value="location" <?php if( 'location' == $basic_settings['options_mode']) { ?> checked <?php } ?>>                              
                                Location
                              </label>
                          </div>
                </div>
                <div class="col-xs-6 col-md-4" style="margin-left:-20px;margin-top:50px;">
                      <a  href="#"  data-toggle="tooltip" data-placement="right" 
                        title="This feature allows you to choose between two different plugin modes. ">      
                          <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                      </a>
                </div>
              </div>



              <?php //if( empty ($basic_settings['options_mode']) or 'user' == $basic_settings['options_mode'] ) { ?>

                  <div class="form-group  <?php if( empty ($basic_settings['options_mode'])) { echo 'show'; } else { ?> <?php if( 'user' == $basic_settings['options_mode'] ) { echo 'show'; } else { echo 'hidden'; } }?>" id="action_user">
                      <label for="inputTag" class="col-sm-2 control-label">
                        User ID:
                      </label>
                      <div class="col-xs-6 col-md-6">
                          <input type="text" class="form-control" name="inputuser_id" id="inputuser_id" placeholder="Enter User ID" value="<?php echo $basic_settings['inputuser_id']; ?>">    
                      </div>
                      <div class="col-xs-6 col-md-4" style="margin-left:-20px;margin-top:9px;">
                        <a  href="#"  data-toggle="tooltip" data-placement="right" 
                          title="Enter the user id of the person you would like to use. If your using the multiuser mode, you can put multiple userids separated with commas. ">      
                            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                        </a>
                      </div>
                  </div>

              <?php //} ?>

              <div class="form-group <?php if( 'tag' == $basic_settings['options_mode'] ) { echo 'show'; } else { echo 'hidden'; }?>" id="action_tag">
                  <label for="inputTag" class="col-sm-2 control-label">
                    Tag:
                  </label>
                  <div class="col-xs-6 col-md-6">
                      <input type="text" class="form-control" name="input_tag" id="input_tag" placeholder="Enter Tag Name" value="<?php echo $basic_settings['input_tag']; ?>">    
                  </div>
                  <div class="col-xs-6 col-md-4" style="margin-left:-20px;margin-top:9px;">
                    <a  href="#"  data-toggle="tooltip" data-placement="right" 
                      title="Enter the tag that you would like to pull from the Instagram API. ">      
                        <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                    </a>
                  </div>
              </div>


              <div class="form-group <?php if( 'multiusers' == $basic_settings['options_mode'] ) { echo 'show'; } else { echo 'hidden'; }?>" id="action_multiusers">
                  <label for="inputTag" class="col-sm-2 control-label">
                    Multi User ID:
                  </label>
                  <div class="col-xs-6 col-md-6">
                      <input type="text" class="form-control" name="input_multi_user_id" id="input_multi_user_id" placeholder="123456,654789,85236" value="<?php echo $basic_settings['input_multi_user_id']; ?>">    
                  </div>
                  <div class="col-xs-6 col-md-4" style="margin-left:-20px;margin-top:9px;">
                    <a  href="#"  data-toggle="tooltip" data-placement="right" 
                      title="Enter the user id of the person you would like to use. If your using the multiuser mode, you can put multiple userids separated with commas. ">      
                        <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                    </a>
                  </div>
              </div>

              <div class="form-group <?php if( 'location' == $basic_settings['options_mode'] ) { echo 'show'; } else { echo 'hidden'; }?>" id="action_location">
                  <label for="inputTag" class="col-sm-2 control-label">
                    Location ID:
                  </label>
                  <div class="col-xs-6 col-md-6">
                      <input type="text" class="form-control" name="input_location_id" id="input_location_id" placeholder="Enter Location ID" value="<?php echo $basic_settings['input_location_id']; ?>">    
                  </div>
                  <div class="col-xs-6 col-md-4" style="margin-left:-20px;margin-top:9px;">
                    <a  href="#"  data-toggle="tooltip" data-placement="right" 
                      title="Enter the user id of the person you would like to use. If your using the multiuser mode, you can put multiple userids separated with commas. ">      
                        <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                    </a>
                  </div>
              </div>



              <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">
                    Gallery Mode:
                  </label>
                  <div class="col-xs-6 col-md-6">
                      <div class="radio">
                          <label>
                            <input type="radio" name="optionsGallery" id="optionsFullscreen" value="Fullscreen" <?php if( empty ($basic_settings['optionsGallery'])) { ?> checked <?php } ?> <?php if( 'Fullscreen' == $basic_settings['optionsGallery']) { ?> checked <?php } ?>>
                            Fullscreen
                          </label>
                      </div>
                      <div class="radio">
                          <label>
                            <input type="radio" name="optionsGallery" id="optionsThumbnails" value="Thumbnails" <?php if( 'Thumbnails' == $basic_settings['optionsGallery']) { ?> checked <?php } ?>>                              
                              Thumbnails
                          </label>
                      </div>

                      <div class="radio">
                          <label>
                            <input type="radio" name="optionsGallery" id="optionsList" value="List" <?php if( 'List' == $basic_settings['optionsGallery']) { ?> checked <?php } ?>>
                              List
                          </label>
                      </div>                    
                  </div>
                <div class="col-xs-6 col-md-4" style="margin-left:-20px;margin-top:30px;">
                      <a  href="#"  data-toggle="tooltip" data-placement="right" 
                        title="This feature allows you to choose between different layout modes. ">      
                          <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                      </a>
                </div>
              </div>


              <div class="form-group">
                  <label for="inputTag" class="col-sm-2 control-label">
                    Gallery Fullscreen Photos HTML:
                  </label>
                  <div class="col-xs-6 col-md-6">
                      <textarea class="form-control" name="html_gallery" rows="5"><?php echo stripslashes($basic_settings['html_gallery']); ?></textarea>    
                  </div>
                  <div class="col-xs-6 col-md-4" style="margin-left:-20px;margin-top:9px;">
                    <a  href="#"  data-toggle="tooltip" data-placement="right" 
                      title='Enter your images here in html format. For example:
                              <img title="Title here" rel="Caption Here" date-created="Date Here" src="imagepathhere" ></div>'>      
                        <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                    </a>
                  </div>
              </div>


              
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-6">
                    <div class="alert alert-success hidden" role="alert" id="success_msg">...</div>
                </div>
              </div>


              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  
                  <button type="button" class="btn btn-primary btn-lg" id="SaveBasicSettings">Save Settings</button>
                </div>
              </div>
            </form>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
            <span class="glyphicon glyphicon-plus"></span>
            Advanced Settings
          </a>
        </h4>
      </div>
      <div id="collapseTwo" class="panel-collapse collapse">
        <div class="panel-body">
          <form class="form-horizontal" name="advanced_settings_form" id="advanced_settings_form">
           <div class="form-group">
                <label for="inputSpeed" class="col-sm-2 control-label">
                  Transition Speed:
                </label>
                <div class="col-xs-6 col-md-6">
                    <input type="text" class="form-control" name="transition_speed" id="transition_speed" placeholder="Enter Transition Speed" value="<?php echo $advanced_settings['transition_speed']?>">    
                </div>
                <div class="col-xs-6 col-md-4" style="margin-left:-20px;margin-top:9px;">
                  <a  href="#"  data-toggle="tooltip" data-placement="right" 
                    title="Enter your transition speed(in milliseconds). This controls the speed in which your photos appear. The default is 700.">      
                      <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                  </a>
                </div>
          </div> 

          <div class="clear"></div>

          <div class="form-group">
                <label for="inputDelay" class="col-sm-2 control-label">
                  Transition Delay Interval:
                </label>
                <div class="col-xs-6 col-md-6">
                    <input type="text" class="form-control" name="transition_delay" id="transition_delay" placeholder="Enter Transition Delay Interval" value="<?php echo $advanced_settings['transition_delay']?>">    
                </div>
                <div class="col-xs-6 col-md-4" style="margin-left:-20px;margin-top:9px;">
                  <a  href="#"  data-toggle="tooltip" data-placement="right" 
                    title="Enter your delay interval. Sets the interval of the delay between photos appearing. The default is 80.">      
                      <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                  </a>
                </div>
          </div>


          <div class="form-group">
                <div class="col-sm-offset-2 col-sm-6">
                    <div class="alert alert-success hidden" role="alert" id="advanced_success_msg">...</div>
                </div>
          </div>


          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              
              <button type="button" class="btn btn-primary btn-lg" id="SaveAdvancedSettings">Save Advanced Settings</button>
            </div>
          </div>

        </form>  


        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  jQuery( '#SaveBasicSettings' ).click(function() {
    var Form_data = jQuery('#basic_settings_form').serializeArray()
    //console.log('Form_data', Form_data[1].name)
    //console.dir( Form_data);
    /*var objects = {};
    var collection = new Object();
    for(var i=0; i < Form_data.length; i++){
          var e = Form_data[i];
          if(e.value == '') {
            val = 'null';
          } else {
            val = e.value;
          }          
          //console.log(e.name+"="+e.value);
          //objects[i] = { key: val};
          //e.name+ ":" + val + ",";
          //fatch_data.push ( e.name+ ":" + val + ",");         
      }*/
      //console.dir(Form_data);
    var get_Data = {
        'access_token':   Form_data[1].value,
        'options_mode':   Form_data[2].value,
        'inputuser_id':   Form_data[3].value,
        'input_tag':      Form_data[4].value,
        'input_multi_user_id':   Form_data[5].value,
        'input_location_id':   Form_data[6].value,
        'optionsGallery':   Form_data[7].value,
        'html_gallery':   Form_data[8].value
    };



    jQuery.ajax({
            url: "<?php echo admin_url( 'admin-ajax.php' )?>",
            type: 'POST',
            dataType: 'json',
            data:{ action: 'wip_basic_settings', get_Data },
            beforeSend: function( xhr ){

            },
            success: function( resp ){
                console.dir( resp );
                // success
              if( resp.status == 'success' ){                
                jQuery('div#success_msg').text(resp.message);
                jQuery('#success_msg').removeClass( "hidden" ).addClass('show');
              }
            }
    });

  });


  jQuery( '#SaveAdvancedSettings' ).click(function() {
    var Form_data = jQuery('#advanced_settings_form').serializeArray()

    console.dir(Form_data);

      var get_Data = {
          'transition_speed':   Form_data[0].value,
          'transition_delay':   Form_data[1].value        
      };

      jQuery.ajax({
            url: "<?php echo admin_url( 'admin-ajax.php' )?>",
            type: 'POST',
            dataType: 'json',
            data:{ action: 'wip_advanced_settings', get_Data },
            beforeSend: function( xhr ){

            },
            success: function( resp ){
                console.dir( resp );
                // success
              if( resp.status == 'success' ){                
                jQuery('div#advanced_success_msg').text(resp.message);
                jQuery('#advanced_success_msg').removeClass( "hidden" ).addClass('show');
              }
            }
      });
  });

</script>
