<?php

	$check_admin=get_userdata($current_user->ID);

		$gascount=0;
		$washcount=0;
	if($check_admin->roles[0]=='administrator'){
 		/* query for get all helps of current user*/
		$booking= $wpdb->get_results("SELECT hs.id as id,hs.start as start,es.service_name as title FROM 
											wp_user_book_event_service hs 
										left Join 
											wp_user_event_service es
										on
											hs.event_id=es.event_id
									
										
									");
		$booking=json_encode($booking);
	}
	else{
		$booking= $wpdb->get_row("SELECT hs.id as id,hs.start as start,es.service_name as title,hs.vehicle,hs.time FROM 
											wp_user_book_event_service hs 
										left Join 
											wp_user_event_service es
										on
											hs.event_id=es.event_id
									
										WHERE 
												hs.user_id = '$current_user->ID'
										and 
												hs.id = '$id'
									");
		$sbooking=json_encode($booking);
		$vehicles= $wpdb->get_results("SELECT * FROM wp_vehicle_info  WHERE user_id = '$current_user->ID'");
				
		
	}
	?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<link href='<?=RG_PLUGIN_ASSETS_URL ?>lib/fullcalendar.min.css' rel='stylesheet' />
<link href='<?=RG_PLUGIN_ASSETS_URL ?>lib/fullcalendar.print.css' rel='stylesheet' media='print' />
<link href='<?=RG_PLUGIN_ASSETS_URL ?>scheduler.min.css' rel='stylesheet' />
<script src='<?=RG_PLUGIN_ASSETS_URL ?>lib/moment.min.js'></script>

<script src='<?=RG_PLUGIN_ASSETS_URL ?>lib/jquery.min.js'></script>
<script src='<?=RG_PLUGIN_ASSETS_URL ?>lib/jquery-ui.min.js'></script>
<script src='<?=RG_PLUGIN_ASSETS_URL ?>lib/fullcalendar.min.js'></script>
<script src='<?=RG_PLUGIN_ASSETS_URL ?>scheduler.min.js'></script>
<!-- select to -->
<link href='<?=RG_PLUGIN_ASSETS_URL ?>css/bootstrap-datetimepicker.css' rel='stylesheet' />
<script src='<?=RG_PLUGIN_ASSETS_URL ?>js/bootstrap-datetimepicker.js'></script>
<!-- end select to -->
<script>
	 
	$(function() { // document ready


		/* initialize the external events
		-----------------------------------------------------------------*/

		$('#external-events .fc-event').each(function() {

			// store data so the calendar knows to render an event upon drop
			$(this).data('event', {
				title: $.trim($(this).text()), // use the element's text as the event title
				event_id: $.trim($(this).attr('event_id')), 
				uni_event:$.trim($(this).attr('uni_event')) ,
				stick: true // maintain when user navigates (see docs on the renderEvent method)
			});

			// make the event draggable using jQuery UI
			$(this).draggable({
				zIndex: 999,
				revert: true,      // will cause the event to go back to its
				revertDuration: 0  //  original position after the drag
			});

		});


		/* initialize the calendar
		-----------------------------------------------------------------*/
		var order_id = 1;

		$('#calendar').fullCalendar({
			now: '<?=date("Y-m-d")?>',
			editable: true, // enable draggable events
			droppable: true, // this allows things to be dropped onto the calendar
			aspectRatio: 1.8,
			scrollTime: '00:00', // undo default 6am scrollTime
			header: {
				left: 'today prev,next',
				center: 'title',
				right: 'month'
			},
			defaultView: 'month',
			views: {
				timelineThreeDays: {
					type: 'timeline',
					duration: { days: 3 }
				}
			},
			eventConstraint: {
            	start: moment().format('YYYY-MM-DD'),
            	end: '2100-01-01' // hard coded goodness unfortunately
        	},
			events: 
				[<?=$sbooking?>],
			drop: function(date, jsEvent, ui, resourceId) {
				//console.log('drop', date.format(), jsEvent);
				//$post
				// is the "remove after drop" checkbox checked?
				//if ($('#drop-remove').is(':checked')) {
					// if so, remove the element from the "Draggable Events" list
					$(this).remove();
				//}
			},
			eventReceive: function(event) { // called when a proper external event is dropped
				//console.log('eventReceive', event);
				console.log('eventReceive', event.uni_event);
				
				
			},
			
			eventDrop: function(event) { // called when an event (already on the calendar) is moved
				// console.log('eventDrop', event);
				//console.log('eventDrop', event.id);
				//console.log('eventDrop', event.id);
				
					$(document).find('.start').val(event.start.format());
					$('.extrainto').show();
				
				
			}
		});

	});
$('.datetimepicker').datetimepicker({
              				// datepicker:false,
      							format:'H'
            			});
</script>
<style>

	
	#external-events {
		
		
		padding: 0 10px;
		border: 1px solid #ccc;
		background: #eee;
		text-align: left;
	}
		
	#external-events h4 {
		font-size: 16px;
		margin-top: 0;
		padding-top: 1em;
	}
		
	#external-events .fc-event {
		margin: 10px 0;
		cursor: pointer;
	}
		
	#external-events p {
		margin: 1.5em 0;
		font-size: 11px;
		color: #666;
	}
		
	#external-events p input {
		margin: 0;
		vertical-align: middle;
	}

	#calendar {
		
		
	}

</style><!-- Main Container Start -->
	<div class="container">
			<!-- .Page heading -->
	  		<h2>Schedule</h2>
	  		<h4>Step 1: Choose date</h4>
	  		<!-- Link for add New vehicles -->
	  		<div class="col-md-12 col-sm-12"> 
	  		<div id='wrap' >
	  			
				<div id='calendar' class="col-md-12 col-sm-12 table-responsive"></div>

				<div style='clear:both'></div>

			</div>	
			</div>
			<form method="post">
				<div class="row extrainto" style="display:none">
					<h4>Step 2 : Please fill Extra Info</h4>
					<div class="col-md-4">
						<?php
							echo "<input type='hidden' class='start' name='event[start]' value='$booking->start'>";
							echo "<label>Enter  Service Time</label>";
							echo "<br>";
							echo "<input type='text' name='event[time]' class='datetimepicker' value='$booking->time' required>";
							echo "<br>";
							echo "<br>";
							$vehicles= $wpdb->get_results("SELECT * FROM wp_vehicle_info  WHERE user_id = '$current_user->ID'");
							echo "<label>Select  Vechicle</label>";
							echo "<br>";
							echo "<select name='event[vehicle]'  required>";

							foreach ($vehicles as $vehicle) {
								if($booking->vehicle==$vehicle->id)
									echo "<option selected value='$vehicle->id'>$vehicle->lpm</option>";
								else
									echo "<option value='$vehicle->id'>$vehicle->lpm</option>";
							}
				
				
							echo '</select>';
							echo "<br>";
							echo "<br>";
							echo "<input type='submit' name='submit' value='save' />"
						?>
					</div>
				</div>
								<div class="clearfix"></div>
				
					<div class="clearfix"></div>
				<div class="row totalprice">
					
				</div>

				</div>
			</form>
		</div>
		<script>
		$('.datetimepicker').datetimepicker({
              				// datepicker:false,
      							format:'H'
            			});
</script>
		