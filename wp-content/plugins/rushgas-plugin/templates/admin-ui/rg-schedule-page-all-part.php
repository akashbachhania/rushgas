<?php
session_start();
unset($_SESSION['total']);
	$check_admin=get_userdata($current_user->ID);

		$gascount=0;
		$washcount=0;
	if($check_admin->roles[0]=='administrator'){
 		/* query for get all helps of current user*/
		$booking= $wpdb->get_results("SELECT hs.id as id,hs.start as start,es.service_name as title FROM 
											wp_user_book_event_service hs 
										left Join 
											wp_user_event_service es
										on
											hs.event_id=es.event_id
									
										
									");
		$booking=json_encode($booking);
	}
	else{
		$booking= $wpdb->get_results("SELECT hs.id as id,hs.start as start,es.service_name as title FROM 
											wp_user_book_event_service hs 
										left Join 
											wp_user_event_service es
										on
											hs.event_id=es.event_id
									
										WHERE 
												hs.user_id = '$current_user->ID'
									");
		$booking=json_encode($booking);
		$carwash= $wpdb->get_results("SELECT * FROM 
											wp_user_book_event_service hs 
									
									WHERE 
												hs.event_id = '1'
									");
	
		$getgas= $wpdb->get_results("SELECT * FROM 
											wp_user_book_event_service hs 
									
									WHERE 
												hs.event_id = '2'
									");
		$membship	=	$wpdb->get_row("SELECT * FROM 
											wp_pmpro_memberships_users hs 
									
									WHERE 
												hs.user_id 	= '$current_user->ID'
									AND 
												hs.status 	=	'active';	
									");
		if($membship->membership_id==1){
			$gascount	=2-count($getgas);
			$washcount	=2-count($carwash);
		}
		if($membship->membership_id==2){
			$gascount	=4-count($getgas);
			$washcount	=4-count($carwash);
		}
			$allservices= $wpdb->get_results("SELECT * FROM 
												wp_user_event_service ws ");
			$payments= $wpdb->get_results("SELECT * FROM wp_add_credit_card_stripe  WHERE user_id = '$current_user->ID'");

	}
	?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<link href='<?=RG_PLUGIN_ASSETS_URL ?>lib/fullcalendar.min.css' rel='stylesheet' />
<link href='<?=RG_PLUGIN_ASSETS_URL ?>lib/fullcalendar.print.css' rel='stylesheet' media='print' />
<link href='<?=RG_PLUGIN_ASSETS_URL ?>scheduler.min.css' rel='stylesheet' />
<script src='<?=RG_PLUGIN_ASSETS_URL ?>lib/moment.min.js'></script>
<script src='<?=RG_PLUGIN_ASSETS_URL ?>lib/jquery.min.js'></script>
<script src='<?=RG_PLUGIN_ASSETS_URL ?>lib/jquery-ui.min.js'></script>
<script src='<?=RG_PLUGIN_ASSETS_URL ?>lib/fullcalendar.min.js'></script>
<script src='<?=RG_PLUGIN_ASSETS_URL ?>scheduler.min.js'></script>
<script>

	$(function() { // document ready


		/* initialize the external events
		-----------------------------------------------------------------*/

		$('#external-events .fc-event').each(function() {

			// store data so the calendar knows to render an event upon drop
			$(this).data('event', {
				title: $.trim($(this).text()), // use the element's text as the event title
				event_id: $.trim($(this).attr('event_id')), 
				stick: true // maintain when user navigates (see docs on the renderEvent method)
			});

			// make the event draggable using jQuery UI
			$(this).draggable({
				zIndex: 999,
				revert: true,      // will cause the event to go back to its
				revertDuration: 0  //  original position after the drag
			});

		});


		/* initialize the calendar
		-----------------------------------------------------------------*/
		var order_id = 1;

		$('#calendar').fullCalendar({
			now: '2016-09-07',
			editable: false, // enable draggable events
			droppable: true, // this allows things to be dropped onto the calendar
			aspectRatio: 1.8,
			scrollTime: '00:00', // undo default 6am scrollTime
			header: {
				left: 'today prev,next',
				center: 'title',
				right: 'timelineDay,timelineThreeDays,agendaWeek,month'
			},
			defaultView: 'month',
			views: {
				timelineThreeDays: {
					type: 'timeline',
					duration: { days: 3 }
				}
			},
			events: 
				<?=$booking?>,
			drop: function(date, jsEvent, ui, resourceId) {
				console.log('drop', date.format(), jsEvent);
				//$post
				// is the "remove after drop" checkbox checked?
				//if ($('#drop-remove').is(':checked')) {
					// if so, remove the element from the "Draggable Events" list
					$(this).remove();
				//}
			},
			eventReceive: function(event) { // called when a proper external event is dropped
				console.log('eventReceive', event);
				console.log('eventReceive', event.start);
				
				//console.log('event_id', event_id);

					var data = {
								'action'	: 	'book_event_service',
								'type'		: 	'extra_service',
								'event_id'	: 	event.event_id,
								'start'	: 	event.start.format(),
								'order_id' 	: 	order_id
								 
								};

				// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
					$.post("<?=admin_url('admin-ajax.php')?>", data, function(response) {
						$('.extrainto').append('<div class="col-md-4 pull-left">'+response+'</div>');
						
					});
					order_id++;
					//$('.extrainto').append('<div class="col-md-4">test</div>');
			},
			eventDrop: function(event) { // called when an event (already on the calendar) is moved
				console.log('eventDrop', event);
			}
		});

	});

</script>
<style>

	
	#external-events {
		
		
		padding: 0 10px;
		border: 1px solid #ccc;
		background: #eee;
		text-align: left;
	}
		
	#external-events h4 {
		font-size: 16px;
		margin-top: 0;
		padding-top: 1em;
	}
		
	#external-events .fc-event {
		margin: 10px 0;
		cursor: pointer;
	}
		
	#external-events p {
		margin: 1.5em 0;
		font-size: 11px;
		color: #666;
	}
		
	#external-events p input {
		margin: 0;
		vertical-align: middle;
	}

	#calendar {
		
		
	}

</style><!-- Main Container Start -->
	<div class="container">
			<!-- .Page heading -->
	  		<h2>Schedule</h2>
	  		<h4>Step 1: Choose date and type of service</h4>
	  		<!-- Link for add New vehicles -->
	  		<div class="col-md-12 col-sm-12"> 
	  		<div id='wrap' >
	  			<?php if($check_admin->roles[0]!='administrator'):?>
				<div id='external-events' class="col-md-2 col-sm-12">
					<h4> Service</h4>
					<?php 
					foreach ($allservices as $service){
						if($service->event_id==1){		
							for ($i=0; $i < $washcount; $i++) { 
								echo "<div class='fc-event' event_id=' $service->event_id'> $service->service_name </div>";
							}
						}
						if($service->event_id==2){	
							for ($i=0; $i < $gascount; $i++) { 
								echo "<div class='fc-event' event_id=' $service->event_id'> Get Gas  </div>";
							}
						}

					}
					?>
					
				</div>
				<?endif;?>
				<div id='calendar' class="col-md-10 col-sm-12 table-responsive"></div>

				<div style='clear:both'></div>

			</div>	
			</div>
			<form method="post">
				<div class="row extrainto">
				</div>
								<div class="clearfix"></div>
				<div class="row selectpayment" style="display:none">
					<h2>Select Card for pay</h2>
					<div class="col-md-4 pull-left">
					
					<?php foreach ($payments as $payment) {
						echo "<input type='radio' name='payment' value='$payment->ID'>$payment->account_number" ;
					}?>
					</div>
					</div>
					<div class="clearfix"></div>
				<div class="row totalprice">
					
				</div>

				</div>
			</form>
		</div>
		<script>
		$(document).on('blur','.gallons',function(){
						
	
					var total=0;
					$('.gallons').each(function(){
       				 total=total+$(this).val()*$(this).attr('price_id');
       				});
       				$('.service').each(function(){
       					if($(this).prop('checked')){
       				 		total=total+1*$(this).attr('price_id');
       				 	}
       				});
					$('.selectpayment').show();
					$('.totalprice').html('<div class="col-md-4 pull-left">'+'<p> Total amout to pay:'+total+"</p><input type='submit' name='pay' />"+'</div>');
	
	});
	$(document).on('click','.service',function(){
			 
			
			
					
					var total=0;
					$('.gallons').each(function(){
       				 total=total+$(this).val()*$(this).attr('price_id');
       				});
       				$('.service').each(function(){
       					if($(this).prop('checked')){
       				 		total=total+1*$(this).attr('price_id');
       				 	}
       				});
					$('.selectpayment').show();
					$('.totalprice').html('<div class="col-md-4 pull-left">'+'<p> Total amout to pay:'+total+"</p><input type='submit' name='pay' />"+'</div>');
	
});
</script>