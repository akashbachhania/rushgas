<?php
#******************************************************************************
#                       Credit Card Payments Wordpress v3.0
#
#	Author: Convergine.com
#	http://www.convergine.com
#	Version: 3.0
#   (c) 2013 Convergine.com
#
#******************************************************************************
	# PLEASE DO NOT EDIT FOLLOWING LINES IF YOU'RE NOT SURE ------->
		if($show_services==1 && $ccpt_show_am_text==2){
			$query="SELECT * FROM ".$wpdb->prefix."ccpt_services WHERE ccpt_services_id =".$service;
			$result=mysql_query($query) or die(mysql_error()."<br>$query");
			$res=mysql_fetch_assoc($result);
			$amount = number_format($res['ccpt_services_price'],2, ".", "");
			$serviceName=$res['ccpt_services_title'];
		} else if($show_services==1 && $ccpt_show_am_text==1){
		        if(empty($amount)){
		            $query="SELECT * FROM ".$wpdb->prefix."ccpt_services WHERE ccpt_services_id =".$service;
		            $result=mysql_query($query) or die(mysql_error()."<br>$query");
		            $res=mysql_fetch_assoc($result);
		            $amount = number_format($res['ccpt_services_price'],2, ".", "");
		            $serviceName=$res['ccpt_services_title'];
		        }
		    }
		$continue = false;
		if(!empty($amount) && is_numeric($amount)){ 	
			$cctype = (!empty($_POST['cctype']))?strip_tags(str_replace("'","`",strip_tags($_POST['cctype']))):'';
			$ccname = (!empty($_POST['ccname']))?strip_tags(str_replace("'","`",strip_tags($_POST['ccname']))):'';
			$ccn = (!empty($_POST['ccn']))?strip_tags(str_replace("'","`",strip_tags($_POST['ccn']))):'';
			$exp1 = (!empty($_POST['exp1']))?strip_tags(str_replace("'","`",strip_tags($_POST['exp1']))):'';
			$exp2 = (!empty($_POST['exp2']))?strip_tags(str_replace("'","`",strip_tags($_POST['exp2']))):'';
			$cvv = (!empty($_POST['cvv']))?strip_tags(str_replace("'","`",strip_tags($_POST['cvv']))):'';
			
			
			
			//CREDIT CARD PHP VALIDATION 
			if(empty($ccn) || empty($cctype) || empty($exp1) || empty($exp2) || empty($ccname) || empty($cvv) || empty($address) || empty($state) || empty($city)){
				$continue = false;
				$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> Not all required fields were filled out.</p></div></div><br />';
			} else { $continue = true; }
			
			if(!is_numeric($cvv)){
				$continue = false;
				$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> CVV number can contain numbers only.</p></div></div><br />';
			} else {
				$continue = true;
			}
			
			if(!is_numeric($ccn)){
				$continue = false;
				$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> Credit Card number can contain numbers only.</p></div></div><br />';
			} else {
				$continue = true;
			}
			
			if(date("Y-m-d", strtotime($exp2."-".$exp1."-01")) < date("Y-m-d")){
				$continue = false;
				$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> Your credit card is expired.</p></div></div><br />';
			} else {
				$continue = true;
			}
			
			if($continue){
				//echo "1";
				if(validateCC($ccn,$cctype)){
					$continue = true;
				} else { 
					$continue = false;
					$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> The number you\'ve entered does not match the card type selected.</p></div></div><br />';
				}
			}
			
			if($continue){
				if(luhn_check($ccn)){
					$continue = true;
				} else { 
					$continue = false; 
					$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> Invalid credit card number.</p></div></div><br />';
				}
			}
			
			switch($cctype){
				case "V":
					$cctype = "VISA";
				break;
				case "M":
					$cctype = "MASTERCARD";
				break;
				case "D":
					$cctype = "DISCOVER";
				break;
				case "A":
					$cctype = "AMEX";
				break;
			}
		}
			
				
			###########################################################################
				###	interetSewure PROCESSING
				###########################################################################
				//PROCESS PAYMENT BY WEBSITE PAYMENTS PRO
				
							
			 $a = new internetSequre();
			 
			 if($a->before_process()===false){
				
				 if($continue){     
						$query="INSERT INTO ".$wpdb->prefix."ccpt_transactions (ccpt_dateCreated, ccpt_amount, ccpt_comment,ccpt_serviceID ) VALUES (NOW(), '".$amount."', '".$item_description."', '".$service."')";
						if(mysql_query($query)){							
						
							$orderID=mysql_insert_id();
							$_SESSION['orderID'] = $orderID;
						}				 
										//adding products
                                        //  array(
                                        //          0=>array('name'=>'Product Name','final_price'=>'Total Price','qty'=>'Quantity','id'=>'Product Id'),
                                        //          1=>array('name'=>'Product Name','final_price'=>'Total Price','qty'=>'Quantity','id'=>'Product Id')..
                                        //      )
                                        $prodArray = array(
                                            array('name' => 'Anmount', 'final_price' => $amount, 'qty' => '1', 'id' => $orderID)
                                        );
                                        $a->add_products($prodArray);


                                        //billing info
                                        $a->add_field('xxxName', $fname . " " . $lname);
                                        //$a->add_field('xxxCompany', $value);
                                        $a->add_field('xxxAddress',$address);
                                        $a->add_field('xxxCity', $city);
                                        $a->add_field('xxxProvince', $state);
                                        $a->add_field('xxxPostal', $zip);
                                        $a->add_field('xxxCountry', $country);
                                        //$a->add_field('xxxPhone', $value);
                                        $a->add_field('xxxEmail', $email); 

                                        

                                        //cart info
                                        $a->add_field('xxxCard_Number', $ccn);
                                        $a->add_field('xxxCCMonth', $exp1);
                                        $a->add_field('xxxCCYear', $exp2);
                                        $a->add_field('CVV2', $cvv);
                                        $a->add_field('CVV2Indicator', '1');
                                        $a->add_field('xxxTransType', '00');
                                        $a->add_field('xxxAuthentication', 'Y');
						
						
						// Process the payment and output the results
						$response=$a->process();
					}
				}
					
					if($a->success){
						
						//$resArray["TRANSACTIONID"] = $a->transactionID; is already herefgdffgd
						
						$my_status="<br/><div>Transaction Successful!<br/>";
						$my_status .="Thank you for your payment<br /><br />";
						$my_status .= "You will receive confirmation email within 5 minutes.<br/><br/></div><br/>";
						$error=0;
						$mess = '<div class="ui-widget"><div class="ui-state-highlight ui-corner-all" style="padding: 0 .7em;">'.$my_status.'</div></div><br />';
						#**********************************************************************************************#
						#		THIS IS THE PLACE WHERE YOU WOULD INSERT ORDER TO DATABASE OR UPDATE ORDER STATUS.
						#**********************************************************************************************#
							
						$q="UPDATE ".$wpdb->prefix."ccpt_transactions SET ccpt_status='2', ccpt_payer_email='".$email."', ccpt_transaction_id='{$_SESSION['ReciptNumber']}', ccpt_payer_name='".$ccname."' WHERE ccpt_id='".$_SESSION['orderID']."'";
							mysql_query($q);
							
						#**********************************************************************************************#
						//-----> send notification 
						//creating message for sending
						$headers  = "MIME-Version: 1.0\n";
						$headers .= "Content-type: text/html; charset=utf-8\n";
						$headers .= "From: 'InternetSequre Payment Terminal' <noreply@".$_SERVER['HTTP_HOST']."> \n";
						$subject = "New Payment Received";
						$message =  "New payment was successfully received through authorize.net <br />";					
						$message .= "from ".$fname." ".$lname."  on ".date('m/d/Y')." at ".date('g:i A').".<br /> Payment total is: $".number_format($amount,2);
						if($show_services){
							$message .= "<br />Payment was made for \"".$services[$service][0]."\"";
						} else { 
							$message .= "<br />Payment description: \"".$item_description."\"";
						}
						$message .= "<br /><br />Billing Information:<br />";
						$message .= "Full Name: ".$fname." ".$lname."<br />";
						$message .= "Email: ".$email."<br />";
						$message .= "Address: ".$address."<br />";
						$message .= "City: ".$city."<br />";
						$message .= "Country: ".$country."<br />";
						$message .= "State/Province: ".$state."<br />";
						$message .= "ZIP/Postal Code: ".$zip."<br />";
						mail($admin_email,$subject,$message,$headers);
						
						$subject = "Payment Received!";
						$message =  "Dear ".$fname.",<br />";
						$message .= "<br /> Thank you for your payment.";
						$message .= "<br /><br />Kind Regards,<br />".$_SERVER['HTTP_HOST'];	
						mail($email,$subject,$message,$headers);
						
						//-----> send notification end 	
						$show_form=0;
						
					}else{
						//$mess = $a->dump_response();
						
							//$a->addError($a->dump_response());
							$mess .= $a->getError();
						
					}
				 
		  
	# END OF PLEASE DO NOT EDIT IF YOU'RE NOT SURE	
?>