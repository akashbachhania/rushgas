��    �     �  w  l      H'     I'     \'     d'     i'     �'     �'     �'     �'     �'     �'     �'     
(  
   &(  /   1(  .   a(     �(     �(     �(  
   �(  |  �(  @   e*  ^   �*  1   +  Q   7+  Y   �+  ]   �+  V   A,  R   �,  b   �,  V   N-     �-     �-     �-     �-     �-  d   �-     S.     p.  
   �.     �.  	   �.     �.     �.  $   �.     �.     �.     /     /     %/  	   2/  *   </     g/     n/  
   r/     }/     �/     �/     �/     �/     �/     �/  �   �/     O0     \0     q0  C   �0     �0  @  �0     2     2     *2     D2     K2     g2     p2     �2     �2     �2     �2     �2  %   �2  	   �2     �2     3     3     3  	   3     3     &3     33  �   @3     �3  	   �3     �3     �3     �3     4     4  +   #4  ;   O4     �4    �4     �5     �5     �5     �5     �5     �5     �5  	   6     6     6     6  �   6    7    8  �   9    �9  �   ;  	   <  &   <     4<  !   I<  !   k<      �<     �<     �<     �<     �<     �<  	    =  	   
=     =     =     %=  %   2=     X=  2   `=     �=  	   �=     �=     �=     �=     �=     �=     �=     >     
>     >     ">  (   =>     f>     r>     �>  t   �>     ?     ?     &?  :   9?     t?     �?     �?     �?     �?  S   �?  M   7@     �@     �@     �@     �@     �@     �@     �@  	   �@  I   �@     0A     @A     OA  	   TA     ^A  i   fA     �A     �A  �   �A  �   �B  �   dC  9   .D  �   hD  �   �D  �   �E  7   �F     �F     �F     �F     �F     G     (G     CG  �   ]G  >   H  �   QH  �   4I  �   �I  �   �J  J   "K  {   mK  t   �K     ^L  ,   eL  �   �L  6   6M     mM     �M     �M     �M     �M  X   �M     7N     DN     YN     hN     wN     �N  
   �N     �N     �N  
   �N     �N     �N     �N     �N  m   �N     _O  =   gO  )   �O     �O     �O     �O     �O     �O     P     'P     4P     AP  
   FP  
   QP     \P     lP     �P     �P  -   �P  	   �P  '   �P  +    Q     ,Q      IQ     jQ  �   oQ     :R     @R  0   QR  2   �R  A   �R  ;   �R  6   3S     jS  $   xS     �S     �S     �S     �S     �S     �S     �S     �S     T     T     T     3T     ;T     DT     KT     `T     fT     }T  #   �T  �   �T  e   AU    �U     �V     �V     �V  "   �V  #   W     ?W  *   WW     �W     �W  
   �W     �W     �W     �W  	   �W     �W     �W     �W     �W     X     X     ,X     5X     BX     UX     eX     rX     ~X     �X     �X     �X     �X     �X     �X     �X  E  �X     6Z     VZ     cZ     hZ     uZ  
   �Z     �Z     �Z  
   �Z  &   �Z     �Z     �Z     �Z     [     [  P   $[  �   u[     \     +\     9\     U\     n\     |\     �\     �\     �\     �\     �\     �\     �\  �   �\  �   Z]     �]     �]     ^     ^     ,^     H^  	   c^  h   m^     �^     �^     �^  
   �^     _     _     &_  G   ._     v_     �_     �_  	   �_  
   �_  `   �_  �   `  2   �`  6   �`  X   *a  0   �a  /   �a  '   �a  J   b  6   Wb  ,   �b  +   �b  
   �b     �b     �b     c     c     c  ;   c  Z   Tc     �c     �c     �c     �c  7   �c     d     d     'd     0d     7d  a  Nd     �e     �e     �e  �   �e     �f     �f     �f  "  �f     �g     �g  	   �g  �   �g  �   �h  j   i  �   �i  �   j  !   �j  $   k     8k     Uk  �  bk  .   �l  "   )m     Lm  	   km     um     �m     �m     �m     �m  $   �m  =   �m     .n     Dn     Zn     nn  
   n     �n     �n     �n     �n     �n     �n     �n     �n      o     o     'o     7o     Fo     Uo     fo     so     �o     �o     �o     �o  J   �o     p     %p  k  8p     �r     �r     �r  %   �r     �r     s     s     s  !   5s      Ws     xs  %   �s     �s  /   �s  5   �s     0t     Nt     _t     ~t  �  �t  ?   :v  a   zv  /   �v  a   w  a   nw  d   �w  j   5x  b   �x  i   y  a   my     �y     �y     �y     �y     z  i   &z     �z  !   �z     �z     �z     �z     {     {  %   ({     N{     `{     m{     �{     �{     �{  (   �{     �{     �{     �{  
   �{     
|     |  
   "|     -|     ?|     L|  �   S|     �|     �|     }  @   *}  	   k}  t  u}     �~     �~     �~          $  
   B     M     i  	   �     �  
   �     �  .   �     �     �     	�     �     �  	   �     '�     4�     A�  �   N�  	   ��  	   �     �     �     �     +�     8�  -   G�  R   u�     ȁ  O  Ձ     %�     -�     ?�     K�     ]�     n�  
   t�     �     ��     ��     ��  �   ��    ��  �   ��  �   ��     }�  �   ~�     u�  $   |�     ��  ,   ��  *   �  *   �  	   7�     A�     Q�     b�     r�     ��     ��     ��     ��     ��     Ǌ     �  1   �      �  
   &�     1�     6�     F�     Z�     a�     q�  
   ��     ��     ��     ��  .   ΋     ��     �     &�  x   4�     ��     ��     Ԍ  ?   �     ,�     =�  (   B�     k�     ��  H   ��  Q   �     ;�     J�     S�     f�     n�     ��     ��  	   ��  B   ��     �     ��     �     �      �  n   )�     ��     ��  �   Ï  �   k�  �   U�  :   5�  �   p�  �   �  �   ��  8   ܔ     �     &�  	   7�     A�     V�     r�     ��  �   ��  R   Z�    ��  �     �   ��  �   t�  R   �  v   Y�  �   К  	   _�  @   i�  �   ��  D   r�     ��     ֜     �     �     �  g   +�     ��     ��     ��     ȝ     ם     �     ��     �  +   �  	   C�     M�     T�  	   [�     e�  m   l�     ڞ  @   �  .   #�     R�     W�     h�     u�     ��     ��     ��     ��     ϟ     ؟     �     ��     �     �     :�  :   G�     ��  =   ��  9   Ϡ     	�  #   (�     L�  �   T�     <�     E�  3   [�  .   ��  E   ��  8   �  0   =�     n�  )   {�     ��     ��     ʣ     Σ     ݣ     �     �     ��  
   �  
   �  #   �  
   A�  
   L�     W�     _�     w�     ��     ��  "   ��  �   ��  Y   l�    ƥ     �     ��     �  0   *�  &   [�     ��     ��     ��     ��  
   ǧ     ҧ  	   ڧ     �     �     ��     �     �     #�     5�     M�  
   \�     g�     s�     ��     ��     ��     ��     Ϩ     �  	   ��     �     �     #�     /�  U  =�     ��     ��     ��     Ǫ     ۪     �     ��     �     �  )   $�     N�     f�     x�     ��     ��  X   ��  �   �     Ѭ     �     ��     �     3�     B�     R�  
   `�  	   k�  
   u�     ��     ��     ��  �   ��  �   p�     �     �  #   -�     Q�  !   d�     ��     ��  V   ��  
   ��  
   	�     �      �     .�     K�  	   T�  J   ^�  !   ��     ˰     �     �  
   �  f   ��  �   d�  3   �  ;   C�  M   �  -   Ͳ  1   ��  )   -�  b   W�  =   ��  -   ��  2   &�  
   Y�     d�     i�     z�     ��     ��  C   ��  ^   д     /�     7�     O�     W�  G   \�     ��     ��     ɵ  	   ֵ     �  v  ��     v�  	   ��     ��  �   ��  "   v�     ��     ��  H  ��     �     ��     ��  �   �  �   ��  x   6�  �   ��  �   T�      �      ;�     \�     s�  �  |�  ,   .�     [�     w�     ��     ��     ��     ¿     ߿     �  4   ��  R   ,�     �     ��     ��     ��     ��     ��     �  
   �  
   "�     -�     @�     R�     n�     ��     ��     ��     ��     ��     ��     ��  &   �     ,�     8�     J�     _�  W   c�     ��     ��     �   j          �   �   O  i   �   v       r  �   �       [   y     d          7  �   �   �   �      S   �  D  _   �  R    �          �      �   �   �  �      �   G   �   �       &          f  �     �      /        M  �   �                  �   Q  �   �   �   |   �   �   ;  �      �   �  �      �      n   �          �   �     �   "       w      �   +  �     �  �                   }  �       '   �   �       `   �   �   �       8  �  ,   �   �   _  �   6              �   B   �           ~      �  7   �      �  |  �   {          �       a  �  N   @       X      P  U         �  ^   �      �      .  �  Y         q     �  �       �   �  H  :       j       z   �   �       �       �     �         �   �       �  V  �      �  ]  �          !  8       �   H   �           �   �  �   t   }       @  "  6   T          �  �   *      �  f   4  1      �   �   (       u           w   �       P       �  �   +           )  �   L  O   4           �  E    :    $      l   s   �   �               �             E   #                 �      �    �  >     Y         �      �  �   L       �   �  �   �       s  3   G  �  B                 �   �   2           x            Z  n  �      �   g   �   �              �   p  �   �  y  K  �  2    a   �      {  �   �   [      �       V       -   �      �  �  %      ?   �       �           h      �   �   �          �   �   �  o   �   �   #               Q       J  K   %       S      �   �   �   C   �  h   �   �  9        U              r      g  .   �          1          �  A  m   9   <   �  D             ]     '  �   J      �                         �   �  &   �  3      k  �     x   C  $   	          �       5          	   ?              W  �  0   �           d   m  �   �  /   z  �  ~   �   e  �  �   Z   �   b  �           �   I  e   ^  )   �    t  �  =  �      �   �   W   (  �  k         �  >  �   �  c   v  �  �           ,  �  �  �           �       F      �   �       �       0      5    =   p   !   �       i      �       �      -      �   �   �           �  \  �  R   �       N    A       �  �  I   �   q   �      �  �    F       �  b   o  �  �  �   ;   �         �   l  `  u  <  �           \   X  �              �      �       �   c      T  �   *       �   �     �      
      M      
   �      �     No staff selected  Reset   to  "I'm available on …" block %d h %d min * Invalid email * Invalid phone number * Please select a service * Please tell us your email * Please tell us your name * Please tell us your phone * Required * The start time must be less than the end time * This coupon code is invalid or has been used * This email is already in use -- Search customers -- -- Select a service -- 2 way sync <b>Important!</b> Please be aware that if your copy of Bookly was not downloaded from Codecanyon (the only channel of Bookly distribution), you may put your website under significant risk - it is very likely that it contains a malicious code, a trojan or a backdoor. Please consider buying a licensed copy of Bookly <a href="http://booking-wp-plugin.com" target="_blank">here</a>. <b>Leave this field empty</b> to work with the default calendar. <b>[[CATEGORY_NAME]]</b> - name of category, <b>[[NUMBER_OF_PERSONS]]</b> - number of persons. <b>[[NUMBER_OF_PERSONS]]</b> - number of persons. <b>[[SERVICE_NAME]]</b> - name of service, <b>[[STAFF_NAME]]</b> - name of staff, <b>[[SERVICE_PRICE]]</b> - price of service, <b>[[CATEGORY_NAME]]</b> - name of category, <b>[[SERVICE_PRICE]]</b> - price of service, <b>[[TOTAL_PRICE]]</b> - total price of booking, <b>[[SERVICE_TIME]]</b> - time of service,  <b>[[SERVICE_DATE]]</b> - date of service, <b>[[STAFF_NAME]]</b> - name of staff,  <b>[[SERVICE_NAME]]</b> - name of service, <b>[[TOTAL_PRICE]]</b> - total price of booking, <b>[[NUMBER_OF_PERSONS]]</b> - number of persons. <h3>The selected time is not available anymore. Please, choose another time slot.</h3> API Login ID API Password API Signature API Transaction Key API Username Accept <a href="javascript:void(0)" data-toggle="modal" data-target="#ab-tos">Terms & Conditions</a> Add Bookly appointments list Add Bookly booking form Add Coupon Add Service Add money Address Administrator phone Administrator phone number is empty. All Services All customers All payment types All providers All services All staff Allow staff members to edit their profiles Amount Any Appearance Apply Appointment Appointment Date Appointments Are you sure? Avatar Back Below you can find a list of available time slots for [[SERVICE_NAME]] by [[STAFF_NAME]].
Click on a time slot to proceed with booking. Booking Time Booking cancellation Booking product Bookly: You do not have sufficient permissions to access this page. Business hours By default Bookly pushes new appointments and any further changes to Google Calendar. If you enable this option then Bookly will fetch events from Google Calendar and remove corresponding time slots before displaying the second step of the booking form (this may lead to a delay when users click Next at the first step). Calendar Calendar ID Calendar ID is not valid. Cancel Cancel appointment page URL Capacity Card Security Code Cart item data Category Change password Checkbox Checkbox Group Click on the underlined text to edit. Client ID Client secret Code Codes Columns Comma (,) Company Company logo Company name Configure what information should be places in the title of Google Calendar event. Available codes are [[SERVICE_NAME]], [[STAFF_NAME]] and [[CLIENT_NAMES]]. Connect Connected Cost Country Country out of service Coupon Coupons Create WordPress user account for customers Create a product in WooCommerce that can be placed in cart. Create customer Create your project's OAuth 2.0 credentials by clicking <b>Create new Client ID</b>, selecting <b>Web application</b>, and providing the information needed to create the credentials. For <b>AUTHORIZED REDIRECT URIS</b> enter the <b>Redirect URI</b> found below on this page. Credit Credit Card Number Currency Custom Fields Custom Range Customer Customer Name Customers Date Day Days off Dear [[CLIENT_NAME]].

Thank you for choosing [[COMPANY_NAME]]. We hope you were satisfied with your [[SERVICE_NAME]].

Thank you and we look forward to seeing you again soon.

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Dear [[CLIENT_NAME]].

This is confirmation that you have booked [[SERVICE_NAME]].

We are waiting you at [[COMPANY_ADDRESS]] on [[APPOINTMENT_DATE]] at [[APPOINTMENT_TIME]].

Thank you for choosing our company.

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Dear [[CLIENT_NAME]].

We would like to remind you that you have booked [[SERVICE_NAME]] tomorrow on [[APPOINTMENT_TIME]]. We are waiting you at [[COMPANY_ADDRESS]].

Thank you for choosing our company.

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Dear [[CLIENT_NAME]].
Thank you for choosing [[COMPANY_NAME]]. We hope you were satisfied with your [[SERVICE_NAME]].
Thank you and we look forward to seeing you again soon.
[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Dear [[CLIENT_NAME]].
This is confirmation that you have booked [[SERVICE_NAME]].
We are waiting you at [[COMPANY_ADDRESS]] on [[APPOINTMENT_DATE]] at [[APPOINTMENT_TIME]].
Thank you for choosing our company.
[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Dear [[CLIENT_NAME]].
We would like to remind you that you have booked [[SERVICE_NAME]] tomorrow on [[APPOINTMENT_TIME]]. We are waiting you at [[COMPANY_ADDRESS]].
Thank you for choosing our company.
[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Deduction Deduction should be a positive number. Default country code Default value for category select Default value for employee select Default value for service select Delete Delete break Delete current photo Delete customer Delete this staff member Delimiter Delivered Details Disabled Discount (%) Discount should be between 0 and 100. Dismiss Display available time slots in client's time zone Done Drop Down Duration Edit appointment Edit booking details Email Email Notifications Email already in use. Employee Empty password. Enabled End time must not be empty End time must not be equal to start time Enter a URL Enter a label Enter a name Enter a phone number in international format. E.g. for the United States a valid phone number would be +17327572923. Enter a value Enter code from email Enter phone number Enter this URL as a redirect URI in the Developers Console Enter your country code Error Error adding the break interval Error connecting to server. Error sending email. Evening notification with the next day agenda to staff member (requires cron setup) Evening reminder to customer about next day appointment (requires cron setup) Expiration Date Expired Export to CSV Failed Failed to send SMS. Filter Final step URL Finish by Follow-up message in the same day after appointment (requires cron setup) Forgot password Form ID error. From Full name General Go to the <a href="https://console.developers.google.com/" target="_blank">Google Developers Console</a>. Google Calendar Google Calendar integration Hello.

An account was created for you at [[SITE_ADDRESS]]

Your user details:
user: [[NEW_USERNAME]]
password: [[NEW_PASSWORD]]

Thanks. Hello.

The following booking has been cancelled.

Service: [[SERVICE_NAME]]
Date: [[APPOINTMENT_DATE]]
Time: [[APPOINTMENT_TIME]]
Client name: [[CLIENT_NAME]]
Client phone: [[CLIENT_PHONE]]
Client email: [[CLIENT_EMAIL]] Hello.

You have new booking.

Service: [[SERVICE_NAME]]
Date: [[APPOINTMENT_DATE]]
Time: [[APPOINTMENT_TIME]]
Client name: [[CLIENT_NAME]]
Client phone: [[CLIENT_PHONE]]
Client email: [[CLIENT_EMAIL]] Hello.

Your agenda for tomorrow is:

[[NEXT_DAY_AGENDA]] Hello.
An account was created for you at [[SITE_ADDRESS]]
Your user details:
user: [[NEW_USERNAME]]
password: [[NEW_PASSWORD]]

Thanks. Hello.
The following booking has been cancelled.
Service: [[SERVICE_NAME]]
Date: [[APPOINTMENT_DATE]]
Time: [[APPOINTMENT_TIME]]
Client name: [[CLIENT_NAME]]
Client phone: [[CLIENT_PHONE]]
Client email: [[CLIENT_EMAIL]] Hello.
You have new booking.
Service: [[SERVICE_NAME]]
Date: [[APPOINTMENT_DATE]]
Time: [[APPOINTMENT_TIME]]
Client name: [[CLIENT_NAME]]
Client phone: [[CLIENT_PHONE]]
Client email: [[CLIENT_EMAIL]] Hello.
Your agenda for tomorrow is:
[[NEXT_DAY_AGENDA]] Hide this block Hide this field Holidays I will pay locally I will pay now with Credit Card I will pay now with PayPal I'm available on or after If email or SMS notifications are enabled and you want the customer or the staff member to be notified about this appointment after saving, tick this checkbox before clicking Save. If needed, edit item data which will be displayed in the cart. If there is a lot of events in Google Calendar sometimes this leads to a lack of memory in PHP when Bookly tries to fetch all events. You can limit the number of fetched events here. This only works when 2 way sync is enabled. If this option is enabled then all staff members who are associated with WordPress users will be able to edit their own profiles, services, schedule and days off. If this setting is enabled then Bookly will be creating WordPress user accounts for all new customers. If the user is logged in then the new customer will be associated with the existing user account. If this staff member requires separate login to access personal calendar, a regular WP user needs to be created for this purpose. If you are not redirected automatically, follow the <a href="%s">link</a>. If you do not see your country in the list please contact us at <a href="mailto:support@ladela.com">support@ladela.com</a>. If you will leave this field blank, this staff member will not be able to access personal calendar using WP backend. Import In the form below enable WooCommerce option. In the sidebar on the left, expand <b>APIs & auth</b>. Next, click <b>APIs</b>. In the list of APIs, make sure the status is <b>ON</b> for the Google Calendar API. In the sidebar on the left, select <b>Credentials</b>. Incorrect email or password. Incorrect password. Incorrect recovery code. Insert Insert Appointment Booking Form Insert a URL of a page that is shown to clients after they have cancelled their booking. Instructions Invalid date or time Invalid email. Invalid number Last 30 Days Last 7 Days Last Month Last appointment Limit number of fetched events Loading... Local Log in Log out Login Look for the <b>Client ID</b> and <b>Client secret</b> in the table associated with each of your credentials. Message Method to include Bookly JavaScript and CSS files on the page Minimum time requirement prior to booking Name New Category New Customer New Staff Member New appointment New booking information New customer New password Next Next Month Next month No appointments No appointments found No coupons found No customers No payments for selected period and criteria. No result No services found. Please add services. No time is available for selected criteria. No, delete just the customer No, update just here in services Note Note that once you have enabled WooCommerce option in Bookly the built-in payment methods will no longer work. All your customers will be redirected to WooCommerce cart instead of standard payment step. Notes Notes (optional) Notification settings were updated successfully. Notification to customer about appointment details Notification to customer about their WordPress user login details Notification to staff member about appointment cancellation Notification to staff member about appointment details Notifications Number of days available for booking Number of persons Number of times used OFF Old password Option Order Out of credit Page Redirection Participants Password Passwords must be the same. Payment Payments Period Personal Information Phone Phone number is empty. Photo Please accept terms and conditions. Please be aware that a value in this field is required in the frontend. If you choose to hide this field, please be sure to select a default value for it Please configure Google Calendar <a href="?page=ab-settings&type=_google_calendar">settings</a> first Please do not forget to specify your purchase code in Bookly <a href="admin.php?page=ab-settings">settings</a>. Upon providing the code you will have access to free updates of Bookly. Updates may contain functionality improvements and important security fixes. Please enter old password. Please select a customer Please select a service Please select at least one coupon. Please select at least one service. Please select service:  Please tell us how you would like to pay:  Previous month Price Price list Profile Provider Purchase Code Purchases Queued Quick search customer Radio Button Radio Button Group Recovery code expired. Redirect URI Register Registration Remember my choice Remove customer Remove field Remove item Repeat every year Repeat new password Repeat password Required Required field Reset SMS Details SMS Notifications SMS Notifications (or "Bookly SMS") is a service for notifying your customers via text messages which are sent to mobile phones.<br/>It is necessary to register in order to start using this service.<br/>After registration you will need to configure notification messages and top up your balance in order to start sending SMS. SMS has been sent successfully. Sandbox Mode Save Save Changes Save Member Save break Save category Schedule Secret Key Select a project, or create a new one. Select category Select file Select from WP users Select product Select service Select the product that you created at step 1 in the drop down list of products. Select the time interval that will be used in frontend and backend, e.g. in calendar, second step of the booking process, while indicating the working hours, etc. Selected / maximum Semicolon (;) Send copy to administrators Send email notifications Send test SMS Sender email Sender name Sending Sent Service Service paid locally Services Session error. Set a URL of a page that the user will be forwarded to after successful booking. If disabled then the default step 5 is displayed. Set a minimum amount of time before the chosen appointment (for example, require the customer to book at least 1 hour before the appointment time). Settings Settings saved. Show blocked timeslots Show calendar Show each day in one column Show form progress tracker Signed up Specify the number of days that should be available for booking at step 2 starting from the current day. Staff Staff Member Staff Members Start from Start time must not be empty Status Subject Synchronize the data of the staff member bookings with Google Calendar. Template for event title Terms & Conditions Text Text Area Text Field Thank you! Your booking is complete. An email with details of your booking has been sent to you. The Calendar ID can be found by clicking on "Calendar settings" next to the calendar you wish to display. The Calendar ID is then shown beside "Calendar Address". The client ID obtained from the Developers Console The client secret obtained from the Developers Console The maximum number of customers allowed to book the service for the certain time period. The number of customers should be not more than  The price for the service is [[SERVICE_PRICE]]. The requested interval is not available The selected period does't match default duration for the selected service The selected period is occupied by another appointment The start time must be less than the end one The value is taken from client’s browser. This Month Time Time slot length Title Titles To To find your client ID and client secret, do the following: To send scheduled notifications please execute the following script hourly with your cron: Today Total appointments Total:  Type URL for cancel appointment link (to use inside <a> tag) Uncategorized Undelivered Untitled Update Update service setting Upon providing the purchase code you will have access to free updates of Bookly. Updates may contain functionality improvements and important security fixes. For more information on where to find your purchase code see this <a href="https://help.market.envato.com/hc/en-us/articles/202822600-Where-can-I-find-my-Purchase-Code-" target="_blank">page</a>. Usage limit User User not found. User with "Administrator" role will have access to calendars and settings of all staff members, user with some other role will have access only to personal calendar and settings. We are not working on this day Website Week With "Enqueue" method the JavaScript and CSS files of Bookly will be included on all pages of your website. This method should work with all themes. With "Print" method the files will be included only on the pages which contain Bookly booking form. This method may not work with all themes. WooCommerce Yes Yesterday You are about to change a service setting which is also configured separately for each staff member. Do you want to update it in staff settings too? You are about to delete a customer which may have a WordPress account associated to them. Do you want to delete that account too (if there is one)? You may import list of clients in CSV format. The file needs to have three columns: Name, Phone and Email. You need to install and activate WooCommerce plugin before using the options below.<br/><br/>Once the plugin is activated do the following steps: You selected a booking for [[SERVICE_NAME]] by [[STAFF_NAME]] at [[SERVICE_TIME]] on [[SERVICE_DATE]]. The price for the service is [[SERVICE_PRICE]].
Please provide your details in the form below to proceed with booking. Your agenda for [[TOMORROW_DATE]] Your appointment at [[COMPANY_NAME]] Your appointment information Your balance Your clients must have their phone numbers in international format in order to receive text messages. However you can specify a default country code that will be used as a prefix for all phone numbers that do not start with "+" or "00". E.g. if you enter "1" as the default country code and a client enters their phone as "(600) 555-2222" the resulting phone number to send the SMS to will be "+1600555222". Your payment has been accepted for processing. Your payment has been interrupted. Your visit to [[COMPANY_NAME]] add break address of your company breaks: cancel appointment link characters max characters min combined values of all custom fields combined values of all custom fields (formatted in 2 columns) customer new password customer new username date of appointment date of next day disconnect email of client email of staff is too long is too short name of category name of client name of service name of staff name of your company number of persons phone of client phone of staff photo of staff price of service site address staff agenda for next day staff members this web-site address time of appointment to total price of booking (service price multiplied by the number of persons) your company logo your company phone Project-Id-Version: Bookly v7.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-06-04 14:49+0200
PO-Revision-Date: 2015-06-04 14:50+0200
Last-Translator: Peter Massar <Hello@marpet.nu>
Language-Team: 
Language: nl_NL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.6.7
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: .
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: ..
 Geen medewerker geselecteerd Ongedaan maken Tot "ik ben beschikbaar op …" blokkeren %d uur %d min * Ongeldig e-mailadres * Ongeldig telefoonr. * Selecteer a.u.b. een activiteit * Gelieve uw e-mail in te vullen *  Gelieve uw naam in te vullen * Gelieve uw telefoonnr. in te vullen * Verplicht * De starttijd moet minder zijn dan de eindtijd * Deze kortingsbon code is ongeldig of is al gebruikt * Deze email is al in gebruik --Zoek klanten-- -- Selecteer een activiteit -- 2-wegs synchronisatie <b>Belangrijke!</b> Wees u ervan bewust dat als uw exemplaar van Bookly was niet gedownload van Codecanyon (het enige kanaal van Bookly distributie), u uw website een significant risico loopt - het is zeer waarschijnlijk dat het kwaadaardige code bevat, een Trojaans paard of een achterpoortje. Overweeg het kopen van een gelicentieerd exemplaar van Bookly <a href="http://booking-wp-plugin.com" target="_blank">hier</a>. <b>Laat dit veld leeg</b> om te werken met de standaard agenda. <b>[[CATEGORY_NAME]]</b> - naam van de categorie, <b>[[NUMBER_OF_PERSONS]]</b> - aantal personen. <b>[[NUMBER_OF_PERSONS]]</b> - aantal personen. <b>[[SERVICE_NAME]]</b> - naam van de activiteit, <b>[[STAFF_NAME]]</b> - naam van de medewerker, <b>[[SERVICE_PRICE]]</b> - prijs van de dienst, <b>[[CATEGORY_NAME]]</b> - naam van de categorie. <b>[[SERVICE_PRICE]]</b> - prijs van de dienst, <b>[[TOTAL_PRICE]]</b> - totaalprijs van de boeking, <b>[[SERVICE_TIME]]</b> - begintijd van de activiteit,  <b>[[SERVICE_DATE]]</b> - datum van de activiteit, <b>[[STAFF_NAME]]</b> - naam van de medewerker,  <b>[[SERVICE_NAME]]</b> - naam van de activiteit, <b>[[TOTAL_PRICE]]</b> - totale prijs van de reservering, <b>[[NUMBER_OF_PERSONS]]</b> - aantal personen. <h3> De geselecteerde tijd is niet meer beschikbaar. Gelieve een ander tijdsblok te kiezen. </h3> API Inlog ID API Wachtwoord API Handtekening API Transactie Sleutel API gebruikersnaam Accepteer <a href="javascript:void(0)" data-toggle="modal" data-target="#ab-tos">Algemene Voorwaarden</a> Voeg Bookly afsprakenlijst toe Voeg Bookly boekingsformulier toe Kortingsbon toevoegen Activiteit toevoegen Geld toevoegen Adres Administrator telefoon Administrator telefoonnummer is leeg. Alle activiteiten Alle klanten Alle soorten betalingen Alle aanbieders Alle activiteiten Alle medewerkers Laat medewerkers op hun profiel bewerken Bedrag Geen voorkeur Weergave Aanbrengen Afspraak Afspraak Datum Afspraken: Weet u het zeker? Profiel foto Vorige Hieronder vind u een lijst van beschikbare tijdsblokken voor [[SERVICE_NAME]] door [[STAFF_NAME]]. 
Klik op een tijdsblok om door te gaan met reserveren. Boeking Tijd Reservering geannuleerd Product voor het boeken Bookly : Je hebt onvoldoende rechten om deze pagina te bekijken. Werk uren Standaard duwt Bookly nieuwe afspraken en alle verdere wijzigingen in Google Calendar. Als u deze optie inschakelt zal Bookly de gebeurtenissen halen uit Google agenda en bijbehorende timeslots verwijderen voordat de tweede stap van het inschrijvingsformulier (dit kan leiden tot een vertraging wanneer gebruikers op volgende klikken bij de eerste stap) wordt weergegeven. Kalender Kalender ID Kalender ID is niet geldig. Annuleer Annuleren afspraak pagina URL Capaciteit Creditcard beveiligingscode Winkelwagen artikelgegevens Categorie Verander wachtwoord Keuzevakje Keuzevakje Groep Klik op de onderstreepte tekst om te wijzigen. Klant ID Klant privé code Kode Codes Kolommen Komma (,) Bedrijfsnaam Bedrijfslogo Bedrijfsnaam Configureren welke informatie plaatsen in de titel van Google Calendar evenement zou moeten zijn. Beschikbare codes [[SERVICE_NAME]], [[STAFF_NAME]] en [[CLIENT_NAMES]]. Verbinden Verbonden Kosten Land Land buiten dienst Kortingscode Kortingsbonnen Maak WordPress gebruikersaccount voor klanten Maak een product in WooCommerce die in het winkelwagentje geplaatst kunnen worden. Nieuwe klant Maak OAuth 2.0 kwalificaties van uw project door te klikken op <b>Maak een nieuwe Client ID,</b>, het selecteren van <b>Web applicatie,</b>, en het verstrekken van de informatie die nodig is om de referenties te creëren. <b>Voor</b> geautoriseerde <b>REDIRECT URLs</b> voer de <b>Redirect URL</b>in, hieronder gevonden op deze pagina. Krediet Creditcard nummer Munteenheid Aangepaste velden Standaard bereik Klant Naam klant Klanten Datum Dag Vrije dagen Geachte [[CLIENT_NAME]].

Bedankt voor het reserveren bij [[COMPANY_NAME]]. Wij hopen dat u tevreden was met uw gereserveerde activiteit [[SERVICE_NAME]].

Dank u en hopelijk tot ziens.

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Geachte [[CLIENT_NAME]].

Dit is een bevestiging voor een [[SERVICE_NAME]] afspraak.

We verwachten je te [[COMPANY_ADDRESS]] op [[APPOINTMENT_DATE]] om [[APPOINTMENT_TIME]].

Bedankt voor het vertrouwen.

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Geachte [[CLIENT_NAME]].

Wij herinneren u eraan dat u een [[SERVICE_NAME]] afspraak hebt, morgen om [[APPOINTMENT_TIME]]. Wij verwachten u te [[COMPANY_ADDRESS]].

Bedankt voor het vertrouwen.

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Geachte [[CLIENT_NAME]].
Bedankt voor het reserveren bij [[COMPANY_NAME]]. Wij hopen dat u tevreden was met uw gereserveerde activiteit [[SERVICE_NAME]].
Dank u en hopelijk tot ziens.
[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Geachte [[CLIENT_NAME]].
Dit is een bevestiging voor een [[SERVICE_NAME]] afspraak.
We verwachten je te [[COMPANY_ADDRESS]] op [[APPOINTMENT_DATE]] om [[APPOINTMENT_TIME]].
Bedankt voor het vertrouwen.
[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Geachte [[CLIENT_NAME]].
Wij herinneren u eraan dat u een [[SERVICE_NAME]] afspraak hebt, morgen om [[APPOINTMENT_TIME]]. Wij verwachten u te [[COMPANY_ADDRESS]].
Bedankt voor het vertrouwen.
[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Aftrek Aftrek moet een positief getal zijn. Standaard landcode Standaardwaarde voor te selecteren categorie Standaardwaarde voor medewerker selecteren Standaardwaarde voor activiteit selecteren Verwijder Verwijder break Wis huidige foto Verwijder klant Verwijder deze medewerker Scheidingsteken Geleverd Details Uitgeschakeld Korting (%) Korting moet tussen 0 en 100. Afwijzen Weergave vrije tijdsblok in tijdzone van de klant Klaar Keuzelijst Duur Wijzig afspraak Reservering Details E-mail Email Meldingen Email is al in gebruik. Medewerker Leeg wachtwoord. Ingeschakeld Eindtijd mag niet leeg zijn Eindtijd mag niet gelijk zijn aan de begintijd Voer een URL-adres in: Voer een label in Voer een naam Voer een telefoonnummer in internationaal formaat. Bijv. voor Nederland een geldig telefoonnummer zou zijn +31202225555. Voer een waarde Vul de code van het e- Voer het telefoonnummer Voer deze URL in als omleidings URL in de Ontwikkelaars Console Voer uw landcode Fout Fout bij toevoegen van de pauze-interval Fout verbinden met de server. Fout e-mail verzenden. Avond melding met de volgende dag agenda medewerker (vereist cron setup) Avond herinnering aan de klant over de volgende dag afspraak (vereist cron setup) Expiratiedatum Verlopen Exporteer naar CSV Mislukt Kan geen SMS verzenden. Filter Laatste stap URL Eindig om Follow-up bericht op dezelfde dag na afspraak (vereist cron setup) Wachtwoord vergeten Form ID fout. Van Volledige Naam Algemeen Ga naar de <a href="https://console.developers.google.com/" target="_blank">Google-ontwikkelaars Console</a> . Google Kalender Google Calendar integratie Goedendag,

Er is een gebruikersnaam voor u gemaakt op [[SITE_ADDRESS]]

Uw gebruikers gegevens:
Gebruikersnaam: [[NEW_USERNAME]]
Wachtwoord: [[NEW_PASSWORD]]

Dank u. Goedendag,

De volgende reservering is geannuleerd.

Activiteit: [[SERVICE_NAME]]
Datum: [[APPOINTMENT_DATE]]
Tijd: [[APPOINTMENT_TIME]]
Klantnaam: [[CLIENT_NAME]]
Klant telefoonnummer: [[CLIENT_PHONE]]
Klant e-mail: [[CLIENT_EMAIL]] Goedendag.

U heeft een nieuwe reservering.

Activiteit: [[SERVICE_NAME]]
Datum: [[APPOINTMENT_DATE]]
Tijd: [[APPOINTMENT_TIME]]
Client naam: [[CLIENT_NAME]]
Client telefoon: [[CLIENT_PHONE]]
Client e-mail: [[CLIENT_EMAIL]] Geachte,

Uw agenda voor morgen is: 

 [[NEXT_DAY_AGENDA]] Goedendag,
Er is een gebruikersnaam voor u gemaakt op [[SITE_ADDRESS]]
Uw gebruikers gegevens:
Gebruikersnaam: [[NEW_USERNAME]]
Wachtwoord: [[NEW_PASSWORD]]

Dank u. Goedendag,
De volgende reservering is geannuleerd.
Activiteit: [[SERVICE_NAME]]
Datum: [[APPOINTMENT_DATE]]
Tijd: [[APPOINTMENT_TIME]]
Klantnaam: [[CLIENT_NAME]]
Klant telefoonnummer: [[CLIENT_PHONE]]
Klant e-mail: [[CLIENT_EMAIL]] Goedendag.
U heeft een nieuwe reservering.
Activiteit: [[SERVICE_NAME]]
Datum: [[APPOINTMENT_DATE]]
Tijd: [[APPOINTMENT_TIME]]
Client naam: [[CLIENT_NAME]]
Client telefoon: [[CLIENT_PHONE]]
Client e-mail: [[CLIENT_EMAIL]] Geachte,
Uw agenda voor morgen is: 
 [[NEXT_DAY_AGENDA]] Verberg dit blok Verberg dit veld Vakanties Ik betaal ter plekke Ik betaal nu met Creditcard Ik betaal nu met Paypal Ik ben beschikbaar op of na Indien e-mail of SMS notificaties aan staan en u wilt uw klanten of medewerkers informeren over deze afspraak na opslaan, vink dan deze checkbox aan vóórdat u op opslaan klikt.  Indien nodig, bewerken punt gegevens die worden weergegeven in het winkelwagentje. Als er een heleboel evenementen in Google agenda soms is leidt dit tot een gebrek aan geheugen in PHP wanneer Bookly probeert te halen alle gebeurtenissen. U kunt het aantal opgehaalde gebeurtenissen hier beperken. Dit werkt alleen als 2 manier synchronisatie is ingeschakeld. Als deze optie wordt vervolgens ingeschakeld alle medewerkers die worden geassocieerd met WordPress gebruikers zullen in staat zijn om hun eigen profiel, diensten, planning en vrije dagen te bewerken. Als deze instelling is ingeschakeld dan Bookly zal het creëren van WordPress gebruikersaccounts voor alle nieuwe klanten. Als de gebruiker is ingelogd, dan is de nieuwe klant zal worden gekoppeld aan de bestaande gebruikersaccount. Als deze medewerker een aparte login vereist voor toegang tot zijn persoonlijke agenda, zal een WP gebruiker gemaakt moeten worden voor dit doel. Als u niet automatisch wordt doorgestuurd, volg dan de <a href="%s">koppeling</a>. Als u uw land niet in de lijst neem dan contact met ons op <a href="mailto:support@ladela.com">support@ladela.com</a>. Als u dit veld leeg laat, zal deze medewerker niet in staat zijn om toegang te krijgen tot zijn persoonlijke agenda met behulp van WP backend. Importeer In het onderstaande formulier in te schakelen WooCommerce optie. In de navigatiekolom aan de linkerkant, uitbreiden <b>API&#39;s & auth.</b> Klik vervolgens <b>API&#39;s.</b> In de lijst van API's, zorg ervoor dat de status <b>AAN</b> is voor de Google Agenda API. In de navigatiekolom aan de linkerkant, selecteer <b>Credentials</b> Onjuiste e-mail of wachtwoord. Incorrect wachtwoord. Onjuiste herstel code. Invoegen Voeg reserveringsformulier toe Voeg de URL toe van de pagina die wordt getoond aan klanten, nadat zij hun afspraak hebben geannuleerd. Instructies Ongeldige datum of tijd Ongeldig e-mail. Ongeldig getal Laatste 30 dagen Laatste 7 dagen Laatste maand Laatste afspraak Beperk het aantal opgehaalde gebeurtenissen Laden ... Lokaal Log in Uitloggen Log In Kijk voor de <b>Klant ID</b> en <b>Klant privé code</b> in de tabel geassocieerd met elk van uw referenties. Melding Methode om Bookly JavaScript en CSS-bestanden op de pagina onder Minimum tijdsduur voorafgaand aan een afspraak Naam Nieuwe Categorie Nieuwe klant Nieuwe medewerker  Nieuwe afspraak Nieuwe afspraak informatie Nieuwe klant Nieuw paswoord Volgende Volgende maand Volgende maand Geen afspraken Geen afspraken gevonden Geen kortingsbonnen gevonden Geen klanten Geen betalingen voor de geselecteerde periode en criteria. Geen resultaat Geen activiteiten gevonden. Gelieve een activiteit toevoegen. Er is geen tijdsblok vrij voor de geselecteerde criteria. Nee, verwijder alleen de klant Nee, werk enkel in activiteiten bij Notitie Merk op dat als je eenmaal WooCommerce optie hebt ingeschakeld in Bookly de ingebouwde betaalmethodes zullen niet meer werken. Al uw klanten worden doorgestuurd naar WooCommerce winkelwagen in plaats van de standaard betaling stap. Notities Opmerking (optioneel) Notificatie instellingen zijn succesvol opgeslagen. Kennisgeving aan de klant over afspraakdetails Kennisgeving aan de klant over hun WordPress gebruiker login-gegevens Kennisgeving aan lid over benoeming annulering personeel Kennisgeving aan medewerker over afspraakdetails Notificaties Aantal dagen beschikbaar voor reservering Aantal personen Aantal keer gebruikt UIT Oud Wachtwoord Optie Orde Uit krediet Pagina Redirection Deelnemers Wachtwoord Wachtwoorden moeten hetzelfde zijn. Betalingen Betalingen Periode Persoonlijke informatie Telefoon Telefoonnummer is leeg. Foto Gelieve voorwaarden te accepteren. Wees u ervan bewust dat een waarde in dit veld verplicht is in de frontend. Als u ervoor kiest om dit veld te verbergen, raden wij u aan een standaard waarde te selecteren Gelieve eerst Google Agenda- <a href="?page=ab-settings">instellingen</a> te configureren Vergeet niet uw aankoop code in te geven in Bookly onder <a href="admin.php?page=ab-settings">instellingen</a> . Bij het verstrekken van de code heeft u toegang tot gratis updates van Bookly. Updates kunnen functionaliteit-verbeteringen en belangrijke beveiliging-oplossingen bevatten. Vul het oude wachtwoord. Selecteer een klant Selecteer een activiteit Gelieve ten minste één kortingsbon selecteren. Selecteer alstublieft één activiteit Selecteer een activiteit: Hoe wenst u te betalen: Vorige maand Prijs Prijslijst Profiel Aanbieder Aankoop Code Aankopen Wachtrij Zoek versneld klant Keuzerondje Keuzerondje Groep Recovery code verlopen. Omleidings URL Registreer Registratie Mijn keuze onthouden Klant verwijderen Veld verwijderen Item verwijderen Herhaal ieder jaar Herhaal nieuw wachtwoord Herhaal wachtwoord Verplicht Verplicht veld Ongedaan maken SMS Details SMS Meldingen SMS Meldingen (of "Bookly SMS") is een dienst voor het melden van uw klanten via sms-berichten die naar mobiele telefoons worden verzonden.<br/>Het is noodzakelijk om te registreren om te beginnen met het gebruik van deze service.<br/>Na registratie moet u meldingen configureren en opwaarderen uw saldo om te beginnen het verzenden van SMS. SMS is succesvol verzonden. Sandloper Modus Opslaan Bewaar aanpassingen Medewerker opslaan Pauze opslaan Categorie Opslaan Agenda Privé sleutel Selecteer een project, of maak een nieuw. Selecteer een categorie Selecteer bestand Kies uit WP-gebruikers Selecteer activiteit Selecteer een activiteit Selecteer het product dat u hebt gemaakt bij stap 1 in de drop down lijst van producten. Selecteer het tijdsinterval dat zal worden gebruikt in de frontend en backend, bijvoorbeeld in de kalender, de tweede stap van het afspraaksproces, bij vermelding van de werktijden, etc. Geselecteerde / maximum Punt-komma (;) Stuur een kopie naar beheerders Stuur e-mail notificatiees Stuur SMS-test E-mail afzender Naam afzender Verzending Verzonden Activiteit Dienst ter plaatse betalen Activiteiten Session fout. Stel een URL van een pagina in, waar de gebruiker naar toegezonden wordt, na een succesvolle afspraak gemaakt te hebben. Indien uitgeschakeld zal de standaard stap 5 weergegeven worden. Stel een minimum tijdsduur in vóór de gekozen afspraak (bijvoorbeeld eisen dat de klant ten minste 1 uur vóór de tijd van de afspraak reserveert). Instellingen Instellingen opgeslagen. Geblokkeerde tijdsblokken weergeven Kalender weergeven Elke dag in één kolom weergeven Laat voortgang zien Ingeschreven Geef het aantal dagen dat moet openstaan voor boeking bij stap 2 vanaf de huidige dag. Medewerker Medewerker Medewerkers Starten vanaf Begintijd mag niet leeg zijn Toestand Onderwerp Synchroniseer de gegevens van de medewerker afspraken met Google Calendar. Template voor het evenement titel Algemene Voorwaarden Tekst Tekstgebied Tekst Veld Bedankt! Uw reservering is voltooid. Een e-mail met de details van uw reservering is naar u verzonden. De Agenda ID kan worden gevonden door te klikken op "Calendar settings" naast de agenda die u wilt weergeven. De Agenda ID wordt dan weergegeven naast "Calendar Address". Het klant-ID verkregen uit de Ontwikkelaars Console De klant privé code verkregen uit de Ontwikkelaars Console Het maximum aantal klanten voor deze activiteit in een bepaalde tijdsperiode. Het aantal klanten dat mag niet meer zijn dan De prijs voor de activiteit is [[SERVICE_PRICE]]. De gevraagde interval is niet beschikbaar De geselecteerde periode komt niet overeen met de standaard duur van de geselecteerde behandeling. De geselecteerde periode wordt bezet door een andere afspraak De starttijd moet minder zijn dan de eindtijd De waarde is ontleend aan de browser van de klant. Deze maand Tijd Tijdsblok lengte Titel Titels Naar Om uw klant ID en klant privé code te vinden, doe je het volgende: Om geplande notificaties te verzenden kunt u elk uur het volgende script met uw cron invoeren: Vandaag Totaal aantal afspraken Totaal: Type URL voor benoeming link Annuleren (om te gebruiken binnen <a>label)</a> Niet gecategoriseerd Niet afgeleverd Zonder titel Bijwerken Service-instellingen bijwerken Bij het verstrekken van de code heeft u toegang tot gratis updates van Bookly. Updates kunnen functionaliteit-verbeteringen en belangrijke beveiliging-oplossingen bevatten. Voor meer informatie over waar te vinden van uw aankoop code zie deze <a href="https://help.market.envato.com/hc/en-us/articles/202822600-Where-can-I-find-my-Purchase-Code-" target="_blank">pagina</a>. Gebruik limiet Gebruiker Gebruiker niet gevonden. De gebruiker met "Administrator" rol zal de toegang tot agenda en instellingen van alle medewerkers hebben, gebruikers met een andere rol krijgen alleen toegang tot hun persoonlijke agenda en instellingen. Wij zijn niet aanwezig op deze dag Website Week Met "Enqueue" methode de JavaScript en CSS-bestanden van Bookly zal worden opgenomen op alle pagina's van uw website. Deze methode zou moeten werken met alle thema's. Met "Print" methode de bestanden worden alleen opgenomen op de pagina's die Bookly boekingsformulier bevatten. Deze methode werkt mogelijk niet met alle thema's. WooCommerce Ja Gisteren U zal een diensten-instelling wijzigen welke ook apart geconfigureerd is voor elke medewerker. Wenst u dit ook in medeweker-instellingen bijwerken? Je bent op het punt om een klant die een WordPress rekening gekoppeld aan hen kan hebben verwijderen. Wilt u die account te verwijderen (als er één is)? U kunt een lijst van klanten importeren in CSV formaat. Het bestand moet drie kolommen hebben: naam, telefoon en e-mail. U moet installeren en activeren WooCommerce plugin voor het gebruik van de onderstaande opties.<br/><br/>Zodra de plugin wordt geactiveerd doen de volgende stappen: U heeft een reservering voor [[SERVICE_NAME]] door [[STAFF_NAME]] op [[SERVICE_TIME]] op [[SERVICE_DATE]] geselecteerd.
Vul uw gegevens in het formulier hieronder in om door te gaan met reserveren. Uw agenda voor [[TOMORROW_DATE]] Uw afspraak bij [[COMPANY_NAME]] Uw afspraak informatie Je saldo Uw klanten moeten hun telefoonnummers in internationaal formaat hebben om sms-berichten te ontvangen. Maar u een standaard land code die gebruikt zal worden als een prefix van alle telefoonnummers die niet beginnen met "+" of "00" kunt opgeven. Bijv. als je voer "1" als de standaard land code en een klant binnenkomt hun telefoon als "(600) 555-2222" de resulterende telefoonnummer om de SMS te sturen naar zal worden "+1600555222". Uw betaling is voor verwerking geaccepteerd. Uw betaling is onderbroken. Uw bezoek bij [[COMPANY_NAME]] voeg pauze toe adres van uw bedrijf Pauzes. afspraak koppeling annuleren tekens max. tekens min. combinatie van de waarden van alle aangepaste velden combinatie van de waarden van alle aangepaste velden (geformatteerd in 2 kolommen) Nieuw wachtwoord klant Nieuwe gebruikersnaam klant Datum in functie datum van de volgende dag verbinding verbroken E-mail cliënt e-mail van medewerker is te lang is te kort naam van categorie Naam van de Klant Benaming van de activiteit: naam van personeel Naam van uw bedrijf aantal  personen telefoon cliënt telefoon medewerker Foto van medewerker prijs van de activiteit website adres medewerker agenda voor de volgende dag Medewerkers Dit website adres tijd van uw afspraak tot totale prijs van de reservering (serviceprijs vermenigvuldigd met het aantal  personen) uw bedrijfslogo uw bedrijf telefoon 