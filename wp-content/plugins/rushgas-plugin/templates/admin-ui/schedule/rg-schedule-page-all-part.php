	<div class="container">
			<!-- .Page heading -->
	  		<h2>Schedule</h2>
	  		
	  		<!-- Link for add New vehicles -->
	  		<div class="col-md-12 col-sm-12"> 
		  		<a class="btn  btn-sm  btn-md btn-danger" href="<?=admin_url('admin.php?page=rg_schedule_service_setup&action=scheduleevent')?>">
		  			Schedule  Service
		  		</a>
		  		<a class="btn btn-sm  btn-md btn-danger" href="<?=admin_url('admin.php?page=rg_schedule_service_setup&action=passedevent')?>">
		  			Service history
		  		</a>
		  		

			</div>
			
			<div style="clear:both"></div><br><br>
			<div class="row">
			<div class="col-md-12 col-sm-12"> 
				<h3>Upcoming Services Scheduled</h3>
		  		<div class="table-responsive">
		  			 <table class="table " >
					    <thead>
					      	<tr>
					      		<th>Event type</th>
						      	<th>Date</th>
						      	<th>Time</th>
						        <th>Vehicle</th>
						        <th>Services</th>
						        <th>Action</th>
					       	</tr>
					    </thead>
				    	<tbody>
				    	<?
				    	foreach ($bookings as $booking) :
				    		
				    	?>
				    		<tr>
				    			<td><?=$booking->title?></td>
				    			<td><?=$booking->start?></td>
				    			<td><?=$booking->time?></td>
				    			<td><?=$booking->lpm?></td>
				    			<td><?=$booking->service?></td>
				    			<td>
				    				<a class="btn btn-danger" href="<?=admin_url('admin.php?page=rg_schedule_service_setup&action=upcomingeventedit&id='.$booking->id)?>">
		  								Edit
		  							</a>
		  							<a class="btn btn-danger" href="<?=admin_url('admin.php?page=rg_schedule_service_setup&action=upcomingeventdelete&id='.$booking->id)?>">
		  								Delete
		  							</a>
		  							<a class="btn btn-danger" href="<?=admin_url('admin.php?page=rg_get_help_setup&helps=addnew&subject=refund')?>">
		  								Refund
		  							</a>
		  						</td>
				    		</tr>
				    	<? endforeach;?>
				    	</tbody>
		  			</table>
		  		</div>

			</div>
			</div>
		</div>
	