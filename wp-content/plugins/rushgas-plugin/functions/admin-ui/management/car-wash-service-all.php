
	<!-- Main Container Start -->
	<div class="container">
			<!-- .Page heading -->
	  		<h2>Cash wash service</h2>
	  		<!-- Link for add New vehicles -->
			<a href="<?=admin_url('admin.php?page=rg_management_setup&action=add-car-wash')?>" class="btn btn-danger pull-right"> 
				Add Service
			</a>


			
			

				 <table class="table" >
				    <thead>
				      <tr>
				      	<th>Title</th>
				      	<th>Price</th>
				        <th>Service Type</th>
				        
						 <th>Action</th>
				        
				      </tr>
				    </thead>
				    <tbody>
				    	<?php 
				    if($allservices):
				    	foreach ($allservices as $service) {
				    		
				    	?>
				      <tr>
				      	
				      	<td><?=$service->extra_service;?></td>
				      	<td><?=$service->sprice;?></td>
				        <td><?=$service->service_name;?></td>
				
				        <td>
				        	<a  class="btn btn-danger" href="<?php echo admin_url('admin.php?page=rg_management_setup&action=update-car-wash&id='.$service->wash_id);?>">
				        		Update
				        	</a>
				        	<a class="btn btn-danger" href="<?php echo admin_url('admin.php?page=rg_management_setup&action=delete-car-wash&id='.$service->wash_id);?>">
				        		Delete
				        	</a>
				        	
				        </td>
				<?php 	//endif;?>
				      </tr>
				     <? }
				     endif;?>
				    </tbody>
				  </table>

		</div>