<?php
	/* define global varibles*/
	global $current_user,$wpdb;
	/* get action*/
	$action=$_GET['action'];
	/**/
	$id=$_GET['id'];

	/* fetching current user role*/
	$check_admin=get_userdata($current_user->ID);
	/* check current user is admin or not*/
	if($check_admin->roles[0]=='administrator'){

		/* action for update price for gas service*/
		if($action!='' && $action=='addgasprice'){
			/* fetch old price of gas service */
			$service= $wpdb->get_row("SELECT * FROM wp_user_event_service where event_id=2");
			/* update old gas price with new price*/
			if(isset($_POST['update_price'])){

				$price_id=$wpdb->update(
										'wp_user_event_service',
										array('price'=>$_POST['price']),
										array('event_id'=>2)

										);
				/* redirect to main page after updating price*/
				if($price_id)

					wp_redirect(admin_url('admin.php?page=rg_management_setup'));

			}
			include_once 'update-gas-price-part.php';	

		}
		/* action for list all service for car wash*/
		else if($action!='' && $action=='car-wash'){

			$allservices= $wpdb->get_results("SELECT * FROM 
												wp_user_car_wash_service ws 
										left join 
												wp_user_event_service es
										on
												ws.service_id=es.event_id
										where 
												ws.service_id=1
									");
			include_once 'car-wash-service-all.php';	
		}
		else if($action!='' && $action=='add-car-wash'){

		
			if(isset($_POST['add_car_service'])){
				$add_car_service=array(
										'extra_service'	=>	$_POST['name'],
										'sprice'			=>	$_POST['price'],
										'service_id'	=>	1
									);
				$price_id=$wpdb->insert(
										'wp_user_car_wash_service',
										$add_car_service
										

										);
				/* redirect to main page after updating price*/
				if($price_id)

					wp_redirect(admin_url('admin.php?page=rg_management_setup&action=car-wash'));

			}
			include_once 'add-new-car-wash-sevice.php';	
		}
		else if($action!='' && $action=='update-car-wash'){

			$services= $wpdb->get_row("SELECT * FROM 
												wp_user_car_wash_service ws 
										
										where 
												ws.wash_id=$id
									");
			
			if(isset($_POST['add_car_service'])){
				$add_car_service=array(
										'extra_service'	=>	$_POST['name'],
										'sprice'			=>	$_POST['price'],
										'service_id'	=>	1
									);
				$price_id=$wpdb->update(
										'wp_user_car_wash_service',
										$add_car_service,
										array('wash_id'=>$id)

										);
				/* redirect to main page after updating price*/
				if($price_id)

					wp_redirect(admin_url('admin.php?page=rg_management_setup&action=car-wash'));

			}
			include_once 'update-new-car-wash-sevice.php';	
		}
		else if($action!='' && $action=='delete-car-wash'){

		
				
				$price_id=$wpdb->delete(
										'wp_user_car_wash_service',
										
										array('wash_id'=>$id)

										);
				/* redirect to main page after updating price*/
				if($price_id)

					wp_redirect(admin_url('admin.php?page=rg_management_setup&action=car-wash'));

				
		}
		else{

				include_once 'all-part.php';
		}
    }
	
