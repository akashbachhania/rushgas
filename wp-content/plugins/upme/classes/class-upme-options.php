<?php

class UPME_Options {

    public $upme_settings;
    public $upme_profile_fields;

    function __construct() {
        add_action('init',array($this,'upme_init'));
        
        $this->upme_settings = array() ;
        $this->upme_profile_fields = array();
        $this->upme_init_settings();
        $this->upme_init_profile_fields();
    }
    
    public function upme_init(){
        $this->upme_init_settings();
        $this->upme_init_profile_fields();
    }

    public function upme_init_settings(){
        $this->upme_settings = get_option('upme_options');
        
    }
    
    public function upme_init_profile_fields(){
        $this->upme_profile_fields = get_option('upme_profile_fields');
    }
}

$upme_options = new UPME_Options();