<?php
#******************************************************************************
#                       Credit Card Payments Wordpress v3.0
#
#	Author: Convergine.com
#	http://www.convergine.com
#	Version: 3.0
#   (c) 2013 Convergine.com
#
#******************************************************************************
	global $wpdb;
	
	if(isset($_POST['ccpt_submit_service']) && $_POST['ccpt_submit_service'] == 'yes') {
		//Form data sent
		$pt_title = $_POST['ccpt_services_title'];
		$pt_descr = $_POST['ccpt_services_descr'];
		$pt_price = $_POST['ccpt_services_price'];
		
		if(is_numeric($pt_price) && !empty($pt_title)){
			$query="INSERT INTO ".$wpdb->prefix."ccpt_services (ccpt_services_title, ccpt_services_descr, ccpt_services_price) VALUES ('".addslashes(strip_tags($pt_title))."','".addslashes(strip_tags($pt_descr))."','".addslashes(strip_tags($pt_price))."')";
			mysql_query($query) or die(mysql_error());
			?><div class="updated"><p><strong><?php _e('Service added!' ); ?></strong></p></div><?php
		} else { 
		?><div class="updated"><p><strong><?php _e('Service not added! Please check your input. Price must contain numbers only and name cannot be blank.' ); ?></strong></p></div><?php
		}

 } $ccpt_services_descr = "";

	if(!empty($_POST['toDelete']) && count($_POST["toDelete"])>0) {
		$deleted=0;
		for($i=0; $i<count($_POST["toDelete"]); $i++){
			$query="DELETE FROM ".$wpdb->prefix."ccpt_services WHERE ccpt_services_id='".$_POST["toDelete"][$i]."'";
			mysql_query($query) or die(mysql_error());
			$deleted++;
		}
		
		if($deleted>0){
		?> <div class="updated"><p><strong><?php _e('Selected service(s) deleted!' ); ?></strong></p></div><?php
		}
	}

?>

	<?php //$wp_url = get_bloginfo('siteurl');
	$wp_url = get_site_url();  ?>
    <link rel="stylesheet" media="screen" href="<?php echo $wp_url?>/wp-content/plugins/payment_terminal_pro/resources/css/admin-style.css" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo $wp_url?>/wp-content/plugins/payment_terminal_pro/resources/js/functions.js"></script>
		<div class="wrap-ccwp-bw">
			<div class="wrap-ccwp-header" >
			<?php    echo "<h2>" . __('Credit Card Payments - Services','') . "</h2>"; ?>        
            </div><!-- end header -->
            <div class="ccwp-subheader-pptr" >
				<p><?php _e("Here you create and manage basic list of products, services or events you'd like to accept payments for. This list will show up on your website for clients to select from when they are going to make a payment." ); ?></p>
			</div>
            <div class="wrap-ccwp-content" >
            	<div class="wrap-ccwp-breadcrumbs" >
                <?php    echo "<h4>" . __( 'Add New Service', '' ) . "</h4>"; ?>
            	</div><!-- end bc -->
                <form name="ccpt_form" method="post" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>">
                    <input type="hidden" name="ccpt_submit_service" value="yes">
                    
                    <div class="ccpw-services-rowwsto143"><label for="ccpt_services_title" ><?php _e("Service Name: " ); ?></label><input class="ccpt_text" type="text" name="ccpt_services_title" id="ccpt_services_title" value="" size="40"><br /><em><?php _e("This is what customers will see in the services dropdown to select from when they decide to pay" ); ?></em></div>
                    <div class="ccpw-services-rowwsto143"><label for="ccpt_services_price" ><?php _e("Service Price&nbsp;&nbsp;: " ); ?></label><input class="ccpt_text" type="text" name="ccpt_services_price" id="ccpt_services_price" onkeyup="noAlpha(this)"  value="" size="40"><br /><em><?php _e("Numbers only. ex. 10.99" ); ?></em></div>
                    
                    <div id="poststuff">
						<div id="<?php echo user_can_richedit() ? 'postdivrich' : 'postdiv'; ?>" class="postarea postare-test-me2">
						<label for="ccpt_services_descr" ><?php _e("Service Description: " ); ?></label><?php the_editor($ccpt_services_descr, $id = 'ccpt_services_descr', $prev_id = 'ccpt_services_descr', $media_buttons = false, $tab_index = 2);?><em><?php _e(" (Optional small text describing the service, won't be displayed to customer, internal use only at the moment)" ); ?></em>
                     	</div>
					</div> 
    
                    <p class="submit">
                    <input style="margin: 0 10px;" class="button button-primary" type="submit" name="Submit" value="<?php _e('Add Service', '' ) ?>" />
                    </p>
                    
                </form>
			</div>



             
             <div class="wrap-ccwp-content" >
             	<div class="wrap-ccwp-breadcrumbs" >    
                <?php    echo "<h4>" . __('List of Services','') . "</h4>"; ?>
             	</div><!-- end bc -->
                <form name="ccpt_form_del" method="post" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>">
                <div class="services_table">
                    <div class="table_wrapper">
                        <table class="table_header">
                            <tr>
                            	<th class="deleter">&nbsp;</th>
								<th class="ccwp-services-list-nor-id" ><?php _e('ID')?></th>
                                <th class="ccwp-services-list-nor-sname" ><?php _e('Service Name')?></th>
                                <th class="ccwp-services-list-nor-sprice" ><?php _e('Price')?></th>
                                <th class="lastColumn ccwp-services-list-nor-descr" style="width:35%"><?php _e('Description')?></th>
                            </tr>

                        <?php 
						//lets get all services from database
						$query="SELECT * FROM ".$wpdb->prefix."ccpt_services ORDER BY ccpt_services_title";
						$result=mysql_query($query) or die(mysql_error());
						if(mysql_num_rows($result)>0){
							$del=true;
							$rClass = "row_b";
							while($row=mysql_fetch_assoc($result)){
						?>
                        <tr class="<?php echo $rClass=($rClass=="row_b"?"row_a":"row_b")?>">

                             	<td class="deleter">&nbsp;&nbsp;<input type="checkbox" value="<?php echo $row["ccpt_services_id"]?>" name="toDelete[]" /></td>
                                <td class="ccwp-services-list-nor-id" ><?php echo stripslashes(strip_tags($row["ccpt_services_id"]))?></td>
								<td class="ccwp-services-list-nor-sname" ><?php echo stripslashes(strip_tags($row["ccpt_services_title"]))?> <a href="admin.php?page=ccpt_admin_services_edit&amp;ccpt_serviceID=<?php echo $row["ccpt_services_id"]?>">edit</a></td>
                                <td class="ccwp-services-list-nor-sprice"  ><?php echo number_format(stripslashes(strip_tags($row["ccpt_services_price"])),2)?></td>
                                <td class="lastColumn ccwp-services-list-nor-descr" style="width:35%"><?php echo stripslashes(strip_tags($row["ccpt_services_descr"]))?></td>

                        </tr>
                        <?php }  } else { $del=false; ?>
                            <tr class="row_msg">

                                <td style="padding-left: 35px;" colspan="7">0 transactions found</td>

                            </tr>
                        <?php } ?>
                    </table>
                </div>
                <?php if($del){?>
                <p style="padding: 0;" class="submit">
                	<input class="button action" style="margin: 10px;" type="submit" name="Submit" value="<?php _e('Delete Selected Services', '' ) ?>" />
                </p>
                <?php } ?>
                </form>
                </div>
                
		</div>
        <div class="pt_footer">
        	<a href="http://www.convergine.com" target="_blank"><img src="<?php echo $wp_url?>/wp-content/plugins/payment_terminal_pro/resources/images/convergine.png" /></a>
        </div>