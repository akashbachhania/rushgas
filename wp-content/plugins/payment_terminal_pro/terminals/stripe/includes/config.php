<?php
#******************************************************************************
#                       Credit Card Payments Wordpress v3.0
#
#	Author: Convergine.com
#	http://www.convergine.com
#	Version: 3.0
#   (c) 2013 Convergine.com
#
#******************************************************************************

session_start();
error_reporting(E_ALL ^ E_NOTICE);

require (CCPT_HOME_DIR."/wp-load.php"); 
require("functions.php"); 

$configProcessor=is_string(get_option('ccwp_details_STRIPE'))?unserialize(get_option('ccwp_details_STRIPE')):get_option('ccwp_details_STRIPE');

//THIS IS TITLE ON PAGES
$title = "Stripe Payment Terminal v1.0"; //site title
$titleAboveForm = "Stripe Payment Terminal";
//THIS IS ADMIN EMAIL FOR NEW PAYMENT NOTIFICATIONS.
$admin_email = get_option('ccpt_admin_email'); //this email is for notifications about new payments
define("PTP_CURRENCY_CODE",get_option('ccpt_paypal_currency'));
		
//NOW, IF YOU WANT TO ACTIVATE THE DROPDOWN WITH SERVICES ON THE TERMINAL
//ITSELF, CHANGE BELOW VARIABLE TO TRUE;			
$show_services = get_option('ccpt_show_dd_text'); 

// set  to   RECUR  - for recurring payments, ONETIME - for onetime payments
$payment_mode = "ONETIME";


//service name   |   price  to charge   | Billing period  "Day", "Week", "Month", "Year"   |  how many periods of previous field per billing period | trial period in days | Trial amount
/*$recur_services = array(
				 array("Service 1 monthly WITH 30 DAYS TRIAL", "49.99", "Month", "1", "30", "24.99"),
				 array("Service 1 monthly", "49.99", "Month", "1", "0", "0"),
				 array("Service 1 quaterly", "149.99", "Month", "3", "0", "0"),
				 array("Service 1 semi-annualy", "249.99", "Month", "6", "0", "0"),
				 array("Service 1 annualy", "349.99", "Year", "1", "0", "0")
				);
*/				
//IF YOU'RE GOING LIVE FOLLOWING VARIABLE SHOULD BE SWITCH TO true IT WILL AUTOMATICALLY REDIRECT ALL NON-HTTTPS REQUESTS TO HTTPS - MAKE SURE SSL IS INSTALLED ALREADY.
$redirect_non_https = false;
// IF YOU'RE GOING LIVE FOLLOWING VARIABLE SHOULD BE SWITCH TO true
$liveMode = get_option('ccpt_test')==2 ? true : false;

/* Please note that Stripe.com will accept payments only in your account currency. 
 * You can set your account currency here: https://manage.stripe.com/account
 * A list with Stripe Test Credit Cards Numbers can be found here: https://stripe.com/docs/testing 
 * 
 * */
if(!$liveMode){
//TEST MODE   
define('PublishableKey', $configProcessor['test']['PUBLISHABLE_KEY']);
define('SecretKey', $configProcessor['test']['SECRET_KEY']);
define('AccountCurrency', get_option('ccpt_paypal_currency'));
define('TEST_MODE', 'TRUE');

} else {
//LIVE MODE
define('PublishableKey', $configProcessor['live']['PUBLISHABLE_KEY']);
define('SecretKey', $configProcessor['live']['SECRET_KEY']);
define('AccountCurrency', get_option('ccpt_paypal_currency'));
define('TEST_MODE', 'FALSE');
}

###### This Custom JS must be defined here, after declaring the publishable key
$customJS[$terminal] = '<script type="text/javascript" src="https://js.stripe.com/v1/"></script><script type="text/javascript">
   Stripe.setPublishableKey(\''.PublishableKey.'\');
   function stripeResponseHandler(status, response) {
   if (response.error) {
   $(\'.submit-btn input\').css({"cursor":"pointer","opacity":"1"});
   /*show the errors on the form, hide the submit button while processing*/
   $(\'#stripe_error\').css("display","block");
   $("#stripe_error_message").html(response.error.message);
   return false;
   } else {
   $(\'.submit-btn input\').css({"cursor":"none","opacity":"0.3"});	
   $(\'#stripe_error\').css("display","none");	
   var form$ = $("#ff1");
   /*token contains id, last4, and card type */
   var token = response[\'id\'];
   /* insert the token into the form so it gets submitted to the server */
   form$.append("<input type=\'hidden\' name=\'stripeToken\' value=\'" + token + "\' />");
   /* and submit */
           form$.get(0).submit();
       }
   }

   $(document).ready(function() {
   $("#ff1").submit(function(event) {

    if($("input[type=\'radio\'][name=\'cctype\']:checked").val()=="PP"){
      document.ff1.submit();
    } else { 

       Stripe.createToken({
       /* User Details */
       name: $(\'#ccname\').val(),
       address_line1: $(\'#address\').val(),
       address_zip: $(\'#zip\').val(),
       address_state: $(\'#state\').val(),
       address_country: $(\'#country\').val(),
       address_city: $(\'#city\').val(),	
       /* Card Details */	
       number: $(\'#ccn\').val(),
       cvc: $(\'#cvv\').val(),
       exp_month: $(\'#exp1\').val(),
       exp_year: $(\'#exp2\').val().substr(2)
       }, stripeResponseHandler);
       return false;

     }
    });
   });
   </script>';

#die(print_r($customJS[$terminal]));
/*******************************************************************************************************
    PAYPAL CONFIGURATION VARIABLES
********************************************************************************************************/
$enable_paypal = false; //shows/hides paypal payment option from payment form.
$paypal_merchant_email = "your_paypal_merchant_email@here.com";
$paypal_success_url = "http://www.domain.com/path/to/stripe-payment-terminal/paypal_thankyou.php";
$paypal_cancel_url = "http://www.domain.com/path/to/stripe-payment-terminal/paypal_cancel.php";
$paypal_ipn_listener_url = "http://www.domain.com/path/to/stripe-payment-terminal/paypal_listener.php";
$paypal_custom_variable = "some_var";
$paypal_currency = "USD";
$sandbox = false; //if you want to test payments with your sandbox account change to true (you must have account at https://developer.paypal.com/ and YOU MUST BE LOGGED IN WHILE TESTING!)
if($liveMode){ $sandbox = false; } else { $sandbox = true; }


//DO NOT CHANGE ANYTHING BELOW THIS LINE, UNLESS SURE OF COURSE
define("PAYMENT_MODE",$payment_mode);
if(!$sandbox){
    define("PAYPAL_URL_STD","https://www.paypal.com/cgi-bin/webscr");
} else {
    define("PAYPAL_URL_STD","https://www.sandbox.paypal.com/cgi-bin/webscr");
}
//DO NOT CHANGE ANYTHING BELOW THIS LINE, UNLESS SURE OF COURSE
if($redirect_non_https){
	if ($_SERVER['SERVER_PORT']!=443) {
		$sslport=443; //whatever your ssl port is
		$url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		header("Location: $url");
		exit();
	}
}
?>