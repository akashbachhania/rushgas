<?php
///echo do_shortcode( '[pmpro_levels]' );
//function numediaweb_custom_user_profile_fields($user) {

global $current_user, $pmpro_levels;
//global $wpdb, $pmpro_msg, $pmpro_msgt, $pmpro_levels, $current_user, $pmpro_currency_symbol;
$action =$_GET['action'];
$id=$_GET['id'];
if($action!='' && $action=='service'){
	include_once('profile/profile-services-address-add.php');
}
elseif ($action!='' && $action=='serviceall') {
	include_once('profile/profile-services-address-all.php');
}
elseif ($action!='' && $action=='update') {
	include_once('profile/profile-services-address-update.php');
}
elseif ($action!='' && $action=='delete') {
	 $address_id = $wpdb->delete( 
                'wp_user_service_addresses', 
              
                array('address_id' => $id )
               
              );
    if($address_id){
        wp_redirect(admin_url('admin.php?page=rg_profile_setup&action=serviceall'));
    }
}
else{

get_currentuserinfo();

if ( isset( $_POST['avatar-manager-upload-avatar'] ) && $_POST['avatar-manager-upload-avatar'] ) {

	
	
	$user_id = wp_update_user( array( 'ID' => $current_user->ID, 
		'display_name' => $_POST['rg_display_name'],
		'first_name'	=> $_POST['rg_first_name'],
		'last_name'	=> $_POST['rg_last_name'],
		'user_email'	=> $_POST['rg_email_address'],
		 ) );
	$phone_data 	= 	$_POST['rg_phone_number'];
	$address_data 	= 	$_POST['rg_address_1'];
	$rg_address_2 	= 	$_POST['rg_address_2'];
	$rg_city 		= 	$_POST['rg_city'];
	$rg_state 		= 	$_POST['rg_state'];
	$rg_postcode 	= 	$_POST['rg_postcode'];
	update_user_meta( $current_user->ID, 'phone', $phone_data );
	update_user_meta( $current_user->ID, 'address', $address_data );
	update_user_meta( $current_user->ID, 'address_2', $rg_address_2 );
	update_user_meta( $current_user->ID, 'city', $rg_city );
	update_user_meta( $current_user->ID, 'state', $rg_state );
	update_user_meta( $current_user->ID, 'postcode', $rg_postcode );
	if(!empty($_FILES['profilePicture']['name'])) {

		if ( ! function_exists( 'wp_handle_upload' ) )
			require_once( ABSPATH . 'wp-admin/includes/file.php' );

			// An associative array with allowed MIME types.
			$mimes = array(
				'bmp'  => 'image/bmp',
				'gif'  => 'image/gif',
				'jpe'  => 'image/jpeg',
				'jpeg' => 'image/jpeg',
				'jpg'  => 'image/jpeg',
				'png'  => 'image/png',
				'tif'  => 'image/tiff',
				'tiff' => 'image/tiff'
			);

			// An associative array to override default variables.
			$overrides = array(
				'mimes'     => $mimes,
				'test_form' => false
			);

			// Handles PHP uploads in WordPress.
			$avatar = wp_handle_upload( $_FILES['profilePicture'], $overrides );


			if ( isset( $avatar['error'] ) )
			// Kills WordPress execution and displays HTML error message.
			wp_die( $avatar['error'],  __( 'Image Upload Error', 'avatar-manager' ) );

			if ( ! empty( $custom_avatar ) )
			// Deletes users old avatar image.
			avatar_manager_delete_avatar( $custom_avatar );

			// An associative array about the attachment.
			$attachment = array(
				'guid'           => $avatar['url'],
				'post_content'   => $avatar['url'],
				'post_mime_type' => $avatar['type'],
				'post_title'     => basename( $avatar['file'] )
			);

			// Inserts the attachment into the media library.
			$attachment_id = wp_insert_attachment( $attachment, $avatar['file'] );

			// Generates metadata for the attachment.
			$attachment_metadata = wp_generate_attachment_metadata( $attachment_id, $avatar['file'] );

			// Updates metadata for the attachment.
			wp_update_attachment_metadata( $attachment_id, $attachment_metadata );

			$custom_avatar = array();		

			// Generates a resized copy of the avatar image.
			// $custom_avatar[ $options['default_size'] ] = avatar_manager_avatar_resize2( $avatar['url'], $options['default_size'] );

			// Updates attachment meta fields based on attachment ID.

			// Updates user meta fields based on user ID.
			update_user_meta( $current_user->ID, 'avatar_manager_avatar_type', 'custom' );
			update_user_meta( $current_user->ID, 'avatar_manager_custom_avatar', $attachment_id );

	}

}

function avatar_manager_avatar_resize2( $url, $size ) {
    // Retrieves path information on the currently configured uploads directory.
    $upload_dir = wp_upload_dir();
 
    $filename  = str_replace( $upload_dir['baseurl'], $upload_dir['basedir'], $url );
    $pathinfo  = pathinfo( $filename );
    $dirname   = $pathinfo['dirname'];
    $extension = $pathinfo['extension'];
 
    // i18n friendly version of basename().
    $basename = wp_basename( $filename, '.' . $extension );
 
    $suffix    = $size . 'x' . $size;
    $dest_path = $dirname . '/' . $basename . '-' . $suffix . '.' . $extension;
    $avatar    = array();
 
    if ( file_exists( $dest_path ) ) {
        $avatar['url']  = str_replace( $upload_dir['basedir'], $upload_dir['baseurl'], $dest_path );
        $avatar['skip'] = true;
    } else {
        // Retrieves a WP_Image_Editor instance and loads a file into it.
        $image = wp_get_image_editor( $filename );
 
        if ( ! is_wp_error( $image ) ) {
            // Resizes current image.
            $image->resize( $size, $size, true );
 
            // Saves current image to file.
            $image->save( $dest_path );
 
            $avatar['url']  = str_replace( $upload_dir['basedir'], $upload_dir['baseurl'], $dest_path );
            $avatar['skip'] = false;
        }
    }
 
    // Calls the functions added to avatar_manager_avatar_resize action hook.
    do_action( 'avatar_manager_avatar_resize2', $url, $size );
 
    return $avatar;
}
?>
<style type="text/css">
	* {
  .border-radius(0) !important;
}

#field {
    margin-bottom:20px;
}
#field_teleno {
    margin-bottom:20px;
}
.btn span.glyphicon {    			
	opacity: 0;				
}
.btn.active span.glyphicon {				
	opacity: 1;				
}


.pmpro_btn {

    -moz-user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
    cursor: pointer;
    display: inline-block;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857;
    margin-bottom: 0;
    padding: 6px 12px;
    text-align: center;
    vertical-align: middle;
    white-space: nowrap;
     background-color: #ED1C24;
    border-color: #ED1C24;
    color: #fff;
    text-decoration: none;
}
.pmpro_btn a:hover ,a:focus {
    outline: 0 none;
    text-decoration: none;
}

</style>
<div class="container">
	<h2 class="pull-left">Profile </h2>
	<h2 ><a style="z-index:999;" href="<?=admin_url('/admin.php?page=rg_profile_setup&action=serviceall');?>" 
		title="Manage and add services addresses for getting car wash and gas fill ups" class="btn pull-right btn-danger">Manage Service Address</a>
	</h2><div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">

		
<form name="form_action" class="form-horizontal" method="post"  enctype="multipart/form-data">
	
	<table class="form-table">
		<tr>
			<th colspan="2">
				<h4>Profile Information</h4>
				
			</th>
			
		</tr>
		<tr>
			<th>
				<label for="rg_profile_image"><?php _e('Profile Image', 'rg'); ?></label>
			</th>
			<td>
				<?php
				$id = get_user_meta($current_user->ID, 'avatar_manager_custom_avatar', true);
				if(empty($id)) {
				?>
					<?php echo get_avatar( $current_user->ID, 200 );  ?>
				<?php } else {
					$url =  wp_get_attachment_url( $id );
				?>
					<img class="avatar avatar-32 wp-user-avatar wp-user-avatar-32 photo avatar-default" width="200" height="200" alt="" src="<?php echo $url;?>">
				<?php
				}
				?>
				
				 <input type='file' name="profilePicture" value="<?php _e( 'Upload Image', 'rg' ); ?>" id="profilePicture"/><br />
				 
				
				
			</td>
		</tr>
		<tr>
			<th>
				<label for="rg_display_name"><?php _e('Display Name', 'rg'); ?></label>
			</th>
			<td>
				<input type="text" name="rg_display_name" id="rg_display_name" value="<?php echo esc_attr(  $current_user->display_name  ); ?>" class="regular-text" />
				<br><span class="description"><?php _e('Enter your display name', 'rg'); ?></span>		
			</td>
		</tr>
		<tr>
			<th>
				<label for="rg_display_name"><?php _e('Password', 'rg'); ?></label>
			</th>
			<td>
				<a href="javascript:void(0);" class="password_update">Update Password</a>	
			</td>
		</tr>
		<tr>
			<th colspan="2">
				<h4>Personel Information</h4>
				
			</th>
			
		</tr>
		<tr>
			<th>
				<label for="rg_first_name"><?php _e('First Name'); ?></label>
			</th>
			<td>
				<input type="text" name="rg_first_name" id="rg_first_name" value="<?php echo esc_attr( get_the_author_meta( 'first_name', $current_user->ID ) ); ?>" class="regular-text" />
				<br><span class="description"><?php _e('Enter first your name.', 'rg'); ?></span>
			</td>
		</tr>
		<tr>
			<th>
				<label for="rg_last_name"><?php _e('Last Name', 'rg'); ?></label>
			</th>
			<td>
				<input type="text" name="rg_last_name" id="rg_last_name" value="<?php echo esc_attr( get_the_author_meta( 'last_name', $current_user->ID ) ); ?>" class="regular-text" />
				<br><span class="description"><?php _e('Enter your last name', 'rg'); ?></span>		
			</td>
		</tr>
		
		</tr>


		<tr>
			<th>
				<label for="rg_email_address"><?php _e('Email Address', 'rg'); ?></label>
			</th>
			<td>
				<input type="text" name="rg_email_address" id="rg_email_address" value="<?php echo esc_attr( $current_user->user_email ); ?>" class="regular-text" /> 
				
				<br><span class="description"><?php _e('Enter your email address', 'rg'); ?></span>
				
			</td>
		</tr>
		<tr>
			<th>
				<label for="rg_phone_number"><?php _e('Phone', 'rg'); ?></label>
			</th>
			<td>
				<input type="text" name="rg_phone_number" id="rg_phone_number" value="<?php echo esc_attr( get_the_author_meta( 'phone', $current_user->ID ) ); ?>" class="regular-text" /> 
				
				<br><span class="description"><?php _e('Enter your phone number', 'rg'); ?></span>
				
			</td>
		</tr>
		<tr>
			<th>
				<label for="rg_address_1"><?php _e('Address', 'rg'); ?></label>
			</th>
			<td>
				<input type="text" name="rg_address_1" id="rg_address_1" value="<?php echo esc_attr( get_the_author_meta( 'address', $current_user->ID ) ); ?>" class="regular-text" /> 
				
				<br><span class="description"><?php _e('Enter your Address', 'rg'); ?></span>
				
			</td>
		</tr>
		<tr>
			<th>
				<label for="rg_address_2"><?php _e('Address 2', 'rg'); ?></label>
			</th>
			<td>
				<input type="text" name="rg_address_2" id="rg_address_2" value="<?php echo esc_attr( get_the_author_meta( 'address_2', $current_user->ID ) ); ?>" class="regular-text" /> 
				
				<br><span class="description"><?php _e('Enter your Address 2', 'rg'); ?></span>
				
			</td>
		</tr>
		<tr>
			<th>
				<label for="rg_city"><?php _e('City', 'rg'); ?></label>
			</th>
			<td>
				<input type="text" name="rg_city" id="rg_city" value="<?php echo esc_attr( get_the_author_meta( 'city', $current_user->ID ) ); ?>" class="regular-text" /> 
				
				<br><span class="description"><?php _e('Enter your City', 'rg'); ?></span>
				
			</td>
		</tr>
		<tr>
			<th>
				<label for="rg_state"><?php _e('State', 'rg'); ?></label>
			</th>
			<td>
				<input type="text" name="rg_state" id="rg_state" value="<?php echo esc_attr( get_the_author_meta( 'state', $current_user->ID ) ); ?>" class="regular-text" /> 
				
				<br><span class="description"><?php _e('Enter your State', 'rg'); ?></span>
				
			</td>
		</tr>
		<tr>
			<th>
				<label for="rg_postcode"><?php _e('Postcode', 'rg'); ?></label>
			</th>
			<td>
				<input type="text" name="rg_postcode" id="rg_postcode" value="<?php echo esc_attr( get_the_author_meta( 'postcode', $current_user->ID ) ); ?>" class="regular-text" /> 
				
				<br><span class="description"><?php _e('Enter your Postcode', 'rg'); ?></span>
				
			</td>
		</tr>
		
		<tr>
			<th colspan="2">
				<h4>Membership Information</h4>
				
			</th>
			
		</tr>
		<tr>

			<th>
				<label for="rg_profile_image"><?php _e('Member Levels', 'rg'); ?></label>
			</th>
			<td>
			<?php //bartag echo pmpro_advanced_levels_shortcode( ); ?>
				<?php echo do_shortcode( '[memberlite_levels]' ) ?>

			</td>
		</tr>

		<tr>
			<td>
				<input type="submit" name="avatar-manager-upload-avatar" class="button button-primary" value="Update Profile">
			</td>
		</tr>


	</table>

</form>
</div></div>
</div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog" data-backdrop="false">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Password</h4>
      </div>
      <div class="modal-body">
      	 <form class="form-horizontal" id="update_pass" action="" method='post'>
        <div class="form-group">
            <label class="control-label col-sm-4" for="old_password">Old Password:</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="old_password"  required name="old_password" placeholder="Enter Old Password">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-4" for="new_password">New Password:</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="new_password"  required name="new_password" placeholder="Enter New Password">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-4" for="c_password">Confirm Password:</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="c_password"  required name="c_password" placeholder="Enter Confirm Password">
            </div>
          </div>
           <div class="form-group"> 
            <div class="col-sm-offset-4 col-sm-8">
              <button type="submit" name="add_payment"  class="btn btn-danger">Submit</button>
            </div>
          </div>
      </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">
	jQuery('.password_update').on('click',function(){
		 	jQuery('#myModal').appendTo("body").modal('show');

		 });   
 jQuery(document).on('submit','#update_pass',function(e){
 	e.preventDefault();
 	var form=jQuery('#update_pass').serializeArray();
 	console.log('update pass');
 	form.push({name: 'action', value: 'update_user_password'});
 	console.log(form);

 	jQuery.post("<?=admin_url('admin-ajax.php')?>", form, function(response) { 
 		if(response.error){
 			var error='<div class="alert alert-danger">'+response.error+'</div>';
 			jQuery('#update_pass').before(error);
 		}
 		else{
 			var success='<div class="alert alert-success">'+response.success+'</div>';
 			jQuery('#update_pass').before(success);
 			console.log(response);
 			location.reload();
 		}
		
		//$('.extrainto').append('<div id="'+event.uni_event+'"class="col-md-4 pull-left">'+response+'</div>');
		
	},'json');
 });

</script>
<?php
/*if(!empty($_FILES['myfile']['name'])) //validation check image uploaded or not
 {
 $uploaded_file_type = $_FILES["myfile"]["type"]; // Get image type
 $allowed_file_types = array('image/jpg','image/jpeg','image/gif','image/png'); // allowed image formats
 if(in_array($uploaded_file_type, $allowed_file_types)) // validate image format with the given formats
{
 if(!function_exists('wp_handle_upload' )) require_once( ABSPATH . 'wp-admin/includes/file.php' ); // include file library where upload handler function included.

 $uploadedfile = $_FILES['myfile'];
 $upload_overrides = array( 'test_form' => false );
 $movefile = wp_handle_upload( $uploadedfile, $upload_overrides ); // save file on server.
 if(!empty($movefile['url'])) // validate image uploading
 {
 update_usermeta( $userdata->ID, 'user_meta_image', $movefile['url'] ); // update user meta database
 }
 }
 else
 {
 $errmsg = "Invalid Image Format."; // errors if invalid format.
 }
 }
*/
}