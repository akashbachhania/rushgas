<?php
	/* define global varibles*/
	global $current_user,$wpdb;
	if(isset($_GET['vehicles']) && $_GET['vehicles']=='addnew'){
		if(isset($_POST['add_vehicle'])){
			
			$nickname	= $_POST['name'];
			$make		= $_POST['make'];
			$model		= $_POST['model'];
			$color		= $_POST['color'];
			$vh_year	= $_POST['vh_year'];
			$lpm		= $_POST['lpm'];
			$user_id	= $current_user->ID;
			if(!empty($_FILES['image']['name'])) {

				if ( ! function_exists( 'wp_handle_upload' ) )
					require_once( ABSPATH . 'wp-admin/includes/file.php' );

					// An associative array with allowed MIME types.
					$mimes = array(
						'bmp'  => 'image/bmp',
						'gif'  => 'image/gif',
						'jpe'  => 'image/jpeg',
						'jpeg' => 'image/jpeg',
						'jpg'  => 'image/jpeg',
						'png'  => 'image/png',
						'tif'  => 'image/tiff',
						'tiff' => 'image/tiff'
					);

					// An associative array to override default variables.
					$overrides = array(
						'mimes'     => $mimes,
						'test_form' => false
					);

					// Handles PHP uploads in WordPress.
					$avatar = wp_handle_upload( $_FILES['image'], $overrides );

                     
					if ( isset( $avatar['error'] ) )
					// Kills WordPress execution and displays HTML error message.
					wp_die( $avatar['error'],  __( 'Image Upload Error', 'avatar-manager' ) );

					if ( empty( $custom_image ) && $custom_image!=0 )
					// Deletes users old avatar image.
					 wp_delete_attachment( $custom_image);

					// An associative array about the attachment.
					$attachment = array(
						'guid'           => $avatar['url'],
						'post_content'   => $avatar['url'],
						'post_mime_type' => $avatar['type'],
						'post_title'     => basename( $avatar['file'] )
					);

					// Inserts the attachment into the media library.
					$attachment_id = wp_insert_attachment( $attachment, $avatar['file'] );

					// Generates metadata for the attachment.
					$attachment_metadata = wp_generate_attachment_metadata( $attachment_id, $avatar['file'] );

					// Updates metadata for the attachment.
					wp_update_attachment_metadata( $attachment_id, $attachment_metadata );
					$addVehicleInfo=		array( 
													'nickname' 	=> $nickname, 
													'make' 		=> $make, 
													'model' 	=> $model , 
													'color' 	=> $color ,
													'vh_year' 	=> $vh_year , 
													'lpm' 		=> $lpm ,
													'v_image' 	=> $attachment_id , 
													'user_id' 	=> $user_id 
												);
				}
				else{

						$addVehicleInfo=		array( 
													'nickname' 	=> $nickname, 
													'make' => $make, 
													'model' => $model , 
													'color' => $color ,
													'vh_year' => $vh_year , 
													'lpm' => $lpm , 
													'user_id' => $user_id 
													);
				}

			$vehicle_id = $wpdb->insert( 
									'wp_vehicle_info', 
									$addVehicleInfo
									
									);
		 	if($vehicle_id){
		 		//$wpdb->insert( 'wp_user_history', array( 'notification' => 2, 'user_id' => $user_id ) );
          		wp_redirect(admin_url('admin.php?page=rg_vehicles_setup'));
      		}
      		

		}
 		include_once('rg-vehicles-page-add-new-part.php');
	}
	else if(isset($_GET['vehicles']) && $_GET['vehicles']=='updatevehicle'){
		$id=$_GET['id'];
		$vehicle_info=$wpdb->get_row('select * from wp_vehicle_info where id='.$id);
		
		if(isset($_POST['update_vehicle'])){
			$nickname	= $_POST['name'];
			$make			= $_POST['make'];
			$model			= $_POST['model'];
			$color			= $_POST['color'];
			$vh_year		= $_POST['vh_year'];
			$lpm			= $_POST['lpm'];
			$user_id		= $current_user->ID;
			$custom_image	= $vehicle_info->v_image;
			if(!empty($_FILES['image']['name'])) {

				if ( ! function_exists( 'wp_handle_upload' ) )
					require_once( ABSPATH . 'wp-admin/includes/file.php' );

					// An associative array with allowed MIME types.
					$mimes = array(
						'bmp'  => 'image/bmp',
						'gif'  => 'image/gif',
						'jpe'  => 'image/jpeg',
						'jpeg' => 'image/jpeg',
						'jpg'  => 'image/jpeg',
						'png'  => 'image/png',
						'tif'  => 'image/tiff',
						'tiff' => 'image/tiff'
					);

					// An associative array to override default variables.
					$overrides = array(
						'mimes'     => $mimes,
						'test_form' => false
					);

					// Handles PHP uploads in WordPress.
					$avatar = wp_handle_upload( $_FILES['image'], $overrides );

                     
					if ( isset( $avatar['error'] ) )
					// Kills WordPress execution and displays HTML error message.
					wp_die( $avatar['error'],  __( 'Image Upload Error', 'avatar-manager' ) );

					if ( empty( $custom_image ) && $custom_image!=0 )
					// Deletes users old avatar image.
					 wp_delete_attachment( $custom_image);

					// An associative array about the attachment.
					$attachment = array(
						'guid'           => $avatar['url'],
						'post_content'   => $avatar['url'],
						'post_mime_type' => $avatar['type'],
						'post_title'     => basename( $avatar['file'] )
					);

					// Inserts the attachment into the media library.
					$attachment_id = wp_insert_attachment( $attachment, $avatar['file'] );

					// Generates metadata for the attachment.
					$attachment_metadata = wp_generate_attachment_metadata( $attachment_id, $avatar['file'] );

					// Updates metadata for the attachment.
					wp_update_attachment_metadata( $attachment_id, $attachment_metadata );
					$updateVehicleInfo=		array( 
													'nickname' 	=> $nickname, 
													'make' => $make, 
													'model' => $model , 
													'color' => $color ,
													'vh_year' => $vh_year , 
													'lpm' => $lpm ,
													'v_image' => $attachment_id , 
													'user_id' => $user_id 
													);
				}
				else{

						$updateVehicleInfo=		array( 
													'nickname' 	=> $nickname, 
													'make' => $make, 
													'model' => $model , 
													'color' => $color ,
													'vh_year' => $vh_year , 
													'lpm' => $lpm , 
													'user_id' => $user_id 
													);
				}


		
			$vehicle_id = $wpdb->update( 
									'wp_vehicle_info',
									$updateVehicleInfo ,
									array('id' => $id )
									
									);
			print_r($vehicle_id);
		 	if($vehicle_id){
		 		//$wpdb->insert( 'wp_user_history', array( 'notification' => 3, 'user_id' => $user_id ) );
          		wp_redirect(admin_url('admin.php?page=rg_vehicles_setup'));
      		}
      	}
		include_once('rg-vehicles-page-update-vehicle-part.php');
	}
	else if(isset($_GET['vehicles']) && $_GET['vehicles']=='deletevehicle'){
		$id=$_GET['id'];
		$vehicle_id = $wpdb->delete( 
									'wp_vehicle_info',
									array('id' => $id ),
									array('%d') 
									);
		if($vehicle_id){
			    $user_id	= $current_user->ID;
				//$wpdb->insert( 'wp_user_history', array( 'notification' => 4, 'user_id' => $user_id ) );
          		wp_redirect(admin_url('admin.php?page=rg_vehicles_setup'));
      	}
	}
	else{
		include_once 'rg-vehicles-page-all-part.php';
    }
	
