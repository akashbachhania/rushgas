<?php
#******************************************************************************
#                       Credit Card Payments Wordpress v3.0
#
#	Author: Convergine.com
#	http://www.convergine.com
#	Version: 3.0
#   (c) 2013 Convergine.com
#
#******************************************************************************
session_start();
error_reporting(E_ALL ^ E_NOTICE);

require (CCPT_HOME_DIR."/wp-load.php"); 
require("functions.php");
date_default_timezone_set("Australia/Sydney");
/*******************************************************************************************************
    GENERAL SCRIPT CONFIGURATION VARIABLES
********************************************************************************************************/

$configProcessor=is_string(get_option('ccwp_details_EWAY'))?unserialize(get_option('ccwp_details_EWAY')):get_option('ccwp_details_EWAY');

//THIS IS TITLE ON PAGES
$title = "eWay Payment Terminal v1.1"; //site title
$titleAboveForm = "eWay Payment Terminal";

//THIS IS ADMIN EMAIL FOR NEW PAYMENT NOTIFICATIONS.
$admin_email = get_option('ccpt_admin_email'); //this email is for notifications about new payments

$show_services = get_option('ccpt_show_dd_text'); 
define("PTP_CURRENCY_CODE",get_option('ccpt_paypal_currency')); 
// set  to   RECUR  - for recurring payments, ONETIME - for one-time payments.
$payment_mode = "ONETIME"; // RECUR  or ONETIME

//SET EXPIRATION OF THE SUBSCRIPTION
$recurring_end_period = date("d/m/Y", strtotime(date("Y-m-d")." +1 year")); // this example will subscribe your customers for 1 year of payments, defined below.
//RECURRING SERVICES DEFINITION (dropdown)
//service name   |   price  to charge   | Billing period  1 = Days, 2 = Weeks, 3 = Months, 4 = Years  |  Interval between payments IN DAYS, can be from 1 to 31 only | subscription end date.
$recur_services = array(
				 array("Service 1 monthly", "49.15", "1", "31", $recurring_end_period),
				 array("Service 1 quaterly", "149.15", "3", "3", $recurring_end_period),
				 array("Service 1 semi-annualy", "249.99", "3", "6", $recurring_end_period),
				 array("Service 1 annualy", "349.99", "4", "1", $recurring_end_period)
				);

/*******************************************************************************************************
    EWAY CONFIGURATION VARIABLES
********************************************************************************************************/
// Set this to your eWAY Customer ID
$eWAY_CustomerID = $configProcessor['live']['CUSTOMMER_ID'];
// Set this to the payment gatway you would like to use (REAL_TIME, REAL_TIME_CVN or GEO_IP_ANTI_FRAUD)
$eWAY_PaymentMethod = REAL_TIME;
// TO GO LIVE CHANGE FOLLOWING LINE TO TRUE
$liveMode = get_option('ccpt_test')==2 ? true : false;
$eWAY_UseLive = $liveMode;

//following line defines if customer has to be redirected to HTTPS (if eWAY is LIVE -> redirects to https, else - stays on http)
$redirect_non_https = $eWAY_UseLive == false?false:true;

/****************************************************
//TEST CREDIT CARD CREDENTIALS for SANDBOX TESTING
Card Type: Visa
Account Number: 4444333322221111
The transaction result is based on the cent value of the transaction. ( http://www.eway.com.au/developers/resources/response-codes.html )
$10.15 will return a failed transaction with a response code of "15 – No Issuer",
while $10.00 will return "00 – Transaction Approved."
****************************************************/

/*******************************************************************************************************
    PAYPAL EXPRESS CHECKOUT CONFIGURATION VARIABLES
********************************************************************************************************/
$enable_paypal = false; //shows/hides paypal payment option from payment form.
$paypal_merchant_email = "your_paypal@email.here";
$paypal_success_url = "http://www.domain.com/path/to/eway-payment-terminal/paypal_thankyou.php";
$paypal_cancel_url = "http://www.domain.com/path/to/eway-payment-terminal/paypal_cancel.php";
$paypal_ipn_listener_url = "http://www.domain.com/path/to/eway-payment-terminal/paypal_listener.php";
$paypal_custom_variable = "some_var";
$paypal_currency = "USD";
$sandbox = false; //if you want to test payments with your sandbox account change to true (you must have account at https://developer.paypal.com/ and YOU MUST BE LOGGED IN WHILE TESTING!)


//DO NOT CHANGE ANYTHING BELOW THIS LINE, UNLESS SURE OF COURSE
if($redirect_non_https){
	if ($_SERVER['SERVER_PORT']!=443) {
		$sslport=443; //whatever your ssl port is
		$url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		header("Location: $url");
		exit();
	}
}

define("PAYMENT_MODE",$payment_mode);
if(!$sandbox){
    define("PAYPAL_URL","https://www.paypal.com/cgi-bin/webscr");
} else {
    define("PAYPAL_URL","https://www.sandbox.paypal.com/cgi-bin/webscr");
}

//define default values for eway
define('EWAY_DEFAULT_CUSTOMER_ID',$eWAY_CustomerID);
define('EWAY_DEFAULT_PAYMENT_METHOD', $eWAY_PaymentMethod);
define('EWAY_DEFAULT_LIVE_GATEWAY', $eWAY_UseLive); //<false> sets to testing mode, <true> to live mode

    //define script constants
define('REAL_TIME', 'REAL-TIME');
define('REAL_TIME_CVN', 'REAL-TIME-CVN');
define('GEO_IP_ANTI_FRAUD', 'GEO-IP-ANTI-FRAUD');

    //define URLs for payment gateway
define('EWAY_PAYMENT_LIVE_REAL_TIME', 'https://www.eway.com.au/gateway/xmlpayment.asp');
define('EWAY_PAYMENT_LIVE_REAL_TIME_TESTING_MODE', 'https://www.eway.com.au/gateway/xmltest/testpage.asp');
define('EWAY_PAYMENT_LIVE_REAL_TIME_CVN', 'https://www.eway.com.au/gateway_cvn/xmlpayment.asp');
define('EWAY_PAYMENT_LIVE_REAL_TIME_CVN_TESTING_MODE', 'https://www.eway.com.au/gateway_cvn/xmltest/testpage.asp');
define('EWAY_PAYMENT_LIVE_GEO_IP_ANTI_FRAUD', 'https://www.eway.com.au/gateway_beagle/xmlbeagle.asp');
define('EWAY_PAYMENT_LIVE_GEO_IP_ANTI_FRAUD_TESTING_MODE', 'https://www.eway.com.au/gateway_beagle/test/xmlbeagle_test.asp'); //in testing mode process with REAL-TIME
define('EWAY_PAYMENT_HOSTED_REAL_TIME', 'https://www.eway.com.au/gateway/payment.asp');
define('EWAY_PAYMENT_HOSTED_REAL_TIME_TESTING_MODE', 'https://www.eway.com.au/gateway/payment.asp');
define('EWAY_PAYMENT_HOSTED_REAL_TIME_CVN', 'https://www.eway.com.au/gateway_cvn/payment.asp');
define('EWAY_PAYMENT_HOSTED_REAL_TIME_CVN_TESTING_MODE', 'https://www.eway.com.au/gateway_cvn/payment.asp');
?>