<?php
 	/* query for get all vehicles of current user*/
	$addresses= $wpdb->get_results("SELECT * FROM wp_user_service_addresses  WHERE user_id = '$current_user->ID'");
	?>
	<!-- Main Container Start -->
	<div class="container">
			<!-- .Page heading -->
	  		<h2>Addresses</h2>
	  		<!-- Link for add New vehicles -->
			<a href="<?=admin_url('/admin.php?page=rg_profile_setup&action=service');?>" class="btn btn-danger pull-right"> 
				Add service address
			</a>		

<?php
			if($addresses):
			
?>				<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="table-responsive">
				 <table class="table " >
				    <thead>
				      <tr>
				        <th>Title</th>
				        <th>Address</th>
				        <th>Address 2</th>
				       
				        <th>City</th>
				        <th>State</th>
				        <th>Postcode</th>
				        <th>Action</th>
				      </tr>
				    </thead>
				    <tbody>
				    	<?php foreach ($addresses as $address) {
				    		
				    		?>
				      <tr>
				      	
				      	<td><?=$address->title;?></td>
				        <td><?=$address->address;?></td>
				        <td><?=$address->address2;?></td>
				      	
				        <td><?=$address->city;?></td>
				        <td><?=$address->state;?></td>
				        <td><?=$address->postcode;?></td>
				        <td>
				        	<a href="<?=admin_url('/admin.php?page=rg_profile_setup&action=update&id='.$address->address_id);?>">
				        		Update
				        	</a>
				        	<a href="<?=admin_url('/admin.php?page=rg_profile_setup&action=delete&id='.$address->address_id);?>">
				        		Delete
				        	</a>
				        </td>
				      </tr>
				     <? }?>
				    </tbody>
				  </table>
				</div>
<?php 		endif;
?>
			</div>
			</div>
		</div>