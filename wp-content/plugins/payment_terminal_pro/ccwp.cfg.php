<?php
#******************************************************************************
#                       Credit Card Payments Wordpress v3.0
#
#	Author: Convergine.com
#	http://www.convergine.com
#	Version: 3.0
#   (c) 2013 Convergine.com
#
#******************************************************************************
$ccwp_cfg_arr = array(
/* terminal =>( LIVE ( field => value ) , TEST ( field => value ) )*/
'PayPall'=>array(
	'LIVE'=>array(
		'Api User Name' =>'your_LIVE_api_user_name',
		'Api Password'  => 'your_LIVE_api_password',
		'Api Signature' => 'your_LIVE_api_signature'
	),
	'TEST'=>array(
		'Api User Name' =>'your_TEST_api_user_name',
		'Api Password'  => 'your_TEST_api_password',
		'Api Signature' => 'your_TEST_api_signature'
	),
	'TIP'=>'Tip: You need to get these settings from merchant account provider.'
),
'Optimal'=>array(
	'LIVE'=>array(
		'Account'=>'your_LIVE_account',
		'Merchant ID' => 'your_LIVE_merchant_id',
		'Merchant Password'=>'your_LIVE_merchant_password' 
	),
	'TEST'=>array(
		'Account'=>'your_TEST_account',
		'Merchant ID' => 'your_TEST_merchant_id',
		'Merchant Password'=>'your_TEST_merchant_password' 
	)
),
'Moneris US'=>array(
	'LIVE'=>array(
		'Moneris Api Token'=>'your_LIVE_api_token',
		'Moneris Api Url'=>'your_LIVE_api_url',
		'Moneris Store ID'=>'your_LIVE_store_id'
	),
	'TEST'=>array(
		'Moneris Api Token'=>'your_TEST_api_token',
		'Moneris Api Url'=>'your_TEST_api_url',
		'Moneris Store ID'=>'your_TEST_store_id'
	)
),
'Moneris Ca'=>array(
	'LIVE'=>array(
		'Moneris Api Token'=>'your_LIVE_api_token',
		'Moneris Api Url'=>'your_LIVE_api_url',
		'Moneris Store ID'=>'your_LIVE_store_id'
	),
	'TEST'=>array(
		'Moneris Api Token'=>'your_TEST_api_token',
		'Moneris Api Url'=>'your_TEST_api_url',
		'Moneris Store ID'=>'your_TEST_store_id'
	)
),
'Authorize.Net'=>array(
	'LIVE'=>array(
		'Merchant Login'=>'your_LIVE_merchant_login',
		'Merchant Tran Key'=>'your_LIVE_merchant_tran_key'
	),
	'TEST'=>array(
		'Merchant Login'=>'your_TEST_merchant_login',
		'Merchant Tran Key'=>'your_TEST_merchant_tran_key'
	)
),
'InternetSecure'=>array(
	'TEST/LIVE'=>array( 
		'Merchant ID'=>'your_LIVE_merchant_id'
	) /* test credentials not required */
),
'Elavon'=>array(
	'TEST/LIVE'=>array(
		'User ID'=>'your_LIVE_user_id',
		'Merchant ID'=>'your_LIVE_merchant_id',
		'PIN'=>'your_LIVE_pin'
	) /* test credentials not required */
),
'FirstData'=>array(
	'LIVE'=>array(
			'Store ID'=>'your_LIVE_store_id',
			'Certificat File Path'=>'your_certificate_path',
			'Select Certificate[filter]'=>'Certificate File Path' # terminal ->case -> value to replace !the value to replace must be above!?
		),	/*must be a empty space before filter ' [filter]' or any text that you want to be displayed there */
	'TEST'=>array(
		'Store ID'=>'your_TEST_store_id',
		'Certificat File Path'=>'your_certificate_path',
		'Select Certificate[filter]'=>'Certificate File Path' # terminal ->case -> value to replace !the value to replace must be above!?
	)
),
'Eway'=>array(
	'TEST/LIVE'=>array(
		'Custommer ID'=>'your_LIVE_custommer_id'
	)/* test credentials not required */
),
'Payment Express'=>array(
	'TEST/LIVE'=>array(
		'Username'=>'your_LIVE_username',
		'Password'=>'your_LIVE_password'
	)
),
'Stripe'=>array(
	'LIVE'=>array(
		'Publishable Key'=>'your_LIVE_publishable_key',
		'Secret Key'=>'your_LIVE_secret_key'
	),
	'TEST'=>array(
		'Publishable Key'=>'your_TEST_publishable_key',
		'Secret Key'=>'your_TEST_secret_key'
	)
)
);
function ccwpCRUD($payment_terminals,$action){
	#die(print_r($_FILES));
	$newarr=array();
	$ccpt_test = get_option('ccpt_test');
	foreach($payment_terminals as $terminal => $details){
		$terminal = strtoupper(str_replace(array(',',' ','.','\''), '_',$terminal));
		$wpdbVars = (is_string(get_option('ccwp_details_'.$terminal)))?unserialize(get_option('ccwp_details_'.$terminal)):get_option('ccwp_details_'.$terminal);
		$postVars = array();
		$pseudocase = '';
		foreach($details as $case => $fields){
			if($case == 'TEST' || $case == 'LIVE' || $case == "TEST/LIVE"){
				if($case == "TEST/LIVE"){
					$case = "LIVE";
					$pseudocase = "TEST/LIVE";
				}
				$case_array = array();
				foreach($fields as $field => $value){
					
					$post_name = strtoupper(str_replace(array(',',' ','.','\''), '_', $field));
					$input_id = strtolower(str_replace(array(' ','"','\'','.'), '_', $terminal."_".$case."_".$field));
					if(isset($_POST['ccpt_submit_settings']) && $_POST['ccpt_submit_settings'] == 'yes' && isset($_POST["{$input_id}"])) :
					$post_value = stripslashes_deep($_POST["{$input_id}"]);
					elseif(isset($_POST['ccpt_submit_settings']) && $_POST['ccpt_submit_settings'] == 'yes' && isCCWPfilter($input_id) !=null && isset($_FILES[isCCWPfilter($input_id)]) && strlen($_FILES[isCCWPfilter($input_id)]['name']) > 0):	
					
					 $input_id = isCCWPfilter($input_id);
					 #die(print_r($_FILES));
					 if(strtolower(end(explode('.', $_FILES["{$input_id}"]['name']))) == 'pem'):
						 $uploads = wp_upload_dir();
						 $path    = $uploads['path'];
						 if(move_uploaded_file($_FILES["{$input_id}"]['tmp_name'], $path."/".$_FILES["{$input_id}"]['name'])):
						 	$post_name = strtoupper(str_replace(array(',',' ','.','\''), '_', $value));
						 	$post_value=  $path."/".$_FILES["{$input_id}"]['name'];
						 	if(isset($case_array[$post_name])) unset($case_array[$post_name]);
							$case_array[$post_name] = $post_value;
							$newarr[$terminal][$case][$value] = $post_value;
							$newarr[$terminal][$case][$field] = 'fgm';
							continue;
						 else:
						 $newarr['CUSTOM_ERROR'][$terminal][] = "Please make sure that upload directory '{$path}' has enough permissions to upload the Certificate.";
						 endif;	 	 
					 else:
						 $newarr['CUSTOM_ERROR'][$terminal][] = "The Certificat File for {$terminal} terminal must be .pem Type!-".$_FILES["{$input_id}"]['name'];
					 endif;	 	 	
					elseif($action == "install" || $action == "uninstall"):
					$post_value = $value;	
					else :
					$post_value = $wpdbVars[strtolower($case)][$post_name];
					endif;		
					$case_array[$post_name] = $post_value;
					if($pseudocase == "TEST/LIVE"):
					$newarr[$terminal][$pseudocase][$field] = $post_value;
					else:
					$newarr[$terminal][$case][$field] = $post_value;
					endif;		
				}
			}if($case == "TIP"){$newarr[$terminal][$case]=$fields;continue;}
			$case = strtolower($case);
			$postVars[$case] = $case_array;
		}
		if($action == "install" || (isset($_POST['ccpt_submit_settings']) && $_POST['ccpt_submit_settings'] == 'yes')) : 
		$postVars = serialize($postVars);
		update_option('ccwp_details_'.$terminal,$postVars);	
		elseif($action == "uninstall"):
		delete_option('ccwp_details_'.$terminal);	
		endif;			
	}   unset($postVars);unset($case_array); 
	
	return $newarr;
}
function displaySelectedCond($i,$ccpt_processor){
		return
        (($ccpt_processor==$i && strlen($ccpt_processor) > 0)
		 || 
		($i==1 && strlen($ccpt_processor) < 1 && !isset($_GET['active_terminal']))
		 || 
		(isset($_GET['active_terminal']) && $i==$_GET['active_terminal'])
		 ? ' selected' : '');
}
function isCCWPfilter($field_name){
	if(strpos($field_name, '[filter]') === false){
		return null;
	}else{
		return str_replace('[filter]','', $field_name);
	}
}	
function ccwpDisplayCustomErrors($array){
	#print_r($array);
	foreach($array as $terminal=>$error){
		foreach($error as $key=>$error_text){
			echo "ERROR :".$error_text."<br />";
		}
	}
}
?>