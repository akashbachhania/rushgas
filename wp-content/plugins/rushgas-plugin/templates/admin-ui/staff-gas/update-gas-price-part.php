
<?php
$allservices= $wpdb->get_results("SELECT * FROM 
												wp_user_car_wash_service ws 
										left join 
												wp_user_event_service es
										on
												ws.service_id=es.event_id
										where 
												ws.service_id=2
									");?>
	<!-- Main Container Start -->
	<div class="container">
			<!-- .Page heading -->
	  		<h2>Update gas price</h2>
	  		<!-- Link for add New vehicles -->
	  		<div class="row">
	  			 <div class="col-md-6">
			       <form class="form-horizontal" id="payment-form" action="" method='post'>
			         <div class="payment-errors alert alert-default text-danger"></div>
			         <? foreach ($allservices as $service) :
			         	# code...
			         ?>
			          <div class="form-group">
			            <label class="control-label col-sm-4" for="price"><?=$service->extra_service?> Gas Price:</label>
			            <div class="col-sm-8">
			              <input type="text" class="form-control" id="price"  required name="price[<?=$service->wash_id;?>]" value="<?=$service->sprice?>" placeholder="Enter Gas Price">
			            </div>
			          </div>
			        
			         
			 		<? endforeach;?>
			          
			          <div class="form-group"> 
			            <div class="col-sm-offset-4 col-sm-8">
			              <button type="submit" name="update_price"  class="btn btn-default">Submit</button>
			            </div>
			          </div>
			      </form>


			     </div>
	  		</div>
	  		
	</div>