

	<!-- Main Container Start -->
	<div class="container">
			<!-- .Page heading -->
	  		<h2>Update gas price</h2>
	  		<!-- Link for add New vehicles -->
	  		<div class="row">
	  			 <div class="col-md-6">
			       <form class="form-horizontal" id="payment-form" action="" method='post'>
			         <div class="payment-errors alert alert-default text-danger"></div>
			          <div class="form-group">
			            <label class="control-label col-sm-4" for="price">Gas Price Per Gallon:</label>
			            <div class="col-sm-8">
			              <input type="text" class="form-control" id="price"  required name="price" value="<?=$service->price?>" placeholder="Enter Gas Price">
			            </div>
			          </div>
			        
			         
			 
			          
			          <div class="form-group"> 
			            <div class="col-sm-offset-4 col-sm-8">
			              <button type="submit" name="update_price"  class="btn btn-default">Submit</button>
			            </div>
			          </div>
			      </form>


			     </div>
	  		</div>
	  		
	</div>