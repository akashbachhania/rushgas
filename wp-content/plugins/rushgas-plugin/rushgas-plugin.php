<?php
/*
Plugin Name: Rushgas Plugin
Plugin URI: http://sologicsolutions.com/extend/plugins/rushgas-plugin
Description: Rushgas Plugin
Author: Sologic Solutions 
Version: 1.0
Author URI: http://sologicsolutions.com/
*/
ob_start();
/**
 * Load callback
 */
function rg_plugin_loaded(){
	// plugin	
    $plugin_name = trailingslashit('rushgas-plugin/rushgas-plugin.php');
	
	// register activate
	register_activation_hook($plugin_name, 'rg_activate');
	
	// register deactivate
	register_deactivation_hook($plugin_name, 'rg_deactivate');
	
	//activate
	rg_activate();
	
}
add_action('plugins_loaded',  'rg_plugin_loaded');

/**
 * Activate callback
 */
function rg_activate(){
	
	$upload_dir = wp_upload_dir();
	// Dirs
	define( 'RG_PLUGIN_DIR', WP_PLUGIN_DIR . '/rushgas-plugin/' );
	define( 'RG_PLUGIN_URL', WP_PLUGIN_URL . '/rushgas-plugin/' );

	// Assets
	define( 'RG_PLUGIN_ASSETS_DIR', RG_PLUGIN_DIR . 'assets/' );
	define( 'RG_PLUGIN_ASSETS_URL', RG_PLUGIN_URL . 'assets/' );

	// Templates	
	define( 'RG_PLUGIN_TEMPLATES_DIR', RG_PLUGIN_DIR . 'templates/' );
	define( 'RG_PLUGIN_TEMPLATES_URL', RG_PLUGIN_URL . 'templates/' );

	// Functions
	define( 'RG_PLUGIN_FUNCTIONS_URL', RG_PLUGIN_URL . 'functions/' );

	// Log
	define( 'RG_LOG_DIR', $upload_dir['basedir'] . '/logs/' );
	
	
	@include( 'rg_function.php' );
	@include( 'functions/rg_callback_functions.php' );
	@include( 'functions/rg_shortcode_settings.php' );
	
	
}

/**
 * Deactivate callback
 */
function rg_deactivate(){	
	// what to do
}