/*
#******************************************************************************
#                      WP Paypal Payment Terminal v1.0
#
#	Author: Convergine.com
#	http://www.convergine.com
#	Version: 1.0
#	Released: March 6 2011
#
#******************************************************************************
*/
jQuery(document).ready(function() {
	jQuery('.row_a ul').hover(function() {
	  jQuery(".row_a ul > li").addClass('pretty-hover');
	}, function() {
	  jQuery(".row_a ul > li").removeClass('pretty-hover');
	});
	
	jQuery('.row_b ul').hover(function() {
	  jQuery(".row_b ul > li").addClass('pretty-hover');
	}, function() {
	  jQuery(".row_b ul > li").removeClass('pretty-hover');
	});
});	

function noAlpha(obj){
	reg = /[^0-9.]/g;
	obj.value =  obj.value.replace(reg,"");
}
 
jQuery(function() {
	jQuery( "#date1" ).datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "yy-mm-dd"
	});

	jQuery( "#date2" ).datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "yy-mm-dd"
	});
});
 
/*
jQuery(document).ready(function() {
	jQuery('.row_a ul, .row_b ul').hover(function() {
	  jQuery(this).addClass('pretty-hover');
	}, function() {
	  jQuery(this).removeClass('pretty-hover');
	});
});	*/