<?
$refund=0;
if($_GET['subject']){
  $refund=5;

}?>
  <div class="container">
    <div class="page-header">
      <h1>Add new help message</h1>
      </div>
    <div class="row">
     
    <div class="col-md-6">
       <form class="form-horizontal" id="payment-form" action="" method='post'>
         <div class="payment-errors alert alert-default text-danger"></div>
          <div class="form-group">
            <label class="control-label col-sm-4" for="card_number">Case Name:</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="name"  required name="name" placeholder="Enter name">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-4" >Subject:</label>
            <div class="col-sm-8"> 
              <select class="form-control" name="subject"> 
                <? foreach ($case_subjects as $subject) :?>
                  <option value="<?=$subject->subject_id?>" <?if($refund==$subject->subject_id) echo "selected";?>><?=$subject->subject?></option>
                <? endforeach;?>
              </select>
            </div>
          </div>
         
          <div class="form-group">
            <label class="control-label col-sm-4" for="message">Message:</label>
            <div class="col-sm-8"> 
              <textarea  class="form-control" id="message" required name="message" placeholder="Type Your Message"></textarea>
            </div>
          </div>
          
          <div class="form-group"> 
            <div class="col-sm-offset-4 col-sm-8">
              <button type="submit" name="add_help_reply"  class="btn btn-danger">Submit</button>
            </div>
          </div>
      </form>


     </div>
    </div>
  </div>
 