<?php
// Single row Delete
    function rg_delete_post() {
    	global $wpdb;	
		$id=$_POST['id'];
    	
    	$where=array( 'id' => $id );

		$row=$wpdb->delete( SG_TBL_ABTESTING, $where );
		if(!empty($row)) {
			$response = array('status'=>'success', 'message'=>sprintf(__('Delete successfully','sg')));
		}
    	echo json_encode($response);
    	die();
    }

    add_action('wp_ajax_rg_delete_post', 'rg_delete_post');