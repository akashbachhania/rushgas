<?php
#******************************************************************************
#                       Credit Card Payments Wordpress v3.0
#
#	Author: Convergine.com
#	http://www.convergine.com
#	Version: 3.0
#   (c) 2013 Convergine.com
#
#******************************************************************************
	# PLEASE DO NOT EDIT FOLLOWING LINES IF YOU'RE NOT SURE ------->
		if($show_services==1 && $ccpt_show_am_text==2){
			$query="SELECT * FROM ".$wpdb->prefix."ccpt_services WHERE ccpt_services_id =".$service;
			$result=mysql_query($query) or die(mysql_error()."<br>$query");
			$res=mysql_fetch_assoc($result);
			$amount = number_format($res['ccpt_services_price'],2, ".", "");
			$serviceName=$res['ccpt_services_title'];
		} else if($show_services==1 && $ccpt_show_am_text==1){
		        if(empty($amount)){
		            $query="SELECT * FROM ".$wpdb->prefix."ccpt_services WHERE ccpt_services_id =".$service;
		            $result=mysql_query($query) or die(mysql_error()."<br>$query");
		            $res=mysql_fetch_assoc($result);
		            $amount = number_format($res['ccpt_services_price'],2, ".", "");
		            $serviceName=$res['ccpt_services_title'];
		        }
		    }
		$continue = false;
		if(!empty($amount) && is_numeric($amount)){ 	
			$cctype = (!empty($_POST['cctype']))?strip_tags(str_replace("'","`",strip_tags($_POST['cctype']))):'';
			$ccname = (!empty($_POST['ccname']))?strip_tags(str_replace("'","`",strip_tags($_POST['ccname']))):'';
			$ccn = (!empty($_POST['ccn']))?strip_tags(str_replace("'","`",strip_tags($_POST['ccn']))):'';
			$exp1 = (!empty($_POST['exp1']))?strip_tags(str_replace("'","`",strip_tags($_POST['exp1']))):'';
			$exp2 = (!empty($_POST['exp2']))?strip_tags(str_replace("'","`",strip_tags($_POST['exp2']))):'';
			$cvv = (!empty($_POST['cvv']))?strip_tags(str_replace("'","`",strip_tags($_POST['cvv']))):'';
			
			
			
			//CREDIT CARD PHP VALIDATION 
			if(empty($ccn) || empty($cctype) || empty($exp1) || empty($exp2) || empty($ccname) || empty($cvv) || empty($address) || empty($state) || empty($city)){
				$continue = false;
				$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> Not all required fields were filled out.</p></div></div><br />';
			} else { $continue = true; }
			
			if(!is_numeric($cvv)){
				$continue = false;
				$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> CVV number can contain numbers only.</p></div></div><br />';
			} else {
				$continue = true;
			}
			
			if(!is_numeric($ccn)){
				$continue = false;
				$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> Credit Card number can contain numbers only.</p></div></div><br />';
			} else {
				$continue = true;
			}
			
			if(date("Y-m-d", strtotime($exp2."-".$exp1."-01")) < date("Y-m-d")){
				$continue = false;
				$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> Your credit card is expired.</p></div></div><br />';
			} else {
				$continue = true;
			}
			
			if($continue){
				//echo "1";
				if(validateCC($ccn,$cctype)){
					$continue = true;
				} else { 
					$continue = false;
					$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> The number you\'ve entered does not match the card type selected.</p></div></div><br />';
				}
			}
			
			if($continue){
				if(luhn_check($ccn)){
					$continue = true;
				} else { 
					$continue = false; 
					$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> Invalid credit card number.</p></div></div><br />';
				}
			}
			
			switch($cctype){
				case "V":
					$cctype = "VISA";
				break;
				case "M":
					$cctype = "MASTERCARD";
				break;
				case "D":
					$cctype = "DISCOVER";
				break;
				case "A":
					$cctype = "AMEX";
				break;
			}
			
			if($continue){
				$query="INSERT INTO ".$wpdb->prefix."ccpt_transactions (ccpt_dateCreated, ccpt_amount, ccpt_comment,ccpt_serviceID ) VALUES (NOW(), '".$amount."', '".$item_description."', '".$service."')";
				if(mysql_query($query)){							
				
					$orderID=mysql_insert_id();
				}
				###########################################################################
				###	MONERIS.COM PROCESSING
				###########################################################################
				//PROCESS PAYMENT BY WEBSITE PAYMENTS PRO
				require 'includes/us.moneris.class.php';
				$store_id=MONERIS_STORE_ID;
				$api_token=MONERIS_API_TOKEN;				
				$exp_datePay = "20".(substr($exp2,-2,2))."-".$exp1."-01";
				
				/************************* Transactional Variables ****************************/
				
				$cust_id="MPT1-".rand(1,999999);
				$type='purchase';
				$order_id='ord-'.$cust_id.'-'.date("dmy-G:i:s");
				$pan=$ccn;
				$expiry_date= date("ym", strtotime($exp_datePay));
				$crypt='7';
				
				/*********************** Transactional Associative Array **********************/
				
				/*$txnArray=array('type'=>$type,
								'order_id'=>$order_id,
								'cust_id'=>$cust_id,
								'amount'=>number_format($amount,2),
								'pan'=>$pan,
								'expdate'=>$expiry_date,
								'crypt_type'=>$crypt,
								'cvd_value'  => $cvv,
								'cvd_indicator' => 1
							   );*/
				$txnArray=array('type'=>'us_purchase',
								 'order_id'=>$order_id,
								 'cust_id'=>$cust_id,
								 'amount'=>str_replace(',', '',number_format($amount,2)),
								 'pan'=>$pan,
								 'expdate'=>$expiry_date,
								 'crypt_type'=>$crypt,
								 'commcard_invoice'=>'Invoice '.$cust_id,
								 //'commcard_tax_amount'=>'0.15',
								 'dynamic_descriptor'=>'online sale'
								   );			   
							   
				 //print var_dump($txnArray);
				/**************************** Transaction Object *****************************/
				$mpgTxn = new mpgTransaction($txnArray);
				/****************************** Request Object *******************************/
				$mpgRequest = new mpgRequest($mpgTxn);
				/***************************** HTTPS Post Object *****************************/
				$mpgHttpPost  =new mpgHttpsPost($store_id,$api_token,$mpgRequest);
				/******************************* Response ************************************/
				$mpgResponse=$mpgHttpPost->getMpgResponse();
				
				$AvsResultCode=$mpgResponse->getAvsResultCode();
				$CvdResultCode=$mpgResponse->getCvdResultCode();
				$RecurSuccess=$mpgResponse->getRecurSuccess();
				$TxnNumber=$mpgResponse->getTxnNumber();
				$TransType=$mpgResponse->getTransType();
				$ISO=$mpgResponse->getISO();
				$Message=$mpgResponse->getMessage();
				$AuthCode=$mpgResponse->getAuthCode();
				$TransDate=$mpgResponse->getTransDate();
				$TransTime=$mpgResponse->getTransTime();
				$ResponseCode=(int)$mpgResponse->getResponseCode();
				$CardType=$mpgResponse->getCardType();
				$TransAmount=$mpgResponse->getTransAmount();
				$ReceiptId=$mpgResponse->getReceiptId();
				$ReferenceNum=$mpgResponse->getReferenceNum();
				$BankTotals=$mpgResponse->getBankTotals();
				$Complete=$mpgResponse->getComplete();
				$Ticket=$mpgResponse->getTicket();
				$TimedOut=$mpgResponse->getTimedOut();
				if($ResponseCode<50 && $ResponseCode > 0){
					$resArray["TRANSACTIONID"] = $TxnNumber;
					$Message = trim($Message, "=");
					$Message = !empty($Message)?$Message:"APPROVED";
					$sMessageResponse= "<br /><div>Your payment was <b>".str_replace("=", "", str_replace("*", "", $Message))."</b>!";
					$sMessageResponse .= "<div><br /><strong>Transaction Receipt:</strong>";
					$sMessageResponse .= "<br />Amount: $".$TransAmount;
					$sMessageResponse .= "<br />Transaction Type: ".$TransType;
					$sMessageResponse .= "<br />Date and Time: ".$TransDate;
					$sMessageResponse .= "<br />Auth Code: ".$AuthCode;
					$sMessageResponse .= "<br />Response Code: ".$ResponseCode;
					$sMessageResponse .= "<br />ISO Code: ".$ISO;
					$sMessageResponse .= "<br />Response Message: ".$Message;
					$sMessageResponse .= "<br />Reference Number: ".$ReferenceNum;
					$sMessageResponse .= "<br />Goods and Services Order: $".$BankTotals;
					$sMessageResponse .= "<br />Merchant Name: ".MERCHANT_NAME;
					$sMessageResponse .= "<br />Merchant URL: ".MERCHANT_URL;
					$sMessageResponse .= "<br />Cardholder Name: ".$ccname."</div>";
					$sMessageResponse .= "<br/><a href='index.php'>Return to payment page</a><br /><br/></div>";
					$mess = '<div class="ui-widget"><div class="ui-state-highlight ui-corner-all" style="padding: 0 .7em;">'.$sMessageResponse.'</div></div><br />';
					
					#**********************************************************************************************#
					#		THIS IS THE PLACE WHERE YOU WOULD INSERT ORDER TO DATABASE OR UPDATE ORDER STATUS.
					#**********************************************************************************************#
					
					$q="UPDATE ".$wpdb->prefix."ccpt_transactions SET ccpt_status='2', ccpt_payer_email='".$email."', ccpt_transaction_id='".$resArray["TRANSACTIONID"]."', ccpt_payer_name='".$ccname."' WHERE ccpt_id='".$orderID."'";
							mysql_query($q);
					
					#**********************************************************************************************#
					//-----> send notification 
					//creating message for sending
					$headers  = "MIME-Version: 1.0\n";
					$headers .= "Content-type: text/html; charset=utf-8\n";
					$headers .= "From: 'Moneris Payment Terminal' <noreply@".$_SERVER['HTTP_HOST']."> \n";
					$subject = "New Payment Received";
					$message =  "New payment was successfully received through moneris <br />";					
					$message .= "from ".$fname." ".$lname."  on ".date('m/d/Y')." at ".date('g:i A').".<br /> Payment total is: $".number_format($amount,2);
					if($show_services==1){
							$message .= "<br />Payment was made for \"".$serviceName."\"";
						} else { 
							$message .= "<br />Payment description: \"".$item_description."\"";
						}
					$message .= "<br /><br />Billing Information:<br />";
					$message .= "Full Name: ".$fname." ".$lname."<br />";
					$message .= "Email: ".$email."<br />";
					$message .= "Address: ".$address."<br />";
					$message .= "City: ".$city."<br />";
					$message .= "Country: ".$country."<br />";
					$message .= "State/Province: ".$state."<br />";
					$message .= "ZIP/Postal Code: ".$zip."<br />";
					mail($admin_email,$subject,$message,$headers);
					
					$subject = "Payment Received!";
					$message =  "Dear ".$fname.",<br />";
					$message .= "<br /> Thank you for your payment.";
					$message .= "<br /><br />Kind Regards,<br />".$_SERVER['HTTP_HOST'];	
					mail($email,$subject,$message,$headers);
					
					//-----> send notification end 	
					$show_form=0;		   
					
				} else { 

					$sMessageResponse= "<br /><div>Payment Status: <b>".str_replace("=", "", str_replace("*", "", $Message))."</b>";
					$sMessageResponse .= "<div><br /><strong>Transaction Response:</strong>";
					$sMessageResponse .= "<br />Amount: $".$TransAmount;
					$sMessageResponse .= "<br />Transaction Type: ".$TransType;
					$sMessageResponse .= "<br />Date and Time: ".$TransDate;
					$sMessageResponse .= "<br />Auth Code: ".$AuthCode;
					$sMessageResponse .= "<br />Response Code: ".$ResponseCode;
					$sMessageResponse .= "<br />ISO Code: ".$ISO;
					$sMessageResponse .= "<br />Response Message: ".$Message;
					$sMessageResponse .= "<br />Reference Number: ".$ReferenceNum;
					$sMessageResponse .= "<br />Goods and Services Order: $".$BankTotals;
					$sMessageResponse .= "<br />Merchant Name: ".MERCHANT_NAME;
					$sMessageResponse .= "<br />Merchant URL: ".MERCHANT_URL;
					$sMessageResponse .= "<br />Cardholder Name: ".$ccname."</div>";
					$sMessageResponse .= "</div><br />";
					
					$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">'.$sMessageResponse.'</div></div><br />';	
					
					
				}
			} // end of if continue.
				
		} elseif(!is_numeric($amount) || empty($amount)) { 
			if($show_services){
				$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> Please select service you\'re paying for.</p></div></div><br />';
			} else { 
				$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> Please type amount to pay for services!</p></div></div><br />';
			}
			$show_form=1; 
		} 
	# END OF PLEASE DO NOT EDIT IF YOU'RE NOT SURE	
?>