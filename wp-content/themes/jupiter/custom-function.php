<?php
/*$current_user = wp_get_current_user();

echo "<pre>";
print_r($current_user);
echo "</pre>";*/

show_admin_bar(false);
function rg_login_redirect( $redirect_to, $request, $user ) {
	//is there a user to check?
	if ( isset( $user->roles ) && is_array( $user->roles ) ) {
		//check for admins
		if ( in_array( 'administrator', $user->roles ) ) {
			// redirect them to the default place
			return $redirect_to;
		}
		elseif(in_array( 'staff', $user->roles )){
			return admin_url();
		}
		 else {
			return admin_url('admin.php?page=rg_profile_setup');
		}
	} else {
		return $redirect_to;
	}
}

add_filter( 'login_redirect', 'rg_login_redirect', 10, 3 );

function rg_menu_remove () {
	$current_user = wp_get_current_user();
	if(  ! empty($current_user->ID) ) {
	?>
		<style>
		.mk-main-navigation {
		    display: none;
		    margin: 0 auto;
		    text-align: center;
		}
		</style>
	<?php
	} else {
	?>
		<style>
		#profile_page_levels {
		    display: none;		    
		}
		</style>
	<?php
	}
}
add_action('wp_head', 'rg_menu_remove');