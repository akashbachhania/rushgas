  <div class="container">
    <div class="page-header">
      <h1>Add new Vehicle</h1>
      </div>
    <div class="row">
      
    <div class="col-md-6 col-sm-6">
       <form class="form-horizontal" action="" method='post' enctype="multipart/form-data">
        <div class="form-group">
            <label class="control-label col-sm-4" for="name">Nick Name:</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="name"  required name="name" placeholder="Enter Name">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-4" for="make">Make:</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="make"  required name="make" placeholder="Enter Make">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-4" for="model">Model:</label>
            <div class="col-sm-8"> 
              <input type="text" class="form-control" id="model" required name="model" placeholder="Enter Model">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-4" for="color">Color:</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="color" required name="color" placeholder="Enter Color">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-4" for="vh_year">Year:</label>
            <div class="col-sm-8"> 
              <input type="text" class="form-control" id="vh_year" required name="vh_year" placeholder="Enter Year">
            </div>
          </div>
           <div class="form-group">
            <label class="control-label col-sm-4" for="lpm">Licence Plate Number:</label>
            <div class="col-sm-8"> 
              <input type="text" class="form-control" id="lpm" required name="lpm" placeholder="Enter Licence Plate Numbe">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-4" for="lpm">Choose Vehicle Image:</label>
            <div class="col-sm-8"> 
              <input type="file" class="form-control" id="image" required name="image" value="<?=$vehicle_info->lpm?>" placeholder="Enter Licence Plate Number">
            </div>
          </div>
          <div class="form-group"> 
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" name="add_vehicle" required class="btn btn-default">Submit</button>
            </div>
          </div>
      </form>


     </div>
    </div>
  </div>