<?php
 	/* query for get all vehicles of current user*/
	$vehicles= $wpdb->get_results("SELECT * FROM wp_vehicle_info  WHERE user_id = '$current_user->ID'");
	?>
	<!-- Main Container Start -->
	<div class="container">
			<!-- .Page heading -->
	  		<h2 class="pull-left">Vehicles</h2>
	  		<!-- Link for add New vehicles -->
			<h2 ><a href="<?php echo admin_url('admin.php?page=rg_vehicles_setup&vehicles=addnew')?>" class="btn btn-danger pull-right"> 
				Add New Vehicle
			</a>	</h2>	

<?php
			if($vehicles):
			
?>				<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="table-responsive">
				 <table class="table " >
				    <thead>
				      <tr>
				        <th>Make</th>
				        <th>Model</th>
				        <th class="hidden-xs">Color</th>
				        <th class="hidden-xs">Year</th>
				       
				        <th>Image</th>
				        <th>Action</th>
				      </tr>
				    </thead>
				    <tbody>
				    	<?php foreach ($vehicles as $vehicle) {
				    		$image_url=wp_get_attachment_url( $vehicle->v_image );
				    		?>
				      <tr>
				      	
				      	<td><?=$vehicle->make;?></td>
				        <td><?=$vehicle->model;?></td>
				        <td class="hidden-xs"><?=$vehicle->color;?></td>
				      	
				        <td class="hidden-xs"><?=$vehicle->vh_year;?></td>
				        <td><?=$vehicle->lpm;?></td>
				        <td><img src="<?=$image_url;?>" height="30" width="30"/></td>
				        <td>
				        	<a href="<?php echo admin_url('admin.php?page=rg_vehicles_setup&vehicles=updatevehicle&id='.$vehicle->id);?>">
				        		Update
				        	</a>
				        	<a href="<?php echo admin_url('admin.php?page=rg_vehicles_setup&vehicles=deletevehicle&id='.$vehicle->id);?>">
				        		Delete
				        	</a>
				        </td>
				      </tr>
				     <? }?>
				    </tbody>
				  </table>
				</div>
<?php 		endif;
?>
			</div>
			</div>
		</div>