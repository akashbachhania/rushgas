<?php
#******************************************************************************
#                       Credit Card Payments Wordpress v3.0
#
#	Author: Convergine.com
#	http://www.convergine.com
#	Version: 3.0
#   (c) 2013 Convergine.com
#
#******************************************************************************
	# PLEASE DO NOT EDIT FOLLOWING LINES IF YOU'RE NOT SURE ------->
		if($show_services==1 && $ccpt_show_am_text==2){
			$query="SELECT * FROM ".$wpdb->prefix."ccpt_services WHERE ccpt_services_id =".$service;
			$result=mysql_query($query) or die(mysql_error()."<br>$query");
			$res=mysql_fetch_assoc($result);
			$amount = number_format($res['ccpt_services_price'],2, ".", "");
			$serviceName=$res['ccpt_services_title'];
		} else if($show_services==1 && $ccpt_show_am_text==1){
		        if(empty($amount)){
		            $query="SELECT * FROM ".$wpdb->prefix."ccpt_services WHERE ccpt_services_id =".$service;
		            $result=mysql_query($query) or die(mysql_error()."<br>$query");
		            $res=mysql_fetch_assoc($result);
		            $amount = number_format($res['ccpt_services_price'],2, ".", "");
		            $serviceName=$res['ccpt_services_title'];
		        }
		    }
		$continue = false;
		if(!empty($amount) && is_numeric($amount)){ 	
			$cctype = (!empty($_POST['cctype']))?strip_tags(str_replace("'","`",strip_tags($_POST['cctype']))):'';
			$ccname = (!empty($_POST['ccname']))?strip_tags(str_replace("'","`",strip_tags($_POST['ccname']))):'';
			$ccn = (!empty($_POST['ccn']))?strip_tags(str_replace("'","`",strip_tags($_POST['ccn']))):'';
			$exp1 = (!empty($_POST['exp1']))?strip_tags(str_replace("'","`",strip_tags($_POST['exp1']))):'';
			$exp2 = (!empty($_POST['exp2']))?strip_tags(str_replace("'","`",strip_tags($_POST['exp2']))):'';
			$cvv = (!empty($_POST['cvv']))?strip_tags(str_replace("'","`",strip_tags($_POST['cvv']))):'';
			
			
			
			//CREDIT CARD PHP VALIDATION 
			if(empty($ccn) || empty($cctype) || empty($exp1) || empty($exp2) || empty($ccname) || empty($cvv) || empty($address) || empty($state) || empty($city)){
				$continue = false;
				$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> Not all required fields were filled out.</p></div></div><br />';
			} else { $continue = true; }
			
			if(!is_numeric($cvv)){
				$continue = false;
				$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> CVV number can contain numbers only.</p></div></div><br />';
			} else {
				$continue = true;
			}
			
			if(!is_numeric($ccn)){
				$continue = false;
				$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> Credit Card number can contain numbers only.</p></div></div><br />';
			} else {
				$continue = true;
			}
			
			if(date("Y-m-d", strtotime($exp2."-".$exp1."-01")) < date("Y-m-d")){
				$continue = false;
				$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> Your credit card is expired.</p></div></div><br />';
			} else {
				$continue = true;
			}
			
			if($continue){
				//echo "1";
				if(validateCC($ccn,$cctype)){
					$continue = true;
				} else { 
					$continue = false;
					$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> The number you\'ve entered does not match the card type selected.</p></div></div><br />';
				}
			}
			
			if($continue){
				if(luhn_check($ccn)){
					$continue = true;
				} else { 
					$continue = false; 
					$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> Invalid credit card number.</p></div></div><br />';
				}
			}
			
			switch($cctype){
				case "V":
					$cctype = "VI";
				break;
				case "M":
					$cctype = "MC";
				break;
				case "D":
					$cctype = "DI";
				break;
				case "A":
					$cctype = "AM";
				break;
			}
			
			if($continue){
			
			$query="INSERT INTO ".$wpdb->prefix."ccpt_transactions (ccpt_dateCreated, ccpt_amount, ccpt_comment,ccpt_serviceID ) VALUES (NOW(), '".$amount."', '".$item_description."', '".$service."')";
				if(mysql_query($query)){							
				
					$orderID=mysql_insert_id();
				}
				###########################################################################
				###	OPTIMALPAYMENTS PROCESSING
				###########################################################################
				$cardNum=$ccn;				
				$eMonth=str_pad($exp1, 2, '0', STR_PAD_LEFT);
				$eYear=$exp2;
				//$creditCardType =urlencode($cctype);
				$cardType=$cctype;
				$cvdIndicator='0';
				$cvd=$cvv;	
				
				$tt = explode(" ",$ccname);
				if(is_array($tt)){  
					$firstName =urlencode( $tt[0]);
					if(isset($tt[2])){ $temp = $tt[1]." ".$tt[2]; } else { if(isset($tt[1])){ $temp = $tt[1]; } else { $temp = ""; } }
					$lastName =urlencode( $temp );
				} else { 
					$firstName =urlencode( $ccname); 
					$lastName =urlencode(""); 
				} 
				
				$bStreet=$address;
				$bStreet2='';
				$bCity=$city;
				$bState=$state;
				$bCountry=$country;
				$bZip=$zip;
				$bPhone = "";
				$bEmail=$email;
				
				
				
				$currencyCode=PTP_CURRENCY_CODE;
		
				$txnMode ="ccPurchase";		
									
				$txnRequest = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
				$txnRequest .= "<ccAuthRequestV1 xmlns=\"http://www.optimalpayments.com/creditcard/xmlschema/v1\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.optimalpayments.com/creditcard/xmlschema/v1\">\n";
				
				$txnRequest .= "<merchantAccount>\n";
				$txnRequest .= "<accountNum>".$account."</accountNum>\n";
				$txnRequest .= "<storeID>".$merchantId."</storeID>\n";
				$txnRequest .= "<storePwd>".$merchantPwd."</storePwd>\n";
				$txnRequest .= "</merchantAccount>\n";
				
				$txnRequest .= "<merchantRefNum>".$merchantRefNum."</merchantRefNum>\n";
				$txnRequest .= "<amount>".number_format($amount,2)."</amount>\n";
				
				$txnRequest .= "<card>\n";
				$txnRequest .= "<cardNum>".$cardNum."</cardNum>\n";
				$txnRequest .= "<cardExpiry>\n<month>".$eMonth."</month>\n<year>".$eYear."</year>\n</cardExpiry>\n";
				$txnRequest .= "<cardType>".$cardType."</cardType>\n";
				$txnRequest .= "<cvdIndicator>".$cvdIndicator."</cvdIndicator>\n";
				$txnRequest .= "<cvd>".$cvd."</cvd>\n";
				$txnRequest .= "</card>\n";
				
				$txnRequest .= "<billingDetails>\n";
				$txnRequest .= "<cardPayMethod>WEB</cardPayMethod>\n";
				$txnRequest .= "<firstName>".$firstName."</firstName>\n";
				$txnRequest .= "<lastName>".$lastName."</lastName>\n";
				$txnRequest .= "<street>".$bStreet."</street>\n";
				$txnRequest .= "<street2>".$bStreet2."</street2>\n";
				$txnRequest .= "<city>".$bCity."</city>\n";
				$txnRequest .= "<state>".$bState."</state>\n";
				$txnRequest .= "<country>".$bCountry."</country>\n";
				$txnRequest .= "<zip>".$bZip."</zip>\n";
				$txnRequest .= "<phone>".$bPhone."</phone>\n";
				$txnRequest .= "<email>".$bEmail."</email>\n";
				$txnRequest .= "</billingDetails>\n";
				
				$txnRequest .= "</ccAuthRequestV1>";
		
                $url = "https://webservices.test.optimalpayments.com/creditcardWS/CreditCardServlet/v1";

				if($redirect_non_https == true){
                    $url = "https://webservices.optimalpayments.com/creditcardWS/CreditCardServlet/v1";
				}
				
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_POST,1);
				curl_setopt($ch, CURLOPT_POSTFIELDS,"&txnMode=".$txnMode."&txnRequest=".urlencode($txnRequest));
				curl_setopt($ch, CURLOPT_URL,$url);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		
				$resultCURL = curl_exec($ch); 
				
				//die(searchXML('description',$resultCURL)." ---- ".$resultCURL." -- ".$resArray["TRANSACTIONID"]);
				//echo("\n" + $resultCURL);
				if (curl_errno($ch)) {
					$sMessageResponse= "<br /><div>Credit Card processing returned <b>ERROR</b>!";
					$sMessageResponse .= "<div>";
					$sMessageResponse .= curl_error($ch);
					$sMessageResponse .= "</div>";				
					$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">'.$sMessageResponse.'</div></div><br />';
				} else {			
					if(stristr($resultCURL,"ACCEPTED")){
					//everything is good.
					$resArray["TRANSACTIONID"] = searchXML('xid',$resultCURL);
						$sMessageResponse= "<br /><div>Your payment was <b>APPROVED</b>!";
						$sMessageResponse .= "<div>";
						$sMessageResponse .= "Thank you for your payment. You will receive an email confirmation shortly";
						$sMessageResponse .= "</div>";
						$sMessageResponse .= "<br/><a href='index.php'>Return to payment page</a><br /><br/></div>";
						$mess = '<div class="ui-widget"><div class="ui-state-highlight ui-corner-all" style="padding: 0 .7em;">'.$sMessageResponse.'</div></div><br />';
						
						#**********************************************************************************************#
						#		THIS IS THE PLACE WHERE YOU WOULD INSERT ORDER TO DATABASE OR UPDATE ORDER STATUS.
						#**********************************************************************************************#
								
							$q="UPDATE ".$wpdb->prefix."ccpt_transactions SET ccpt_status='2', ccpt_payer_email='".$email."', ccpt_transaction_id='".$resArray["TRANSACTIONID"]."', ccpt_payer_name='".$ccname."' WHERE ccpt_id='".$orderID."'";
							mysql_query($q);
									
						#**********************************************************************************************#	
						
						//-----> send notification 
						//creating message for sending
						$headers  = "MIME-Version: 1.0\n";
						$headers .= "Content-type: text/html; charset=utf-8\n";
						$headers .= "From: 'OptimalPayments Payment Terminal' <noreply@".$_SERVER['HTTP_HOST']."> \n";
						$subject = "New Payment Received";
						$message =  "New payment was successfully received through authorize.net <br />";					
						$message .= "from ".$fname." ".$lname."  on ".date('m/d/Y')." at ".date('g:i A').".<br /> Payment total is: $".number_format($amount,2);
						if($show_services==1){
							$message .= "<br />Payment was made for \"".$serviceName."\"";
						} else { 
							$message .= "<br />Payment description: \"".$item_description."\"";
						}
						$message .= "<br /><br />Billing Information:<br />";
						$message .= "Full Name: ".$fname." ".$lname."<br />";
						$message .= "Email: ".$email."<br />";
						$message .= "Address: ".$address."<br />";
						$message .= "City: ".$city."<br />";
						$message .= "Country: ".$country."<br />";
						$message .= "State/Province: ".$state."<br />";
						$message .= "ZIP/Postal Code: ".$zip."<br />";
						mail($admin_email,$subject,$message,$headers);
						
						$subject = "Payment Received!";
						$message =  "Dear ".$fname.",<br />";
						$message .= "<br /> Thank you for your payment.";
						$message .= "<br /><br />Kind Regards,<br />".$_SERVER['HTTP_HOST'];	
						mail($email,$subject,$message,$headers);
						//-----> send notification end 	
						
						$show_form=0;			
					} else {					
						$sMessageResponse= "<br /><div>Transaction Un-successful!";
						$sMessageResponse .= "<div>";
						$sMessageResponse .= "Your credit card was declined, please check the accuracy of entered information and try again or try different credit card.<br /><br />";
						$sMessageResponse .= "<b>Merchant Response:</b> ".($resultCURL);
						$sMessageResponse .= "</div><br />"; 
						$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">'.$sMessageResponse.'</div></div><br />';
					}
					
					curl_close($ch);
				}
				

			} // end of if continue.
				
		} elseif(!is_numeric($amount) || empty($amount)) { 
			if($show_services){
				$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> Please select service you\'re paying for.</p></div></div><br />';
			} else { 
				$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> Please type amount to pay for services!</p></div></div><br />';
			}
			$show_form=1; 
		} 
	# END OF PLEASE DO NOT EDIT IF YOU'RE NOT SURE	
?>