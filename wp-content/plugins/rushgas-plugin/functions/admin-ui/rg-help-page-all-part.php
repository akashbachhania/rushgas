<?php
	$check_admin=get_userdata($current_user->ID);
	if($check_admin->roles[0]=='administrator'){
 		/* query for get all helps of current user*/
		$helps= $wpdb->get_results("SELECT * FROM 
											wp_user_help_section hs 
									left join 
											wp_user_help_section_subject  hss
									ON 
											hs.subject_help_id=hss.subject_id");
	}
	else{
		$helps= $wpdb->get_results("SELECT hs.*,hss.*,IF(
														hs.status=1,
														'Closed',
														'Open') as cstatus
									FROM 
											wp_user_help_section hs 
									left join 
											wp_user_help_section_subject  hss
									ON 
											hs.subject_help_id=hss.subject_id
									WHERE 
												hs.user_id = '$current_user->ID'
									");
	}
	?>
	<!-- Main Container Start -->
	<div class="container">
			<!-- .Page heading -->
	  		<h2 class="pull-left">Help</h2>
	  		<!-- Link for add New vehicles -->
		<?php
	        if($check_admin->roles[0]!='administrator'):
		?>
			<h2><a href="<?php echo admin_url('admin.php?page=rg_get_help_setup&helps=addnew')?>" class="btn btn-danger pull-right"> 
				Add New Case
			</a></h2>
		<?php endif;?>		

<?php
			if($helps):
			
?>
				<div class="table-resposive">
				 <table class="table " >
				    <thead>
				      <tr>
				      	<th>Title</th>
				      	<th>Subject</th>
				        <th>Message</th>
				        
				<?php
				        if($check_admin->roles[0]=='administrator'):
				?>
						<th>User Name</th>
						<th>Date</th>
 				<?php endif;?>
 						 <th>Status</th>
				        <th>View Cases</th>
				<?php //endif;	?>
				        
				      </tr>
				    </thead>
				    <tbody>
				    	<?php foreach ($helps as $help) {
				    		$message_author=get_userdata($help->user_id);
				    			?>
				      <tr>
				      	
				      	<td><?=$help->name;?></td>
				      	<td><?=$help->subject;?></td>
				        <td><?=$help->comment;?></td>
				<?php
				        if($check_admin->roles[0]=='administrator'):
				?>
						<td><?=$message_author->user_login?></td>
						 <td><?php echo date_i18n(get_option("date_format"),strtotime($case->created_at));?></td>
 						
 				<?php endif;?>
 						<td><?=$help->cstatus?></td>
				        <td>
				        	<a href="<?php echo admin_url('admin.php?page=rg_get_help_setup&helps=view_cases&id='.$help->help_id);?>">
				        		View Cases
				        	</a>
				        	
				        </td>
				<?php 	//endif;?>
				      </tr>
				     <? }?>
				    </tbody>
				  </table>
				
<?php 		endif;
?>
		</div>
		</div>