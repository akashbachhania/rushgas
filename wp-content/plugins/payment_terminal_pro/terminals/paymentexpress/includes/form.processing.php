<?php
#******************************************************************************
#                       Credit Card Payments Wordpress v3.0
#
#	Author: Convergine.com
#	http://www.convergine.com
#	Version: 3.0
#   (c) 2013 Convergine.com
#
#******************************************************************************

	# PLEASE DO NOT EDIT FOLLOWING LINES IF YOU'RE NOT SURE ------->
	    $province = str_replace("-AU-","",$state);
		if($show_services==1 && $ccpt_show_am_text==2){
			$query="SELECT * FROM ".$wpdb->prefix."ccpt_services WHERE ccpt_services_id =".$service;
			$result=mysql_query($query) or die(mysql_error()."<br>$query");
			$res=mysql_fetch_assoc($result);
			$amount = number_format($res['ccpt_services_price'],2, ".", "");
			$serviceName=$res['ccpt_services_title'];
		} else if($show_services==1 && $ccpt_show_am_text==1){
		        if(empty($amount)){
		            $query="SELECT * FROM ".$wpdb->prefix."ccpt_services WHERE ccpt_services_id =".$service;
		            $result=mysql_query($query) or die(mysql_error()."<br>$query");
		            $res=mysql_fetch_assoc($result);
		            $amount = number_format($res['ccpt_services_price'],2, ".", "");
		            $serviceName=$res['ccpt_services_title'];
		        }
		    }
		$continue = false;
		if(!empty($amount) && is_numeric($amount)){ 	
			$cctype = (!empty($_POST['cctype']))?strip_tags(str_replace("'","`",strip_tags($_POST['cctype']))):'';
			$ccname = (!empty($_POST['ccname']))?strip_tags(str_replace("'","`",strip_tags($_POST['ccname']))):'';
			$ccn = (!empty($_POST['ccn']))?strip_tags(str_replace("'","`",strip_tags($_POST['ccn']))):'';
			$exp1 = (!empty($_POST['exp1']))?strip_tags(str_replace("'","`",strip_tags($_POST['exp1']))):'';
			$exp2 = (!empty($_POST['exp2']))?strip_tags(str_replace("'","`",strip_tags($_POST['exp2']))):'';
			$cvv = (!empty($_POST['cvv']))?strip_tags(str_replace("'","`",strip_tags($_POST['cvv']))):'';

            if($cctype!="PP"){
                //CREDIT CARD PHP VALIDATION
                if(empty($ccn) || empty($cctype) || empty($exp1) || empty($exp2) || empty($ccname) || empty($cvv) || empty($address) || empty($state) || empty($city)){
                    $continue = false;
                    $mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> Not all required fields were filled out.</p></div></div><br />';
                } else { $continue = true; }

                if(!is_numeric($cvv)){
                    $continue = false;
                    $mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> CVV number can contain numbers only.</p></div></div><br />';
                } else {
                    $continue = true;
                }

                if(!is_numeric($ccn)){
                    $continue = false;
                    $mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> Credit Card number can contain numbers only.</p></div></div><br />';
                } else {
                    $continue = true;
                }

                if(date("Y-m-d", strtotime($exp2."-".$exp1."-01")) < date("Y-m-d")){
                    $continue = false;
                    $mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> Your credit card is expired.</p></div></div><br />';
                } else {
                    $continue = true;
                }

                if($continue){
                    //echo "1";
                    if(validateCC($ccn,$cctype)){
                        $continue = true;
                    } else {
                        $continue = false;
                        $mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> The number you\'ve entered does not match the card type selected.</p></div></div><br />';
                    }
                }

                if($continue){
                    if(luhn_check($ccn)){
                        $continue = true;
                    } else {
                        $continue = false;
                        $mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> Invalid credit card number.</p></div></div><br />';
                    }
                }

            } else {
                $continue = true;
            }

			
			switch($cctype){
				case "V":
					$cctype = "VISA";
				break;
				case "M":
					$cctype = "MASTERCARD";
				break;
				case "DI":
					$cctype = "DINERS CLUB";
				break;
				case "D":
					$cctype = "DISCOVER";
				break;
				case "A":
					$cctype = "AMEX";
				break;
				case "PP":
					$cctype = "PAYPAL";
				break;
			}

            $transactID = mktime()."-".rand(1,999);

			if($continue && $cctype!="PAYPAL"){
				$query="INSERT INTO ".$wpdb->prefix."ccpt_transactions (ccpt_dateCreated, ccpt_amount, ccpt_comment,ccpt_serviceID ) VALUES (NOW(), '".$amount."', '".$item_description."', '".$service."')";
				if(mysql_query($query)){							
				
					$orderID=mysql_insert_id();
				}
                //CREDIT CARD INFO
                $tt = explode(" ",trim($ccname));
                if(is_array($tt)){
                    $firstName = $tt[0];
                    if(isset($tt[2])){ $temp = $tt[1]." ".$tt[2]; } else { if(isset($tt[1])){ $temp = $tt[1]; } else { $temp = ""; } }
                    $lastName = $temp ;
                } else {
                    $firstName =$ccname;
                    $lastName ="";
                }
                $creditCardType =$cctype;
                $creditCardNumber = trim($ccn);
                $expDateMonth = $exp1;
                $padDateMonth = str_pad($exp1, 2, '0', STR_PAD_LEFT);
                $expDateYear =$exp2;
                $expDateYear = substr($exp2,-2);
                $cvv2Number = trim($cvv);
                $amount = number_format($amount,2);

                /*******************************************************************************************************
                    PAYMENT PROCESSING
                *******************************************************************************************************/

                   require("includes/pxpost.class.php");


                    $DPSresponse = DPSProcessPayment($ccname, $amount, $creditCardNumber, $padDateMonth, $expDateYear, $transactID);

                      if(strtolower($DPSresponse["TXN"]["RESPONSETEXT"])!="approved")
                      {

                          $my_status = "<div>Transaction Un-successful!<br/>";
                          $my_status .= "There was an error with your credit card processing:<br/>";
                          $my_status .= "Merchant Gateway Response: ".$DPSresponse["TXN"][1]["CARDHOLDERHELPTEXT"]."<br />".$DPSresponse["TXN"][1]["CARDHOLDERRESPONSEDESCRIPTION"]."<br/>";
                          $my_status .= "</div>";
                          $error=1;
                          $mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">'.$my_status.'</div></div><br />';
                      }
                      else if(strtolower($DPSresponse["TXN"]["RESPONSETEXT"])=="approved")
                      {
                      	  		
                      	  $resArray["TRANSACTIONID"] = $DPSresponse["TXN"]["DPSTXNREF"];
                           // payment succesfully sent to gateway
                          $my_status = "<div>Transaction Successful!<br/>";
                          $my_status .= "Thank you for your payment<br /><br />";
                          $my_status .= "AuthCode: ".$DPSresponse["TXN"][1]["AUTHCODE"]."<br />";
                          $my_status .= "Transaction ID: ".$DPSresponse["TXN"]["DPSTXNREF"]."<br />";
                          $my_status .= "You will receive confirmation email within 5 minutes.<br/><br/><a href='index.php'>Return to payment page</a></div><br/>";
                          $error=0;
                          $mess = '<div class="ui-widget"><div class="ui-state-highlight ui-corner-all" style="padding: 0 .7em;">'.$my_status.'</div></div><br />';

                          #**********************************************************************************************#
                          #		THIS IS THE PLACE WHERE YOU WOULD INSERT ORDER TO DATABASE OR UPDATE ORDER STATUS.
                          #**********************************************************************************************#
							$q="UPDATE ".$wpdb->prefix."ccpt_transactions SET ccpt_status='2', ccpt_payer_email='".$email."', ccpt_transaction_id='".$resArray["TRANSACTIONID"]."', ccpt_payer_name='".$ccname."' WHERE ccpt_id='".$orderID."'";
							mysql_query($q);
                          #**********************************************************************************************#
                          /******************************************************************
                               ADMIN EMAIL NOTIFICATION
                          ******************************************************************/
                          $headers  = "MIME-Version: 1.0\n";
                          $headers .= "Content-type: text/html; charset=utf-8\n";
                          $headers .= "From: 'PaymentExpress Payment Terminal' <noreply@".$_SERVER['HTTP_HOST']."> \n";
                          $subject = "New Payment Received";
                          $message =  "New payment was successfully received through PaymentExpress <br />";
                          $message .= "from ".$fname." ".$lname."  on ".date('m/d/Y')." at ".date('g:i A').".<br /> Payment total is: $".number_format($amount,2);
                          if($show_services){
                              $message .= "<br />Payment was made for \"".$services[$service][0]."\"";
                          } else {
                              $message .= "<br />Payment description: \"".$item_description."\"";
                          }
                          $message .= "<br />Transaction ID: \"".$DPSresponse["TXN"]["DPSTXNREF"]."\"";
                          $message .= "<br />AuthCode: \"".$DPSresponse["TXN"][1]["AUTHCODE"]."\"";
                          $message .= "<br /><br />Billing Information:<br />";
                          $message .= "Full Name: ".$fname." ".$lname."<br />";
                          $message .= "Email: ".$email."<br />";
                          $message .= "Address: ".$address."<br />";
                          $message .= "City: ".$city."<br />";
                          $message .= "Country: ".$country."<br />";
                          $message .= "State/Province: ".$state."<br />";
                          $message .= "ZIP/Postal Code: ".$zip."<br />";

                          mail($admin_email,$subject,$message,$headers);

                          /******************************************************************
                                CUSTOMER EMAIL NOTIFICATION
                          ******************************************************************/
                          $subject = "Payment Received!";
                          $message =  "Dear ".$fname.",<br />";
                          $message .= "<br /> Thank you for your payment.";
                          $message .= "<br /><br />";
                            if($show_services){
                                $message .= "<br />Payment was made for \"".$services[$service][0]."\"";
                            } else {
                                $message .= "<br />Payment was made for: \"".$item_description."\"";
                            }
                          $message .= "<br />Payment amount: $".number_format($amount,2);
                          $message .= "<br />Transaction ID: \"".$DPSresponse["TXN"]["DPSTXNREF"]."\"";
                          $message .= "<br />AuthCode: \"".$DPSresponse["TXN"][1]["AUTHCODE"]."\"";
                          $message .= "<br /><br />Billing Information:<br />";
                          $message .= "Full Name: ".$fname." ".$lname."<br />";
                          $message .= "Email: ".$email."<br />";
                          $message .= "Address: ".$address."<br />";
                          $message .= "City: ".$city."<br />";
                          $message .= "Country: ".$country."<br />";
                          $message .= "State/Province: ".$state."<br />";
                          $message .= "ZIP/Postal Code: ".$zip."<br />";

                          $message .= "<br /><br />Kind Regards,<br />".$_SERVER['HTTP_HOST'];
                          mail($email,$subject,$message,$headers);

                          //-----> send notification end
                          $show_form=0;
                      }



			} else if($continue && $cctype=="PAYPAL"){
                require('includes/paypal.class.php');
                $paypal = new paypal_class;
                $paypal->add_field('business', $paypal_merchant_email);
                $paypal->add_field('return', $paypal_success_url);
                $paypal->add_field('cancel_return', $paypal_cancel_url);
                $paypal->add_field('notify_url', $paypal_ipn_listener_url);
                if($show_services){
                    $paypal->add_field('item_name_1', strip_tags(str_replace("'","",$services[$service][0])));
                } else {
                    $paypal->add_field('item_name_1', strip_tags(str_replace("'","",$item_description)));
                }
                $paypal->add_field('amount_1', $amount);
                $paypal->add_field('item_number_1', $transactID);
                $paypal->add_field('quantity_1', '1');
                $paypal->add_field('custom', $paypal_custom_variable);
                $paypal->add_field('upload', 1);
                $paypal->add_field('cmd', '_cart');
                $paypal->add_field('txn_type', 'cart');
                $paypal->add_field('num_cart_items', 1);
                $paypal->add_field('payment_gross', $amount);
                $paypal->add_field('currency_code',$paypal_currency);
                $show_form=0;
                $mess = $paypal->submit_paypal_post(); // submit the fields to paypal
            }
				
		} elseif(!is_numeric($amount) || empty($amount)) { 
			if($show_services){
				$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> Please select service you\'re paying for.</p></div></div><br />';
			} else { 
				$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> Please type amount to pay for services!</p></div></div><br />';
			}
			$show_form=1; 
		} 
	# END OF PLEASE DO NOT EDIT IF YOU'RE NOT SURE	
?>