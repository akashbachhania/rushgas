<?php

/*

Plugin Name: Credit Card Payments Wordpress 3.2

Plugin URI: http://www.convergine.com

Description: Plugin for easily accepting payments or donations by credit cards on your blog

Author: Convergine.com

Version: 3.2

Release Date: 31 December 2013

Initial Release Date: August 9 2011

Author URI: http://www.convergine.com

*/



#################################### INSTALL & UNINSTALL PART #########################################################################

//installation function

global $wp_url;



//$wp_url = get_bloginfo('siteurl');

$wp_url = get_site_url();



function ccpt_install(){

    include('ccwp.cfg.php');

    global $wpdb;

    //let's create transaction table.

    $table = $wpdb->prefix."ccpt_transactions";

    $structure = "CREATE TABLE IF NOT EXISTS `$table` (

						  `ccpt_id` int(20) NOT NULL auto_increment,

						  `ccpt_dateCreated` datetime default '0000-00-00 00:00:00',

						  `ccpt_amount` double NOT NULL,

						  `ccpt_payer_email` varchar(255) default NULL,

						  `ccpt_comment` longtext,

						  `ccpt_transaction_id` varchar(255) default NULL,

						  `ccpt_status` tinyint(5) default '1',

						  `ccpt_payer_name` varchar(255) NOT NULL,

						  `ccpt_serviceID` int(20) NOT NULL default '0',

						  UNIQUE KEY `ccpt_id` (`ccpt_id`)

						) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";

    $wpdb->query($structure);



    //now create services table

    $table = $wpdb->prefix."ccpt_services";

    $structure = "CREATE TABLE IF NOT EXISTS $table (

        				ccpt_services_id INT(20) NOT NULL AUTO_INCREMENT,

						ccpt_services_title VARCHAR(255) NOT NULL,

						ccpt_services_price DOUBLE NOT NULL,

						ccpt_services_descr MEDIUMTEXT NULL,

						UNIQUE KEY ccpt_services_id (ccpt_services_id)

    		);";

    $wpdb->query($structure);



    $add_default_terminals = ccwpCRUD($ccwp_cfg_arr,'install');



    update_option('ccpt_processor',"1");

    update_option('ccpt_paypal_currency',"USD");

    update_option('ccpt_ty_title',"Thank You!");

    update_option('ccpt_ty_text',"<p>Thank you for your payment! We really appreciate it.</p>");

    //$wp_url = get_bloginfo('siteurl');

    update_option('ccpt_payment_description',"<p>Payment Description</p>");

    update_option('ccpt_admin_email',"changeme@email.com");

    update_option('ccpt_admin_send',"1");

    update_option('ccpt_show_comment_field',"1");

    update_option('ccpt_license',"");

    update_option('ccpt_button_text',"Pay!");

    update_option('ccpt_show_am_text',"1");

    update_option('ccpt_show_dd_text',"2"); //show drop down with services 1 or show text box for input 2

    update_option('ccpt_test',"1");

}



function ccpt_uninstall(){

    //in case somebody want's to remove the script.

    //we are leaving intact transactions and services table - for history and in case client will want to re-instantiate the script.

    include('ccwp.cfg.php');

    $remove_terminals = ccwpCRUD($ccwp_cfg_arr,'uninstall');





    delete_option('ccpt_processor');

    delete_option('ccpt_paypal_currency');

    delete_option('ccpt_ty_title');

    delete_option('ccpt_ty_text');

    delete_option('ccpt_button_text');

    delete_option('ccpt_payment_description');

    delete_option('ccpt_admin_email');

    delete_option('ccpt_admin_send');

    delete_option('ccpt_show_comment_field');

    delete_option('ccpt_show_dd_text');

    delete_option('ccpt_license');

    delete_option('ccpt_show_am_text');

    delete_option('ccpt_test');

}

##############################################################################################################################################





//Creating our menu in WP admin.

function ccpt_admin_actions() {

    global $wp_url;

    add_menu_page( "Payment Terminal", "Credit Card Payments", "administrator", basename(__file__), "ccpt_admin_overview", $wp_url."/wp-content/plugins/payment_terminal_pro/resources/images/payment_icon.png");

    add_submenu_page( basename(__file__), 'Payment Terminal Settings', 'Settings', 'administrator', 'ccpt_admin_settings','ccpt_admin_settings');

    add_submenu_page( basename(__file__), 'Payment Terminal Services', 'Services', 'administrator', 'ccpt_admin_services','ccpt_admin_services');

    add_submenu_page( basename(__file__), 'Payment Terminal Transactions', 'Transactions', 'administrator', 'ccpt_admin_transactions','ccpt_admin_transactions');

    add_submenu_page( basename(__file__), 'Payment Terminal Service Edit', '', 'administrator', 'ccpt_admin_services_edit','ccpt_admin_services_edit');



}





//function for including needed page into wp admin upon request from menu

function ccpt_admin_overview() { /* plugin overview page */

    include('payment_terminal_overview.php');

}



function ccpt_admin_settings() { /* settings page */

    include('payment_terminal_settings.php');

}

function ccpt_admin_services() { /* services page */

    include('payment_terminal_services.php');

}

function ccpt_admin_services_edit() { /* services editing page */

    include('payment_terminal_services_edit.php');

}

function ccpt_admin_transactions() { /* transactions page */

    include('payment_terminal_transactions.php');

}







function ccpt_tinymce()

{

    global $wp_version;

    $cos_search_provider_wp_version = "3.3";
    wp_print_scripts('jquery');
    if ( version_compare($wp_version, $cos_search_provider_wp_version, "<") ) {



        wp_enqueue_script('common');

        wp_enqueue_script('jquery-color');

        wp_admin_css('thickbox');

        wp_print_scripts('post');

        wp_print_scripts('media-upload');

        wp_print_scripts('jquery');

        wp_print_scripts('jquery-ui-core');

        wp_print_scripts('jquery-ui-tabs');

        wp_print_scripts('tiny_mce');

        wp_print_scripts('editor');

        wp_print_scripts('editor-functions');

        add_thickbox();

        wp_tiny_mce();

        wp_admin_css();

        wp_enqueue_script('utils');

        do_action("admin_print_styles-post-php");

        do_action('admin_print_styles');

        remove_all_filters('mce_external_plugins');

    }



}



function ccpt_add_widget(){

    register_widget( 'ccpt_widget' );

}



function ccpt_init_jquery() {

    wp_enqueue_script('jquery');

    wp_enqueue_script('jquery-ui-core');

    wp_enqueue_script('ccpt_colorbox',



        plugins_url('/resources/js/jquery.colorbox.js', __FILE__),

        //array('scriptaculous'),

        '1.0' );


}



//add the hooks for install/uninstall and menu.

register_activation_hook( __FILE__, 'ccpt_install' );

register_deactivation_hook(__FILE__, 'ccpt_uninstall');

add_action('admin_menu', 'ccpt_admin_actions');

add_filter('admin_head','ccpt_tinymce');

add_action('widgets_init', 'ccpt_add_widget');

add_action('init', 'ccpt_init_jquery');

add_action('parse_request', 'ccpt_parse_request');



########################################## BEGIN SHORTCODE FUNCTION ######################################

add_shortcode('ccpt_paybutton', 'ccpt_paypalform_display'); //NEW IN v2



function ccpt_paypalform_display($atts) {

    $copyright_img = ""; //powered by convergine logo.

    global $wp_url;

    global $wpdb;

    extract( shortcode_atts( array(

        'id' => ''

    ), $atts ) );

    $ccpt_form='';

    $ccpt_processors=array(

        1=>'paypal',

        2=>'optimal',

        3=>'moneris_us',

        4=>'moneris_ca',

        5=>'autorize',

        6=>'internet_sequre',

        7=>'elavon',

        8=>'firstdata',

        9=>'eway',

        10=>'paymentexpres',

        11=>'stripe'

    );

    $ccpt_current_processor_dir=$ccpt_processors[get_option('ccpt_processor')];

    $serviceID="?terminal=".$ccpt_current_processor_dir;



    if(!empty($id)){

        $serviceID.="&serviceID=$id";

        $query="SELECT * FROM ".$wpdb->prefix."ccpt_services WHERE ccpt_services_id={$id} ORDER BY ccpt_services_title";

        $result=mysql_query($query) or die(mysql_error());

        if(mysql_num_rows($result)==0){

            return "";

        }else{$r=mysql_fetch_assoc($result);}



    }

    $ccpt_form.= '<link rel="stylesheet" type="text/css" media="all" href="'.$wp_url.'/wp-content/plugins/payment_terminal_pro/resources/css/colorbox.css" />';

    $ccpt_form.= '<link rel="stylesheet" type="text/css" media="all" href="'.$wp_url.'/wp-content/plugins/payment_terminal_pro/resources/css/style.css" />';



    $ccpt_form.='<div class="ccpt_pay_conteiner">';

    $ccpt_form.='<div class="ccpt_pay_conteiner_left"></div>';

    $ccpt_form.= '<div class="ccpt_description">'.((isset($r["ccpt_services_descr"]) && !empty($r["ccpt_services_descr"]))?stripslashes($r["ccpt_services_descr"]):get_option('ccpt_payment_description')).'</div>';

    $ccpt_form.='<div class="ccpt_pay_conteiner_right">';

    $ccpt_form.= '<a href="javascript:void(0)" class="ccpt_description_link" onclick="jQuery.colorbox({href:\''.$wp_url.'/wp-content/plugins/payment_terminal_pro/resources/lightbox/index.php'.$serviceID.'\',innerWidth:\'700px\',innerHeight:\'1000px\',iframe:true,scrolling:false});">PAY</a></div>';



    $ccpt_form.= '<div style="clear:both"></div></div>';

    return $ccpt_form;



}

########################################## END OF SHORTCODE FUNCTION #####################################



class ccpt_widget extends WP_Widget {





    function ccpt_widget(){

        /* Widget settings. */

        $widget_ops = array( 'classname' => 'ccpt_widget', 'description' => __('Credit Card Payment Terminal Widget allows you to place your selected payment processor form in a sidebar of your site.', 'ccpt_widget') );

        /* Widget control settings. */

        $control_ops = array( 'width' => 250, 'height' => 200, 'id_base' => 'ccpt_widget' );

        /* Create the widget. */

        $this->WP_Widget( 'ccpt_widget', __('Credit Card Payment Terminal', 'ccpt_widget'), $widget_ops, $control_ops );

    }





    function widget( $args, $instance ) {

        $copyright_img = ""; //powered by convergine logo.

        global $wp_url;

        global $wpdb;

        extract( $args );



        /* User-selected settings. */

        $title = apply_filters('widget_title', $instance['title'] );



        $ccpt_processors=array(

            1=>'paypal',

            2=>'optimal',

            3=>'moneris_us',

            4=>'moneris_ca',

            5=>'autorize',

            6=>'internet_sequre',

            7=>'elavon',

            8=>'firstdata',

            9=>'eway',

            10=>'paymentexpres',

            11=>'stripe'

        );

        $ccpt_current_processor_dir=$ccpt_processors[get_option('ccpt_processor')];

        /* Before widget (defined by themes). */

        echo $before_widget;



        echo '<link rel="stylesheet" type="text/css" media="all" href="'.$wp_url.'/wp-content/plugins/payment_terminal_pro/resources/css/colorbox.css" />';

        echo '<link rel="stylesheet" type="text/css" media="all" href="'.$wp_url.'/wp-content/plugins/payment_terminal_pro/resources/css/style.css" />';





        echo '<div>'.get_option('ccpt_payment_description').'</div>';

        echo '<p><div class="ccpt_pay_button" onclick="jQuery.colorbox({href:\''.$wp_url.'/wp-content/plugins/payment_terminal_pro/resources/lightbox/index.php?terminal='.$ccpt_current_processor_dir.'\',innerWidth:\'700px\',innerHeight:\'1000px\',iframe:true});">'.get_option('ccpt_button_text').'</div></p>';









        echo $after_widget;

    }



    function update( $new_instance, $old_instance ) {

        $instance = $old_instance;



        /* Strip tags (if needed) and update the widget settings. */

        $instance['title'] = strip_tags( $new_instance['title'] );

        //$instance['name'] = strip_tags( $new_instance['name'] );

        //$instance['sex'] = $new_instance['sex'];

        //$instance['show_sex'] = $new_instance['show_sex'];



        return $instance;

    }



    function form( $instance ) {



        /* Set up some default widget settings. */

        //$defaults = array( 'title' => 'Example', 'name' => 'John Doe', 'sex' => 'male', 'show_sex' => true );

        $defaults = array( 'title' => 'Credit Card Payment Terminal Widget');

        $instance = wp_parse_args( (array) $instance, $defaults ); ?>

        <p>

            <label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label>

            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />

        </p>

    <?php

    }



}

function ccpt_parse_request(){

    global $wpdb;



}

?>