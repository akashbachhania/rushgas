<?php
class internetSequre {

   var $field_string;
   
   var $fields = array();
   var $errorList = array();
   var $error = false;
   var $success = false;

   var $common_flags;
   var $products_list_string;
   
   var $response_string;
   var $response = array();
   
   //var $transactionID;
   
   var $gateway_url = GATEWAY_URL;
   var $merchantID = MERCHANT_ID;
   var $currency = PTP_CURRENCY_CODE;
   
   var $data = array();
   
   function __construct() {
       
       global $testAccount;
       
      // some default values
      //
      // Set common flags
      $this->common_flags = '{' . $this->currency . '}';  
      
      //... test transactions?
      if ($testAccount)
      {
        $this->common_flags = '{TEST}';
      }
      
     
   }
   
   function add_field($field, $value) {
   
      // adds a field/value pair to the list of fields which is going to be 
      // passed to authorize.net.  For example: "x_version=3.1" would be one
      // field/value pair.  A list of the required and optional fields to pass
      // to the authorize.net payment gateway are listed in the AIM document
      // available in PDF form from www.authorize.net

      $this->fields["$field"] = $value;   

   }
	function before_process(){
	
		if (!empty($_REQUEST['PaRes']))
		{
		  //
		  // This will execute when returning from the issuer
		  $xml_part  = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><TranxRequest><PaRes>" . $_REQUEST['PaRes'] . "</PaRes></TranxRequest>";

		  $post_data = "TRX=" . $_SESSION['ReciptNumber'] .
					   "&GUID=" . $_SESSION['GUID'] . 
					   "&xxxRequestMode=X" .
					   "&xxxRequestData=$xml_part";
		  $request=$this->send_request($post_data);
	 
		  $this->process_response($request);
		}
		else
		{
			return false;
		}
	}
   function process() {
       
      $payment_request =  "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?> \n"; 
      $payment_request .= "<TranxRequest> \n";
      
      $payment_request .= "<GatewayID>" . $this->merchantID . "</GatewayID> \n";
      $payment_request .= "<Products>" . $this->products_list_string . "</Products> \n";
      
      foreach($this->fields as $key=>$value){
          $payment_request .= "<$key>" . urlencode(trim($value)) . "</$key> \n";
      }
      
      $payment_request .= "</TranxRequest>\n";
      
      $post_data = "xxxRequestMode=X&xxxRequestData=$payment_request";
       //print $post_data;
      $request=$this->send_request($post_data);
 
      $this->process_response($request);

   }
   function send_request($post_data){
		$ch = curl_init();
        //print $this->common_flags.$post_data;
        curl_setopt($ch, CURLOPT_URL, $this->gateway_url);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);

        $response_data = curl_exec($ch);        

        curl_close($ch);

        if ($response_data)
        {    
          return $response_data;
        }
        else
        {
          //
          // cURL error, redirect's to checkout_payment in order to the user may select another payment method
          return false;
        }
   }
    function process_response($response_data)
  {  
//print $response_data;
    //
    // The response data is coming with information before the XML starting tag, 
    // we need to remove that before process it as a DOMDocument
    $i = strpos($response_data, '<?xml');
    if ($i === false) // Not a response in XML format
    {
      //-------------------------------------------------------------- LOG.BEGIN
      // log $log_tracking_number and $response_data
      //-------------------------------------------------------------- LOG.END
      
		$this->addError("None XML Response");
    }
    else
    {    
      $response_data = substr($response_data, $i);
    }
    
    //-------------------------------------------------------------- LOG.BEGIN
    //-------------------------------------------------------------- LOG.END
    
    
    //
    // Process the XML response
    //
    $dom_doc = new DOMDocument();
	
    $response_data = utf8_encode($response_data); // Defensive programming, the response could be not UTF-8
    $dom_doc->loadXML($response_data);
    
    $_SESSION['Date'] = $dom_doc->getElementsByTagName('Date')->item(0)->nodeValue;
    $_SESSION['Page'] = $dom_doc->getElementsByTagName('Page')->item(0)->nodeValue;
    $_SESSION['ApprovalCode'] = $dom_doc->getElementsByTagName('ApprovalCode')->item(0)->nodeValue;    
    $_SESSION['SalesOrderNumber'] = $dom_doc->getElementsByTagName('SalesOrderNumber')->item(0)->nodeValue;
    $_SESSION['ReciptNumber'] = $dom_doc->getElementsByTagName('ReceiptNumber')->item(0)->nodeValue;    
    $_SESSION['GUID'] = $dom_doc->getElementsByTagName('GUID')->item(0)->nodeValue;
    $_SESSION['AVSResponseCode'] = $dom_doc->getElementsByTagName('AVSResponseCode')->item(0)->nodeValue;
    $_SESSION['CVV2ResponseCode'] = $dom_doc->getElementsByTagName('CVV2ResponseCode')->item(0)->nodeValue;
    $_SESSION['TotalAmount'] = $dom_doc->getElementsByTagName('TotalAmount')->item(0)->nodeValue;     
    //-------------------------------------------------------------- LOG.BEGIN
    $response_error_message = trim($dom_doc->getElementsByTagName('Error')->item(0)->nodeValue);
    if (!empty($response_error_message) && empty($_SESSION['ApprovalCode'])) 
    {
      $this->addError($response_error_message);
    }
    //-------------------------------------------------------------- LOG.END

    //
    // Is a VBV or similar response page?
    if ($_SESSION['Page'] == "X-Q0")
    {
      //-------------------------------------------------------------- LOG.BEGIN
      //$tolog = $log_tracking_number . "\r\n Processing X-Q0";
      //-------------------------------------------------------------- LOG.END

      $get_string = "";//print var_dump ($_REQUEST);
      if (!empty($_REQUEST['PaReq']))
      {
        $get_string = "&TRX=" . $_SESSION['ReciptNumber'] . "&GUID=" . $_SESSION['GUID']."&process=yes";
      }$get_string = "&TRX=" . $_SESSION['ReciptNumber'] . "&GUID=" . $_SESSION['GUID']."&process=yes";
      //
      // Create the return page URL to come back from VBV (or similar)
      if (ENABLE_SSL == true) $return_url = "https://";
      else $return_url = "http://";
      
      if ( ($_SERVER['SERVER_PORT'] != "80") && ($_SERVER['SERVER_PORT'] != "443") && !empty($_SERVER['SERVER_PORT']) )
      {
        $return_url .= ':' . $_SERVER['SERVER_PORT'];
      }
      $return_url .=$_SERVER['SERVER_NAME']. $_SERVER['REQUEST_URI'];
	  (strpos($return_url,"?")===false)?$return_url."?".$get_string:$return_url.$get_string; // REQUEST_URI includes osCid!!
      //
      // Check for issuer URL sent by InternetSecure. If present, we must redirect to it.
      // NOTE: A form is used (i think) in order to send parameters in a secure hidden maneer.
      $xxxSecureURL = $dom_doc->getElementsByTagName('xxxSecureURL')->item(0)->nodeValue;
      $xxxSecureRequestMsg = $dom_doc->getElementsByTagName('xxxSecureRequestMsg')->item(0)->nodeValue;
      
      if (($xxxSecureURL != "" && $xxxSecureRequestMsg != ""))
      {
        //-------------------------------------------------------------- LOG.BEGIN
        // log $log_tracking_number . "\r\n xxxSecureURL = set \r\n xxxSecureRequestMsg = set \r\n return URL = $return_url \r\n Proceeding to authentication... "
        //-------------------------------------------------------------- LOG.END
        echo '<html><body>' . "\n";
        echo '<form name="downloadForm" action="' . $xxxSecureURL . '" method="POST">' . "\n";
        echo '  <input type="hidden" name="PaReq" value="' . $xxxSecureRequestMsg . '">' . "\n";
        echo '  <input type="hidden" name="TermUrl" value="' . $return_url . '">' . "\n";
        echo '  <input type="hidden" name="MD" value="nothing">' . "\n";
        echo '  <NOSCRIPT>' . "\n";
        echo '    <div align="center" style="width: 600px; margin: auto; border: 25px solid #eee; padding: 10px; font-family: Arial; font-size: 12px;">' . "\n";
		echo '    <input type="Submit" value="Send To InternetSequre">' . "\n";
        echo '    </div>' . "\n";
        echo '  </NOSCRIPT>' . "\n";
        echo '</form>';
        echo '<script language="Javascript">';
        //echo '  document.downloadForm.submit();';
        echo '</script>';
        echo '</body></html>';
        //
        // When X-Q0 is received, ApprovalCode may be emtpy, which generally means the transaction was declined by InternetSecure.
        // So, PHP will continue executing this function after drawing the form above, and will reach the 
        // if ($trn_response_codes['ApprovalCode'] == "") question, which will decline the transaction without
        // cotinuing to the issuer authentication step.
        // However, IF we receive X-Q0 and xxxSecureURL and xxxSecureRequestMsg are set, we may continue to the authentication
        // process with the issuer, then lets stop here and go to the issuer's site shall we?
        exit();
      }
    }
     
    $trn_response_codes = array();
    $this->data['approval_code'] = $_SESSION['ApprovalCode'];     
    $this->data['AVS_response_code'] = $_SESSION['AVSResponseCode']; 
    $this->data['CVV2_response_code'] = $_SESSION['CVV2ResponseCode']; 
    //
    // Save transaction information for tracking purposes    
    $this->data['trn_date'] = $_SESSION['Date'];
    $this->data['receipt_number'] = $_SESSION['ReciptNumber'];
    $this->data['auth_code'] = $_SESSION['ApprovalCode'];
    $this->data['order_number'] = $_SESSION['SalesOrderNumber'];
	$this->data['total_amount'] = $_SESSION['TotalAmount'];
	$this->data['Page'] = $_SESSION['Page'];
	
    if ($this->data['approval_code'] == "") // Transaction declined from InternetSecure
    {
      //
      // ...and redirect
      $this->addError("Transaction declined from InternetSecure");
	  $this->addError($dom_doc->getElementsByTagName('Verbiage')->item(0)->nodeValue);
    }else{
    	//$this->transactionID = strlen($this->data['receipt_number']) > 0 ? $this->data['receipt_number'] : '' ;
		$this->success=true;
	}
    //
    // Now process the AVS and CVN responses and decide what to do; we may want to
    // decline the transaction if AVS or CVN does not satisfy us
    // ... AVS 
    //if (!empty($trn_response_codes['AVSResponseCode'])) $this->_process_avs_response($trn_response_codes['AVSResponseCode'], $log_tracking_number);
    // ... CVN
    //if (!empty($trn_response_codes['CVV2ResponseCode'])) $this->_process_cvn_response($trn_response_codes['CVV2ResponseCode'], $log_tracking_number);
  }
  
   function add_products($prodArray){
       
       //<Products>Price::Qty::Product Code::Description::Flags</Products> 
       
       for ($i=0; $i<sizeof($prodArray); $i++) 
      {
        $flags = '';
        $flags .= $this->common_flags;
		
	$name = str_replace('&', '', $prodArray[$i]['name']);
        $name = str_replace(':', '', urlencode($name) );
        
        $idProd = str_replace('&', '', $prodArray[$i]['id']);
        $idProd = str_replace(':', '', urlencode($idProd) );
        
        $this->products_list_string .=  $prodArray[$i]['final_price'] . '::' .
                                  $prodArray[$i]['qty'] . '::' .
                                  $idProd . '::' . 
                                  $name . '::' .
                                  $flags . '|'; 
      }
      
   }
   function addError($error){
   
    $this->error = true;
	$this->errorList[] = $error;
	
   }
   
   function getError(){
		
	    $errorStr='';
		
		foreach($this->errorList as $error){
		
			$errorStr .= $error."<br>";
		}
		return '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">'.$errorStr.'</div></div>';
   }
   function dump_response(){
		
		$str='';
		foreach($this->data as $name=>$value){
			$str .= $name ." : " . $value ."<br>"; 
		}
		return '<div class="ui-widget"><div class="ui-state-highlight ui-corner-all" style="padding: 0 .7em;">'.$str.'</div></div>';
   }
}


 
?>
