  <div class="container">
    <div class="page-header">
      <h1>Add new Credit Card</h1>
      </div>
    <div class="row">
     
    <div class="col-md-6">
       <form class="form-horizontal" id="payment-form" action="" method='post'>
         <div class="payment-errors alert alert-default text-danger"></div>
          <div class="form-group">
            <label class="control-label col-sm-4" for="card_number">Card Number:</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="card_number"  required name="card_number" placeholder="Enter Card Number">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-4" for="expire_month">Expiration Month:</label>
            <div class="col-sm-8"> 
              <select  class="form-control" id="expire_month" required name="expire_month" >
                <? for ($i=1; $i <=12 ; $i++) { 
                 echo "<option>$i</option>";
                }?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-4" for="expire_year">Expiration Year:</label>
            <div class="col-sm-8">
              <select class="form-control" id="expire_year" required name="expire_year" >
                <? for ($i=date('Y'); $i <=date('Y')+30; $i++) { 
                 echo "<option>$i</option>";
                }?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-4" for="cvv">CVV:</label>
            <div class="col-sm-8"> 
              <input type="text" class="form-control" id="cvv" required name="cvv" placeholder="Enter CVV">
            </div>
          </div>
          
          <div class="form-group"> 
            <div class="col-sm-offset-4 col-sm-8">
              <button type="submit" name="add_payment"  class="btn btn-default">Submit</button>
            </div>
          </div>
      </form>


     </div>
    </div>
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
  <script type="text/javascript">
  Stripe.setPublishableKey('<? echo pmpro_getOption("stripe_publishablekey");?>');
   $(function() {
  var $form = $('#payment-form');
  $form.submit(function(event) {
    // Disable the submit button to prevent repeated clicks:
    $form.find('.submit').prop('disabled', true);

    // Request a token from Stripe:
    Stripe.card.createToken({
  number: $('#card_number').val(),
  cvc: $('#cvc').val(),
  exp_month: $('#expire_month').val(),
  exp_year: $('#expire_year').val()
}, stripeResponseHandler);

    // Prevent the form from being submitted:
    return false;
  });
});

function stripeResponseHandler(status, response) {

  // Grab the form:
  var $form = $('#payment-form');

  if (response.error) { // Problem!

    // Show the errors on the form
    $form.find('.payment-errors').text(response.error.message);
    $form.find('button').prop('disabled', false); // Re-enable submission

  } else { // Token was created!

    // Get the token ID:
    var token = response.id;

    // Insert the token into the form so it gets submitted to the server:
    $form.append($('<input type="hidden" name="stripeToken" />').val(token));

    // Submit the form:
    $form.get(0).submit();

  }
}
 </script>