  <div class="container">
    <div class="page-header">
      <h1>Add new help message</h1>
      </div>
    <div class="row">
     
    <div class="col-md-6">
       <form class="form-horizontal" id="payment-form" action="" method='post'>
         <div class="payment-errors alert alert-default text-danger"></div>
          <div class="form-group">
            <label class="control-label col-sm-4" for="card_number">Case Title:</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="name"  required name="name" placeholder="Enter Case Title">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-4" for="phone_number">Phone Number:</label>
            <div class="col-sm-8"> 
             <select>
              <option></option>
             </select>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-4" for="email_address">Email Address:</label>
            <div class="col-sm-8">
              <input type="email" class="form-control" id="email_address" required name="email_address" placeholder="Enter Email">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-4" for="message">Message:</label>
            <div class="col-sm-8"> 
              <textarea  class="form-control" id="message" required name="message" placeholder="Type Your Message"></textarea>
            </div>
          </div>
          
          <div class="form-group"> 
            <div class="col-sm-offset-4 col-sm-8">
              <button type="submit" name="add_help_reply"  class="btn btn-default">Submit</button>
            </div>
          </div>
      </form>


     </div>
    </div>
  </div>
 