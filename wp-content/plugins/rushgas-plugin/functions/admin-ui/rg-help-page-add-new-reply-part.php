  <div class="container">
    <div class="page-header">
      <h1>Reply</h1>
      </div>
    <div class="row">
     
    <div class="col-md-6">
       <form class="form-horizontal" id="payment-form" action="" method='post'>
         <div class="payment-errors alert alert-default text-danger"></div>
          
          <div class="form-group">
            <label class="control-label col-sm-4" for="message">Reply Message:</label>
            <div class="col-sm-8"> 
              <textarea  class="form-control" id="message" required name="message" placeholder="Type Your Reply Message"></textarea>
            </div>
          </div>
          
          <div class="form-group"> 
            <div class="col-sm-offset-4 col-sm-8">
              <button type="submit" name="add_help"  class="btn btn-default">Submit</button>
            </div>
          </div>
      </form>


     </div>
    </div>
  </div>
 