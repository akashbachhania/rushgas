<?php
#******************************************************************************
#                       Credit Card Payments Wordpress v3.0
#
#	Author: Convergine.com
#	http://www.convergine.com
#	Version: 3.0
#   (c) 2013 Convergine.com
#
#******************************************************************************
	# PLEASE DO NOT EDIT FOLLOWING LINES IF YOU'RE NOT SURE ------->
		if($show_services==1 && $ccpt_show_am_text==2){
			$query="SELECT * FROM ".$wpdb->prefix."ccpt_services WHERE ccpt_services_id =".$service;
			$result=mysql_query($query) or die(mysql_error()."<br>$query");
			$res=mysql_fetch_assoc($result);
			$amount = number_format($res['ccpt_services_price'],2, ".", "");
			$serviceName=$res['ccpt_services_title'];
		} else if($show_services==1 && $ccpt_show_am_text==1){
		        if(empty($amount)){
		            $query="SELECT * FROM ".$wpdb->prefix."ccpt_services WHERE ccpt_services_id =".$service;
		            $result=mysql_query($query) or die(mysql_error()."<br>$query");
		            $res=mysql_fetch_assoc($result);
		            $amount = number_format($res['ccpt_services_price'],2, ".", "");
		            $serviceName=$res['ccpt_services_title'];
		        }
		    }
		$continue = false;
		if(strlen($amount) > 0 && is_numeric($amount)){ 	
			$cctype = (!empty($_POST['cctype']))?strip_tags(str_replace("'","`",strip_tags($_POST['cctype']))):'';
			$ccname = (!empty($_POST['ccname']))?strip_tags(str_replace("'","`",strip_tags($_POST['ccname']))):'';
			$ccn = (!empty($_POST['ccn']))?strip_tags(str_replace("'","`",strip_tags($_POST['ccn']))):'';
			$exp1 = (!empty($_POST['exp1']))?strip_tags(str_replace("'","`",strip_tags($_POST['exp1']))):'';
			$exp2 = (!empty($_POST['exp2']))?strip_tags(str_replace("'","`",strip_tags($_POST['exp2']))):'';
			$cvv = (!empty($_POST['cvv']))?strip_tags(str_replace("'","`",strip_tags($_POST['cvv']))):'';
			
			
			//CREDIT CARD PHP VALIDATION 
			if(empty($ccn) || empty($cctype) || empty($exp1) || empty($exp2) || empty($ccname) || empty($cvv) || empty($address) || empty($state) || empty($city)){
				$continue = false;
				$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> Not all required fields were filled out.</p></div></div><br />';
			} else { $continue = true; }
			
			if(!is_numeric($cvv)){
				$continue = false;
				$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> CVV number can contain numbers only.</p></div></div><br />';
			} else {
				$continue = true;
			}
			
			if(!is_numeric($ccn)){
				$continue = false;
				$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> Credit Card number can contain numbers only.</p></div></div><br />';
			} else {
				$continue = true;
			}
			
			if(date("Y-m-d", strtotime($exp2."-".$exp1."-01")) < date("Y-m-d")){
				$continue = false;
				$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> Your credit card is expired.</p></div></div><br />';
			} else {
				$continue = true;
			}
			
			if($continue){
				//echo "1";
				if(validateCC($ccn,$cctype)){
					$continue = true;
				} else { 
					$continue = false;
					$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> The number you\'ve entered does not match the card type selected.</p></div></div><br />';
				}
			}
			
			if($continue){
				if(luhn_check($ccn)){
					$continue = true;
				} else { 
					$continue = false; 
					$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> Invalid credit card number.</p></div></div><br />';
				}
			}
			
			switch($cctype){
				case "V":
					$cctype = "VISA";
				break;
				case "M":
					$cctype = "MASTERCARD";
				break;
				case "D":
					$cctype = "DISCOVER";
				break;
				case "A":
					$cctype = "AMEX";
				break;
			}
			
			if($continue){
			
				$query="INSERT INTO ".$wpdb->prefix."ccpt_transactions (ccpt_dateCreated, ccpt_amount, ccpt_comment,ccpt_serviceID ) VALUES (NOW(), '".$amount."', '".$item_description."', '".$service."')";
				if(mysql_query($query)){							
				
					$orderID=mysql_insert_id();
				}
				###########################################################################
				###	PAYPAL PRO PROCESSING
				###########################################################################
				//PROCESS PAYMENT BY WEBSITE PAYMENTS PRO
				require_once 'includes/paypal.callerservice.php';	
				$API_UserName=API_USERNAME;
				$API_Password=API_PASSWORD;
				$API_Signature=API_SIGNATURE;
				$API_Endpoint =API_ENDPOINT;
				//$subject = SUBJECT;
				$paymentType =urlencode("Sale");
				//CREDIT CARD INFO
				$tt = explode(" ",trim($ccname));
				if(is_array($tt)){  
					$firstName =urlencode( $tt[0]);
					if(isset($tt[2])){ $temp = $tt[1]." ".$tt[2]; } else { if(isset($tt[1])){ $temp = $tt[1]; } else { $temp = ""; } }
					$lastName =urlencode( $temp );
				} else { 
					$firstName =urlencode( $ccname); 
					$lastName =urlencode(""); 
				} 
				$creditCardType =urlencode($cctype);
				$creditCardNumber = urlencode(trim($ccn));
				$expDateMonth =urlencode( $exp1);
				$padDateMonth = str_pad($exp1, 2, '0', STR_PAD_LEFT);
				$expDateYear =urlencode(  $exp2);
				$cvv2Number = urlencode(trim($cvv));
				
				//CUSTOMER INFO
				$address1 = urlencode($address);
				$countryCode = urlencode($country);
				$city = urlencode($city);
				$state =urlencode( $state);
				$zip = urlencode($zip);

				$amount_paypal = urlencode(number_format($amount,2));
				$currencyCode=PTP_CURRENCY_CODE;
				/* Construct the request string that will be sent to PayPal.
				   The variable $nvpstr contains all the variables and is a
				   name value pair string with & as a delimiter */
				$nvpstr="&PAYMENTACTION=$paymentType&AMT=$amount_paypal&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber&EXPDATE=".$padDateMonth.$expDateYear."&CVV2=$cvv2Number&FIRSTNAME=$firstName&LASTNAME=$lastName&STREET=$address1&CITY=$city&STATE=$state&ZIP=$zip&COUNTRYCODE=$countryCode&CURRENCYCODE=$currencyCode";
				$getAuthModeFromConstantFile = true;
				$nvpHeader = "";
				
				if(!$getAuthModeFromConstantFile) {
					$AuthMode = "3TOKEN"; //Merchant's API 3-TOKEN Credential is required to make API Call.
					//$AuthMode = "FIRSTPARTY"; //Only merchant Email is required to make EC Calls.
					//$AuthMode = "THIRDPARTY"; //Partner's API Credential and Merchant Email as Subject are required.
				} else {
					if(!empty($API_UserName) && !empty($API_Password) && !empty($API_Signature) && !empty($subject)) {
						$AuthMode = "THIRDPARTY";
					}else if(!empty($API_UserName) && !empty($API_Password) && !empty($API_Signature)) {
						$AuthMode = "3TOKEN";
					}else if(!empty($subject)) {
						$AuthMode = "FIRSTPARTY";
					}
				}
				
				switch($AuthMode) {
					
					case "3TOKEN" : 
							$nvpHeader = "&PWD=".urlencode($API_Password)."&USER=".urlencode($API_UserName)."&SIGNATURE=".urlencode($API_Signature);
							break;
					case "FIRSTPARTY" :
							$nvpHeader = "&SUBJECT=".urlencode($subject);
							break;
	
					case "THIRDPARTY" :
							$nvpHeader = "&PWD=".urlencode($API_Password)."&USER=".urlencode($API_UserName)."&SIGNATURE=".urlencode($API_Signature)."&SUBJECT=".urlencode($subject);
							break;		
					
				}
				
				$nvpstr = $nvpHeader.$nvpstr;
				//print $nvpstr."<br/><br/>";
				/* Make the API call to PayPal, using API signature.
				   The API response is stored in an associative array called $resArray */
				$resArray=hash_call("doDirectPayment",$nvpstr);
				
				/* Display the API response back to the browser.
				   If the response from PayPal was a success, display the response parameters'
				   If the response was an error, display the errors received using APIError.php.
				   */
				$ack = strtoupper($resArray["ACK"]);
				//print "<br/><br/>".var_dump($resArray);
				if($ack!="SUCCESS")  {
					$_SESSION['reshash']=$resArray;
					$resArray=$_SESSION['reshash']; 
					if(isset($_SESSION['curl_error_no'])) { 
						$errorCode= $_SESSION['curl_error_no'] ;
						$errorMessage=$_SESSION['curl_error_msg'] ;	
						$my_status="<div>Transaction Un-successful!<br/>";
						$my_text="There was an error with your credit card processing:<br/>";
						$my_text.="Error Code: ".$errorCode."<br/>";
						$my_text.="Error Message: ".$errorMessage."<br/>";
						$my_status .= $mytext."</div>"; 
						$error=1;
						$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">'.$my_status.'</div></div><br />';
					} else {
						$count=0;
						$my_status="<div>Transaction Un-successful!<br/>";
						$my_text="There was an error with your credit card processing:<br/>";
						while (isset($resArray["L_SHORTMESSAGE".$count])) {		
							$errorCode    = $resArray["L_ERRORCODE".$count];
							$shortMessage = $resArray["L_SHORTMESSAGE".$count];
							$longMessage  = $resArray["L_LONGMESSAGE".$count]; 
							$count=$count+1; 					
							$my_text.="Error Code: ".$errorCode."<br/>";
							$my_text.="Error Message: ".$longMessage."<br/>";
						}//end while
						$my_status .= $my_text."</div>";
						$error=1;
						$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">'.$my_status.'</div></div><br />';
					}// end else
				 } else { 
						$my_status="<br/><div>Transaction Successful!<br/>";
						$my_status .=$ccpt_ty_title;
						$my_status .= $ccpt_ty_text;
						$error=0;
						$mess = '<div class="ui-widget"><div class="ui-state-highlight ui-corner-all" style="padding: 0 .7em;">'.$my_status.'</div></div><br />';
						#**********************************************************************************************#
						#		THIS IS THE PLACE WHERE YOU WOULD INSERT ORDER TO DATABASE OR UPDATE ORDER STATUS.
						#**********************************************************************************************#
						
							$q="UPDATE ".$wpdb->prefix."ccpt_transactions SET ccpt_status='2', ccpt_payer_email='".$email."', ccpt_transaction_id='".$resArray["TRANSACTIONID"]."', ccpt_payer_name='".$ccname."' WHERE ccpt_id='".$orderID."'";
							mysql_query($q);
							
						#**********************************************************************************************#
						//-----> send notification 
						//creating message for sending
						$headers  = "MIME-Version: 1.0\n";
						$headers .= "Content-type: text/html; charset=utf-8\n";
						$headers .= "From: 'Paypal Payment Terminal' <noreply@".$_SERVER['HTTP_HOST']."> \n";
						$subject = "New Payment Received";
						$message =  "New payment was successfully received through paypal <br />";					
						$message .= "from ".$fname." ".$lname." on ".date('m/d/Y')." at ".date('g:i A').".<br /> Payment total is: ".($amount)." ".PTP_CURRENCY_CODE;
						if($show_services==1){
							$message .= "<br />Payment was made for \"".$serviceName."\"";
						} else { 
							$message .= "<br />Payment description: \"".$item_description."\"";
						}
						$message .= "<br /><br />Billing Information:<br />";
						$message .= "Full Name: ".$fname." ".$lname."<br />";
						$message .= "Email: ".$email."<br />";
						$message .= "Address: ".$address."<br />";
						$message .= "City: ".$city."<br />";
						$message .= "Country: ".$country."<br />";
						$message .= "State/Province: ".$state."<br />";
						$message .= "ZIP/Postal Code: ".$zip."<br />";
						
						mail($admin_email,$subject,$message,$headers);
						
						$subject = "Payment Received!";
						$message =  "Dear ".$fname.",<br />";
						$message .= "<br /> Thank you for your payment.";
						$message .= "<br /><br />Kind Regards,<br />".$_SERVER['HTTP_HOST'];
						mail($email,$subject,$message,$headers);	
						
						//-----> send notification end 	
						$show_form=0;		   
				 }
			} // end of if continue.
				
		} elseif(!is_numeric($amount) || empty($amount)) { 
			if($show_services==1){
				$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> Please select service you\'re paying for.</p></div></div><br />';
			} else { 
				$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> Please type amount to pay for services!</p></div></div><br />';
			}
			$show_form=1; 
		} 
	# END OF PLEASE DO NOT EDIT IF YOU'RE NOT SURE	
?>