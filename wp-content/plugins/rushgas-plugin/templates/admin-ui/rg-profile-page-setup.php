<?php
///echo do_shortcode( '[pmpro_levels]' );
//function numediaweb_custom_user_profile_fields($user) {

global $current_user, $pmpro_levels;
//global $wpdb, $pmpro_msg, $pmpro_msgt, $pmpro_levels, $current_user, $pmpro_currency_symbol;


get_currentuserinfo();

if ( isset( $_POST['avatar-manager-upload-avatar'] ) && $_POST['avatar-manager-upload-avatar'] ) {

	
	
	$user_id = wp_update_user( array( 'ID' => $current_user->ID, 
		'display_name' => $_POST['rg_display_name'],
		'first_name'	=> $_POST['rg_first_name'],
		'last_name'	=> $_POST['rg_last_name'],
		'user_email'	=> $_POST['rg_email_address'],
		 ) );
	$phone_data = wp_json_encode(  array_filter($_POST['rg_phone_number']) );
	$address_data = wp_json_encode(  array_filter($_POST['rg_address']) );
	update_user_meta( $current_user->ID, 'rg_phone_number', $phone_data );
	update_user_meta( $current_user->ID, 'rg_address', $address_data );

	if(!empty($_FILES['profilePicture']['name'])) {

		if ( ! function_exists( 'wp_handle_upload' ) )
			require_once( ABSPATH . 'wp-admin/includes/file.php' );

			// An associative array with allowed MIME types.
			$mimes = array(
				'bmp'  => 'image/bmp',
				'gif'  => 'image/gif',
				'jpe'  => 'image/jpeg',
				'jpeg' => 'image/jpeg',
				'jpg'  => 'image/jpeg',
				'png'  => 'image/png',
				'tif'  => 'image/tiff',
				'tiff' => 'image/tiff'
			);

			// An associative array to override default variables.
			$overrides = array(
				'mimes'     => $mimes,
				'test_form' => false
			);

			// Handles PHP uploads in WordPress.
			$avatar = wp_handle_upload( $_FILES['profilePicture'], $overrides );


			if ( isset( $avatar['error'] ) )
			// Kills WordPress execution and displays HTML error message.
			wp_die( $avatar['error'],  __( 'Image Upload Error', 'avatar-manager' ) );

			if ( ! empty( $custom_avatar ) )
			// Deletes users old avatar image.
			avatar_manager_delete_avatar( $custom_avatar );

			// An associative array about the attachment.
			$attachment = array(
				'guid'           => $avatar['url'],
				'post_content'   => $avatar['url'],
				'post_mime_type' => $avatar['type'],
				'post_title'     => basename( $avatar['file'] )
			);

			// Inserts the attachment into the media library.
			$attachment_id = wp_insert_attachment( $attachment, $avatar['file'] );

			// Generates metadata for the attachment.
			$attachment_metadata = wp_generate_attachment_metadata( $attachment_id, $avatar['file'] );

			// Updates metadata for the attachment.
			wp_update_attachment_metadata( $attachment_id, $attachment_metadata );

			$custom_avatar = array();		

			// Generates a resized copy of the avatar image.
			$custom_avatar[ $options['default_size'] ] = avatar_manager_avatar_resize( $avatar['url'], $options['default_size'] );

			// Updates attachment meta fields based on attachment ID.

			// Updates user meta fields based on user ID.
			update_user_meta( $current_user->ID, 'avatar_manager_avatar_type', 'custom' );
			update_user_meta( $current_user->ID, 'avatar_manager_custom_avatar', $attachment_id );

	}

}

function avatar_manager_avatar_resize( $url, $size ) {
    // Retrieves path information on the currently configured uploads directory.
    $upload_dir = wp_upload_dir();
 
    $filename  = str_replace( $upload_dir['baseurl'], $upload_dir['basedir'], $url );
    $pathinfo  = pathinfo( $filename );
    $dirname   = $pathinfo['dirname'];
    $extension = $pathinfo['extension'];
 
    // i18n friendly version of basename().
    $basename = wp_basename( $filename, '.' . $extension );
 
    $suffix    = $size . 'x' . $size;
    $dest_path = $dirname . '/' . $basename . '-' . $suffix . '.' . $extension;
    $avatar    = array();
 
    if ( file_exists( $dest_path ) ) {
        $avatar['url']  = str_replace( $upload_dir['basedir'], $upload_dir['baseurl'], $dest_path );
        $avatar['skip'] = true;
    } else {
        // Retrieves a WP_Image_Editor instance and loads a file into it.
        $image = wp_get_image_editor( $filename );
 
        if ( ! is_wp_error( $image ) ) {
            // Resizes current image.
            $image->resize( $size, $size, true );
 
            // Saves current image to file.
            $image->save( $dest_path );
 
            $avatar['url']  = str_replace( $upload_dir['basedir'], $upload_dir['baseurl'], $dest_path );
            $avatar['skip'] = false;
        }
    }
 
    // Calls the functions added to avatar_manager_avatar_resize action hook.
    do_action( 'avatar_manager_avatar_resize', $url, $size );
 
    return $avatar;
}
?>
<style type="text/css">
	* {
  .border-radius(0) !important;
}

#field {
    margin-bottom:20px;
}
#field_teleno {
    margin-bottom:20px;
}
.btn span.glyphicon {    			
	opacity: 0;				
}
.btn.active span.glyphicon {				
	opacity: 1;				
}


.pmpro_btn {

    -moz-user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
    cursor: pointer;
    display: inline-block;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857;
    margin-bottom: 0;
    padding: 6px 12px;
    text-align: center;
    vertical-align: middle;
    white-space: nowrap;
     background-color: #5bc0de;
    border-color: #46b8da;
    color: #fff;
    text-decoration: none;
}
.pmpro_btn a:hover ,a:focus {
    outline: 0 none;
    text-decoration: none;
}

</style>
<div class="container">
	<div class="row">
<form name="form_action" method="post"  enctype="multipart/form-data">
	<table class="form-table">

		<tr>
			<th>
				<label for="rg_display_name"><?php _e('Display Name', 'rg'); ?></label>
			</th>
			<td>
				<input type="text" name="rg_display_name" id="rg_display_name" value="<?php echo esc_attr(  $current_user->display_name  ); ?>" class="regular-text" />
				<br><span class="description"><?php _e('Enter your display name', 'rg'); ?></span>		
			</td>
		</tr>

		<tr>
			<th>
				<label for="rg_first_name"><?php _e('First Name'); ?></label>
			</th>
			<td>
				<input type="text" name="rg_first_name" id="rg_first_name" value="<?php echo esc_attr( get_the_author_meta( 'first_name', $current_user->ID ) ); ?>" class="regular-text" />
				<br><span class="description"><?php _e('Enter first your name.', 'rg'); ?></span>
			</td>
		</tr>
		<tr>
			<th>
				<label for="rg_last_name"><?php _e('Last Name', 'rg'); ?></label>
			</th>
			<td>
				<input type="text" name="rg_last_name" id="rg_last_name" value="<?php echo esc_attr( get_the_author_meta( 'last_name', $current_user->ID ) ); ?>" class="regular-text" />
				<br><span class="description"><?php _e('Enter your last name', 'rg'); ?></span>		
			</td>
		</tr>
		<tr>
			<th>
				<label for="rg_address"><?php _e('Address', 'rg'); ?></label>
			</th>
			<td>
				<?php
				$address_array_jeson = get_the_author_meta( 'rg_address', $current_user->ID );
				$address_array =  json_decode($address_array_jeson);
				?>
				<!-- <input type="text" name="rg_phone_number" id="rg_phone_number" value="<?php echo esc_attr( get_the_author_meta( 'rg_phone_number', $current_user->ID ) ); ?>" class="regular-text" /> -->

				<?php if(!empty($address_array)) { $i=0; foreach ($address_array as $key => $value) { $i++; ?>
					<div id="delete_rg_address<?php echo $i;?>">
					<input id="delete_rg_address" class="input form-control" type="text" name="rg_address[]"  value="<?php echo $value;?>">


					<label class="btn btn-danger">
					<input type="checkbox" autocomplete="off"  onclick="delete_address(<?php echo $i;?>)">
					<span class="glyphicon glyphicon-ok"></span> Delete
					</label>
					</div>	
						
						<br>
					<?php } } ?>

				<div id="field"><input autocomplete="off" class="input" id="field1" name="rg_address[]" type="text" placeholder="Type something" data-items="8"/><button id="b1" class="btn add-more" type="button">+</button></div>
				<br><span class="description"><?php _e('Enter your address', 'rg'); ?></span>
				
			</td>
		</tr>


		<tr>
			<th>
				<label for="rg_email_address"><?php _e('Email Address', 'rg'); ?></label>
			</th>
			<td>
				<input type="text" name="rg_email_address" id="rg_email_address" value="<?php echo esc_attr( $current_user->user_email ); ?>" class="regular-text" /> 
				
				<br><span class="description"><?php _e('Enter your email address', 'rg'); ?></span>
				
			</td>
		</tr>

		<tr>
			<th>
				<label for="rg_phone_number"><?php _e('Phone Number', 'rg'); ?></label>
			</th>
			<td>
			<?php
			$phone_array_jeson = get_the_author_meta( 'rg_phone_number', $current_user->ID );
			$phone_array =  json_decode($phone_array_jeson);
			?>
				<!-- <input type="text" name="rg_phone_number" id="rg_phone_number" value="<?php echo esc_attr( get_the_author_meta( 'rg_phone_number', $current_user->ID ) ); ?>" class="regular-text" /> -->

				<?php if(!empty($phone_array)) { $i=0; foreach ($phone_array as $key => $value) { $i++; ?>
					<div id="delete_teleno<?php echo $i;?>">
					<input id="delete_teleno" class="input form-control" type="text" name="rg_phone_number[]"  value="<?php echo $value;?>">


					<label class="btn btn-danger">
					<input type="checkbox" autocomplete="off"  onclick="delete_div(<?php echo $i;?>)">
					<span class="glyphicon glyphicon-ok"></span> Delete
					</label>
					</div>	
						
						<br>
					<?php } } ?>

				<div id="field_teleno">					
					<input autocomplete="off" class="input" id="field_teleno1" name="rg_phone_number[]" type="text" placeholder="Type something" data-items="8"/>
					<button id="bt" class="btn teleno" type="button">+</button>
				</div>
				<br><span class="description"><?php _e('Enter your phone number', 'rg'); ?></span>
				
			</td>
		</tr>

		<tr>
			<th>
				<label for="rg_profile_image"><?php _e('Profile Image', 'rg'); ?></label>
			</th>
			<td>
				<?php
				$id = get_user_meta($current_user->ID, 'avatar_manager_custom_avatar', true);
				if(empty($id)) {
				?>
					<?php echo get_avatar( $current_user->ID, 32 );  ?>
				<?php } else {
					$url =  wp_get_attachment_url( $id );
				?>
					<img class="avatar avatar-32 wp-user-avatar wp-user-avatar-32 photo avatar-default" width="32" height="32" alt="" src="<?php echo $url;?>">
				<?php
				}
				?>
				
				 <input type='file' name="profilePicture" value="<?php _e( 'Upload Image', 'rg' ); ?>" id="profilePicture"/><br />
				 
				<br><span class="description"><?php _e('Enter your phone number', 'rg'); ?></span>
				
			</td>
		</tr>

		<tr>

			<th>
				<label for="rg_profile_image"><?php _e('Member Levels', 'rg'); ?></label>
			</th>
			<td>
			<?php //bartag echo pmpro_advanced_levels_shortcode( ); ?>
				<?php echo do_shortcode( '[memberlite_levels]' ) ?>

			</td>
		</tr>

		<tr>
			<td>
				<input type="submit" name="avatar-manager-upload-avatar" class="button button-primary" value="Update Profile">
			</td>
		</tr>


	</table>

</form>
</div>
</div>

<script type="text/javascript">
	jQuery(document).ready(function(){

		 var next = 1;
    jQuery(".add-more").click(function(e){
        e.preventDefault();
        var addto = "#field" + next;
        var addRemove = "#field" + (next);
        next = next + 1;
        var newIn = '<input autocomplete="off" class="input form-control" id="field' + next + '" name="rg_address[]" type="text">';
        var newInput = jQuery(newIn);
        var removeBtn = '<button id="remove' + (next - 1) + '" class="btn btn-danger remove-me" >-</button></div><div id="field">';
        var removeButton = jQuery(removeBtn);
        jQuery(addto).after(newInput);
        jQuery(addRemove).after(removeButton);
        jQuery("#field" + next).attr('data-source',jQuery(addto).attr('data-source'));
        jQuery("#count").val(next);  
        
            jQuery('.remove-me').click(function(e){
                e.preventDefault();
                var fieldNum = this.id.charAt(this.id.length-1);
                var fieldID = "#field" + fieldNum;
                jQuery(this).remove();
                jQuery(fieldID).remove();
            });
    });
    	

    	var teleno_next = 1;
    	
    jQuery(".teleno").click(function(et){

        et.preventDefault();
        var addto_t = "#field_teleno" + teleno_next;
        //alert(addto);
        var addRemove_t = "#field_teleno" + (teleno_next);
        teleno_next = teleno_next + 1;
        //alert(teleno_next);
        var newIn_t = '<input autocomplete="off" class="input form-control-t" id="field_teleno' + teleno_next + '" name="rg_phone_number[]" type="text">';
        var newInput_t = jQuery(newIn_t);
        //alert(newIn);

        var removeBtn_t = '<button id="remove_t' + (teleno_next - 1) + '" class="btn btn-danger remove-me-tel" >-</button></div><div id="field_teleno">';
        var removeButton_t = jQuery(removeBtn_t);
        jQuery(addto_t).after(newInput_t);
        jQuery(addRemove_t).after(removeButton_t);
        jQuery("#field_teleno" + teleno_next).attr('data-source',jQuery(addto_t).attr('data-source'));
        jQuery("#count_teleno").val(teleno_next);  
        
            jQuery('.remove-me-tel').click(function(e){
                et.preventDefault();
                var fieldNum_t = this.id.charAt(this.id.length-1);
                var fieldID_t = "#field_teleno" + fieldNum_t;
                jQuery(this).remove();
                jQuery(fieldID_t).remove();
            });
    });

    delete_div = function(val) {
    	//alert(val);
    	jQuery( "#delete_teleno"+val ).remove();

    }
    delete_address = function(val) {
    	jQuery( "#delete_rg_address"+val ).remove();
    }
    

 
});
</script>
<?php
/*if(!empty($_FILES['myfile']['name'])) //validation check image uploaded or not
 {
 $uploaded_file_type = $_FILES["myfile"]["type"]; // Get image type
 $allowed_file_types = array('image/jpg','image/jpeg','image/gif','image/png'); // allowed image formats
 if(in_array($uploaded_file_type, $allowed_file_types)) // validate image format with the given formats
{
 if(!function_exists('wp_handle_upload' )) require_once( ABSPATH . 'wp-admin/includes/file.php' ); // include file library where upload handler function included.

 $uploadedfile = $_FILES['myfile'];
 $upload_overrides = array( 'test_form' => false );
 $movefile = wp_handle_upload( $uploadedfile, $upload_overrides ); // save file on server.
 if(!empty($movefile['url'])) // validate image uploading
 {
 update_usermeta( $userdata->ID, 'user_meta_image', $movefile['url'] ); // update user meta database
 }
 }
 else
 {
 $errmsg = "Invalid Image Format."; // errors if invalid format.
 }
 }
*/