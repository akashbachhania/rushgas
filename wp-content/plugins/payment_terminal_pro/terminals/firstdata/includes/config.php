<?php
#******************************************************************************
#                       Credit Card Payments Wordpress v3.0
#
#	Author: Convergine.com
#	http://www.convergine.com
#	Version: 3.0
#   (c) 2013 Convergine.com
#
#******************************************************************************
session_start();
error_reporting(E_ALL ^ E_NOTICE);

require (CCPT_HOME_DIR."/wp-load.php"); 
require("functions.php");

$configProcessor=is_string(get_option('ccwp_details_FIRSTDATA'))?unserialize(get_option('ccwp_details_FIRSTDATA')):get_option('ccwp_details_FIRSTDATA');
/*******************************************************************************************************
    GENERAL SCRIPT CONFIGURATION VARIABLES
********************************************************************************************************/

//THIS IS TITLE ON PAGES
$title = "FirstData (LinkPoint) Payment Terminal v1.0"; //site title
$titleAboveForm = "FirstData (LinkPoint) Payment Terminal";

//THIS IS ADMIN EMAIL FOR NEW PAYMENT NOTIFICATIONS.
$admin_email = get_option('ccpt_admin_email'); //this email is for notifications about new payments

//IF YOU NEED TO ADD MORE SERVICES JUST ADD THEM THE SAME WAY THEY APPEAR BELOW.
$show_services = get_option('ccpt_show_dd_text'); 

// set  to   RECUR  - for recurring payments, ONETIME - for one-time payments.
$payment_mode = "ONETIME"; // RECUR  or ONETIME
define("PTP_CURRENCY_CODE",get_option('ccpt_paypal_currency')); 
//RECURRING SERVICES DEFINITION (dropdown)
$liveMode = get_option('ccpt_test')==2 ? true : false;
/*******************************************************************************************************
    FIRST DATA / LINKPOINT CONFIGURATION VARIABLES
********************************************************************************************************/
$myorder["host"] = "secure.linkpt.net";
$myorder["port"] = "1129";
# Change this to the name and location of your certificate file

# Change this to your store number
$myorder["configfile"] = $configProcessor['live']['STORE_ID'];
// TO GO LIVE CHANGE FOLLOWING LINE TO TRUE

//  Enable debugging mode, switch to false if going live.
$liveModeDebug = false;

if(!$liveMode){
    # For test transactions, set to GOOD, DECLINE, or DUPLICATE
    $myorder["keyfile"] =  $configProcessor['test']['CERTIFICAT_FILE_PATH'];
    $myorder["result"] = "GOOD";
} else {
    $myorder["result"] = "LIVE";
	$myorder["keyfile"] =  $configProcessor['live']['CERTIFICAT_FILE_PATH'];
}
###die($myorder["keyfile"]);
/*******************************************************************************************************
    PAYPAL EXPRESS CHECKOUT CONFIGURATION VARIABLES
********************************************************************************************************/
$enable_paypal = false; //shows/hides paypal payment option from payment form.
$paypal_merchant_email = "your_paypal_email@domain.com";
$paypal_success_url = "http://www.your_domain.com/path/to/firstdata-payment-terminal/paypal_thankyou.php";
$paypal_cancel_url = "http://www.your_domain.com/path/to/firstdata-payment-terminal/paypal_cancel.php";
$paypal_ipn_listener_url = "http://www.your_domain.com/path/to/firstdata-payment-terminal/paypal_listener.php";
$paypal_custom_variable = "some_var";
$paypal_currency = "USD";
$sandbox = false; //if you want to test payments with your sandbox account change to true (you must have account at https://developer.paypal.com/ and YOU MUST BE LOGGED IN WHILE TESTING!)


//DO NOT CHANGE ANYTHING BELOW THIS LINE, UNLESS SURE OF COURSE
//detecting actual path for keyfile...
$myorder["keyfile"] = dirname(__FILE__)."/".$myorder["keyfile"];
//just in case lets' check the file presence (if live mode).
if($liveMode){
    if(!is_file($myorder["keyfile"])){
        if(!isset($mess)){ $mess = ""; }
        $mess .= '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong>.PEM CERTIFICATE FILE NOT FOUND! PLEASE ADJUST THE PATH IN CONFIG FILE</p></div></div><br />';
    }
}
define("PAYMENT_MODE",$payment_mode);
if(!$sandbox){
    define("PAYPAL_URL","https://www.paypal.com/cgi-bin/webscr");
} else {
    define("PAYPAL_URL","https://www.sandbox.paypal.com/cgi-bin/webscr");
}
//following line defines if customer has to be redirected to HTTPS (if FirstData is LIVE -> redirects to https, else - stays on http)
$redirect_non_https = $liveMode == false?false:true;

if($redirect_non_https){
	if ($_SERVER['SERVER_PORT']!=443) {
		$sslport=443; //whatever your ssl port is
		$url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		header("Location: $url");
		exit();
	}
}
?>