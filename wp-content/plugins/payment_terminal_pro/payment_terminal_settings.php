<?php
#******************************************************************************
#                       Credit Card Payments Wordpress v3.0
#
#	Author: Convergine.com
#	http://www.convergine.com
#	Version: 3.0
#   (c) 2013 Convergine.com
#
#******************************************************************************
include('ccwp.cfg.php');
$payment_terminals = ccwpCRUD($ccwp_cfg_arr,'default');

$ccpt_submit_settings = "";
$ccpt_submit_license = "";
$ccpt_ty_title = "";
$wp_url = get_site_url();
?>
    <link rel="stylesheet" media="screen" href="<?php echo $wp_url?>/wp-content/plugins/payment_terminal_pro/resources/css/admin-style.css" />
	<script type="text/javascript" src="<?php echo $wp_url?>/wp-content/plugins/payment_terminal_pro/resources/js/jquery.dd.js" ></script>
<script>
function ccpt_selectProcessor(el,element){
	jQuery("#BW-change-ccpt_processor").val(el);
	jQuery(".BW-ccwp-terminal-holder").hide();
	jQuery("#terminal_"+el).show();
	jQuery(".BW-ccwp-terminal-holder ,.BW-ccwp-terminal-liselector").removeClass("selected");
	jQuery(element).addClass("selected")
	jQuery("#terminal_"+el).addClass("selected");
	
	return false;
}
jQuery(document).ready(function($){
	 $('.select').msDropdown();
});
</script>
<?php
	
		if(isset($_POST['ccpt_submit_settings']) && $_POST['ccpt_submit_settings'] == 'yes') {
						
			update_option('ccpt_paypal_currency', isset($_POST['ccpt_paypal_currency'])?$_POST['ccpt_paypal_currency']:'');
			update_option('ccpt_ty_title', isset($_POST['ccpt_ty_title'])?$_POST['ccpt_ty_title']:'');
			update_option('ccpt_ty_text', isset($_POST['ccpt_ty_text'])?$_POST['ccpt_ty_text']:'');
			update_option('ccpt_cancel_title', isset($_POST['ccpt_cancel_title'])?$_POST['ccpt_cancel_title']:'');
			update_option('ccpt_payment_description', isset($_POST['ccpt_payment_description'])?$_POST['ccpt_payment_description']:'');
			update_option('ccpt_admin_email', isset($_POST['ccpt_admin_email'])?$_POST['ccpt_admin_email']:'');
			update_option('ccpt_show_comment_field', isset($_POST['ccpt_show_comment_field'])?$_POST['ccpt_show_comment_field']:'');
			update_option('ccpt_show_dd_text', isset($_POST['ccpt_show_dd_text'])?$_POST['ccpt_show_dd_text']:'');
			update_option('ccpt_button_text', isset($_POST['ccpt_button_text'])?$_POST['ccpt_button_text']:'');
			update_option('ccpt_show_am_text', isset($_POST['ccpt_show_am_text'])?$_POST['ccpt_show_am_text']:'');
			update_option('ccpt_processor', isset($_POST['ccpt_processor'])?$_POST['ccpt_processor']:'');
			update_option('ccpt_test', isset($_POST['ccpt_test'][$_POST['ccpt_processor']])?$_POST['ccpt_test'][$_POST['ccpt_processor']]:$_POST['AHAM']);
			
			
			?>
			<div class="updated BW-ccwp-terminal_settings_wrap" ><p><strong><?php if(!isset($payment_terminals['CUSTOM_ERROR'])){ _e('Settings saved!' );}else{ ccwpDisplayCustomErrors($payment_terminals['CUSTOM_ERROR']);} ?></strong></p></div>
			<?php
		} 
		
		
		if(isset($_POST['ccpt_submit_license']) && $_POST['ccpt_submit_license'] == 'yes') {
			
			$headers  = "MIME-Version: 1.0\n";
			$headers .= "Content-type: text/html; charset=utf-8\n";
			$headers .= "From: 'authorization' <noreply@".$_SERVER['HTTP_HOST']."> \n";
			$subject = "Authorization[Credit Card Payments Terminal v2.0]";
			//$wp_url = get_bloginfo('siteurl');
			$wp_url = get_site_url();
			$message = "License: ".$_POST['ccpt_licensekey']."<br /> Url: ".$wp_url. "<br /> Username: ".$_POST['codecanyon_username']."<br />Host: ".$_SERVER['HTTP_HOST']."<br />URI: ".$_SERVER['REQUEST_URI'];
			update_option('ccpt_license', $_POST['ccpt_licensekey']);
			wp_mail("info@convergine.com", $subject, $message, $headers);
			?>
			<div class="updated BW-ccwp-terminal_settings_wrap"><p><strong><?php _e('License saved! Thank you for purchasing our script, we hope you will enjoy it!' ); ?></strong></p></div>
			<?php
			
		} 	
			$ccpt_paypal_currency = get_option('ccpt_paypal_currency');
			$ccpt_ty_title = get_option('ccpt_ty_title');
			$ccpt_ty_text = get_option('ccpt_ty_text');
			$ccpt_cancel_title = get_option('ccpt_cancel_title');
			$ccpt_payment_description = get_option('ccpt_payment_description');
			$ccpt_admin_email = get_option('ccpt_admin_email');
			$ccpt_show_comment_field = get_option('ccpt_show_comment_field');
			$ccpt_show_dd_text = get_option('ccpt_show_dd_text');
			$ccpt_button_text = get_option('ccpt_button_text');
			$ccpt_license = get_option('ccpt_license');
			$ccpt_show_am_text = get_option('ccpt_show_am_text');
			$ccpt_processor = get_option('ccpt_processor');
			$ccpt_test = get_option('ccpt_test');
			
				
			
				
			
			if(empty($ccpt_license)){?>
            <div class="wrap">
			<?php    echo "<h4>" . __('CodeCanyon License Key','') . "</h4>"; ?>
				<p><?php _e("To finish the installation please enter your valid codecanyon license key and codecanyon username which comes in receipt with your purchase of our script.<br />Don't worry, you will be prompted to do this only once.<br /><br />Please don't forget that for <b>EVERY INSTALLATION</b> of this script you need to get separate license from codecanyon.net (by purchasing our script, each purchase equals to 1 additional license, as stated in <a href='http://codecanyon.net/wiki/support/legal-terms/licensing-terms/' target='_blank'>Envato Licensing Terms</a>) " ); ?></p>	
                
                <form name="ccpt_form" method="post" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>">
                    <input type="hidden" name="ccpt_submit_license" value="yes">
                    
                    <p><?php _e("CodeCanyon License Key: " ); ?><input type="text" name="ccpt_licensekey" value="" size="40"></p>
                   
                    
                    <p><?php _e("CodeCanyon Username: " ); ?><input type="text" name="codecanyon_username" value="" size="40"></p>
                 
                    <p class="submit">
                    <input type="submit" name="Submit" value="<?php _e('Save License Key') ?>" />
                    </p>
                </form>   
			</div>
				
			<?php } else { ?>
            <div class="wrap-ccwp-bw">
            			<div class="wrap-ccwp-header" >
			<?php    echo "<h2>" . __('Credit Card Payments - Settings','') . "</h2>"; ?>
				</div>
                
		
                <div class="ccwp-subheader-pptr" >
				<p><?php _e("Here you define your Payment Terminal settings such as your e-mail address associated with your merchant account, currency, confirmation and cancellation messages and other settings. Please note: highlighted tab with merchant logo - is the current active gateway." ); ?></p>
                </div>

                <div class="wrap-ccwp-content-noborder" >
                    <form enctype="multipart/form-data" name="ccpt_form" id="ccpt_form" method="post" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>">
                        <input type="hidden" name="ccpt_submit_settings" value="yes">
                        <div class="BW-ccwp-terminal_settings_wrap" >
                            <div id="BW-ccwp-select_terminal_nav_list" >
                                <ul>
                                    <?php $i=1;foreach($payment_terminals as $terminals => $details) : if($terminals != "CUSTOM_ERROR"): ?>
                                        <li class="BW-ccwp-terminal-liselector<?php echo displaySelectedCond($i,$ccpt_processor); ?>" onclick="return ccpt_selectProcessor('<?php echo $i; ?>',this);" ><a title="Select <?php echo strtoupper($terminals); ?> as your payment Gateway" id="terminal_nav_bg_<?php echo strtolower(str_replace(array(' ','"','\'','.'), '_', $terminals)); ?>" href="#<?php if(isset($_GET['page']) && $_GET['page'] == 'ccpt_admin_settings'){echo $_SERVER['REQUEST_URI']."&active_terminal=".$i;} ?>#You should not see this in your browser url with javascript enabled!" ><?php echo $terminals; ?></a></li>
                                    <?php $i++;endif;endforeach; ?>
                                </ul>
                            </div>
                            <div id="BW-ccwp-terminal_settings_wrap_left" >
                                <div id="BW-ccwp-terminal-fields" >
                                    <h2 class="BW-ccwp-settings-h2 BW-ccwp-settings-tip-text-left" ><?php _e('TERMINAL SETTINGS'); ?></h2>
                                    <input type="hidden" name="ccpt_processor" id="BW-change-ccpt_processor" value="<?php echo (isset($_GET['active_terminal']) ? $_GET['active_terminal'] : $ccpt_processor); ?>" />
                                    <INPUT TYPE="HIDDEN" NAME="AHAM" VALUE="<?php echo $ccpt_test; ?>" />
                                <?php $i=1;foreach($payment_terminals as $terminal => $details) : if($terminal != "CUSTOM_ERROR"): ?>
                                    <div id="terminal_<?php echo $i; ?>" class="BW-ccwp-terminal-holder<?php echo displaySelectedCond($i,$ccpt_processor); ?>" >
                                        <?php if(isset($details['TIP'])) : ?>
                                            <p class="BW-ccwp-settings-tip BW-ccwp-settings-tip-text-left" ><?php echo __($details['TIP']); ?></p>
                                        <?php else : ?>
                                            <p class="BW-ccwp-settings-tip BW-ccwp-settings-tip-text-left" ><?php _e('Tip: You get these settings from your merchant provider'); ?></p>
                                        <?php endif; ?>
                                        <?php foreach($details as $case => $fields) : if ($case == "LIVE" || $case == "TEST"): ?>
                                        <div class="BW-ccwp-terminal-<?php echo strtolower($case); ?> " >
                                            <div class="BW-left-terminal-case<?php if($ccpt_test == "2" && $case == "LIVE") : echo " active"; elseif($ccpt_test == "1" && $case == "TEST") : echo " active"; endif; ?>" ><span><?php echo $case." SITE"; ?></span></div>
                                            <div class="BW-ccwp-terminal-field-wrap<?php if($ccpt_test == "2" && $case == "LIVE") : echo " active"; elseif($ccpt_test == "1" && $case == "TEST") : echo " active"; endif; ?>">
                                                <?php foreach($fields as $field_name => $field_value) : $input_id = strtolower(str_replace(array(' ','"','\'','.'), '_', $terminal."_".$case."_".$field_name)); ?>
                                                    <?php $filter = isCCWPfilter($field_name); if($filter != null): $field_name = $filter;$input_id = isCCWPfilter($input_id); ?>
                                                    <p><label <?php echo strlen($field_name) <= 13 ? 'style="padding-top:13px;"': ''; ?> for="<?php echo $input_id; ?>"><?php echo __(strtoupper($field_name)); ?>:</label>
                                                       <input onchange="jQuery('#trigger_<?php echo $input_id; ?>').addClass('button-disabled').text('File Selected !');" class="icancustomizethistooiguess" style="display:none;width: 185px !important;height: 30px !important;" type="file" autocomplete="off" id="<?php echo $input_id; ?>" name="<?php echo $input_id; ?>"  />
                                                       <span id="trigger_<?php echo $input_id; ?>" onclick="jQuery('#<?php echo $input_id; ?>').trigger('click')" class="ccwp-upload-btn button" >Click to select File</span>
                                                    </p>
                                                    <?php else : ?>
                                                    <p><label <?php echo strlen($field_name) <= 13 ? 'style="padding-top:13px;"': ''; ?> for="<?php echo $input_id; ?>"><?php echo __(strtoupper($field_name)); ?>:</label><input type="text" autocomplete="off" id="<?php echo $input_id; ?>" name="<?php echo $input_id; ?>" value="<?php echo $field_value; ?>" /></p>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            </div>
                                            <div class="BW-settings-change-case<?php if(($ccpt_test == "2" && $case == "LIVE") || ($ccpt_test == "1" && $case == "TEST")) : echo " active";endif; ?>" >
                                                <button type="submit" name="ccpt_test[<?php echo $i; ?>]" value="<?php echo ($case == "TEST" ? 1 : 2); ?>" title="<?php if(($ccpt_test == "2" && $case == "LIVE") || ($ccpt_test == "1" && $case == "TEST")) : echo $case." Settings are ON !";endif; ?>" >
                                                    <?php if($ccpt_test != "2" && $case == "LIVE") : _e(" Turn "); elseif($ccpt_test != "1" && $case == "TEST") : _e(" Turn  "); endif; ?><?php echo __($case); ?><?php _e(' Settings ');?><?php if($ccpt_test == "2" && $case == "LIVE") : _e(" Are "); elseif($ccpt_test == "1" && $case == "TEST") : _e("Are"); endif; ?><?php _e(' On '); ?>
                                                </button>
                                            </div>
                                        </div>
                                    <?php elseif($case == "TEST/LIVE") : $carr = explode('/',$case); if($ccpt_test == "2"){$case = "LIVE";}elseif($ccpt_test == "1"){$case = "TEST";}  ?>
                                        <div class="BW-ccwp-terminal-<?php echo strtolower($case); ?> " >
                                            <div class="BW-left-terminal-case<?php if($case == "LIVE") : echo " active"; endif; ?>" ><span><?php echo $case." SITE"; ?></span></div>
                                            <div class="BW-ccwp-terminal-field-wrap<?php if($case == "LIVE") : echo " active"; endif; ?>">
                                                <?php foreach($fields as $field_name => $field_value) : $input_id = strtolower(str_replace(array(' ','"','\'','.',']','['), '_', $terminal."_live_".$field_name)); ?>
                                                    <?php $filter = isCCWPfilter($field_name); if($filter != null): $field_name = $filter;$input_id = isCCWPfilter($input_id); ?>
                                                    <p><label <?php echo strlen($field_name) <= 13 ? 'style="padding-top:13px;"': ''; ?> for="<?php echo $input_id; ?>"><?php echo __(strtoupper($field_name)); ?>:</label><input class="icancustomizethistooiguess" style="width: 185px !important;height: 30px !important;" type="file" autocomplete="off" id="<?php echo $input_id; ?>" name="<?php echo $input_id; ?>"  /></p>
                                                    <?php else : ?>
                                                    <p><label <?php echo strlen($field_name) <= 13 ? 'style="padding-top:13px;"': ''; ?> for="<?php echo $input_id; ?>"><?php echo __(strtoupper($field_name)); ?>:</label><input type="text" autocomplete="off" id="<?php echo $input_id; ?>" name="<?php echo $input_id; ?>" value="<?php echo $field_value; ?>" /></p>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            </div>
                                            <div class="BW-settings-change-case<?php if($case == "LIVE") : echo " active";endif; ?>" >
                                                <?php # foreach($carr as  $case): ?>
                                                <button <?php echo ($case == "LIVE" ? 'class="youtoo"' : ''); ?> type="submit" name="ccpt_test[<?php echo $i; ?>]" value="<?php echo ($case == "TEST" ? 2 : 1); ?>" title="<?php if(($ccpt_test == "2" && $case == "LIVE") || ($ccpt_test == "1" && $case == "TEST")) : echo $case." Settings are ON !";endif; ?>" >
                                                    <?php if($ccpt_test == "2" && $case == "LIVE") : _e(" Turn "); elseif($ccpt_test == "1" && $case == "TEST") : _e(" Turn "); endif; ?><?php echo __(end(array_diff($carr, array($case)))); ?> <?php _e('Settings On'); ?>
                                                </button>
                                                <?php # endforeach; ?>
                                            </div>
                                        </div>
                                    <?php endif;endforeach; ?>
                                    </div>
                                <?php $i++;endif;endforeach; ?>
                                </div>

                        <p class="BW-ccwp-settings-tip-text-left" ><b><?php _e("Default Currency: " ); ?></b><br>
                        <span class='ccpt_tip'><i><?php _e("Tip:" ); ?></i><?php _e(" make sure that your selected merchant account provider can accept selected currency" ); ?></span></p>
                        <p style="clear: both;" ><select name="ccpt_paypal_currency" style="height: 32px;width: 247px;" class="select">
                             <option value="AUD" <?php echo $ccpt_paypal_currency=="AUD"?"selected":""?>>Australian Dollar       (AUD)</option>
                             <option value="GBP" <?php echo $ccpt_paypal_currency=="GBP"?"selected":""?>>British Pound       (GBP)</option>
                             <option value="BRL" <?php echo $ccpt_paypal_currency=="BRL"?"selected":""?>>Brazil Real       (BRL)</option>
                             <option value="CAD" <?php echo $ccpt_paypal_currency=="CAD"?"selected":""?>>Canadian Dollar       (CAD)</option>
                             <option value="CHF" <?php echo $ccpt_paypal_currency=="CHF"?"selected":""?>>Swiss Franc       (CHF)</option>
                             <option value="CZK" <?php echo $ccpt_paypal_currency=="CZK"?"selected":""?>>Czech Koruna       (CZK)</option>
                             <option value="DKK" <?php echo $ccpt_paypal_currency=="DKK"?"selected":""?>>Danish Krone       (DKK)</option>
                             <option value="EUR" <?php echo $ccpt_paypal_currency=="EUR"?"selected":""?>>European Euro       (EUR)</option>
                             <option value="HKD" <?php echo $ccpt_paypal_currency=="HKD"?"selected":""?>>Hong Kong Dollar       (HKD)</option>
                             <option value="HUF" <?php echo $ccpt_paypal_currency=="HUF"?"selected":""?>>Hungarian Forint       (HUF)</option>
                             <option value="ILS" <?php echo $ccpt_paypal_currency=="ILS"?"selected":""?>>Israeli Shekel       (ILS)</option>
                             <option value="JPY" <?php echo $ccpt_paypal_currency=="JPY"?"selected":""?>>Japanese Yen       (JPY)</option>
                             <option value="MXN" <?php echo $ccpt_paypal_currency=="MXN"?"selected":""?>>Mexican pesos       (MXN)</option>
                             <option value="MYR" <?php echo $ccpt_paypal_currency=="MYR"?"selected":""?>>Malaysian Ringgit       (MYR)</option>
                             <option value="NOK" <?php echo $ccpt_paypal_currency=="NOK"?"selected":""?>>Norwegian Krone       (NOK)</option>
                             <option value="NZD" <?php echo $ccpt_paypal_currency=="NZD"?"selected":""?>>New Zealand Dollar       (NZD)</option>
                             <option value="PHP" <?php echo $ccpt_paypal_currency=="PHP"?"selected":""?>>Philippines Peso       (PHP)</option>
                             <option value="PLN" <?php echo $ccpt_paypal_currency=="PLN"?"selected":""?>>Polish zloty       (PLN)</option>
                             <option value="SEK" <?php echo $ccpt_paypal_currency=="SEK"?"selected":""?>>Swedish Krona       (SEK)</option>
                             <option value="SGD" <?php echo $ccpt_paypal_currency=="SGD"?"selected":""?>>Singapore Dollar       (SGD)</option>
                             <option value="THB" <?php echo $ccpt_paypal_currency=="THB"?"selected":""?>>Thai Baht       (THB)</option>
                             <option value="TWD" <?php echo $ccpt_paypal_currency=="TWD"?"selected":""?>>Taiwan Dollar       (TWD)</option>
                             <option value="USD" <?php echo $ccpt_paypal_currency=="USD"?"selected":""?>>United States Dollar       (USD)</option>
                            </select>                    </p>

                        <p class="BW-ccwp-settings-tip-text-left" style="clear: both;padding-top: 20px;" ><b><?php _e("Website Owner Notification Email:" ); ?></b></p>
                        <p><input type="text"  class="ccpt_text" name="ccpt_admin_email" value="<?php echo $ccpt_admin_email; ?>" size="40"></p>

                        <p style="height: 0px;border-bottom: 1px solid #e6e6e6;" >&nbsp;</p>

                        <?php echo "<h4 class='BW-ccwp-settings-tip-text-left' >" . __( '"Thank-You" Message' ) . "</h4>"; ?>
                        <span class='ccpt_tip BW-ccwp-settings-tip-text-left'><i><?php _e("Tip:" ); ?></i><?php _e(" small text describing next step, appears in widget" ); ?></span>
                        <?php /*<p><?php _e("Title: " ); ?><input type="text" name="ccpt_ty_title" value="<?php echo $ccpt_ty_title; ?>" size="40"><?php _e(" (will appear as heading on page)" ); ?></p>*/?>
                        <div id="poststuff">
                            <div id="<?php echo user_can_richedit() ? 'postdivrich' : 'postdiv'; ?>" class="postarea">
                            <?php the_editor($ccpt_ty_text, $id = 'ccpt_ty_text', $prev_id = 'ccpt_ty_text', $media_buttons = false, $tab_index = 2);?><?php _e(" (small text describing next step, appears in widget)" ); ?>
                            </div>
                        </div>


                        <p style="height: 0px;border-bottom: 1px solid #e6e6e6;" >&nbsp;</p>

                        <?php echo "<h4>" . __( 'Payment Description' ) . "</h4>"; ?>
                        <span class='ccpt_tip'><i><?php _e("Tip:" ); ?></i><?php _e(" small text describing next step, if needed, appears in widget" ); ?></span>
                        <?php /*<p><?php _e("Title: " ); ?><input type="text" name="ccpt_cancel_title" value="<?php echo $ccpt_cancel_title; ?>" size="40"><?php _e(" (will appear as heading on page)" ); ?></p>*/?>
                        <div id="poststuff">
                            <div id="<?php echo user_can_richedit() ? 'postdivrich' : 'postdiv'; ?>" class="postarea">
                            <?php the_editor($ccpt_payment_description, $id = 'ccpt_payment_description', $prev_id = 'ccpt_payment_description', $media_buttons = false, $tab_index = 2);?><?php _e(" (small text describing next step, if needed, appears in widget)" ); ?>
                            </div>
                        </div>

                        <p style="height: 0px;border-bottom: 1px solid #e6e6e6;" >&nbsp;</p>

                        <?php echo "<h4 class='BW-ccwp-settings-tip-text-left' >" . __( 'Widget/Popup Settings' ) . "</h4>"; ?>
                        <p class="BW-ccwp-settings-tip-text-left"  >Pay button text:</p>
                        <p class="BW-ccwp-settings-tip-text-left" ><input type="text"  class="ccpt_text" name="ccpt_button_text" value="<?php echo $ccpt_button_text?>" size="40"><i><?php _e("Example:" ); ?></i><b><?php _e(" Pay NOW!" ); ?></b></p>

                        <table cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <td class="BW-ccwp-settings-tip-text-left" align="left" height="30"><?php _e("Show comment field<span style='float:right' >:</span>" ); ?>&nbsp;&nbsp;</td>
                            <td class="BW-ccwp-settings-tip-text-left" ><input type="radio" name="ccpt_show_comment_field" value="1" <?php echo $ccpt_show_comment_field=="1"?"checked":""?>><b> <?php _e("Yes" ); ?></b> <input type="radio" name="ccpt_show_comment_field" value="2" <?php echo $ccpt_show_comment_field=="2"?"checked":""?>><b> <?php _e("No" ); ?></b></td>
                        <tr>

                        <tr>
                            <td class="BW-ccwp-settings-tip-text-left" align="left" height="30"><?php _e("Show services field<span style='float:right' >:</span>" ); ?>&nbsp;&nbsp;</td>
                            <td class="BW-ccwp-settings-tip-text-left"><input type="radio" name="ccpt_show_dd_text" value="1" <?php echo $ccpt_show_dd_text=="1"?"checked":""?>><b> <?php _e("Yes" ); ?></b> <input type="radio" name="ccpt_show_dd_text" value="2" <?php echo $ccpt_show_dd_text=="2"?"checked":""?>><b> <?php _e("No," ); ?></b><?php _e(" allow user to type amount" ); ?></td>
                        <tr>

                        <tr>
                            <td class="BW-ccwp-settings-tip-text-left" align="left" height="30"><?php _e("Show amount field<span style='float:right' >:</span>" ); ?>&nbsp;&nbsp;</td>
                            <td class="BW-ccwp-settings-tip-text-left" ><input type="radio" name="ccpt_show_am_text" value="1" <?php echo $ccpt_show_am_text=="1"?"checked":""?>><b> <?php _e("Yes" ); ?></b> <input type="radio" name="ccpt_show_am_text" value="2" <?php echo $ccpt_show_am_text=="2"?"checked":""?>><b> <?php _e("No," ); ?></b><?php _e(" user will need to select service" ); ?></td>
                        <tr>
                        </table>

                       <?php/*?><p><?php _e("Live Or Test: " ); ?><input type="radio" name="ccpt_test1" value="1" <?php echo $ccpt_test=="1"?"checked":""?>> <?php _e("Live" ); ?> <input type="radio" name="ccpt_test1" value="2" <?php echo $ccpt_test=="2"?"checked":""?>> <?php _e("Test" ); ?></p><?*/?>

                       <p class="BW-ccwp-settings-tip-text-left" style="background-color:#FFFFCC;padding:5px"><?php _e("Important Note: if you don’t want your users to be able to enter amount themselves - select yes in “Show services field” AND select no in “Show amount field” " ); ?>

                        <br />
                        <p class="submit">
                        <input class="button-primary" type="submit" name="Submit" value="<?php _e('Update Settings') ?>" />
                        </p>
                       </div><!-- end right wrapper -->
                       </div><!-- end wrapper -->
                    </form>
                </div>
		</div>
        <?php } //check ?>
        <div class="pt_footer">
        	<a href="http://www.convergine.com" target="_blank"><img src="<?php echo $wp_url?>/wp-content/plugins/payment_terminal_pro/resources/images/convergine.png" /></a>
        </div>