<?php
#******************************************************************************
#                       Credit Card Payments Wordpress v3.0
#
#	Author: Convergine.com
#	http://www.convergine.com
#	Version: 3.0
#   (c) 2013 Convergine.com
#
#******************************************************************************
session_start();
error_reporting(E_ALL ^ E_NOTICE);
require (CCPT_HOME_DIR."/wp-load.php"); 
require("functions.php");

$configProcessor=is_string(get_option('ccwp_details_PAYMENTEXPRESS'))?unserialize(get_option('ccwp_details_PAYMENTEXPRESS')):get_option('ccwp_details_PAYMENTEXPRESS');

/*******************************************************************************************************
    GENERAL SCRIPT CONFIGURATION VARIABLES
********************************************************************************************************/
//Default Timezone
date_default_timezone_set("Australia/Sydney");

//THIS IS TITLE ON PAGES
$title = "PaymentExpress Payment Terminal v1.0"; //site title
$titleAboveForm = "PaymentExpress Payment Terminal";

//THIS IS ADMIN EMAIL FOR NEW PAYMENT NOTIFICATIONS.
$admin_email = get_option('ccpt_admin_email'); //this email is for notifications about new payments

$system_live = get_option('ccpt_test') ==2 ? true: false; 
 
//NOW, IF YOU WANT TO ACTIVATE THE DROPDOWN WITH SERVICES ON THE TERMINAL ITSELF, CHANGE BELOW VARIABLE TO TRUE;
$show_services = get_option('ccpt_show_dd_text'); 
define("PTP_CURRENCY_CODE",get_option('ccpt_paypal_currency')); 
/*******************************************************************************************************
    PAYMENTEXPRESS CONFIGURATION VARIABLES
********************************************************************************************************/
// Set this to your PaymentExpress Customer ID
define("DPS_USERNAME",$configProcessor['live']['USERNAME']);
define("DPS_PASSWORD",$configProcessor['live']['PASSWORD']);
define("DPS_CURRENCY",get_option('ccpt_paypal_currency'));

// TO GO LIVE CHANGE FOLLOWING LINE TO TRUE

//following line defines if customer has to be redirected to HTTPS (if PaymentExpress is LIVE -> redirects to https, else - stays on http)
$redirect_non_https = $system_live;

/****************************************************
//TEST CREDIT CARD CREDENTIALS for PAYMENT EXPRESS SANDBOX TESTING
Card Type: Visa
Account Number: 4111111111111111
Expiry: any date in future
CVV: any 3 numbers
****************************************************/

/*******************************************************************************************************
    PAYPAL EXPRESS CHECKOUT CONFIGURATION VARIABLES
********************************************************************************************************/
$enable_paypal = false; //shows/hides paypal payment option from payment form.
$paypal_merchant_email = "your_paypal@email.here";
$paypal_success_url = "http://www.YOUURDOMAIN.com/path/to/paymentexpress-payment-terminal/paypal_thankyou.php";
$paypal_cancel_url = "http://www.YOUURDOMAIN.com/path/to/paymentexpress-payment-terminal/paypal_cancel.php";
$paypal_ipn_listener_url = "http://www.YOUURDOMAIN.com/path/to/paymentexpress-payment-terminal/paypal_listener.php";
$paypal_custom_variable = "some_var";
$paypal_currency = "USD";
$sandbox = false; //if you want to test payments with your sandbox account change to true (you must have account at https://developer.paypal.com/ and YOU MUST BE LOGGED IN WHILE TESTING!)


//DO NOT CHANGE ANYTHING BELOW THIS LINE, UNLESS SURE OF COURSE
if(!$sandbox){
    define("PAYPAL_URL","https://www.paypal.com/cgi-bin/webscr");
} else {
    define("PAYPAL_URL","https://www.sandbox.paypal.com/cgi-bin/webscr");
}

if($redirect_non_https){
	if ($_SERVER['SERVER_PORT']!=443) {
		$sslport=443; //whatever your ssl port is
		$url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		header("Location: $url");
		exit();
	}
}
?>