<?php
#******************************************************************************
#                       Credit Card Payments Wordpress v3.0
#
#	Author: Convergine.com
#	http://www.convergine.com
#	Version: 3.0
#   (c) 2013 Convergine.com
#
#******************************************************************************

session_start();
error_reporting(E_ALL ^ E_NOTICE);

require (CCPT_HOME_DIR."/wp-load.php"); 
require("functions.php"); 

$configProcessor=is_string(get_option('ccwp_details_AUTHORIZE_NET'))?unserialize(get_option('ccwp_details_AUTHORIZE_NET')):get_option('ccwp_details_AUTHORIZE_NET'); 


//THIS IS TITLE ON PAGES
$title = "Authorize.net Payment Terminal v2.0"; //site title
$titleAboveForm = "Authorize.net Payment Terminal";
//THIS IS ADMIN EMAIL FOR NEW PAYMENT NOTIFICATIONS.
//THIS IS ADMIN EMAIL FOR NEW PAYMENT NOTIFICATIONS.
$admin_email = get_option('ccpt_admin_email'); //this email is for notifications about new payments
define("PTP_CURRENCY_CODE",get_option('ccpt_paypal_currency'));
		
//NOW, IF YOU WANT TO ACTIVATE THE DROPDOWN WITH SERVICES ON THE TERMINAL
//ITSELF, CHANGE BELOW VARIABLE TO TRUE;			
$show_services = get_option('ccpt_show_dd_text'); 

// set  to   RECUR  - for recurring payments, ONETIME - for onetime payments
$payment_mode = "ONETIME";


//service name   |   price  to charge   | Billing period  "Day", "Week", "Month", "Year"   |  how many periods of previous field per billing period | trial period in days | Trial amount
$recur_services = array(
				 array("Service 1 monthly WITH 30 DAYS TRIAL", "49.99", "Month", "1", "30", "24.99"),
				 array("Service 1 monthly", "49.99", "Month", "1", "0", "0"),
				 array("Service 1 quaterly", "149.99", "Month", "3", "0", "0"),
				 array("Service 1 semi-annualy", "249.99", "Month", "6", "0", "0"),
				 array("Service 1 annualy", "349.99", "Year", "1", "0", "0")
				);
//IF YOU'RE GOING LIVE FOLLOWING VARIABLE SHOULD BE SWITCH TO true
// IT WILL AUTOMATICALLY REDIRECT ALL NON-HTTTPS REQUESTS TO HTTPS.
// MAKE SURE SSL IS INSTALLED ALREADY.
$redirect_non_https = false;
$liveMode = get_option('ccpt_test')==2 ? true : false;


if(!$liveMode){
//TEST MODE
define('MERCHANT_LOGIN', $configProcessor['test']['MERCHANT_LOGIN']);
define('MERCHANT_TRAN_KEY', $configProcessor['test']['MERCHANT_TRAN_KEY']);
define('GATEWAY_URL', 'https://test.authorize.net/gateway/transact.dll');
define('TEST_MODE', 'TRUE');
define('ARBHOST', 'apitest.authorize.net');
define('ARBPATH', '/xml/v1/request.api');
} else {
//LIVE MODE
define('MERCHANT_LOGIN', $configProcessor['live']['MERCHANT_LOGIN']);
define('MERCHANT_TRAN_KEY', $configProcessor['live']['MERCHANT_TRAN_KEY']);
define('GATEWAY_URL', 'https://secure.authorize.net/gateway/transact.dll');
define('TEST_MODE', 'FALSE');
define('ARBHOST', 'api.authorize.net');
define('ARBPATH', '/xml/v1/request.api');
}






/*******************************************************************************************************
    PAYPAL EXPRESS CHECKOUT CONFIGURATION VARIABLES
********************************************************************************************************/
$enable_paypal = false; //shows/hides paypal payment option from payment form.
$paypal_merchant_email = "your_paypal_merchant_email@here.com";
$paypal_success_url = "http://www.domain.com/path/to/authorize-payment-terminal/paypal_thankyou.php";
$paypal_cancel_url = "http://www.domain.com/path/to/authorize-payment-terminal/paypal_cancel.php";
$paypal_ipn_listener_url = "http://www.domain.com/path/to/authorize-payment-terminal/paypal_listener.php";
$paypal_custom_variable = "some_var";
$paypal_currency = "USD";
$sandbox = false; //if you want to test payments with your sandbox account change to true (you must have account at https://developer.paypal.com/ and YOU MUST BE LOGGED IN WHILE TESTING!)
if($liveMode){ $sandbox = false; } else { $sandbox = true; }


//DO NOT CHANGE ANYTHING BELOW THIS LINE, UNLESS SURE OF COURSE
define("PAYMENT_MODE",$payment_mode);
if(!$sandbox){
    define("PAYPAL_URL_STD","https://www.paypal.com/cgi-bin/webscr");
} else {
    define("PAYPAL_URL_STD","https://www.sandbox.paypal.com/cgi-bin/webscr");
}

//DO NOT CHANGE ANYTHING BELOW THIS LINE, UNLESS SURE OF COURSE
if($redirect_non_https){
	if ($_SERVER['SERVER_PORT']!=443) {
		$sslport=443; //whatever your ssl port is
		$url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		header("Location: $url");
		exit();
	}
}
?>