// JavaScript Document
(function() {
    tinymce.create('tinymce.plugins.pppt', {
        init : function(ed, url) {
            ed.addButton('pppt', {
                title : 'PayPal Payment Form',
                image : url+'/resources/images/paypal.png',
                onclick : function() {
                     ed.selection.setContent('[pppt_paypalform]');
 
                }
            });
        },
        createControl : function(n, cm) {
            return null;
        },
    });
    tinymce.PluginManager.add('pppt', tinymce.plugins.mylink);
})();