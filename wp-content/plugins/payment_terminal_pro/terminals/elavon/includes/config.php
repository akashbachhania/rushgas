<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);

require (CCPT_HOME_DIR."/wp-load.php");  
require("functions.php"); 
#******************************************************************************
#                       Credit Card Payments Wordpress v3.0
#
#	Author: Convergine.com
#	http://www.convergine.com
#	Version: 3.0
#   (c) 2013 Convergine.com
#
#******************************************************************************

$configProcessor=is_string(get_option('ccwp_details_ELAVON'))?unserialize(get_option('ccwp_details_ELAVON')):get_option('ccwp_details_ELAVON');

//THIS IS TITLE ON PAGES
$title = "Elavon Payment Terminal v1.1"; //site title
$titleAboveForm = "Elavon Payment Terminal";
//THIS IS ADMIN EMAIL FOR NEW PAYMENT NOTIFICATIONS.
$admin_email = get_option('ccpt_admin_email'); //this email is for notifications about new payments

define("PTP_CURRENCY_CODE",get_option('ccpt_paypal_currency')); 
//IF YOU NEED TO ADD MORE SERVICES JUST ADD THEM THE SAME WAY THEY APPEAR BELOW.

//NOW, IF YOU WANT TO ACTIVATE THE DROPDOWN WITH SERVICES ON THE TERMINAL
//ITSELF, CHANGE BELOW VARIABLE TO TRUE;			
$show_services = get_option('ccpt_show_dd_text'); 

$redirect_non_https = (get_option('ccpt_test')==2)? true : false;;

$testAccount=$redirect_non_https===true?false:true;


//IF YOU'RE GOING LIVE FOLLOWING VARIABLE SHOULD BE SWITCH TO true
// IT WILL AUTOMATICALLY REDIRECT ALL NON-HTTTPS REQUESTS TO HTTPS.
// MAKE SURE SSL IS INSTALLED ALREADY.
define("ENABLE_SSL",$redirect_non_https);

//FOLLOWING MUST BE SET TO TRUE - ONLY IF YOU'RE TESTING ELAVON WITH TEST ACCOUNT

//Internetsequre RELATED VARIABLES
if($testAccount){
    define('GATEWAY_URL','https://demo.myvirtualmerchant.com/VirtualMerchantDemo/process.do');
}else{
    define('GATEWAY_URL','https://www.myvirtualmerchant.com/VirtualMerchant/process.do');
}

define('USER_ID', $configProcessor['live']['USER_ID']);  //ELAVON USER_ID
define('MERCHANT_ID', $configProcessor['live']['MERCHANT_ID']);  // ELAVON MERCHANT_ID
define('PIN', $configProcessor['live']['PIN']); //ELAVON_PIN


//DO NOT CHANGE ANYTHING BELOW THIS LINE, UNLESS SURE OF COURSE
if(ENABLE_SSL){
	if ($_SERVER['SERVER_PORT']!=443) {
		$sslport=443; //whatever your ssl port is
		
		$url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		print $url;
		//header("Location: $url");
		exit();
	}
}
?>