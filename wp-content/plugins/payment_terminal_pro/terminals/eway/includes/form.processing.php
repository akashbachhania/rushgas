<?php
#******************************************************************************
#                       Credit Card Payments Wordpress v3.0
#
#	Author: Convergine.com
#	http://www.convergine.com
#	Version: 3.0
#   (c) 2013 Convergine.com
#
#******************************************************************************

	# PLEASE DO NOT EDIT FOLLOWING LINES IF YOU'RE NOT SURE ------->
	if($show_services==1 && $ccpt_show_am_text==2){
			$query="SELECT * FROM ".$wpdb->prefix."ccpt_services WHERE ccpt_services_id =".$service;
			$result=mysql_query($query) or die(mysql_error()."<br>$query");
			$res=mysql_fetch_assoc($result);
			$amount = number_format($res['ccpt_services_price'],2, ".","");
			$serviceName=$res['ccpt_services_title'];
		} else if($show_services==1 && $ccpt_show_am_text==1){
		        if(empty($amount)){
		            $query="SELECT * FROM ".$wpdb->prefix."ccpt_services WHERE ccpt_services_id =".$service;
		            $result=mysql_query($query) or die(mysql_error()."<br>$query");
		            $res=mysql_fetch_assoc($result);
		            $amount = number_format($res['ccpt_services_price'],2, ".", "");
		            $serviceName=$res['ccpt_services_title'];
		        }
		    }
	$province = str_replace("-AU-","",$state);

		$continue = false;
		if(!empty($amount) && is_numeric($amount)){ 	
			$cctype = (!empty($_POST['cctype']))?strip_tags(str_replace("'","`",strip_tags($_POST['cctype']))):'';
			$ccname = (!empty($_POST['ccname']))?strip_tags(str_replace("'","`",strip_tags($_POST['ccname']))):'';
			$ccn = (!empty($_POST['ccn']))?strip_tags(str_replace("'","`",strip_tags($_POST['ccn']))):'';
			$exp1 = (!empty($_POST['exp1']))?strip_tags(str_replace("'","`",strip_tags($_POST['exp1']))):'';
			$exp2 = (!empty($_POST['exp2']))?strip_tags(str_replace("'","`",strip_tags($_POST['exp2']))):'';
			$cvv = (!empty($_POST['cvv']))?strip_tags(str_replace("'","`",strip_tags($_POST['cvv']))):'';

            if($cctype!="PP"){
                //CREDIT CARD PHP VALIDATION
                if(empty($ccn) || empty($cctype) || empty($exp1) || empty($exp2) || empty($ccname) || empty($cvv) || empty($address) || empty($state) || empty($city)){
                    $continue = false;
                    $mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> Not all required fields were filled out.</p></div></div><br />';
                } else { $continue = true; }

                if(!is_numeric($cvv)){
                    $continue = false;
                    $mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> CVV number can contain numbers only.</p></div></div><br />';
                } else {
                    $continue = true;
                }

                if(!is_numeric($ccn)){
                    $continue = false;
                    $mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> Credit Card number can contain numbers only.</p></div></div><br />';
                } else {
                    $continue = true;
                }

                if(date("Y-m-d", strtotime($exp2."-".$exp1."-01")) < date("Y-m-d")){
                    $continue = false;
                    $mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> Your credit card is expired.</p></div></div><br />';
                } else {
                    $continue = true;
                }

                if($continue){
                    //echo "1";
                    if(validateCC($ccn,$cctype)){
                        $continue = true;
                    } else {
                        $continue = false;
                        $mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> The number you\'ve entered does not match the card type selected.</p></div></div><br />';
                    }
                }

                if($continue){
                    if(luhn_check($ccn)){
                        $continue = true;
                    } else {
                        $continue = false;
                        $mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> Invalid credit card number.</p></div></div><br />';
                    }
                }

            } else {
                $continue = true;
            }
			
			switch($cctype){
                case "V":
                    $cctype = "VISA";
                    break;
                case "M":
                    $cctype = "MASTERCARD";
                    break;
                case "DI":
                    $cctype = "DINERS CLUB";
                    break;
                case "D":
                    $cctype = "DISCOVER";
                    break;
                case "A":
                    $cctype = "AMEX";
                    break;
                case "PP":
                    $cctype = "PAYPAL";
				break;
			}


                $transactID = mktime()."-".rand(1,999);
			
            if($continue && $cctype!="PAYPAL"){
				$query="INSERT INTO ".$wpdb->prefix."ccpt_transactions (ccpt_dateCreated, ccpt_amount, ccpt_comment,ccpt_serviceID ) VALUES (NOW(), '".$amount."', '".$item_description."', '".$service."')";
				if(mysql_query($query)){							
				
					$orderID=mysql_insert_id();
				}
                //CREDIT CARD INFO
                $tt = explode(" ",trim($ccname));
                if(is_array($tt)){
                    $firstName = $tt[0];
                    if(isset($tt[2])){ $temp = $tt[1]." ".$tt[2]; } else { if(isset($tt[1])){ $temp = $tt[1]; } else { $temp = ""; } }
                    $lastName = $temp ;
                } else {
                    $firstName =$ccname;
                    $lastName ="";
                }
                $creditCardType =$cctype;
                $creditCardNumber = trim($ccn);
                $expDateMonth = $exp1;
                $padDateMonth = str_pad($exp1, 2, '0', STR_PAD_LEFT);
                $expDateYear =$exp2;
                $expDateYear = substr($exp2,-2);
                $cvv2Number = trim($cvv);
                $amount = number_format($amount,2,".","");


                switch($payment_mode){
                /*******************************************************************************************************
                    ONE TIME PAYMENT PROCESSING
                *******************************************************************************************************/
                case "ONETIME":
                    require_once('eway.payment.class.php');
                    // Set the payment details
                    $eway = new EwayPaymentLive($eWAY_CustomerID, $eWAY_PaymentMethod, $eWAY_UseLive);


                    $eway->setTransactionData("TotalAmount", $amount * 100); //mandatory field
                    $eway->setTransactionData("CustomerFirstName", $firstName);
                    $eway->setTransactionData("CustomerLastName", $lastName);
                    $eway->setTransactionData("CustomerEmail", $email);
                    $eway->setTransactionData("CustomerAddress", $address);
                    $eway->setTransactionData("CustomerPostcode", $zip);
                    if($show_services){
                        $eway->setTransactionData("CustomerInvoiceDescription", $services[$service][0]);
                    } else {
                        $eway->setTransactionData("CustomerInvoiceDescription", $item_description);
                    }

                    $eway->setTransactionData("CustomerInvoiceRef", $transactID);
                    $eway->setTransactionData("CardHoldersName", $ccname); //mandatory field
                    $eway->setTransactionData("CardNumber", $creditCardNumber); //mandatory field
                    $eway->setTransactionData("CardExpiryMonth", $padDateMonth); //mandatory field
                    $eway->setTransactionData("CardExpiryYear", $expDateYear); //mandatory field
                    //option1 -3 -> will be returned as ewayTrxnOption1-3, and TrxnNumber will be returned as ewayTrxnReference
                    $eway->setTransactionData("TrxnNumber",$transactID );
                    $eway->setTransactionData("Option1", $txtOption1);
                    $eway->setTransactionData("Option2", $txtOption2);
                    $eway->setTransactionData("Option3", $txtOption3);

                    $eway->setCurlPreferences(CURLOPT_SSL_VERIFYPEER, 0); // Require for Windows hosting

                    // Send the transaction
                    $ewayResponseFields = $eway->doPayment();

                      if(strtolower($ewayResponseFields["EWAYTRXNSTATUS"])=="false")
                      {
                          /*print "Transaction Error: " . $ewayResponseFields["EWAYTRXNERROR"] . "<br>\n";
                          foreach($ewayResponseFields as $key => $value)
                          print "\n<br>\$ewayResponseFields[\"$key\"] = $value";*/
                          $my_status = "<div>Transaction Un-successful!<br/>";
                          $my_status .= "There was an error with your credit card processing:<br/>";
                          $my_status .= "Merchant Gateway Response: ".$ewayResponseFields["EWAYTRXNERROR"]."<br/>";
                          $my_status .= "</div>";
                          $error=1;
                          $mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">'.$my_status.'</div></div><br />';
                      }
                      else if(strtolower($ewayResponseFields["EWAYTRXNSTATUS"])=="true")
                      {
                           // payment succesfully sent to gateway
                           // Payment succeeded get values returned

                          $my_status = "<div>Transaction Successful!<br/>";
                          $my_status .= "Thank you for your payment<br /><br />";
                          $my_status .= "AuthCode: ".$ewayResponseFields["EWAYAUTHCODE"]."<br />";
                          $my_status .= "Transaction ID: ".$ewayResponseFields["EWAYTRXNNUMBER"]."<br />";
                          $my_status .= "You will receive confirmation email within 5 minutes.<br/><br/><a href='index.php'>Return to payment page</a></div><br/>";
                          $error=0;
                          $mess = '<div class="ui-widget"><div class="ui-state-highlight ui-corner-all" style="padding: 0 .7em;">'.$my_status.'</div></div><br />';
						  $resArray["TRANSACTIONID"] = 	$ewayResponseFields["EWAYTRXNNUMBER"];
                          #**********************************************************************************************#
                          #		THIS IS THE PLACE WHERE YOU WOULD INSERT ORDER TO DATABASE OR UPDATE ORDER STATUS.
                          #**********************************************************************************************#
                                //eway returned values:
                                /*$lblResult = " Result: " . $ewayResponseFields["EWAYTRXNSTATUS"] . "<br>";
                                 $lblResult .= " AuthCode: " . $ewayResponseFields["EWAYAUTHCODE"] . "<br>";
                                 $lblResult .= " Error: " . $ewayResponseFields["EWAYTRXNERROR"] . "<br>";
                                 $lblResult .= " eWAYInvoiceRef: " . $ewayResponseFields["EWAYTRXNREFERENCE"] . "<br>";
                                 $lblResult .= " Amount: " . $ewayResponseFields["EWAYRETURNAMOUNT"] . "<br>";
                                 $lblResult .= " Txn Number: " . $ewayResponseFields["EWAYTRXNNUMBER"] . "<br>";
                                 $lblResult .= " Option1: " . $ewayResponseFields["EWAYOPTION1"] . "<br>";
                                 $lblResult .= " Option2: " . $ewayResponseFields["EWAYOPTION2"] . "<br>";
                                 $lblResult .= " Option3: " . $ewayResponseFields["EWAYOPTION3"] . "<br>";
                                 echo $lblResult; */
                          #**********************************************************************************************#
						  
							$q="UPDATE ".$wpdb->prefix."ccpt_transactions SET ccpt_status='2', ccpt_payer_email='".$email."', ccpt_transaction_id='".$resArray["TRANSACTIONID"]."', ccpt_payer_name='".$ccname."' WHERE ccpt_id='".$orderID."'";
							mysql_query($q);	
                          /******************************************************************
                               ADMIN EMAIL NOTIFICATION
                          ******************************************************************/
                          $headers  = "MIME-Version: 1.0\n";
                          $headers .= "Content-type: text/html; charset=utf-8\n";
                          $headers .= "From: 'eWay Payment Terminal' <noreply@".$_SERVER['HTTP_HOST']."> \n";
                          $subject = "New Payment Received";
                          $message =  "New payment was successfully received through eWay <br />";
                          $message .= "from ".$fname." ".$lname."  on ".date('m/d/Y')." at ".date('g:i A').".<br /> Payment total is: $".number_format($amount,2);
                          if($show_services){
                              $message .= "<br />Payment was made for \"".$services[$service][0]."\"";
                          } else {
                              $message .= "<br />Payment description: \"".$item_description."\"";
                          }
                          $message .= "<br />Transaction Number: \"".$ewayResponseFields["EWAYTRXNNUMBER"]."\"";
                          $message .= "<br />AuthCode: \"".$ewayResponseFields["EWAYAUTHCODE"]."\"";
                          $message .= "<br /><br />Billing Information:<br />";
                          $message .= "Full Name: ".$fname." ".$lname."<br />";
                          $message .= "Email: ".$email."<br />";
                          $message .= "Address: ".$address."<br />";
                          $message .= "City: ".$city."<br />";
                          $message .= "Country: ".$country."<br />";
                          $message .= "State/Province: ".$state."<br />";
                          $message .= "ZIP/Postal Code: ".$zip."<br />";

                          mail($admin_email,$subject,$message,$headers);

                          /******************************************************************
                                CUSTOMER EMAIL NOTIFICATION
                          ******************************************************************/
                          $subject = "Payment Received!";
                          $message =  "Dear ".$fname.",<br />";
                          $message .= "<br /> Thank you for your payment.";
                          $message .= "<br /><br />";
                            if($show_services){
                                $message .= "<br />Payment was made for \"".$services[$service][0]."\"";
                            } else {
                                $message .= "<br />Payment was made for: \"".$item_description."\"";
                            }
                          $message .= "<br />Payment amount: $".number_format($amount,2);
                          $message .= "<br />Transaction Number: \"".$ewayResponseFields["EWAYTRXNNUMBER"]."\"";
                          $message .= "<br />AuthCode: \"".$ewayResponseFields["EWAYAUTHCODE"]."\"";
                          $message .= "<br /><br />Billing Information:<br />";
                          $message .= "Full Name: ".$fname." ".$lname."<br />";
                          $message .= "Email: ".$email."<br />";
                          $message .= "Address: ".$address."<br />";
                          $message .= "City: ".$city."<br />";
                          $message .= "Country: ".$country."<br />";
                          $message .= "State/Province: ".$state."<br />";
                          $message .= "ZIP/Postal Code: ".$zip."<br />";

                          $message .= "<br /><br />Kind Regards,<br />".$_SERVER['HTTP_HOST'];
                          mail($email,$subject,$message,$headers);

                          //-----> send notification end
                          $show_form=0;
                      }
                      else
                      {
                        // invalid response recieved from server.
                        $my_status = "<div>Transaction Un-successful!<br/>";
                        $my_status .= "Error: An invalid response was recieved from the payment gateway.<br/>";
                        $my_status .= "</div>";
                        $error=1;
                        $mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">'.$my_status.'</div></div><br />';
                      }


                break;
                /*******************************************************************************************************
                    RECURRING PROCESSING (REBILL FUNCTIONALITY)
                *******************************************************************************************************/
                case "RECUR":

                    require_once('eway.rebill.payment.php');
                    require_once('eway.rebill.connector.php');


                    $startDate = date("d/m/Y");
                    $billingPeriod = $recur_services[$service][2];	// or "1", "2", "3", "4" ( 1 = Days, 2 = Weeks, 3 = Months, 4 = Years )
                    $billingFreq = $recur_services[$service][3];		// combination of this and billingPeriod must be at most a year
                    $subscription_expiry = $recur_services[$service][4];
                    $desc = $recur_services[$service][0];
                    switch($billingPeriod){
                        case "1": $tt = "day(s)"; break;
                        case "2": $tt = "week(s)"; break;
                        case "3": $tt = "month(s)"; break;
                        case "4": $tt = "year(s)"; break;
                    }
                    $payment_cycle = "every $billingFreq $tt";
                    if($eWAY_UseLive){
                        $amount = number_format($recur_services[$service][1],2);
                    } else {
                        $amount = number_format($recur_services[$service][1],0); //for some reason test account does not accept cents.
                    }
                    //echo $amount;
                    $objRebill = new RebillPayment();
                    //$objRebill->CustomerRef($_POST['txtCustomerRef']);
                    //$objRebill->CustomerTitle($_POST['txtTitle']);
                    $objRebill->CustomerFirstName($fname);
                    $objRebill->CustomerLastName($lname);
                    //$objRebill->CustomerCompany($_POST['txtCompany']);
                    //$objRebill->CustomerJobDesc($_POST['txtPosition']);
                    $objRebill->CustomerEmail($email);
                    $objRebill->CustomerAddress($address);
                    //$objRebill->CustomerSuburb($_POST['txtSuburb']);
                    $objRebill->CustomerState($state);
                    $objRebill->CustomerPostCode($zip);
                    $objRebill->CustomerCountry($country);
                    //$objRebill->CustomerPhone1($_POST['txtPhone1']);
                    //$objRebill->CustomerPhone2($_POST['txtPhone2']);
                    //$objRebill->CustomerFax($_POST['txtFax']);
                    //$objRebill->CustomerURL($_POST['txtURL']);
                   // $objRebill->CustomerComments($_POST['txtComments']);
                    //$objRebill->RebillInvRef($_POST['txtInvRef']);
                    $objRebill->RebillInvDesc($desc);
                    $objRebill->RebillCCName($ccname);
                    $objRebill->RebillCCNumber($creditCardNumber);
                    $objRebill->RebillInitAmt(0);
                    $objRebill->RebillInitDate($startDate);
                    $objRebill->RebillRecurAmt($amount*100);
                    $objRebill->RebillStartDate($startDate);
                    $objRebill->RebillEndDate($subscription_expiry);

                    $objRebill->eWAYCustomerID($eWAY_CustomerID);
                    $objRebill->RebillCCExpMonth($exp1);
                    $objRebill->RebillCCExpYear($exp2);
                    $objRebill->RebillInterval($billingFreq);
                    $objRebill->RebillIntervalType($billingPeriod);

                    $objConnector = new GatewayConnector($eWAY_UseLive?"web.config":"webtest.config");
                    if ($objConnector->ProcessRequest($objRebill)){
                        $objResponse = $objConnector->Response();
                        if ($objResponse != null){
                            $lblResult = $objResponse->Result();
                            $lblErrorDescription = $objResponse->ErrorDetails();
                            $lblErrorSeverity = $objResponse->ErrorSeverity();
                            if(strtolower($lblResult)=="success"){
                                $my_status="<br/><div>Subscription Created Successfully!<br/>";
                                $my_status .="Thank you for your payment<br /><br />";
                                $my_status .= "You will receive confirmation email within 5 minutes.<br/><br/><a href='index.php'>Return to payment page</a></div><br/>";
                                $error=0;
                                $mess = '<div class="ui-widget"><div class="ui-state-highlight ui-corner-all" style="padding: 0 .7em;">'.$my_status.'</div></div><br />';
                                #**********************************************************************************************#
                                #		THIS IS THE PLACE WHERE YOU WOULD INSERT ORDER TO DATABASE OR UPDATE ORDER STATUS FOR RECURRING
                                #**********************************************************************************************#

                                #**********************************************************************************************#
                                /******************************************************************
                                    ADMINISTRATOR EMAIL NOTIFICATION
                                ******************************************************************/
                                $headers  = "MIME-Version: 1.0\n";
                                $headers .= "Content-type: text/html; charset=utf-8\n";
                                $headers .= "From: 'eWay Payment Terminal' <noreply@".$_SERVER['HTTP_HOST']."> \n";
                                $subject = "Created Recurring Payment Profile";
                                $message =  "New Recurring Payment was successfully received through eWay <br />";
                                $message .= "from ".$fname." ".$lname."  on ".date('m/d/Y')." at ".date('g:i A').".<br /> Payment total is: $".number_format($amount,2);
                                if($show_services){
                                    $message .= "<br />Payment was made for \"".$recur_services[$service][0]."\"";
                                } else {
                                    $message .= "<br />Payment description: \"".$item_description."\"";
                                }
                                $message .= "<br/>Start Date: ".date("Y-m-d")."<br />";
                                $message .= "Billing Frequency: ".$payment_cycle."<br />";


                                $message .= "<br /><br />Billing Information:<br />";
                                $message .= "Full Name: ".$fname." ".$lname."<br />";
                                $message .= "Email: ".$email."<br />";
                                $message .= "Address: ".$address."<br />";
                                $message .= "City: ".$city."<br />";
                                $message .= "Country: ".$country."<br />";
                                $message .= "State/Province: ".$state."<br />";
                                $message .= "ZIP/Postal Code: ".$zip."<br />";
                                mail($admin_email,$subject,$message,$headers);

                                  /******************************************************************
                                        CUSTOMER EMAIL NOTIFICATION
                                  ******************************************************************/
                                  $subject = "Payment Received!";
                                  $message =  "Dear ".$fname.",<br />";
                                  $message .= "<br /> Thank you for your payment.";
                                  $message .= "<br /><br />";
                                    if($show_services){
                                        $message .= "<br />Payment was made for \"".$recur_services[$service][0]."\"";
                                    } else {
                                        $message .= "<br />Payment description: \"".$item_description."\"";
                                    }
                                  $message .= "<br />Payment amount: $".number_format($amount,2);
                                  $message .= "<br />Payment cycle:".$payment_cycle;
                                  $message .= "<br />Subscription start date: ".date("d/m/Y");
                                  $message .= "<br />Subscription expiry date: ".$subscription_expiry;

                                  $message .= "<br /><br />Billing Information:<br />";
                                  $message .= "Full Name: ".$fname." ".$lname."<br />";
                                  $message .= "Email: ".$email."<br />";
                                  $message .= "Address: ".$address."<br />";
                                  $message .= "City: ".$city."<br />";
                                  $message .= "Country: ".$country."<br />";
                                  $message .= "State/Province: ".$state."<br />";
                                  $message .= "ZIP/Postal Code: ".$zip."<br />";

                                  $message .= "<br /><br />Kind Regards,<br />".$_SERVER['HTTP_HOST'];
                                  mail($email,$subject,$message,$headers);

                                //-----> send notification end
                                $show_form=0;
                            } else {
                                $count=0;
                                $my_status="<div>Transaction Un-successful!<br/>";
                                $my_status .="Merchant Gateway Response: <br />Error Severity:".$lblErrorSeverity . "<br />Error Description: " . $lblErrorDescription ."</div>";
                                $error=1;
                                $mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">'.$my_status.'</div></div><br />';
                            }
                        }
                    } else {
                        $lblResult = "Rebill can't be uploaded. Please try again.";
                        $count=0;
                        $my_status="<div>Transaction Un-successful!<br/>";
                        $my_status .="Merchant Gateway Response: ". $lblResult ."</div>";
                        $error=1;
                        $mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">'.$my_status.'</div></div><br />';
                    }
                break;
                }

            }  else if($continue && $cctype=="PAYPAL"){
                require('includes/paypal.class.php');
                $paypal = new paypal_class;

                $paypal->add_field('business', $paypal_merchant_email);
                $paypal->add_field('return', $paypal_success_url);
                $paypal->add_field('cancel_return', $paypal_cancel_url);
                $paypal->add_field('notify_url', $paypal_ipn_listener_url);

                    if($payment_mode=="ONETIME"){
                        if($show_services){
                            $paypal->add_field('item_name_1', strip_tags(str_replace("'","",$services[$service][0])));
                        } else {
                            $paypal->add_field('item_name_1', strip_tags(str_replace("'","",$item_description)));
                        }
                        $paypal->add_field('amount_1', $amount);
                        $paypal->add_field('item_number_1', $transactID);
                        $paypal->add_field('quantity_1', '1');
                        $paypal->add_field('custom', $paypal_custom_variable);
                        $paypal->add_field('upload', 1);
                        $paypal->add_field('cmd', '_cart');
                        $paypal->add_field('txn_type', 'cart');
                        $paypal->add_field('num_cart_items', 1);
                        $paypal->add_field('payment_gross', $amount);
                        $paypal->add_field('currency_code',$paypal_currency);

                    } else if($payment_mode=="RECUR"){
                        if($show_services){
                            $paypal->add_field('item_name', strip_tags(str_replace("'","",$recur_services[$service][0])));
                        } else {
                            $paypal->add_field('item_name', strip_tags(str_replace("'","",$item_description)));
                        }
                        $paypal->add_field('item_number', $transactID);
                        $paypal->add_field('a3', $amount);
                        $paypal_duration = getDurationPaypal($recur_services[$service][2]); //get duration based on eway recurring_services array
                        $paypal->add_field('p3', $recur_services[$service][3]);
                        $paypal->add_field('t3', (is_array($paypal_duration)?$paypal_duration[0]:$paypal_duration));
                        $paypal->add_field('src', '1');
                        $paypal->add_field('no_note', '1');
                        $paypal->add_field('no_shipping', '1');
                        $paypal->add_field('custom', $paypal_custom_variable);
                        $paypal->add_field('currency_code',$paypal_currency);
                    }
                    $show_form=0;
                    $mess = $paypal->submit_paypal_post(); // submit the fields to paypal


            }
				
		} elseif(!is_numeric($amount) || empty($amount)) { 
			if($show_services){
				$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> Please select service you\'re paying for.</p></div></div><br />';
			} else { 
				$mess = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error!</strong> Please type amount to pay for services!</p></div></div><br />';
			}
			$show_form=1; 
		} 
	# END OF PLEASE DO NOT EDIT IF YOU'RE NOT SURE	
?>