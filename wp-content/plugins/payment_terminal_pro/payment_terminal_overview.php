<?php
#******************************************************************************
#                       Credit Card Payments Wordpress v3.0
#
#	Author: Convergine.com
#	http://www.convergine.com
#	Version: 3.0
#   (c) 2013 Convergine.com
#
#******************************************************************************
//$wp_url = get_bloginfo('siteurl'); 
$wp_url = get_site_url();
global $wpdb;
$sqlfilter = "";
$sqlorder = "";
?>
        <link rel="stylesheet" media="screen" href="<?php echo $wp_url?>/wp-content/plugins/payment_terminal_pro/resources/css/admin-style.css" />
		<div class="wrap-ccwp-bw">
			<div class="wrap-ccwp-header" >
			<?php    echo "<h2>" . __('Credit Card Payments - Overview','') . "</h2>"; ?>
				    
			</div><!-- end header -->
			<div class="ccwp-subheader-pptr" >
				<p><?php _e("This simple plugin enables you to accept credit card payments on your WP website and manage transactions in your website's WP control panel. " ); ?></p>
			</div>
			<div class="wrap-ccwp-content" >
			<div class="wrap-ccwp-breadcrumbs" >
				<?php    echo "<h4>" . __('Last 15 Transactions','') . "</h4>"; ?>
       		</div><!-- end bc --> 
       	          <div class="transactions_overview_table ">
                    <div class="table_wrapper">
                        <table class="table_header">
                            <tr>

                            	<th class="lastTransColumn"><?php _e('Transaction ID')?></th>
                                <th class="bw-content-listtable-date"  ><?php _e('Date')?></th>
                                <th class="bw-content-listtable-name" ><?php _e('Name')?></th>
                                <th class="bw-content-listtable-email" ><?php _e('Email')?></th>
                                <th class="bw-content-listtable-amount" ><?php _e('Amount')?></th>
                                <th class="bw-content-listtable-service_title" ><?php _e('Service')?></th>
                            </tr>

                        <?php 
						//lets get all services from database
						$query="SELECT * FROM ".$wpdb->prefix."ccpt_transactions  WHERE 1 AND ccpt_status='2' $sqlfilter $sqlorder";
						//echo $query;
						$result=mysql_query($query) or die(mysql_error());
						if(mysql_num_rows($result)>0){
							$del=true;
							$rClass = "row_b";
							while($row=mysql_fetch_assoc($result)){
						?>
                        <tr class="<?php echo $rClass=($rClass=="row_b"?"row_a":"row_b")?>">


                                <td class="lastTransColumn"><?php echo stripslashes(strip_tags($row["ccpt_transaction_id"]))?></td>
                                <td class="bw-content-listtable-date" ><?php echo date("d M Y, h:i a", strtotime($row["ccpt_dateCreated"]))?></td>
                                <td class="bw-content-listtable-name" ><?php echo stripslashes(strip_tags($row["ccpt_payer_name"]))?></td>
                                <td class="bw-content-listtable-email" ><?php echo stripslashes(strip_tags($row["ccpt_payer_email"]))?></td>
                                <td class="bw-content-listtable-amount" ><?php echo number_format(stripslashes(strip_tags($row["ccpt_amount"])),2); $ppptTotal+=$row["ccpt_amount"];?></td>
                                <?php
									$query2="SELECT ccpt_services_title FROM ".$wpdb->prefix."ccpt_services WHERE ccpt_services_id='".$row["ccpt_serviceID"]."'";
									$result2=mysql_query($query2);
									$row2=mysql_fetch_assoc($result2);
								?>
                                <td class="bw-content-listtable-service_title" ><?php if($row["ccpt_serviceID"]!=0){ echo stripslashes(strip_tags($row2["ccpt_services_title"])); } else { echo "N/A"; }?></td>

                        </tr>
                        
                        <?php }  } else { $del=false; ?>
                            <tr class="row_msg">

                                <td style="padding-left: 35px;" colspan="6">0 transactions found</td>

                            </tr>
                        <?php } ?>
                    </table>
                 </div>
                 </div>
            <div class="wrap-ccwp-content-noborder">
                <input class="button button-primary" type="button" onclick="window.location='admin.php?page=ccpt_admin_transactions'" name="Submit" value="<?php _e('View All Transactions', '' ) ?>" />
            </div>

		</div>
        <div class="pt_footer">
        	<a href="http://www.convergine.com" target="_blank"><img src="<?php echo $wp_url?>/wp-content/plugins/payment_terminal_pro/resources/images/convergine.png" /></a>
        </div>