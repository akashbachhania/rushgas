<?php
/*
Plugin Name: Register Helper Example
Plugin URI: http://www.paidmembershipspro.com/wp/pmpro-customizations/
Description: Register Helper Initialization Example
Version: .1
Author: sohel
Author URI: http://www.multiwebsupport.com
*/
//we have to put everything in a function called on init, so we are sure Register Helper is loaded



function my_pmprorh_init()
{
    //don't break if Register Helper is not loaded
    if(!function_exists("pmprorh_add_registration_field"))
    {
        return false;
    }

    //define the fields
    $fields = array();



    $fields[] = new PMProRH_Field(
        "f-name",              // input name, will also be used as meta key
        "text",                 // type of field
        array(
            "label"=>"First Name",
            "size"=>40,         // input size
            "class"=>"first-name", // custom class
            "profile"=>true,    // show in user profile
            "required"=>true    // make this field required
        ));

    $fields[] = new PMProRH_Field(
        "l-name",              // input name, will also be used as meta key
        "text",                 // type of field
        array(
            "label"=>"Last Name",
            "size"=>40,         // input size
            "class"=>"last-name", // custom class
            "profile"=>true,    // show in user profile
            "required"=>true    // make this field required
        ));


    $fields[] = new PMProRH_Field(
        "phone-number",              // input name, will also be used as meta key
        "text",                 // type of field
        array(
            "label"=>"Cell Phone Number",
            "size"=>40,         // input size
            "class"=>"phone-number", // custom class
            "profile"=>true,    // show in user profile
            "required"=>true    // make this field required
        ));

    $fields[] = new PMProRH_Field(
        "street-address",              // input name, will also be used as meta key
        "text",                 // type of field
        array(
            "label"=>"Street Address",
            "size"=>40,         // input size
            "class"=>"street-address", // custom class
            "profile"=>true,    // show in user profile
            "required"=>true    // make this field required
        ));

    $fields[] = new PMProRH_Field(
        "city",              // input name, will also be used as meta key
        "text",                 // type of field
        array(
            "label"=>"City",
            "size"=>40,         // input size
            "class"=>"city", // custom class
            "profile"=>true,    // show in user profile
            "required"=>true    // make this field required
        ));


    $fields[] = new PMProRH_Field(
        "state",                   // input name, will also be used as meta key
        "select",                   // type of field
        array(
            "options"=>array(       // <option> elements for select field
                    "" => "",       // blank option - cannot be selected if this field is required
                "alabama"=>"Alabama",     // <option value="Alabama">Alabama</option>
                "alaska"=>"Alaska",  // <option value="Alaska">Alaska</option>
                "arizona"=>"Arizona",     // <option value="Arizona">Arizona</option>
                "arkansas"=>"Arkansas",  // <option value="Arkansas">Arkansas</option>
                "california"=>"California",     // <option value="California">California</option>
                "colorado"=>"Colorado",  // <option value="Colorado">Colorado</option>
                "connecticut"=>"Connecticut",     // <option value="Connecticut">Connecticut</option>
                "delaware"=>"Delaware",  // <option value="Delaware">Delaware</option>
                "florida"=>"Florida",     // <option value="Florida">Florida</option>
                "georgia"=>"Georgia",  // <option value="Georgia">Georgia</option>
                "hawaii"=>"Hawaii",     // <option value="Hawaii">Hawaii</option>
                "idaho"=>"Idaho",  // <option value="Idaho">Idaho</option>
                "illinois"=>"Illinois",  // <option value="Illinois">Illinois</option>
                "indiana"=>"Indiana",  // <option value="Indiana">Indiana</option>
                "iowa"=>"Iowa",  // <option value="Iowa">Iowa</option>
                "kansas"=>"Kansas",  // <option value="Kansas">Kansas</option>
                "kentucky"=>"Kentucky",  // <option value="Kentucky[D]">Kentucky[D]</option>
                "louisiana"=>"Louisiana",  // <option value="Louisiana">Louisiana</option>
                "maine"=>"Maine",  // <option value="Maine">Maine</option>
                "maryland"=>"Maryland",  // <option value="Maryland">Maryland</option>
                "massachusetts"=>"Massachusetts",  // <option value="Massachusetts[E]">Massachusetts[E]</option>
                "michigan"=>"Michigan",  // <option value="Michigan">Michigan</option>
                "minnesota"=>"Minnesota",  // <option value="Minnesota">Minnesota</option>
                "mississippi"=>"Mississippi",  // <option value="Mississippi">Mississippi</option>
                "missouri"=>"Missouri",  // <option value="Missouri">Missouri</option>
                "montana"=>"Montana",  // <option value="Montana">Montana</option>
                "nebraska"=>"Nebraska",  // <option value="Nebraska">Nebraska</option>
                "new-hampshire"=>"New Hampshire",  // <option value="New Hampshire">New Hampshire</option>
                "new-jersey"=>"New Jersey",  // <option value="New Jersey">New Jersey</option>
                "new-mexico"=>"New Mexico",  // <option value="New Mexico">New Mexico</option>
                "new-york"=>"New York",  // <option value="New York">New York</option>
                "north-carolina"=>"North Carolina",  // <option value="North Carolina">North Carolina</option>
                "north-dakota"=>"North Dakota",  // <option value="North Dakota">North Dakota</option>
                "ohio"=>"Ohio",  // <option value="Ohio">Ohio</option>
                "oklahoma"=>"Oklahoma",  // <option value="Oklahoma">Oklahoma</option>
                "oregon"=>"Oregon",  // <option value="Oregon">Oregon</option>
                "pennsylvania"=>"Pennsylvania[F]",  // <option value="Pennsylvania[F]">Pennsylvania[F]</option>
                "rhode-island"=>"Rhode Island[G]",  // <option value="Rhode Island[G]">Rhode Island[G]</option>
                "south-carolina"=>"South Carolina",  // <option value="South Carolina">South Carolina</option>
                "south-dakota"=>"South Dakota",  // <option value="South Dakota">South Dakota</option>
                "tennessee"=>"Tennessee",  // <option value="Tennessee">Tennessee</option>
                "texas"=>"Texas",  // <option value="Texas">Texas</option>
                "utah"=>"Utah",  // <option value="Utah">Utah</option>
                "vermont"=>"Vermont",  // <option value="Vermont">Vermont</option>
                "virginia"=>"Virginia[H]",  // <option value="Virginia[H]">Virginia[H]</option>
                "washington"=>"Washington",  // <option value="Washington">Washington</option>
                "west-virginia"=>"West Virginia",  // <option value="West Virginia">West Virginia</option>
                "wisconsin"=>"Wisconsin",  // <option value="Wisconsin">Wisconsin</option>
                "wyoming"=>"Wyoming"  // <option value="Wyoming">Wyoming</option>

        )));


    $fields[] = new PMProRH_Field(
        "zipcode",              // input name, will also be used as meta key
        "text",                 // type of field
        array(
            "label"=>"Zipcode",
            "size"=>40,         // input size
            "class"=>"zipcode", // custom class
            "profile"=>true,    // show in user profile
            "required"=>true    // make this field required
        ));

    $fields[] = new PMProRH_Field(
        "vehicle-make",              // input name, will also be used as meta key
        "text",                 // type of field
        array(
            "label"=>"Vehicle Make",
            "size"=>40,         // input size
            "class"=>"vehicle-make", // custom class
            "profile"=>true,    // show in user profile
            "required"=>true    // make this field required
        ));

    $fields[] = new PMProRH_Field(
        "vehicle-model",              // input name, will also be used as meta key
        "text",                 // type of field
        array(
            "label"=>"Vehicle Model",
            "size"=>40,         // input size
            "class"=>"vehicle-model", // custom class
            "profile"=>true,    // show in user profile
            "required"=>true    // make this field required
        ));

    $fields[] = new PMProRH_Field(
        "vehicle-color",              // input name, will also be used as meta key
        "text",                 // type of field
        array(
            "label"=>"Vehicle Color",
            "size"=>40,         // input size
            "class"=>"vehicle-color", // custom class
            "profile"=>true,    // show in user profile
            "required"=>true    // make this field required
        ));

    $fields[] = new PMProRH_Field(
        "license-plate-number",              // input name, will also be used as meta key
        "text",                 // type of field
        array(
            "label"=>"License Plate Number",
            "size"=>40,         // input size
            "class"=>"license-plate-number", // custom class
            "profile"=>true,    // show in user profile
            "required"=>true    // make this field required
        ));


    //add the fields into a new checkout_boxes are of the checkout page
    foreach($fields as $field)
        pmprorh_add_registration_field(
            "checkout_boxes", // location on checkout page
            $field            // PMProRH_Field object
        );

    //that's it. see the PMPro Register Helper readme for more information and examples.
}
add_action("init", "my_pmprorh_init");

