<?php

#******************************************************************************

#                       Credit Card Payments Wordpress v3.0

#

#	Author: Convergine.com

#	http://www.convergine.com

#	Version: 3.0

#   (c) 2013 Convergine.com

#

#******************************************************************************

global $wpdb;

$sqlfilter = "";

$sqlorder = " ORDER BY ccpt_dateCreated DESC ";

$ppptTotal = 0;



if(!empty($_POST['toDelete']) && count($_POST["toDelete"])>0) {

    $deleted=0;

    for($i=0; $i<count($_POST["toDelete"]); $i++){

        $query="DELETE FROM ".$wpdb->prefix."ccpt_transactions WHERE ccpt_id='".$_POST["toDelete"][$i]."'";

        mysql_query($query) or die(mysql_error());

        $deleted++;

    }



    if($deleted>0){

        ?> <div class="deleted"><p><strong><?php _e('Selected transaction(s) deleted!' ); ?></strong></p></div><?php

    }

}





if(isset($_POST['ccpt_filter_submit']) && $_POST['ccpt_filter_submit'] == 'yes') {

    if(!empty($_POST["ccpt_date1"]) || !empty($_POST["ccpt_date2"])){

        $tmp1 = explode("/",$_POST["ccpt_date1"]);

        $tmp2 = explode("/",$_POST["ccpt_date2"]);

        $ppptd1 = (!empty($_POST["ccpt_date1"])?$tmp1[2]."-".$tmp1[0]."-".$tmp1[1]:date("Y-m-d"))." 00:00:00";

        $ppptd2 = (!empty($_POST["ccpt_date2"])?$tmp2[2]."-".$tmp2[0]."-".$tmp2[1]:date("Y-m-d"))." 23:59:59";

        $sqlfilter  .= " AND (ccpt_dateCreated BETWEEN '".$ppptd1."' AND '".$ppptd2."') ";

    }



    if(!empty($_POST["ccpt_sortby"]) && !empty($_POST["ccpt_dir"])){

        $sqlorder  = " ORDER BY ".$_POST["ccpt_sortby"]." ".$_POST["ccpt_dir"];

    }



    if(!empty($_POST["ccpt_keyword"])){

        $sqlfilter  .= " AND ( ccpt_payer_name LIKE '%".$_POST["ccpt_keyword"]."%' OR ccpt_payer_email LIKE '%".$_POST["ccpt_keyword"]."%' OR ccpt_transaction_id LIKE '%".$_POST["ccpt_keyword"]."%' )";

    }

}





?>

<?php //$wp_url = get_bloginfo('siteurl');

$wp_url = get_site_url(); ?>

<link rel="stylesheet" media="screen" href="<?php echo $wp_url?>/wp-content/plugins/payment_terminal_pro/resources/css/admin-style.css" />

<script type="text/javascript" src="<?php echo $wp_url?>/wp-content/plugins/payment_terminal_pro/resources/js/jquery-1.9.1.min.js"></script>

<script type="text/javascript" src="<?php echo $wp_url?>/wp-content/plugins/payment_terminal_pro/resources/js/functions.js"></script>

<link rel="stylesheet" media="screen" href="<?php echo $wp_url?>/wp-content/plugins/payment_terminal_pro/resources/css/ui-lightness/jquery-ui-1.9.2.custom.css" />

<script type="text/javascript" src="<?php echo $wp_url?>/wp-content/plugins/payment_terminal_pro/resources/js/jquery-ui-1.9.2.custom.js"></script>

<script type="text/javascript" src="<?php echo $wp_url?>/wp-content/plugins/payment_terminal_pro/resources/js/jquery.dd.js"></script>

<script>

    jQuery(function() {

        jQuery( "#ccpt_date1" ).datepicker();

        jQuery( "#ccpt_date2" ).datepicker();

        jQuery('.select').msDropdown();

    });



</script>





<div class="wrap-ccwp-bw">

<div class="wrap-ccwp-header" >

    <?php    echo "<h2>" . __('Credit Card Payments - Transactions','') . "</h2>"; ?>

</div><!-- end header -->





<div class="ccwp-subheader-pptr" >

    <p><?php _e("Please use filters and sorting functions of this page to view all or specific transactions. " ); ?></p>

</div>

<div class="wrap-ccwp-content" style="padding-bottom: 20px;" >

    <form name="ccpt_form" id="ccpt_form" method="post" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>">

        <input type="hidden" name="ccpt_filter_submit" value="yes">

        <div class="fieldset">

            <div class="wrap-ccwp-breadcrumbs" >

                <h4><?php _e('Search settings'); ?></h4>

            </div>

            <div class="ccwp-row-settings-search" >

                <div class=" title settings-span-text" >

                    <label for="ccpt_date1"><?php    echo   __('Filter by Date:',''); ?></label>

                </div>

                <div class="left paddedL5" >

                    From:&nbsp;&nbsp;&nbsp;<input type="text" class="short ccpt_text" id="ccpt_date1" name="ccpt_date1" value="<?php echo isset($_POST["ccpt_date1"])?$_POST["ccpt_date1"]:""?>">

                </div>

                <div class="right" >

                    To: <input type="text" class="short ccpt_text" id="ccpt_date2" name="ccpt_date2" value="<?php echo isset($_POST["ccpt_date2"])?$_POST["ccpt_date2"]:""?>">

                </div>

                <div style="clear:both"></div>

            </div>

            <div class="ccwp-row-settings-search" >

                <div class=" title settings-span-text" >

                    <label for="ccpt_sortby" ><?php    echo __('Sorting:',''); ?></label>

                </div>

                <div class="left" >

                    <span class="left settings-span-text" >Sort By: </span>

							 <span class="left paddedL5" >

							 <select style="width: 150px;" name="ccpt_sortby" id="ccpt_sortby"  class="short select">

                                 <option value="">Please Select</option>

                                 <option value="ccpt_amount" <?php echo (isset($_POST["ccpt_sortby"]) && $_POST["ccpt_sortby"]=="ccpt_amount")?"selected":""?>>Amount</option>

                                 <option value="ccpt_payer_email" <?php echo (isset($_POST["ccpt_sortby"]) && $_POST["ccpt_sortby"]=="ccpt_payer_email")?"selected":""?>>Email</option>

                                 <option value="ccpt_payer_name" <?php echo (isset($_POST["ccpt_sortby"]) && $_POST["ccpt_sortby"]=="ccpt_payer_name")?"selected":""?>>Name</option>

                                 <option value="ccpt_serviceID" <?php echo (isset($_POST["ccpt_sortby"]) && $_POST["ccpt_sortby"]=="ccpt_serviceID")?"selected":""?>>Service</option>

                                 <option value="ccpt_dateCreated" <?php echo (isset($_POST["ccpt_sortby"]) && $_POST["ccpt_sortby"]=="ccpt_dateCreated")?"selected":""?>>Transaction Date</option>

                                 <option value="ccpt_transaction_id" <?php echo (isset($_POST["ccpt_sortby"]) && $_POST["ccpt_sortby"]=="ccpt_transaction_id")?"selected":""?>>Transaction ID</option>

                             </select>

			                 </span>

                </div>

                <div class="right" >

                    <span class="left settings-span-text" >In:&nbsp;&nbsp;</span>

							 <span class="left settings-span-text" >

							  <select style="width: 150px;" name="ccpt_dir" id="ccpt_dir"  class="short select">

                                  <option value="ASC" <?php echo isset($_POST["ccpt_dir"]) && $_POST["ccpt_dir"]=="ASC"?"selected":""?>>Ascending</option>

                                  <option value="DESC"  <?php echo isset($_POST["ccpt_dir"]) && $_POST["ccpt_dir"]=="DESC"?"selected":""?>>Descending</option>

                              </select>

							  </span>

                </div>

                <div style="clear:both"></div>

            </div>

            <div class="ccwp-row-settings-search" >

                <div class="left willsubmit" >

                    <input class="button" type="submit" name="Submit" value="<?php _e('Apply Settings', '' ) ?>" />

                </div>

                <div style="clear:both"></div>

            </div>

        </div>

        <div class="ccwp-row-settings-search" >

            <div class="left title settings-span-text"  style="width: 140px;">

                <label for="whylableifnoused" >Search transaction</label>

            </div>

            <div class="left" >

                		<span class="left settings-span-text" >

                			<?php   echo __('By Keyword: &nbsp;','') ; ?>

                		</span>

                		<span class="right" >

                			<input id="whylableifnoused" type="text" class="ccpt_text" id="ccpt_keyword" name="ccpt_keyword" value="<?php echo isset($_POST["ccpt_keyword"])?$_POST["ccpt_keyword"]:""?>">

                		</span>

            </div>

            <div style="clear:both"></div>

        </div>

        <div class="ccwp-row-settings-search"  >

            <div class="left willsubmit" >

                <input class="button-secondary" style="position: relative;bottom: 12px;" type="submit" name="Submit" value="<?php _e('Search', '' ) ?>" />

            </div>

            <div style="clear:both"></div>

        </div>

    </form>

</div>

<div style="clear:both"></div>







<div class="wrap-ccwp-content" >

    <div class="wrap-ccwp-breadcrumbs" >

        <?php    echo "<h4>" . __('All Transactions','') . "</h4>"; ?>

    </div><!-- end bc -->



    <form name="ccpt_form_del" method="post" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>">

        <div class="transactions_table">

            <div class="table_wrapper">

                <table class="table_header">

                    <tr>

                        <th class="deleter">&nbsp;</th>

                        <th class="lastTransColumn"><?php _e('Transaction ID')?></th>

                        <th class="bw-content-listtable-date"  ><?php _e('Date')?></th>

                        <th class="bw-content-listtable-name" ><?php _e('Name')?></th>

                        <th class="bw-content-listtable-email" ><?php _e('Email')?></th>

                        <th class="bw-content-listtable-amount" ><?php _e('Amount')?></th>

                        <th class="bw-content-listtable-service_title" ><?php _e('Service')?></th>



                    </tr>



                    <?php

                    //lets get all services from database

                    $query="SELECT * FROM ".$wpdb->prefix."ccpt_transactions  WHERE 1 AND ccpt_status='2' $sqlfilter $sqlorder";

                    //echo $query;

                    $result=mysql_query($query) or die(mysql_error());

                    if(mysql_num_rows($result)>0){

                        $del=true;

                        $rClass = "row_b";

                        while($row=mysql_fetch_assoc($result)){

                            ?>

                            <tr class="<?php echo $rClass=($rClass=="row_b"?"row_a":"row_b")?>">



                                <td class="deleter">&nbsp;&nbsp;<input type="checkbox" value="<?php echo $row["ccpt_id"]?>" name="toDelete[]" /></td>

                                <td class="lastTransColumn"><?php echo stripslashes(strip_tags($row["ccpt_transaction_id"]))?></td>

                                <td class="bw-content-listtable-date" ><?php echo date("d M Y, h:i a", strtotime($row["ccpt_dateCreated"]))?></td>

                                <td class="bw-content-listtable-name" ><?php echo stripslashes(strip_tags($row["ccpt_payer_name"]))?></td>

                                <td class="bw-content-listtable-email" ><?php echo stripslashes(strip_tags($row["ccpt_payer_email"]))?></td>

                                <td class="bw-content-listtable-amount" ><?php echo number_format(stripslashes(strip_tags($row["ccpt_amount"])),2); $ppptTotal+=$row["ccpt_amount"];?></td>

                                <?php

                                $query2="SELECT ccpt_services_title FROM ".$wpdb->prefix."ccpt_services WHERE ccpt_services_id='".$row["ccpt_serviceID"]."'";

                                $result2=mysql_query($query2);

                                $row2=mysql_fetch_assoc($result2);

                                ?>

                                <td class="bw-content-listtable-service_title" ><?php if($row["ccpt_serviceID"]!=0){ echo stripslashes(strip_tags($row2["ccpt_services_title"])); } else { echo "N/A"; }?></td>





                            </tr>



                        <?php } ?>

                        <?php if($ppptTotal>0){?>

                            <tr>

                                <td colspan="5">&nbsp;</td>

                                <td colspan="2" style="text-align: left">

                                    <strong>Total Amount:</strong> <?php echo number_format($ppptTotal, 2) ?>

                                </td>



                            </tr>

                        <?php } ?>

                    <?php } else { $del=false; ?>

                        <tr class="row_msg">



                            <td style="padding-left: 35px;" colspan="7">0 transactions found</td>



                        </tr>

                    <?php } ?>

                </table>



            </div>

            <div class="wrap-ccwp-breadcrumbs bc-bottom" >



                <?php if($del){?>

                    <span class="left-btn" ><input class="button action" style="font-size: 13px;margin: 0;" type="submit" name="Submit" value="<?php _e('Delete Selected Transactions', '' ) ?>" /> </span>

                <?php } ?>



            </div>

    </form>

</div>







</div>

<div class="pt_footer">

    <a href="http://www.convergine.com" target="_blank"><img src="<?php echo $wp_url?>/wp-content/plugins/payment_terminal_pro/resources/images/convergine.png" /></a>

</div>

     	