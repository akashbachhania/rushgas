<?php
	/* define global varibles*/
	global $current_user,$wpdb;
	$to='rushgas954@gmail.com';
	$to2='rushgassupport@gmail.com';
	$case_subjects= $wpdb->get_results("SELECT * FROM wp_user_help_section_subject");
	if(isset($_GET['helps']) && $_GET['helps']=='addnew'){
		if(isset($_POST['add_help_reply'])){

			$name				= $_POST['name'];
	
			$subject 			= $_POST['subject'];
			$message			= $_POST['message'];
			
			$user_id	= $current_user->ID;

			$add_help=		array( 
									'name' 				=> $name, 
									'subject_help_id'	=> $subject , 
									
									'comment' 			=> $message , 
									'user_id' 		=> $user_id 
								);

			$help_id = $wpdb->insert( 
									'wp_user_help_section', 
									$add_help
									
									);
		 	if($help_id){
		 		$wpdb->insert( 'wp_user_history', array( 'notification' => 7, 'user_id' => $user_id ) );
		 		$get_subject=$wpdb->get_row("SELECT * from wp_user_help_section_subject where subject_id='$subject'");
		 		$subject = $name.' '.$get_subject->subject.' '.$current_user->user_login;
		 		wp_mail($to,$subject,$message);
		 		wp_mail($to2,$subject,$message);
          		wp_redirect(admin_url('admin.php?page=rg_get_help_setup'));
      		}
      		

		}
 		include_once('rg-help-page-add-new-part.php');
	}
	else if(isset($_GET['helps']) && $_GET['helps']=='reply'){
		$check_admin=get_userdata($current_user->ID);
		if($check_admin->roles[0]=='administrator'){
			$id = $_GET['id'];
			if(isset($_POST['add_help'])){

				$message	= $_POST['message'];
			
				$user_id	= $current_user->ID;

			

	 			$add_help_reply=	array( 
													 
										'comment_id' 		=> $id ,
										'message' 			=> $message , 
										'user_id' 			=> $user_id 
									);

				$help_reply_id = $wpdb->insert( 
										'wp_user_help_section_reply', 
										$add_help_reply
										
										);
			 	if($help_reply_id){
			 		$get_case=$wpdb->get_row("SELECT * from wp_user_help_section where help_id='$id'");
			 		$get_subject=$wpdb->get_row("SELECT * from wp_user_help_section_subject where subject_id='$get_case->subject_help_id'");
		 			$subject = $get_case->name.' '.$get_subject->subject.' '.$current_user->user_login;
		 			wp_mail($to,$subject,$message);
		 			wp_mail($to2,$subject,$message);
			 		//$wpdb->insert( 'wp_user_history', array( 'notification' => 7, 'user_id' => $user_id ) );
	          		wp_redirect(admin_url('admin.php?page=rg_get_help_setup'));
	          	}
      		}
			include_once('rg-help-page-add-new-reply-part.php');
		}

	}

	else if(isset($_GET['helps']) && $_GET['helps']=='view_cases'){
		$check_admin=get_userdata($current_user->ID);
		$id = $_GET['id'];
		if(isset($_POST['add_help_reply'])){

				$message	= $_POST['message'];
			
				$user_id	= $current_user->ID;

			

	 			$add_help_reply=	array( 
													 
										'comment_id' 		=> $id ,
										'message' 			=> $message , 
										'user_id' 			=> $user_id 
									);

				$help_reply_id = $wpdb->insert( 
										'wp_user_help_section_reply', 
										$add_help_reply
										
										);
				if($_POST['closecase']!=''){
					$wpdb->update( 
										'wp_user_help_section', 
										array('status'=>1),
										array('help_id'=>$id)
										);

				}
			 	if($help_reply_id){
			 		//$wpdb->insert( 'wp_user_history', array( 'notification' => 7, 'user_id' => $user_id ) );
			 		$get_case=$wpdb->get_row("SELECT * from wp_user_help_section where help_id='$id'");
			 		$get_subject=$wpdb->get_row("SELECT * from wp_user_help_section_subject where subject_id='$get_case->subject_help_id'");
		 			$subject = $get_case->name.' '.$get_subject->subject.' '.$current_user->user_login;
		 			wp_mail($to,$subject,$message);
		 			wp_mail($to2,$subject,$message);
	          		wp_redirect(admin_url('admin.php?page=rg_get_help_setup'));
	          	}
      		}
			
		include_once('rg-help-page-add-new-case-part.php');
		

	}
	else{

		include_once 'rg-help-page-all-part.php';
    }
	
