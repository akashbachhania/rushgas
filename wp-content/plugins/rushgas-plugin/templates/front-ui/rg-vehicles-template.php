<style>
/** {
    box-sizing: border-box;
}
.row::after {
    content: "";
    clear: both;
    display: block;
}
[class*="col-"] {
    float: left;
    padding-right: 2px;
}
.col-default {
	float: left;
    padding-right: 2px;
    width: 25%;
}
.col-1 {width: 8.33%;}
.col-2 {width: 16.66%;}
.col-3 {width: 25%;}
.col-4 {width: 33.33%;}
.col-5 {width: 41.66%;}
.col-6 {width: 50%;}
.col-7 {width: 58.33%;}
.col-8 {width: 66.66%;}
.col-9 {width: 75%;}
.col-10 {width: 83.33%;}
.col-11 {width: 91.66%;}
.col-12 {width: 100%;}
html {
    font-family: "Lucida Sans", sans-serif;
}

.vehicles ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
}
.vehicles li {
    padding: 8px;
    margin-bottom: 1px;
    background-color :#33b5e5;
    color: #ffffff;
    box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
}
.vehicles li:hover {
    background-color: #0099cc;
}*/

#theme-page .theme-page-wrapper.left-layout .theme-content {
    float: left;
}
th {
    background-color: #ED1C24;
    color: white;
}
/*.menu-left-menu-container ul.menu {
    
    max-width: 250px !important;
}*/
.submittz_custom {
    background: transparent none repeat scroll 0 0;
    border: 1px solid #fff;
    color: #fff;
    float: left;
    left: 10px;
    margin-top: 24px;
    background-color: red;
}
.rg_table {
    width: 100%;
}
#theme-page .theme-page-wrapper .theme-content {
    padding: 0 !important;
}
</style>
<!-- <div class="row">

<div class="col-3 vehicles">
<ul >
<li>The Flight</li>
<li>The City</li>
<li>The Island</li>
<li>The Food</li>
</ul>
</div>
<div class="col-3 vehicles">
<ul>
<li>The Flight</li>
<li>The City</li>
<li>The Island</li>
<li>The Food</li>
</ul>
</div>

</div> -->
<?php

//print_r($_POST);
// Create post object
if(isset($_POST['action']) && 'Submit' == $_POST['action']) {

    /*echo "<pre>";
    print_r($_FILES);
    echo "</pre>";*/
    $filename = $_FILES['upload_image']['name'];
    //exit();
    
    $rushgas_post = array(
      'post_title'      => wp_strip_all_tags( $_POST['vehicle_make'] ),
      'post_type'       =>'wp-rushgas',
      'post_content'    => $_POST['special_features'],
      'post_status'     => 'publish',
      'post_author'     => get_current_user_id()
    );
     
    // Insert the post into the database
    $post_id = wp_insert_post( $rushgas_post );
    if(!empty($post_id)) {
        update_post_meta($post_id, 'rg_model_color', $_POST['model_color']);
        update_post_meta($post_id, 'rg_year', $_POST['year']);
        update_post_meta($post_id, 'rg_tag_number', $_POST['tag_number']);


        if (!function_exists('wp_generate_attachment_metadata')){
                require_once(ABSPATH . "wp-admin" . '/includes/image.php');
                require_once(ABSPATH . "wp-admin" . '/includes/file.php');
                require_once(ABSPATH . "wp-admin" . '/includes/media.php');
            }
             if ($_FILES) {
                foreach ($_FILES as $file => $array) {
                    if ($_FILES[$file]['error'] !== UPLOAD_ERR_OK) {
                        return "upload error : " . $_FILES[$file]['error'];
                    }
                    $attach_id = media_handle_upload( $file );
                }   
            }
            if ($attach_id > 0){
                //and if you want to set that image as Post  then use:
                update_post_meta($post_id,'rg_thumbnail_id',$attach_id);
            }


        /*$wp_upload_dir = wp_upload_dir();

        // Prepare an array of post data for the attachment.
        $attachment = array(
            'guid'           => $wp_upload_dir['url'] . '/' .  $filename , 
            'post_mime_type' => $_FILES['upload_image']['type'],
            'post_title'     => preg_replace( '/\.[^.]+$/', '',  $filename  ),
            'post_content'   => '',
            'post_status'    => 'inherit'
        );

        // Insert the attachment.
        $attach_id = wp_insert_attachment( $attachment, $filename, $post_id );

        // Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
        require_once( ABSPATH . 'wp-admin/includes/image.php' );

        // Generate the metadata for the attachment, and update the database record.
        $attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
        wp_update_attachment_metadata( $attach_id, $attach_data );

        set_post_thumbnail( $post_id, $attach_id );*/



        update_post_meta($post_id, 'rg_upload_image', $filename);
    }
}
?>
<form name="form" method="post" enctype="multipart/form-data">
<table class="rg_table">
<tr >
   
    <th>Vehicle make</th>
    <th>Model Color</th>
    <th>Year</th>
    <th>Tag Number</th>
    <th>Upload Image</th>
    <th>Action</th> 
</tr>
<?php
$args = array(
    'post_status' => 'publish',
    'post_type'   => 'wp-rushgas',
    'posts_per_page' => 100
);
$query = new WP_Query( $args );
if ( $query->have_posts() ) {

    /*echo "<pre>";
    print_r($query);
    echo "</pre>";*/
    // The Loop
    while ( $query->have_posts() ) { $query->the_post();
        $image_id = get_post_meta( $query->post->ID, '_thumbnail_id', true );
        $image_src = wp_get_attachment_url( $image_id );
?>
        <tr>
            
            <td><?php echo $query->post->post_title;?></td>
            <td><?php echo get_post_meta( get_the_ID(), 'rg_model_color', true );?></td>
            <td><?php echo get_post_meta( get_the_ID(), 'rg_year', true );?></td>
            <td><?php echo get_post_meta( get_the_ID(), 'rg_tag_number', true );?></td>
            <td><img id="book_image" src="<?php echo $image_src ?>" style="max-width:25%;" /></td>
            <td>
                <a href="javascript://" id="rg_edit_post" onclick="rg_edit_post('<?php echo $query->post->ID?>')"> Edite </a> | 
                <a href="javascript://" id="rg_delete_post" onclick="rg_delete_post('<?php echo $query->post->ID?>')"> Delete </a> 
            </td>   
        </tr>
        <tr id="rg_tr_edit<?php echo $query->post->ID?>" style="display: none;">
            <td colspan="6">

                <table width="100%">
                    <tr>
                        <td style="text-align:left"><strong>Vehicle Make</strong></td>
                        <td style="text-align:left"><input type="text" name="edit_vehicle_make" id="edit_vehicle_make<?php echo $query->post->ID?>" value="<?php echo $query->post->post_title;?>"></td>    
                    </tr>

                    <tr>
                        <td style="text-align:left"><strong>Model Color</strong></td>
                        <td style="text-align:left"><input type="text" name="edit_model_color" id="edit_model_color<?php echo $query->post->ID?>" value="<?php echo get_post_meta( get_the_ID(), 'rg_model_color', true );?>"></td>    
                    </tr>

                    <tr>
                        <td style="text-align:left"><strong>Year</strong></td>
                        <td style="text-align:left"><input type="text" name="edit_year" id="edit_year<?php echo $query->post->ID?>" value="<?php echo get_post_meta( get_the_ID(), 'rg_year', true );?>"></td>    
                    </tr>

                    <tr>
                        <td style="text-align:left"><strong>Tag Number</strong></td>
                        <td style="text-align:left"><input type="text" name="edit_tag_number" id="edit_tag_number<?php echo $query->post->ID?>" value="<?php echo get_post_meta( get_the_ID(), 'rg_tag_number', true );?>"></td>    
                    </tr>

                    <tr>
                        <td style="text-align:left"><strong>Upload Image</strong></td>
                        <td style="text-align:left"><input type="file" name="file" id="upload_image"></td>    
                    </tr>

                    <tr>
                        <td style="text-align:left"><strong>Special Features</strong></td>
                        <td style="text-align:left"><textarea name="edit_special_features" id="edit_special_features<?php echo $query->post->ID?>"><?php echo $query->post->post_content;?></textarea></td>    
                    </tr>

                    <tr>
                        <td style="text-align:left" colspan="2">
                            <input type="button" onclick="rg_edit_post_informations('<?php echo $query->post->ID?>')" class="wpcf7-submit submittz_custom" name="rg_editButton" value="Edit Information">
                        </td>
                    </tr>
                        
                </table>
                
            </td>
        </tr>

<?php 
    }
    wp_reset_postdata();
} else {
?>
    <tr><td colspan="6" style="text-align:center;"> Result not found </td></tr>
<?php
}
?>
    <tr>
       <td colspan="6">
        <input type="button" class="wpcf7-submit submittz_custom" name="open_table" id="open_table" onclick="rg_open_table()" value="Add Information">
       </td> 
   </tr>
</table>

<table id="new_insert_table" style="display: none;" class="rg_table">
<tr>
    <td style="text-align:left"><strong>Vehicle Make</strong></td>
    <td style="text-align:left"><input type="text" name="vehicle_make" id="vehicle_make"></td>    
</tr>

<tr>
    <td style="text-align:left"><strong>Model Color</strong></td>
    <td style="text-align:left"><input type="text" name="model_color" id="model_color"></td>    
</tr>

<tr>
    <td style="text-align:left"><strong>Year</strong></td>
    <td style="text-align:left"><input type="text" name="year" id="year"></td>    
</tr>

<tr>
    <td style="text-align:left"><strong>Tag Number</strong></td>
    <td style="text-align:left"><input type="text" name="tag_number" id="tag_number"></td>    
</tr>

<tr>
    <td style="text-align:left"><strong>Upload Image</strong></td>
    <td style="text-align:left"><input type="file" name="file" id="upload_image"></td>    
</tr>

<tr>
    <td style="text-align:left"><strong>Special Features</strong></td>
    <td style="text-align:left"><input type="text" name="special_features" id="special_features"></td>    
</tr>

<tr>
    <td style="text-align:left" colspan="2">
        <input type="submit" name="action" value="Submit" class="wpcf7-form-control wpcf7-submit submittz">
    </td>
</tr>
    
</table>

</form>
<script type="text/javascript">
jQuery( document ).ready(function() {
    rg_edit_post = function (val) {
        jQuery('#rg_tr_edit' + val).show();
    // blah
    };
    rg_delete_post = function (val) {
    // blah
    };
    rg_open_table = function () {
        jQuery('#new_insert_table').show();
        jQuery('#open_table').hide();
    };
    rg_edit_post_informations = function(val) {


      //console.dir(data);
      edit_vehicle_make = jQuery.trim(jQuery('#edit_vehicle_make' +val).val());
      edit_model_color = jQuery.trim(jQuery('#edit_model_color' +val).val());  
      edit_tag_number = jQuery.trim(jQuery('#edit_tag_number' +val).val()); 
      edit_special_features = jQuery.trim(jQuery('#edit_special_features' +val).val());   

      jQuery.ajax({             
            url: '<?php bloginfo('url'); ?>/wp-admin/admin-ajax.php', 
            type: 'POST',
            dataType: 'json',
            data: {action: 'rg_edit_post_callback', post_id: val, edit_vehicle_make: edit_vehicle_make, edit_model_color: edit_model_color, edit_tag_number: edit_tag_number},         
              beforeSend: function(xhr){      
                //jQuery('#message').html('Loading...');
                
              },
              success:function(data) {    
              //alert(data.url);
                /*if( data.status == 'success' ){
                  jQuery('#success').val('');
                  jQuery('#success').html(data.message);                  
                      setTimeout(function() {
                     //jQuery('#sgModal').modal('hide');
                      jQuery(".popup-wrapper").fadeOut("slow");
                      jQuery("#popup_box").fadeOut("slow");
                      sg_cooki_set();
                     if(data.url != "no") {
                       window.location.href='http://'+data.url
                      }
                      }, 2000); // milliseconds 
                   return false; 
                }else{
                  //alert(data.message);
                  jQuery('#message').val('');
                  jQuery('#message').html(data.message); 
                  setTimeout(function() {
                      //jQuery('#sgModal').modal('hide');
                      jQuery(".popup-wrapper").fadeOut("slow");
                      jQuery("#popup_box").fadeOut("slow");
                      }, 2000); // milliseconds                 
                   return false; 

                }*/
                
              }              
      }); 
    
    };

 

    
});
</script>